//'use strict';
//console.log('service');
angular.module('angularGoogleMapsExample.services', ['angularGoogleMapsExample.services'])

  .constant("myConfig", {
     "url": "http://localhost",
   // "url": "http://10.56.110.120",
    "port": "6699",
    "url2": "http://10.56.110.120",
    "port2": "6699"

  })

  .config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
  }])

  /*.factory('Yelp', function($http, $q) {
    return {
      search: function(position) {
        return $http({
          method: "get",
          url: apiUrl + 'api/v1/yelp/search',
          params: {
            limit: 10,
            radius_filter: 500,
            sort: 1,
            ll: [position.coords.latitude, position.coords.longitude].join()
          }
        });
      }
    };
  })
  


  /*.run(function ($ionicPlatform, $cordovaDevice, $rootScope) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }



    });
  })




  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/MetaDataRestService.svc/GetMetaData/')
      
      .success(function (data) {
        $rootScope.metadata = data.metadata;
      })
      .error(function (data, status, headers, config, $scope) {
        //$ionicPopup.alert({
        //  title: 'Sin conexion ' + status,
        //  content: 'Lo sentimos, La aplicacion necesita tener acceso al servidor de Servicios.'
        //});
      });
  })

  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup , myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetDataReport_I/')
      .success(function (data) {
        $rootScope.report1 = data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })


  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetDataReport_II/')
      .success(function (data) {
        $rootScope.report2 = data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })

  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetDataReport_III/')
      .success(function (data) {
        $rootScope.report3 = data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })


  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetAllDataReport_IV/')
      .success(function (data) {
        $rootScope.report4 = data.data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })

  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetAllDataReport_V/')
      .success(function (data) {
        $rootScope.report5 = data.data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })

  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetAllDataReport_VI/')
      .success(function (data) {
        $rootScope.report6 = data.data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })

  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetDataReportAllEquipoment_IV/')
      .success(function (data) {
        $rootScope.equipment_iv = data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })


  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetDataReportAllEquipoment_V/')
      .success(function (data) {
        $rootScope.equipment_v = data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })

  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/EquipmentRestService.svc/GetGridStatusEquipment/')
      .success(function (data) {
        $rootScope.equipments_status = data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })

  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/MineGraphicsRestService.svc/GetPitgraphics/')
      .success(function (data) {
        $rootScope.pitgraphics = data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })


  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/MetaDataRestService.svc/GetUpdateData/')
      .success(function (data) {
        $rootScope.updatedata = data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })

  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetDataReportAllEquipoment_VI')
      .success(function (data) {
        $rootScope.equipment_vi = data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })

  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetAllDataReport_V/')
      .success(function (data) {
        $rootScope.alldatareportv = data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })
  .run(function ($rootScope, $http, $location, $ionicPlatform, $ionicPopup, myConfig) {
    $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetAllDataReport_VI/')
      .success(function (data) {
        $rootScope.alldatareportvi = data;
      })
      .error(function (data, status, headers, config, $scope) {
        $ionicPopup.alert({
          title: 'Sin conexion ' + status,
          content: 'Lo sentimos, Debe solucionar su problema de conección'
        });
      });
  })


  /*.run(function($ionicPlatform,$cordovaDevice,$rootScope) {
       $ionicPlatform.ready(function() {
         // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
         // for form inputs)
         if (window.cordova && window.cordova.plugins.Keyboard) {
           cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
         }
         if (window.StatusBar) {
           StatusBar.styleDefault();
         }
 
        //  var uuid = $cordovaDevice.getUUID();
 
       $rootScope.xxx = $cordovaDevice.getModel();
        //appdata.state.deviceUUID = $cordovaDevice.getUUID();
 
        
          
       });
     })*/

  /*.factory('$imeidevide', ['ngCordova', function($cordovaDevice) {
    return {
      imei: function(){
         return $cordovaDevice.getUUID();
      }
    }

   }])*/

  /*.factory('imeidevide', function ($http, $rootScope,$ionicPlatform,$cordovaDevice) {
    return {
      get: function (){
           var vuelta =   $ionicPlatform.ready(function() {
                    return $cordovaDevice.getUUID();
           })
           return vuelta;
      }
    }
  })*/

  .factory('$localstorage', ['$window', function ($window) {

    return {
      set: function (key, value) {
        $window.localStorage[key] = value;
      },
      get: function (key, defaultValue) {
        return $window.localStorage[key] || false;
      },
      setObject: function (key, value) {
        $window.localStorage[key] = JSON.stringify(value);
      },
      getObject: function (key) {
        if ($window.localStorage[key] != undefined)
          return JSON.parse($window.localStorage[key] || false);

        return false;
      },
      remove: function (key) {
        $window.localStorage.removeItem(key);
      },
      clear: function () {
        $window.localStorage.clear();
      }
    }
  }])


  .factory('GetDataCSVCiclosCortos', function ($http, $rootScope, myConfig) {
    var GetDataCSVCiclosCortos = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVCiclosCortos', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataCSVCiclosCortos;
  })

  .factory('GetDataCSVCiclosCortosGrupos', function ($http, $rootScope, myConfig) {
    var GetDataCSVCiclosCortosGrupos = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVCiclosCortosGrupos', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataCSVCiclosCortosGrupos;
  })


  .factory('GetDataCSVBalizasPerdidas', function ($http, $rootScope, myConfig) {
    var GetDataCSVBalizasPerdidas = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVBalizasPerdidas', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataCSVBalizasPerdidas;
  })


.factory('GetDataIAuditor', function ($http, $rootScope, myConfig) {
    var GetDataIAuditor = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/getDataIAuditor', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataIAuditor;
  })




.factory('GetDataNoTalks', function ($http, $rootScope, myConfig) {
    var GetDataNoTalks = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataNoTalks', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataNoTalks;
  })



  .factory('GetDataCSVBalizasPerdidasGrupos', function ($http, $rootScope, myConfig) {
    var GetDataCSVBalizasPerdidasGrupos = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVBalizasPerdidasGrupos', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataCSVBalizasPerdidasGrupos;
  })

  .factory('GetDataCSVCargasExtras', function ($http, $rootScope, myConfig) {
    var GetDataCSVCargasExtras = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVCargasExtras', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataCSVCargasExtras;
  })


  .factory('GetDataCSVCargasExtrasGrupos', function ($http, $rootScope, myConfig) {
    var GetDataCSVCargasExtrasGrupos = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVCargasExtraGrupos', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataCSVCargasExtrasGrupos;
  })

  .factory('GetDataCSVLlegadasTardes', function ($http, $rootScope, myConfig) {
    var GetDataCSVLlegadasTardes = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVLlegadasTardes', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataCSVLlegadasTardes;
  })

  .factory('GetDataCSVLlegadasTardesGrupos', function ($http, $rootScope, myConfig) {
    var GetDataCSVLlegadasTardesGrupos = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVLlegadasTardesGrupos', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataCSVLlegadasTardesGrupos;
  })


  .factory('GetDataCSVIntegridad', function ($http, $rootScope, myConfig) {
    var GetDataCSVIntegridad = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVIntegridad', { params: params1 }).then(function (response) {
          
          return response.data;
           
        })

        return promise;
      }
    };
    return GetDataCSVIntegridad;
  })

  .factory('GetDataCSVIntegridadCiclosCortos', function ($http, $rootScope, myConfig) {
    var GetDataCSVIntegridadCiclosCortos = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVIntegridadCiclosCortos', { params: params1 }).then(function (response) {
          
          return response.data;
           
        })

        return promise;
      }
    };
    return GetDataCSVIntegridadCiclosCortos;
  })

  .factory('GetDataAsignacionesDespacho', function ($http, $rootScope, myConfig) {
    var GetDataAsignacionesDespacho = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataAsignacionesDespacho', { params: params1 }).then(function (response) {
          
          return response.data;
           
        })

        return promise;
      }
    };
    return GetDataAsignacionesDespacho;
  })

  .factory('GetDataExcepciones', function ($http, $rootScope, myConfig) {
    var GetDataExcepciones = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataExcepciones', { params: params1 }).then(function (response) {
          
          return response.data;
           
        })

        return promise;
      }
    };
    return GetDataExcepciones;
  })

  .factory('GetDataExcepcionesGrupos', function ($http, $rootScope, myConfig) {
    var GetDataExcepcionesGrupos = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataExcepcionesGrupos', { params: params1 }).then(function (response) {
          
          return response.data;
           
        })

        return promise;
      }
    };
    return GetDataExcepcionesGrupos;
  })

   .factory('GetDataComunicaciones', function ($http, $rootScope, myConfig) {
    var GetDataComunicaciones = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataComunicaciones', { params: params1 }).then(function (response) {
          
          return response.data;
           
        })

        return promise;
      }
    };
    return GetDataComunicaciones;
  })

  .factory('GetDataInfluxDB', function ($http, $rootScope, myConfig) {
    var GetDataInfluxDB = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataInfluxDB', { params: params1 }).then(function (response) {
          
          return response.data;
           
        })

        return promise;
      }
    };
    return GetDataInfluxDB;
  })

  .factory('GetDataInfluxDBFreezer', function ($http, $rootScope, myConfig) {
    var GetDataInfluxDBFreezer = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataInfluxDBFreezer', { params: params1 }).then(function (response) {
          
          return response.data;
           
        })

        return promise;
      }
    };
    return GetDataInfluxDBFreezer;
  })

  .factory('GetDataSinComunicaciones', function ($http, $rootScope, myConfig) {
    var GetDataSinComunicaciones = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataSinComunicaciones', { params: params1 }).then(function (response) {
          
          return response.data;
           
        })

        return promise;
      }
    };
    return GetDataSinComunicaciones;
  })

  .factory('GetDataGPS', function ($http, $rootScope, myConfig) {
    var GetDataGPS = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataGPS', { params: params1 }).then(function (response) {
          
          return response.data;
           
        })

        return promise;
      }
    };
    return GetDataGPS;
  })

  .factory('GetDataGPSAlta', function ($http, $rootScope, myConfig) {
    var GetDataGPSAlta = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataGPSAlta', { params: params1 }).then(function (response) {
          
          return response.data;
           
        })

        return promise;
      }
    };
    return GetDataGPSAlta;
  })



  .factory('GetDataIntegridadTurno', function ($http, $rootScope, myConfig) {
    var GetDataIntegridadTurno = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataIntegridadTurno', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataIntegridadTurno;
  })

  .factory('GetDataIntegridadCamion', function ($http, $rootScope, myConfig) {
    var GetDataIntegridadCamion = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataIntegridadCamion', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataIntegridadCamion;
  })

  .factory('GetDataIntegridadPala', function ($http, $rootScope, myConfig) {
    var GetDataIntegridadPala = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataIntegridadPala', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataIntegridadPala;
  })

  .factory('GetDataIntegridadOperador', function ($http, $rootScope, myConfig) {
    var GetDataIntegridadOperador = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataIntegridadOperador', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataIntegridadOperador;
  })




  .factory('GetDataCSVIntegridadGrupos', function ($http, $rootScope, myConfig) {
    var GetDataCSVIntegridadGrupos = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVIntegridadGrupos', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataCSVIntegridadGrupos;
  })

  .factory('GetDataCSVUsoDinamico', function ($http, $rootScope, myConfig) {
    var GetDataCSVUsoDinamico = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVUsoDinamico', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataCSVUsoDinamico;
  })


  .factory('GetDataCSVUsoDinamicoGrupos', function ($http, $rootScope, myConfig) {
    var GetDataCSVUsoDinamicoGrupos = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataCSVUsoDinamicoGrupos', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataCSVUsoDinamicoGrupos;
  })

  .factory('GetDataEfectividadPLM', function ($http, $rootScope, myConfig) {
    var GetDataEfectividadPLM = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataEfectividadPLM', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataEfectividadPLM;
  })





  .factory('GetDataReport1', function ($http, $rootScope, myConfig) {
    var GetDataReport1 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataChar1Angular', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataReport1;
  })

  .factory('GetDataReport2', function ($http, $rootScope, myConfig) {
    var GetDataReport2 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataChar2Angular', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataReport2;
  })


  .factory('GetDataReport3', function ($http, $rootScope, myConfig) {
    var GetDataReport3 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataChar3Angular', { params: params1 }).then(function (response) {
          return response.data;

        })

        return promise;
      }
    };
    return GetDataReport3;
  })

  .factory('GetDataReport4', function ($http, $rootScope, myConfig) {
    var GetDataReport4 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataChar4Angular', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReport4;
  })

  .factory('GetDataReport5', function ($http, $rootScope, myConfig) {
    var GetDataReport5 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataChar5Angular', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReport5;
  })


  .factory('GetDataReport6', function ($http, $rootScope, myConfig) {
    var GetDataReport6 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataChar6Angular', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReport6;
  })

  .factory('GetDataReport7', function ($http, $rootScope, myConfig) {
    var GetDataReport7 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataChar7Angular', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReport7;
  })

  .factory('GetDataReport8', function ($http, $rootScope, myConfig) {
    var GetDataReport8 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataChar8Angular', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReport8;
  })

  .factory('GetDataReport9', function ($http, $rootScope, myConfig) {
    var GetDataReport9 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataChar9Angular', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReport9;
  })

  .factory('GetDataReport10', function ($http, $rootScope, myConfig) {
    var GetDataReport10 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataChar10Angular', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReport10;
  })

  .factory('GetDataReport11', function ($http, $rootScope, myConfig) {
    var GetDataReport11 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataChar11Angular', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReport11;
  })

  .factory('GetDataChartDirtyDozen', function ($http, $rootScope, myConfig) {
    var GetDataChartDirtyDozen = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataCharDirtyDozen', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataChartDirtyDozen;
  })

  .factory('GetDataFileVentaFinanzas', function ($http, $rootScope, myConfig) {
    var GetDataFileVentaFinanzas = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataFileVentasFinanzas', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataFileVentaFinanzas;
  })


  .factory('GetDataChart1Montly', function ($http, $rootScope, myConfig) {
    var GetDataChart1Montly = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/GetDataChart1Montly', { params: params1 }).then(function (response) {
          $rootScope.processing = false;
          return response.data;
        })

        return promise;
      }
    };
    return GetDataChart1Montly;
  })



  .factory('GetDataToneladasPalaHora', function ($http, $rootScope, myConfig) {
    var GetDataToneladasPalaHora = {
      async: function (params1) {


        var promise = $http.get(myConfig.url2 + ':' + myConfig.port2 + '/restservicemodular/services/GetDataToneladasPalaHora', { params: params1 }).then(function (response) {
          $rootScope.processing = false;
          return response.data;
        })

        return promise;
      }
    };
    return GetDataToneladasPalaHora;
  })



  .factory('GetDataChart2Montly', function ($http, $rootScope, myConfig) {
    var GetDataChart2Montly = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/GetDataPerdidaOperacional', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataChart2Montly;
  })


  .factory('getDataFrecuenciaEsperaPala', function ($http, $rootScope, myConfig) {
    var getDataFrecuenciaEsperaPala = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/getDataFrecuenciaEsperaPala', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return getDataFrecuenciaEsperaPala;
  })


  .factory('GetDataChart3Montly', function ($http, $rootScope, myConfig) {
    var GetDataChart3Montly = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/GetDataChart2Montly', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataChart3Montly;
  })

  .factory('GetDataChart4Montly', function ($http, $rootScope, myConfig) {
    var GetDataChart4Montly = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/GetDataChart3Montly', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataChart4Montly;
  })

  .factory('GetDataChart5Montly', function ($http, $rootScope, myConfig) {
    var GetDataChart5Montly = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/GetDataChart4Montly', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataChart5Montly;
  })

  .factory('GetDataChart6Montly', function ($http, $rootScope, myConfig) {
    var GetDataChart6Montly = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/GetDataChart5Montly', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataChart6Montly;
  })


  .factory('getDataEstimadaVSReal', function ($http, $rootScope, myConfig) {
    var getDataEstimadaVSReal = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/getDataEstimadaVSReal', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return getDataEstimadaVSReal;
  })



  .factory('GetDataChartMontlyScatter', function ($http, $rootScope, myConfig) {
    var GetDataChartMontlyScatter = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/GetDataPerdidaOperacionalScatter', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataChartMontlyScatter;
  })


  .factory('GetDataReportCCAChart1', function ($http, $rootScope, myConfig) {
    var GetDataReportCCAChart1 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataReportCCAChart1', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReportCCAChart1;
  })

  .factory('GetDataReportCCAChart2', function ($http, $rootScope, myConfig) {
    var GetDataReportCCAChart2 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataReportCCAChart2', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReportCCAChart2;
  })

  .factory('GetDataReportCCAChart3', function ($http, $rootScope, myConfig) {
    var GetDataReportCCAChart3 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataReportCCAChart3', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReportCCAChart3;
  })

  .factory('GetDataReportTicketCCA', function ($http, $rootScope, myConfig) {
    var GetDataReportTicketCCA = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataReportTicketCCA', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReportTicketCCA;
  })

  .factory('GetDataReportTicketRMA1', function ($http, $rootScope, myConfig) {
    var GetDataReportTicketRMA1 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataReportTicketRMA1', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReportTicketRMA1;
  })

  .factory('GetDataReportTicketRMA2', function ($http, $rootScope, myConfig) {
    var GetDataReportTicketRMA2 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataReportTicketRMA2', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReportTicketRMA2;
  })

  .factory('GetDataReportTicketRMA3', function ($http, $rootScope, myConfig) {
    var GetDataReportTicketRMA3 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataReportTicketRMA3', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReportTicketRMA3;
  })

  .factory('GetDataReportTicketRMA4', function ($http, $rootScope, myConfig) {
    var GetDataReportTicketRMA4 = {
      async: function (params1) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataReportTicketRMA4', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataReportTicketRMA4;
  })



  .factory('InsertDataReportMensual', function ($http, $rootScope, myConfig) {
    var InsertDataReportMensual = {
      async: function (params1) {

        console.log(params1);
        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getInsertDataMensual', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return InsertDataReportMensual;
  })

  .factory('InsertDataReportDirtyMensual', function ($http, $rootScope, myConfig) {
    var InsertDataReportDirtyMensual = {
      async: function (params1) {

        console.log(params1);
        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getInsertDataDirtyMensual', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return InsertDataReportDirtyMensual;
  })

  .factory('InsertDataReportImagesMensual', function ($http, $rootScope, myConfig) {
    var InsertDataReportImagesMensual = {
      async: function (params1) {

        console.log(params1);
        var promise = $http.post(myConfig.url + ':' + myConfig.port + '/restservicemodular/getInsertDataImagesMensual', params1).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return InsertDataReportImagesMensual;
  })

  .factory('InsertDataImagesDirtyMensual', function ($http, $rootScope, myConfig) {
    var InsertDataImagesDirtyMensual = {
      async: function (params1) {

        console.log(params1);
        var promise = $http.post(myConfig.url + ':' + myConfig.port + '/restservicemodular/getInsertDataImagesDirtyMensual', params1).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return InsertDataImagesDirtyMensual;
  })

  .factory('GetDataInput', function ($http, $rootScope, myConfig) {
    var GetDataInput = {
      async: function (params1) {
        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataInput', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataInput;
  })

  .factory('GetDataValidate', function ($http, $rootScope, myConfig) {
    var GetDataValidate = {
      async: function (params1) {
        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataCharValidate', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataValidate;
  })

  .factory('GetDataValidateMensual', function ($http, $rootScope, myConfig) {
    var GetDataValidateMensual = {
      async: function (params1) {
        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getDataCharValidateMensual', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataValidateMensual;
  })

  .factory('GetDataFlotaReport', function ($http, $rootScope, myConfig) {
    var GetDataFlotaReport = {
      async: function (params1) {
        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/GetDataFlotaReport', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetDataFlotaReport;
  })


  //$http.get('/retrievePDFFiles', {responseType: 'arraybuffer'})
  //     .success(function (data) {
  //         var file = new Blob([data], {type: 'application/pdf'});
  //         var fileURL = URL.createObjectURL(file);
  //         window.open(fileURL);
  //  });

  .factory('GetPdfReport', function ($http, $rootScope, myConfig) {
    var GetPdfReport = {
      async: function (params1) {
        var promise = $http.get('http://192.168.12.64:4000/restservicemodular/getReportServer?id=' + params1, { responseType: 'arraybuffer' }).then(function (response) {
          var file = new Blob([response.data], { type: 'application/pdf' });
          var fileURL = URL.createObjectURL(file);
          return fileURL;
        })
        return promise;
      }
    };
    return GetPdfReport;
  })


  .factory('GetInformationKPIMensual', function ($http, $rootScope, myConfig) {
    var GetInformationKPIMensual = {
      async: function (params1) {
        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/getInformationKPIMensual', { params: params1 }).then(function (response) {
          return response.data;
        })

        return promise;
      }
    };
    return GetInformationKPIMensual;
  })



  /*.factory('GetDataReport2', function ($http, $rootScope, myConfig) {
    var GetDataReport2 = {
      async: function () {


        var promise = $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetDataReport_II/', { timeout: 3000 }).then(function (response) {

          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    }
    return GetDataReport2;
  })*/

  /*.factory('GetDataReport3', function ($http, $rootScope, myConfig) {
    var GetDataReport3 = {
      async: function () {


        var promise = $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetDataReport_III/', { timeout: 3000 }).then(function (response) {
          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetDataReport3;
  })**/

  .factory('GetDataReport99', function ($http, $rootScope, myConfig) {
    var GetDataReport99 = {
      async: function (mmm) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/ReportRestService.svc/GetDataReport_IV/' + mmm, { timeout: 3000 }).then(function (response) {
          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetDataReport99;
  })


  .factory('GetAllDataReportIV', function ($http, $rootScope, myConfig) {
    var GetAllDataReportIV = {
      async: function () {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/ReportRestService.svc/GetAllDataReport_IV/', { timeout: 3000 }).then(function (response) {
          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetAllDataReportIV;
  })


  .factory('GetAllDataReportV', function ($http, $rootScope, myConfig) {
    var GetAllDataReportV = {
      async: function () {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/ReportRestService.svc/GetAllDataReport_V/', { timeout: 3000 }).then(function (response) {

          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetAllDataReportV;
  })

  .factory('GetAllDataReportVI', function ($http, $rootScope, myConfig) {
    var GetAllDataReportVI = {
      async: function () {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/ReportRestService.svc/GetAllDataReport_VI/', { timeout: 3000 }).then(function (response) {

          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetAllDataReportVI;
  })





  /*.factory('GetDataReport5', function ($http, $rootScope, myConfig) {
    var GetDataReport5 = {
      async: function (mmm) {


        var promise = $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetDataReport_V/' + mmm, { timeout: 3000 }).then(function (response) {
          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetDataReport5;
  })


  .factory('GetDataReport6', function ($http, $rootScope, myConfig) {
    var GetDataReport6 = {
      async: function (mmm) {


        var promise = $http.get(myConfig.url + ':'+ myConfig.port +'/restservicemodular/services/ReportRestService.svc/GetDataReport_VI/' + mmm, { timeout: 3000 }).then(function (response) {
          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetDataReport6;
  })*/

  .factory('GetDataReportAllEquipment_IV', function ($http, $rootScope, myConfig) {
    var GetDataReportAllEquipment_IV = {
      async: function () {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/ReportRestService.svc/GetDataReportAllEquipoment_IV/', { timeout: 3000 }).then(function (response) {
          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetDataReportAllEquipment_IV;
  })

  .factory('GetDataReportAllEquipment_V', function ($http, $rootScope, myConfig) {
    var GetDataReportAllEquipment_V = {
      async: function () {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/ReportRestService.svc/GetDataReportAllEquipoment_V/', { timeout: 3000 }).then(function (response) {
          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetDataReportAllEquipment_V;
  })

  .factory('GetDataReportAllEquipment_VI', function ($http, $rootScope, myConfig) {
    var GetDataReportAllEquipment_VI = {
      async: function () {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/ReportRestService.svc/GetDataReportAllEquipoment_VI/', { timeout: 3000 }).then(function (response) {
          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetDataReportAllEquipment_VI;
  })






  .factory('GetReportSumaryPalas', function ($http, $rootScope) {
    var GetReportSumaryPalas = {
      async: function () {

        var promise = $http.get($rootScope.host + '/EscondidaRESTService/HaulRouteRESTService.svc/GetReportSumaryPalas/').then(function (response) {

          return response.data;
        });

        return promise;
      }
    };
    return GetReportSumaryPalas;
  })

  .factory('GetReportAsignacionFijaCamionPala', function ($http, $rootScope) {
    var GetReportAsignacionFijaCamionPala = {
      async: function () {

        var promise = $http.get($rootScope.host + '/EscondidaRESTService/HaulRouteRESTService.svc/GetReportAsignacionFijaCamionPala/').then(function (response) {

          return response.data;
        });

        return promise;
      }
    };
    return GetReportAsignacionFijaCamionPala;
  })

  .factory('GetMensageNotification', function ($http, $rootScope, myConfig) {
    var GetMensageNotification = {
      async: function () {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/MessageRestService.svc/GetNotificationList/').then(function (response) {
          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetMensageNotification;
  })


  .factory('GetGridStatusEquipment', function ($http, $rootScope, myConfig) {
    var GetGridStatusEquipment = {
      async: function () {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/EquipmentRestService.svc/GetGridStatusEquipment/', { timeout: 3000 }).then(function (response) {
          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetGridStatusEquipment;
  })

  .factory('GetGraphicsShovel', function ($http, $rootScope, myConfig) {
    var GetGraphicsShovel = {
      async: function () {

        var promise = $http.get($rootScope.host + '/EscondidaRESTService/HaulRouteRESTService.svc/GetGraphicsShovel/').then(function (response) {

          return response.data;
        });

        return promise;
      }
    };
    return GetGraphicsShovel;
  })

  .factory('GetinfoMineGraphicsById', function ($http, $rootScope, myConfig) {
    var GetinfoMineGraphicsById = {
      async: function (idc) {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/MineGraphicsRestService.svc/GetinfoMineGraphicsById/' + idc).then(function (response) {
          return response.data;
        });

        return promise;
      }
    };
    return GetinfoMineGraphicsById;
  })

  .factory('GetPitgraphics', function ($http, $rootScope, myConfig) {
    var GetPitgraphics = {
      async: function () {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/MineGraphicsRestService.svc/GetPitgraphics/', { timeout: 3000 }).then(function (response) {
          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetPitgraphics;
  })

  .factory('GetOrderHaulrouteGoBackList', function ($http, $rootScope) {
    var GetOrderHaulrouteGoBackList = {
      async: function () {
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.get($rootScope.host + '/EscondidaRESTService/HaulRouteRESTService.svc/GetOrderHaulrouteGoBackList/').then(function (response) {
          // The then function here is an opportunity to modify the response
          //console.log(response);
          // The return value gets picked up by the then in the controller.
          return response.data;
        });
        // Return the promise to the controller
        return promise;
      }
    };
    return GetOrderHaulrouteGoBackList;
  })

  .factory('GetOrderHaulrouteByGoList', function ($http, $rootScope) {
    var GetOrderHaulrouteByGoList = {
      async: function () {
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.get($rootScope.host + '/EscondidaRESTService/HaulRouteRESTService.svc/GetOrderHaulrouteByGoList/').then(function (response) {
          // The then function here is an opportunity to modify the response
          //console.log(response);
          // The return value gets picked up by the then in the controller.
          return response.data;
        });
        // Return the promise to the controller
        return promise;
      }
    };
    return GetOrderHaulrouteByGoList;
  })

  .factory('GetHaulrouteByBackList', function ($http, $rootScope) {
    var GetHaulrouteByBackList = {
      async: function () {
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.get($rootScope.host + '/EscondidaRESTService/HaulRouteRESTService.svc/GetHaulrouteByBackList/').then(function (response) {
          // The then function here is an opportunity to modify the response
          // console.log(response);
          // The return value gets picked up by the then in the controller.
          return response.data;
        });
        // Return the promise to the controller
        return promise;
      }
    };
    return GetHaulrouteByBackList;
  })

  .factory('GetHaulrouteByIdGoList', function ($http, $rootScope) {
    var GetHaulrouteByIdGoList = {
      async: function () {
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.get($rootScope.host + '/EscondidaRESTService/HaulRouteRESTService.svc/GetHaulrouteByIdGoList/').then(function (response) {
          // The then function here is an opportunity to modify the response
          // console.log(response);
          // The return value gets picked up by the then in the controller.
          return response.data;
        });
        // Return the promise to the controller
        return promise;
      }
    };
    return GetHaulrouteByIdGoList;
  })

  .factory('GetHaulrouteList', function ($http, $rootScope) {
    var GetHaulrouteList = {
      async: function () {
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.get($rootScope.host + '/EscondidaRESTService/HaulRouteRESTService.svc/GetHaulrouteList/').then(function (response) {
          // The then function here is an opportunity to modify the response
          //console.log(response);
          // The return value gets picked up by the then in the controller.
          return response.data;
        });
        // Return the promise to the controller
        return promise;
      }
    };
    return GetHaulrouteList;
  })

  .factory('GetHaulrouteAllBackList', function ($http, $rootScope) {
    var GetHaulrouteAllBackList = {
      async: function () {
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.get($rootScope.host + '/EscondidaRESTService/HaulRouteRESTService.svc/GetHaulrouteAllBackList/').then(function (response) {
          // The then function here is an opportunity to modify the response
          //console.log(response);
          // The return value gets picked up by the then in the controller.
          return response.data;
        });
        // Return the promise to the controller
        return promise;
      }
    };
    return GetHaulrouteAllBackList;
  })



  .factory('GetHaulrouteAllInitList', function ($http, $rootScope) {
    var GetHaulrouteAllInitList = {
      async: function () {
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.get($rootScope.host + '/EscondidaRESTService/HaulRouteRESTService.svc/GetHaulrouteAllInitList/').then(function (response) {
          // The then function here is an opportunity to modify the response
          // console.log(response);
          // The return value gets picked up by the then in the controller.
          return response.data;
        });
        // Return the promise to the controller
        return promise;
      }
    };
    return GetHaulrouteAllInitList;
  })

  .factory('GetHaulrouteAllDownloadList', function ($http, $rootScope) {
    var GetHaulrouteAllDownloadList = {
      async: function () {
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.get($rootScope.host + '/EscondidaRESTService/HaulRouteRESTService.svc/GetHaulrouteAllDownloadList/').then(function (response) {
          // The then function here is an opportunity to modify the response
          // console.log(response);
          // The return value gets picked up by the then in the controller.
          return response.data;
        });
        // Return the promise to the controller
        return promise;
      }
    };
    return GetHaulrouteAllDownloadList;
  })


  .factory('GetUpdateData', function ($http, $rootScope, myConfig) {
    var GetUpdateData = {
      async: function () {


        var promise = $http.get(myConfig.url + ':' + myConfig.port + '/restservicemodular/services/MetaDataRestService.svc/GetUpdateData/', { timeout: 3000 }).then(function (response) {
          return response.data;
        }, function (rejected) {
          return 0;
        });

        return promise;
      }
    };
    return GetUpdateData;
  })



  .factory('DBA', function ($cordovaSQLite, $q, $ionicPlatform) {
    var self = this;
    self.query = function (query, parameters) {
      parameters = parameters || [];
      var q = $q.defer();
      $ionicPlatform.ready(function () {
        $cordovaSQLite.execute(db, query, parameters)
          .then(function (result) {
            q.resolve(result);
          }, function (error) {
            q.reject(error);
          });
      });
      return q.promise;
    }
    self.getAll = function (result) {
      var output = [];
      for (var i = 0; i < result.rows.length; i++) {
        output.push(result.rows.item(i));
      }
      return output;
    }
    self.getById = function (result) {
      var output = null;
      output = angular.copy(result.rows.item(0));
      return output;
    }
    return self;
  })
  .factory('Data', function ($cordovaSQLite, DBA) {
    var self = this;
    self.all = function () {
      return DBA.query("SELECT key, value FROM your_table")
        .then(function (result) {
          return DBA.getAll(result);
        });
    }
    self.get = function (key) {
      var parameters = [key];
      return DBA.query("SELECT key , value FROM your_table WHERE key = (?)", parameters)
        .then(function (result) {
          return DBA.getById(result);
        });
    }
    self.add = function (obj) {
      var parameters = [obj.key, obj.name];
      return DBA.query("INSERT INTO your_table (key , value) VALUES (?,?)", parameters);
    }
    self.remove = function (obj) {
      var parameters = [obj.key];
      return DBA.query("DELETE FROM your_table WHERE key = (?)", parameters);
    }
    self.update = function (oldkey, newDataObj) {
      var parameters = [newDataObj.key, newDataObj.value, oldkey];
      return DBA.query("UPDATE your_table SET key = (?), value = (?) WHERE key = (?)", parameters);
    }
    return self;
  })
