

// angular.module is a global place for creating, registering and retrieving Angular modules
// the 2nd parameter is an array of 'requires'
var db = null;

var app = angular.module('angularGoogleMapsExample', [ 'angularGoogleMapsExample.controllers', 
    'angularGoogleMapsExample.controllers'])

 

    

   /* .run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    var push = new Ionic.Push({
      "debug": true
    });
 
    push.register(function(token) {
      console.log("My Device token:",token.token);
      push.saveToken(token);  // persist the token in the Ionic Platform
    });
  });
})*/app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
    }])


    .run(function($ionicPlatform,$cordovaDevice,$cordovaSQLite,$rootScope) {
      $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
          StatusBar.styleDefault();
        }

        //if (window.cordova) {
        //    $rootScope.showHeader = false;
        //    db = $cordovaSQLite.openDB({ name: 'myapp.db', location: 'default' });
            //alert('1');
        //} else {
        //    db = window.openDatabase("myapp.db", "1.0", "My app", -1);
        //    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS msg(id integer primary key, msg text)");
            //alert('2');
        //  }

        //$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS joke(id integer primary key, joke text)");

       //alert($cordovaDevice.getUUID());

      //$rootScope.xxx = $cordovaDevice.getModel();
       //appdata.state.deviceUUID = $cordovaDevice.getUUID();

       

       
         
      });
    })

    .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {


      $ionicConfigProvider.views.maxCache(0);

       $stateProvider.state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })


    .state('app.deletemsg', {
        url: '/deletemsg',
        views: {
            'menuContent': {
                //templateUrl: 'templates/haltrout3.html',
                controller: 'DeleteCrlMsg'
            },
            'fabContent': {
                template: '',
                controller: function ($timeout) {
                   // $timeout(function () {
                   //     document.getElementById('fab-activity').classList.toggle('on');
                   // }, 200);
                }
            }
        }
    })

    .state('app.halt3', {
        url: '/halt3',
        views: {
            'menuContent': {
                templateUrl: 'templates/haltrout3.html',
                controller: 'Halt3Ctrl'
            },
            'fabContent': {
                template: '',
                controller: function ($timeout) {
                   // $timeout(function () {
                   //     document.getElementById('fab-activity').classList.toggle('on');
                   // }, 200);
                }
            }
        }
    })

    .state('app.activity', {
        url: '/activity',
        views: {
            'menuContent': {
                templateUrl: 'templates/activity.html',
                controller: 'ActivityCtrl'
            },
            'fabContent': {
               template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-gallery').classList.toggle('on');
                    }, 200);
                }
            }
        }
    })

    .state('app.friends', {
        url: '/friends',
        views: {
            'menuContent': {
                templateUrl: 'templates/friends.html',
                controller: 'FriendsCtrl'
            },
            'fabContent': {
                //template: '<button id="fab-friends" class="button button-fab button-fab-top-right expanded button-energized-900 spin"><i class="icon ion-checkmark-circled"></i></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-friends').classList.toggle('on');
                    }, 900);
                }
            }
        }
    })

    .state('app.setting', {
        url: '/setting',
        views: {
            'menuContent': {
                templateUrl: 'templates/setting.html',
                controller: 'SettingCtrl'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-gallery').classList.toggle('on');
                    }, 900);
                }
            }
        }
    })

     .state('app.all_setting', {
        url: '/al_setting',
        views: {
            'menuContent': {
                templateUrl: 'templates/all_setting.html',
                controller: 'AllSettingCtrl'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-gallery').classList.toggle('on');
                    }, 900);
                }
            }
        }
    })

    .state('app.setting2', {
        url: '/setting2',
        views: {
            'menuContent': {
                templateUrl: 'templates/setting2.html',
                controller: 'SettingCtrl2'
            },
            'fabContent': {
                //template: '<button id="fab-friends" class="button button-fab button-fab-top-right expanded button-energized-900 spin"><i class="icon ion-chatboxes"></i></button>',
                controller: function ($timeout) {
                    //$timeout(function () {
                    //    document.getElementById('fab-friends').classList.toggle('on');
                    //}, 900);
                }
            }
        }
    })

    .state('app.report', {
        url: '/report',
        views: {
            'menuContent': {
                templateUrl: 'templates/report.html',
                controller: 'ReportCtrl'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })


    .state('app.report2', {
        url: '/report2:foo',
       params: {        
        foo: {
          value: 'defaultValue',
          squash: false,
        },       
        hiddenParam: 'YES',
      },
        views: {
            'menuContent': {
                templateUrl: 'templates/report2.html',
                controller: 'ReportCtrl2'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                       document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })

    .state('app.report3', {
        url: '/report3',
        views: {
            'menuContent': {
                templateUrl: 'templates/report3.html',
                controller: 'ReportCtrl3'
            },
            'fabContent': {
               template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })

     .state('app.report4', {
        url: '/report4',
        views: {
            'menuContent': {
                templateUrl: 'templates/report4.html',
                controller: 'ReportCtrl4'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);

                
                }
            }
        }
    })


    .state('app.report5', {
        url: '/report5',
        views: {
            'menuContent': {
                templateUrl: 'templates/report5.html',
                controller: 'ReportCtrl5'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                      document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })

    .state('app.report6', {
        url: '/report6',
        views: {
            'menuContent': {
                templateUrl: 'templates/report6.html',
                controller: 'ReportCtrl6'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                       document.getElementById('fab-gallery').classList.toggle('on');
                    }, 800);
                    
                }
            }
        }
    })

    .state('app.report6opcion', {
        url: '/report6opcion:eq',
        params: {        
        eq: {
          value: 'defaultValue',
          squash: false,
        },       
        //hiddenParam: 'YES',
        },
        views: {
            'menuContent': {
                templateUrl: 'templates/report6opcion.html',
                controller: 'ReportCtrl6Opcion'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                       document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })


    .state('app.report7', {
        url: '/report7',
        views: {
            'menuContent': {
                templateUrl: 'templates/report7.html',
                controller: 'ReportCtrl7'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                       document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })



     .state('app.report7opcion', {
        url: '/report7opcion:eq',
        params: {        
        eq: {
          value: 'defaultValue',
          squash: false,
        },       
        //hiddenParam: 'YES',
        },
        views: {
            'menuContent': {
                templateUrl: 'templates/report7opcion.html',
                controller: 'ReportCtrl7Opcion'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                       document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })

    .state('app.report8', {
        url: '/report8',
        views: {
            'menuContent': {
                templateUrl: 'templates/report8.html',
                controller: 'ReportCtrl8'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                       document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })

    .state('app.report8opcion', {
        url: '/report8opcion:eq',
        params: {        
        eq: {
          value: 'defaultValue',
          squash: false,
        },       
        //hiddenParam: 'YES',
        },
        views: {
            'menuContent': {
                templateUrl: 'templates/report8opcion.html',
                controller: 'ReportCtrl8Opcion'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                       document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })

    .state('app.report9', {
        url: '/report9',
        views: {
            'menuContent': {
                templateUrl: 'templates/report9.html',
                controller: 'ReportCtrl9'
            },
            'fabContent': {
               template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                   $timeout(function () {
                         document.getElementById('fab-gallery').classList.toggle('on');
                    }, 800);
                }
            }
        }
    })


    .state('app.login', {
        url: '/login',
        views: {
            'menuContent': {
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            },
            'fabContent': {
                template: ''
            }
        }
    })

    .state('app.profile', {
        url: '/profile',
        views: {
            'menuContent': {
                templateUrl: 'templates/profile.html',
                controller: 'ProfileCtrl'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                         document.getElementById('fab-gallery').classList.toggle('on');
                    }, 800);
                }
            }
        }
    })


    
   

    .state('app.halt', {
        url: '/halt',
        views: {
            'menuContent': {
                templateUrl: 'templates/haltrout.html',
                controller: 'HaltCtrl'
            },
            'fabContent': {
                template: '',
                
            }
        }
    })

    .state('app.halt2', {
        url: '/halt2',
        views: {
            'menuContent': {
                templateUrl: 'templates/haltrout2.html',
                controller: 'Halt2Ctrl'
            },
            'fabContent': {
                template: '',
                
            }
        }
    })

    

    .state('app.map', {
        url: '/map',
        views: {
            'menuContent': {
                templateUrl: 'templates/map.html',
                controller: 'MapCtrl'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                         document.getElementById('fab-gallery').classList.toggle('on');
                    }, 800);
                }
            }
        }
    })



    .state('app.gallery2', {
        url: '/gallery2:foo',
        //params: {        
        //foo: {
        //  value: 'defaultValue',
        //  squash: false,
        //},       
        //hiddenParam: 'YES' },
        views: {
            'menuContent': {
                templateUrl: 'templates/gallery2.html',
                controller: 'GalleryCtrl2'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                //template: ' <span id="fab-gallery" class="avatar" style="background: url(\'img/crown.jpg\'); background-size: cover;"></span>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })


    .state('app.menu_equipos', {
        url: '/menu_equipos',
        views: {
            'menuContent': {
                templateUrl: 'templates/menu_equipos.html',
                controller: 'MenuEquipos'
            },
            'fabContent': {
                template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                //template: ' <span id="fab-gallery" class="avatar" style="background: url(\'img/crown.jpg\'); background-size: cover;"></span>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })

    .state('app.history_msg', {
        url: '/history_msg',
        views: {
            'menuContent': {
                templateUrl: 'templates/msg_history.html',
                controller: 'HistoryMsgCtrl'
            },
            'fabContent': {
                 template: '<button id="fab-gallery" ui-sref="app.gallery2"  class="modular button button-fab button-fab-top-right expanded button-energized-900 drop avatar"></button>',
                //template: ' <span id="fab-gallery" class="avatar" style="background: url(\'img/crown.jpg\'); background-size: cover;"></span>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })



    // if none of the above states are matched, use this as the fallback
    

     // $stateProvider

    //    .state('app.map', {
    //      url: '/map',
    //      templateUrl: 'templates/map.html',
    //      controller: 'MapCtrl'
    //    });

     $urlRouterProvider.otherwise('/app/login');

      // if none of the above states are matched, use this as the fallback
     //$urlRouterProvider.otherwise('/map');

    })
  ;
