//angular.module( 'YourApp', [ 'ngMaterial' ] )


angular
  .module('YourApp', ['angularGoogleMapsExample.services', 'ngMaterial', 'chart.js', 'ngMessages',  'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.cellNav', 'ui.grid.infiniteScroll', 'ui.grid.autoResize'])

  /*.config(function($mdThemingProvider) {
    //disable theme generation
    $mdThemingProvider.generateThemesOnDemand(true);
  
    //themes are still defined in config, but the css is not generated
    $mdThemingProvider.theme('altTheme')
      .primaryPalette('green')
      .accentPalette('red');
  })*/
  .run(function ($rootScope) {
    $rootScope.flota = {};

   })
 

  .config(function ($mdThemingProvider) {

    // Extend the red theme with a different color and make the contrast color black instead of white.
    // For example: raised button text will be black instead of white.
    var neonRedMap = $mdThemingProvider.extendPalette('red', {
      '500': '#ff0000',
      'contrastDefaultColor': 'dark'
    });

    // Register the new color palette map with the name <code>neonRed</code>
    $mdThemingProvider.definePalette('neonRed', neonRedMap);

    // Use that theme for the primary intentions
    $mdThemingProvider.theme('default')
      .primaryPalette('blue')
      .accentPalette('indigo');

     
  })

  .config(function($mdDateLocaleProvider) {
    $mdDateLocaleProvider.months = ['Enero', 'Febrero', 'Marzo', 'Abril','Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $mdDateLocaleProvider.shortMonths = ['Enero', 'Febrero', 'Marzo', 'Abril','Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $mdDateLocaleProvider.days = ['Domingo','Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    $mdDateLocaleProvider.shortDays = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'];
    $mdDateLocaleProvider.formatDate = function(date) {
       return moment(date).format('YYYY-MM-DD');
    };
   })

 
  

  .controller('gridListDemoCtrl', function ($scope) {

    this.tiles = buildGridModel({
      icon: "avatar:svg-",
      title: "Svg-",
      background: ""
    });



    function buildGridModel(tileTmpl) {
      var it, results = [];

      for (var j = 0; j < 11; j++) {

        it = angular.extend({}, tileTmpl);
        it.icon = it.icon + (j + 1);
        it.title = it.title + (j + 1);
        it.span = { row: 1, col: 1 };

        switch (j + 1) {
          case 1:
            it.background = "red";
            it.span.row = it.span.col = 2;
            break;

          case 2: it.background = "green"; break;
          case 3: it.background = "darkBlue"; break;
          case 4:
            it.background = "blue";
            it.span.col = 2;
            break;

          case 5:
            it.background = "yellow";
            it.span.row = it.span.col = 2;
            break;

          case 6: it.background = "pink"; break;
          case 7: it.background = "darkBlue"; break;
          case 8: it.background = "purple"; break;
          case 9: it.background = "deepBlue"; break;
          case 10: it.background = "lightPurple"; break;
          case 11: it.background = "yellow"; break;
        }

        results.push(it);
      }
      return results;
    }
  })








  .controller('PanelGroupsCtrl', PanelGroupsCtrl)

  .directive('fileModel', ['$parse', function ($parse) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;

        element.bind('change', function () {
          scope.$apply(function () {
            modelSetter(scope, element[0].files[0]);
          });
        });
      }
    };
  }])

  .directive('apsUploadFile', ['$interval', 'dateFilter', function ($interval, dateFilter) {

    var directive = {
      restrict: 'E',
      template: '<input id="fileInput" type="file" class="ng-hide" ng-model="myFile1" base-sixty-four-input> <md-button id="uploadButton" class="md-raised md-primary" aria-label="attach_file">    <md-icon>assignment</md-icon>   Seleccione Archivo </md-button><md-input-container  md-no-float>    <input id="textInput" ng-model="fileName" type="text" placeholder="Archivo sin Seleccion" ng-readonly="true"></md-input-container>',
      //template: '<input id="fileInput" file-model="myFile" type="file" class="ng-hide" ng-model="myFile" base-sixty-four-input> <md-button id="uploadButton" class="md-raised md-accent" aria-label="attach_file"> <md-icon>assignment</md-icon>   Seleccione Archivo </md-button><md-input-container  md-no-float>    <input id="textInput" ng-model="fileName" type="text" placeholder="Archivo sin Seleccion" ng-readonly="true" ></md-input-container>',
      link: apsUploadFileLink
    };

    function apsUploadFileLink(scope, element, attrs) {
      var input = $(element[0].querySelector('#fileInput'));
      var button = $(element[0].querySelector('#uploadButton'));
      var textInput = $(element[0].querySelector('#textInput'));

      if (input.length && button.length && textInput.length) {
        button.click(function (e) {
          input.click();
        });
        textInput.click(function (e) {
          input.click();
        });
      }

      input.on('change', function (e) {
        var files = e.target.files;
        var to = e.target;

        if (files[0]) {
          scope.fileName = files[0].name;
        } else {
          scope.fileName = null;
        }
        scope.$apply();
      });
    }

    return directive;

  }])

  .directive('apsUploadFile2', ['$interval', 'dateFilter', function ($interval, dateFilter) {

    var directive = {
      restrict: 'E',
      template: '<input id="fileInput2" type="file" class="ng-hide" ng-model="myFile2" base-sixty-four-input> <md-button id="uploadButton2" class="md-raised md-primary" aria-label="attach_file">    <md-icon>assignment</md-icon>   Seleccione Archivo </md-button><md-input-container  md-no-float>    <input id="textInput2" ng-model="fileName2" type="text" placeholder="Archivo sin Seleccion" ng-readonly="true"></md-input-container>',
      //template: '<input id="fileInput" file-model="myFile" type="file" class="ng-hide" ng-model="myFile" base-sixty-four-input> <md-button id="uploadButton" class="md-raised md-accent" aria-label="attach_file"> <md-icon>assignment</md-icon>   Seleccione Archivo </md-button><md-input-container  md-no-float>    <input id="textInput" ng-model="fileName" type="text" placeholder="Archivo sin Seleccion" ng-readonly="true" ></md-input-container>',
      link: apsUploadFileLink
    };

    function apsUploadFileLink(scope, element, attrs) {
      var input = $(element[0].querySelector('#fileInput2'));
      var button = $(element[0].querySelector('#uploadButton2'));
      var textInput = $(element[0].querySelector('#textInput2'));

      if (input.length && button.length && textInput.length) {
        button.click(function (e) {
          input.click();
        });
        textInput.click(function (e) {
          input.click();
        });
      }

      input.on('change', function (e) {
        var files = e.target.files;
        var to = e.target;

        if (files[0]) {
          scope.fileName2 = files[0].name;
        } else {
          scope.fileName2 = null;
        }
        scope.$apply();
      });
    }

    return directive;

  }])


  .directive('apsUploadFile3', ['$interval', 'dateFilter', function ($interval, dateFilter) {

    var directive = {
      restrict: 'E',
      template: '<input id="fileInput3" type="file" class="ng-hide" ng-model="myFile3" base-sixty-four-input> <md-button id="uploadButton3" class="md-raised md-primary" aria-label="attach_file">    <md-icon>assignment</md-icon>   Seleccione Archivo </md-button><md-input-container  md-no-float>    <input id="textInput3" ng-model="fileName3" type="text" placeholder="Archivo sin Seleccion" ng-readonly="true"></md-input-container>',
      //template: '<input id="fileInput" file-model="myFile" type="file" class="ng-hide" ng-model="myFile" base-sixty-four-input> <md-button id="uploadButton" class="md-raised md-accent" aria-label="attach_file"> <md-icon>assignment</md-icon>   Seleccione Archivo </md-button><md-input-container  md-no-float>    <input id="textInput" ng-model="fileName" type="text" placeholder="Archivo sin Seleccion" ng-readonly="true" ></md-input-container>',
      link: apsUploadFileLink
    };

    function apsUploadFileLink(scope, element, attrs) {
      var input = $(element[0].querySelector('#fileInput3'));
      var button = $(element[0].querySelector('#uploadButton3'));
      var textInput = $(element[0].querySelector('#textInput3'));

      if (input.length && button.length && textInput.length) {
        button.click(function (e) {
          input.click();
        });
        textInput.click(function (e) {
          input.click();
        });
      }

      input.on('change', function (e) {
        var files = e.target.files;
        var to = e.target;

        if (files[0]) {
          scope.fileName3 = files[0].name;
        } else {
          scope.fileName3 = null;
        }
        scope.$apply();
      });
    }

    return directive;

  }])

  .directive('apsUploadFile4', ['$interval', 'dateFilter', function ($interval, dateFilter) {

    var directive = {
      restrict: 'E',
      template: '<input id="fileInput4" type="file" class="ng-hide" ng-model="myFile4" base-sixty-four-input> <md-button id="uploadButton4" class="md-raised md-primary" aria-label="attach_file">    <md-icon>assignment</md-icon>   Seleccione Archivo </md-button><md-input-container  md-no-float>    <input id="textInput4" ng-model="fileName4" type="text" placeholder="Archivo sin Seleccion" ng-readonly="true"></md-input-container>',
      //template: '<input id="fileInput" file-model="myFile" type="file" class="ng-hide" ng-model="myFile" base-sixty-four-input> <md-button id="uploadButton" class="md-raised md-accent" aria-label="attach_file"> <md-icon>assignment</md-icon>   Seleccione Archivo </md-button><md-input-container  md-no-float>    <input id="textInput" ng-model="fileName" type="text" placeholder="Archivo sin Seleccion" ng-readonly="true" ></md-input-container>',
      link: apsUploadFileLink
    };

    function apsUploadFileLink(scope, element, attrs) {
      var input = $(element[0].querySelector('#fileInput4'));
      var button = $(element[0].querySelector('#uploadButton4'));
      var textInput = $(element[0].querySelector('#textInput4'));

      if (input.length && button.length && textInput.length) {
        button.click(function (e) {
          input.click();
        });
        textInput.click(function (e) {
          input.click();
        });
      }

      input.on('change', function (e) {
        var files = e.target.files;
        var to = e.target;

        if (files[0]) {
          scope.fileName4 = files[0].name;
        } else {
          scope.fileName4 = null;
        }
        scope.$apply();
      });
    }

    return directive;

  }])


  .directive('apsUploadFile5', ['$interval', 'dateFilter', function ($interval, dateFilter) {

    var directive = {
      restrict: 'E',
      template: '<input id="fileInput5" type="file" class="ng-hide" ng-model="myFile5" base-sixty-four-input> <md-button id="uploadButton5" class="md-raised md-primary" aria-label="attach_file">    <md-icon>assignment</md-icon>   Seleccione Archivo </md-button><md-input-container  md-no-float>    <input id="textInput5" ng-model="fileName5" type="text" placeholder="Archivo sin Seleccion" ng-readonly="true"></md-input-container>',
      //template: '<input id="fileInput" file-model="myFile" type="file" class="ng-hide" ng-model="myFile" base-sixty-four-input> <md-button id="uploadButton" class="md-raised md-accent" aria-label="attach_file"> <md-icon>assignment</md-icon>   Seleccione Archivo </md-button><md-input-container  md-no-float>    <input id="textInput" ng-model="fileName" type="text" placeholder="Archivo sin Seleccion" ng-readonly="true" ></md-input-container>',
      link: apsUploadFileLink
    };

    function apsUploadFileLink(scope, element, attrs) {
      var input = $(element[0].querySelector('#fileInput5'));
      var button = $(element[0].querySelector('#uploadButton5'));
      var textInput = $(element[0].querySelector('#textInput5'));

      if (input.length && button.length && textInput.length) {
        button.click(function (e) {
          input.click();
        });
        textInput.click(function (e) {
          input.click();
        });
      }

      input.on('change', function (e) {
        var files = e.target.files;
        var to = e.target;

        if (files[0]) {
          scope.fileName5 = files[0].name;
        } else {
          scope.fileName5 = null;
        }
        scope.$apply();
      });
    }

    return directive;

  }])



  .directive('chooseFile', function () {
    return {
      link: function (scope, elem, attrs) {
        var button = angular.element(elem[0].querySelector('#uploadButton'));
        var input = angular.element(elem[0].querySelector('input#fileInput'));
        button.bind('click', function () {
          input[0].click();
        });
        input.bind('change', function (e) {
          scope.$apply(function () {
            var files = e.target.files;
            if (files[0]) {
              scope.fileName = files[0].name;
            } else {
              scope.fileName = null;
            }
          });
        });
      }
    };
  })


 

  .controller('PanelMenuCtrl', PanelMenuCtrl);






function PanelGroupsCtrl($rootScope,$mdPanel,$mdDialog, $sce, $interval,GetDataIAuditor,getDataFrecuenciaEsperaPala, getDataEstimadaVSReal,  $filter,GetDataNoTalks,GetDataInfluxDB,GetDataInfluxDBFreezer,GetDataValidateMensual,GetDataGPSAlta,GetDataExcepcionesGrupos,GetDataExcepciones, GetDataComunicaciones,GetDataSinComunicaciones,GetDataGPS, GetInformationKPIMensual, $scope, $http, $timeout, $q, $interval, $mdToast, $window, GetDataReport1, GetDataReport2, GetDataReport3, GetDataReport4, GetDataReport5, GetDataReport6, GetDataReport7, GetDataReport8, GetDataReport9, GetDataReport10, GetDataReport11, GetDataReportCCAChart1, GetDataReportCCAChart2, GetDataReportCCAChart3, GetDataReportTicketCCA, GetDataReportTicketRMA1, GetDataReportTicketRMA2, GetDataReportTicketRMA3, GetDataReportTicketRMA4, GetPdfReport, InsertDataReportMensual, InsertDataReportImagesMensual, $sce, $mdDialog, GetDataInput, GetDataValidate, GetDataChartDirtyDozen, GetDataFileVentaFinanzas,GetDataChart1Montly,GetDataChart2Montly,GetDataChartMontlyScatter,GetDataChart3Montly,GetDataChart4Montly,GetDataChart5Montly,GetDataChart6Montly,InsertDataReportDirtyMensual,InsertDataImagesDirtyMensual,GetDataFlotaReport,GetDataCSVCiclosCortos,GetDataCSVIntegridad,GetDataCSVLlegadasTardes,GetDataCSVBalizasPerdidas,GetDataCSVCargasExtras,GetDataCSVUsoDinamico,GetDataCSVBalizasPerdidasGrupos,GetDataCSVCiclosCortosGrupos,GetDataCSVIntegridadGrupos,GetDataCSVLlegadasTardesGrupos,GetDataCSVCargasExtrasGrupos,GetDataCSVUsoDinamicoGrupos,GetDataEfectividadPLM,GetDataIntegridadTurno,GetDataIntegridadCamion,GetDataIntegridadPala,GetDataIntegridadOperador,GetDataToneladasPalaHora,GetDataCSVIntegridadCiclosCortos,GetDataAsignacionesDespacho) {
  Chart.defaults.global.defaultFontColor = '#000700';
  Chart.defaults.global.defaultFontFamily = 'Arial';
  //Chart.defaults.global.defaultFontSize = '4';
  Chart.defaults.global.defaultFontStyle = 'bold';

   $rootScope.processing = true;
   
  

  var last = {
    bottom: false,
    top: true,
    left: false,
    right: true
  };

    $scope.selectedImpact = null;
   /* $scope.myDate = $filter('date')(new Date());
    $scope.myDate2 = $filter('date')(new Date());
    $scope.myDateAlta = $filter('date')(new Date());
    $scope.myDateAlta2 = $filter('date')(new Date());
    $scope.fechaIniCom = $filter('date')(new Date());
    $scope.fechaTerCom = $filter('date')(new Date());
    $scope.fechaIniPlm = $filter('date')(new Date());
    $scope.fechaTerPlm = $filter('date')(new Date());*/


     $scope.myDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.myDate2 = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.myDateAlta = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.myDateAlta2 = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.fechaIniCom = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.fechaTerCom = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.fechaIniPlm = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.fechaTerPlm = $filter('date')(new Date(), 'yyyy-MM-dd');


    $scope.cambiogps = function () {
      $scope.param1 = {
        fechaini: $filter('date')($scope.myDate, 'yyyy-MM-dd'),
        fechater: $filter('date')($scope.myDate2, 'yyyy-MM-dd'),
        flota: $scope.flota.id
      }
      GetDataGPS.async($scope.param1).then(function (datos) {
        $scope.data_csv_gps = [];
        $scope.data_csv_gps_temp = [];
        $scope.labels_csv_gps = [];
        for (i = 0; i < datos.data.length; i++)
          $scope.data_csv_gps_temp.push(datos.data[i]['%Good']);

        $scope.data_csv_gps.push($scope.data_csv_gps_temp);
        $scope.data_csv_gps_temp = [];
        for (i = 0; i < datos.data.length; i++)
          $scope.data_csv_gps_temp.push(datos.data[i]['%Bad']);

        $scope.data_csv_gps.push($scope.data_csv_gps_temp);
        $scope.data_csv_gps_temp = [];
        for (i = 0; i < datos.data.length; i++)
          $scope.labels_csv_gps.push(datos.data[i]['UnitId']);
      })

    }

    $scope.showAdvanced = function(ev) {
      $mdDialog.show({
        controller: DialogController,
        templateUrl: '../dialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
        $scope.status = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
    };

    function DialogController($scope, $mdDialog) {
      $scope.hide = function() {
        $mdDialog.hide();
      };
  
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
  
      $scope.answer = function(answer) {
        $mdDialog.hide(answer);
      };
    }

     $scope.cambiogpsalta = function () {
      $scope.param1 = {
        fechaini: $filter('date')($scope.myDateAlta, 'yyyy-MM-dd'),
        fechater: $filter('date')($scope.myDateAlta2, 'yyyy-MM-dd'),
        flota: ''
      }
      GetDataGPSAlta.async($scope.param1).then(function (datos) {
        $scope.data_csv_gps_alta = [];
        $scope.data_csv_gps_alta_temp = [];
        $scope.labels_csv_gps_alta = [];
        for (i = 0; i < datos.data.length; i++)
          $scope.data_csv_gps_alta_temp.push(datos.data[i]['%Good']);

        $scope.data_csv_gps_alta.push($scope.data_csv_gps_alta_temp);
        $scope.data_csv_gps_alta_temp = [];
        for (i = 0; i < datos.data.length; i++)
          $scope.data_csv_gps_alta_temp.push(datos.data[i]['%Bad']);

        $scope.data_csv_gps_alta.push($scope.data_csv_gps_alta_temp);
        $scope.data_csv_gps_alta_temp = [];
        for (i = 0; i < datos.data.length; i++)
          $scope.labels_csv_gps_alta.push(datos.data[i]['UnitId']);

        
        $scope.datasetOverride_csv_gps_alta = [
      {
        label: " Buenos ",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: " Malos ",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false,
        //borderDash: [5, 5]
      }
      
    ];



    $scope.color_csv_gps_alta =  ['#00FF00','#ff0000', '#00D015', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_gps_alta = {      
      title: {
        display: true,
        text: " GPS "       
      },
      elements: { point: { radius: 2 } },  
      legend: { display: true, fontColor: '#fcfeff' },  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12  
      },
      responsive: true,    
  
      scales: {
        xAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: $scope.fecha_,           
            display: true,
          },
          ticks: {
            autoSkip : false,      
            display: true
            
        }
        }
        ],
        yAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: ' Estado [%] ',            
            display: true,
          },
          ticks: {
                  autoSkip : false,
                  beginAtZero: true,
                  
              }
        }]
      },       
    
      animation: {
        duration: 1,
        onComplete: function (animation) {
           var chartInstance = this.chart;
    var ctx = chartInstance.ctx;
    ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";                          
    Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
      var meta = chartInstance.controller.getDatasetMeta(i);
      Chart.helpers.each(meta.data.forEach(function (bar, index) {
        ctx.save();
        // Translate 0,0 to the point you want the text
        ctx.translate(bar._model.x +5 , bar._model.y + 30);

        // Rotate context by -90 degrees
        ctx.rotate(-0.5 * Math.PI);

        // Draw text
         if (dataset.data[index] > 10)
        ctx.fillText(dataset.data[index] + '%', 0, 0);
        ctx.restore();
      }),this)
    }),this);


  }}
    }




      })

    }

    $scope.cambio_combo_fecha_com = function(){


      $scope.param1 = {
       fechaini: $filter('date')($scope.fechaIniCom, 'yyyy-MM-dd'),
        fechater: $filter('date')($scope.fechaTerCom, 'yyyy-MM-dd'),
      flota : null
    }

  GetDataComunicaciones.async($scope.param1).then(function (datos) {
   
    $scope.data_csv_co =  [];
    $scope.data_csv_co_temp =  [];
    $scope.labels_csv_co =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_co_temp.push(datos.data[i]['%Comm']);
    
    $scope.data_csv_co.push($scope.data_csv_co_temp);
   
    $scope.data_csv_co_temp =  [];
      
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_co_temp.push(datos.data[i]['%NoComm']);
    
    $scope.data_csv_co.push($scope.data_csv_co_temp);


     $scope.data_csv_co_temp =  [];
      
   
    
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_co.push(datos.data[i]['UnitId']);
   

  })
    }

  $scope.cambio_combo_fecha_plm = function(){


      $scope.param1 = {
       fechaini: $filter('date')($scope.fechaIniPlm, 'yyyy-MM-dd'),
        fechater: $filter('date')($scope.fechaTerPlm, 'yyyy-MM-dd'),
      
    }

     GetDataEfectividadPLM.async($scope.param1).then(function (datos) {

     
    $scope.data_csv_13 =  [];
    $scope.data_csv_13_temp =  [];
    $scope.labels_csv_13 =  [];
    for (i=0;i < datos.data[0].length; i++)    
    $scope.data_csv_13_temp.push(datos.data[0][i].result);
    
    
    $scope.data_csv_13.push($scope.data_csv_13_temp);

   
    
    for (i=0;i < datos.data[0].length; i++)
       $scope.labels_csv_13.push(datos.data[0][i].truck);
     })


    }  


  $scope.theHtmlString = $sce.trustAsHtml("hola");

 
  $scope.active_print = true;
  $scope.active_ingreso = true;
  $scope.active_resumen = true;
  $scope.active_dirty = false;
  $scope.active_cca = true;
  $scope.active_rma = true;
  $scope.active_ventas = true;
  $scope.active_finanzas = true;

  $scope.isDisabled = true;

  $scope.file1 = {};
  $scope.file2 = {};
  $scope.file3 = {};

  $scope.flota = null;
  $scope.flotas = null;

  $scope.alta = null;
  $scope.altas = null;

  $scope.loadFlotas = function (){
 return $timeout(function () {
  $scope.flotas = $scope.flotas || [
      {id: 0, name: 'Todos'},
     {id: 37, name: 'Komatsu 730-E'},
     {id: 39, name: 'Cat 789-C'},
     {id:52, name: 'KOM 730 ADO'},
     {id: 53, name: 'Caterp. 795'}
  ];
 },650);
  };


  $scope.loadFlotasAltas = function (){
 return $timeout(function () {
  $scope.altas = $scope.altas || [
      {id: 0, name: 'Todos'},
     {id: 37, name: 'Komatsu 730-E'},
     {id: 39, name: 'Cat 789-C'},
     {id:52, name: 'KOM 730 ADO'},
     {id: 53, name: 'Caterp. 795'}
  ];
 },650);
  };



  $scope.mylocale = {
  formatDate: function(date) {
    var m = moment(date);
    return m.isValid() ? m.format('YYYY') : '';
  }
};
 
  var promise;

  $scope.count = 0; 

  var SECOND = 1000; // PRIVATE
  var MINUTE = SECOND * 60;
  var HOUR = MINUTE * 60;
  var DAY = HOUR * 24;

  var oneTimer = $interval(function() {
    if ($scope.count >= 5) {
      
      var actual = $scope.selectedIndex;


            
    } else {
      $scope.count++;
    }
  }, 5 * SECOND);

 


  GetDataCSVCiclosCortos.async().then(function (datos) {

    
    $scope.data_csv_1 =  [];
    $scope.data_csv_1_temp =  [];
    $scope.labels_csv_1 =  [];
    console.log(datos.data.length);
    for (i=0; i< datos.data.length; i++){
    $scope.data_csv_1_temp.push(datos.data[i]['CiclosCorto']);
  
    }
    $scope.data_csv_1.push($scope.data_csv_1_temp);

    $scope.data_csv_2_temp =  [];

    for (i=0; i< datos.data.length; i++){
    $scope.data_csv_2_temp.push(datos.data[i]['Target[<0.5%]']);
    
    }
    $scope.data_csv_1.push($scope.data_csv_2_temp);
    //datos[0].data[1] ;
    for (i=0; i< datos.data.length; i++){
    $scope.labels_csv_1.push(datos.data[i]['Periodo']);
   
    }
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_1 = [
      {
        label: "Ciclos Cortos",
        borderWidth: 1,
        type: 'line',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [<0.5%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_1 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_1 = {

      
      title: {
        display: true,
        text: " Ciclos cortos "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Ciclos Cortos [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                  ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +5);
              }
          });

          //document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }



    


    };



    

  });




  GetDataCSVIntegridad.async().then(function (datos) {
    console.log(datos);
    $scope.data_csv_2 =  [];
    $scope.data_csv_2_temp =  [];
    $scope.labels_csv_2 =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_2_temp.push(datos.data[i]['Integridad']);
    
  
    $scope.data_csv_2.push($scope.data_csv_2_temp);
   
    $scope.data_csv_2_temp =  [];
      
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_2_temp.push(datos.data[i]['Target[>68%]']);
    
    $scope.data_csv_2.push($scope.data_csv_2_temp);
    //datos[0].data[1] ;
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_2.push(datos.data[i]['Periodo']);
    
    $scope.datasetOverride_csv_2 = [
      {
        label: "Integridad",
        borderWidth: 1,
        type: 'line',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [>68%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_2 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_2 = {

      
      title: {
        display: true,
        text: " Integridad "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' integridad [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                  ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +5);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }



    };

  });






  GetDataCSVIntegridadCiclosCortos.async().then(function (datos) {
    console.log(datos);
    $scope.data_csv_cc =  [];
    $scope.data_csv_cc_temp =  [];
    $scope.labels_csv_cc =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_cc_temp.push(datos.data[i]['Integridad']);
    
    $scope.data_csv_cc.push($scope.data_csv_cc_temp);
   
    $scope.data_csv_cc_temp =  [];
      
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_cc_temp.push(datos.data[i]['Target[>68%]']);
   
    $scope.data_csv_cc.push($scope.data_csv_cc_temp);
    //datos[0].data[1] ;
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_cc.push(datos.data[i]['Periodo']);
   
    $scope.datasetOverride_csv_cc = [
      {
        label: "Integridad",
        borderWidth: 1,
        type: 'line',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [<10%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_cc =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_cc = {

      
      title: {
        display: true,
        text: " Integridad Ciclos Nulos"
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' integridad [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                  ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +5);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }



    };

  });



  GetDataAsignacionesDespacho.async().then(function (datos) {
   
    $scope.data_csv_ad =  [];
    $scope.data_csv_ad_temp =  [];
    $scope.labels_csv_ad =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_ad_temp.push(datos.data[i]['Porc_Desp']);
    
    $scope.data_csv_ad.push($scope.data_csv_ad_temp);
   
    $scope.data_csv_ad_temp =  [];
      
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_ad_temp.push(datos.data[i]['target']);
    
    $scope.data_csv_ad.push($scope.data_csv_ad_temp);
    
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_ad.push(datos.data[i]['Fecha']);
    
    $scope.datasetOverride_csv_ad = [
      {
        label: "Asignaciones",
        borderWidth: 1,
        type: 'line',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [<10%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_ad =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_ad = {

      
      title: {
        display: true,
        text: "Asignaciones de Despacho"
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' integridad [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                  ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +5);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }



    };

  });



      GetDataExcepciones.async().then(function (datos) {
   
    $scope.data_csv_ex =  [];
    $scope.data_csv_ex_temp =  [];
    $scope.labels_csv_ex =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_ex_temp.push(datos.data[i]['%Acceptadas']);    
    $scope.data_csv_ex.push($scope.data_csv_ex_temp);   
    $scope.data_csv_ex_temp =  [];
      
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_ex_temp.push(datos.data[i]['%Ignoradas']);    
    $scope.data_csv_ex.push($scope.data_csv_ex_temp);

     $scope.data_csv_ex_temp =  [];
      
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_ex_temp.push(80);
    
    $scope.data_csv_ex.push($scope.data_csv_ex_temp);
    
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_ex.push(datos.data[i]['Fecha']);
    
    $scope.datasetOverride_csv_ex = [
      {
        label: "Aceptadas",
        borderWidth: 1,
        type: 'line',
        lineTension: 0,
        fill: false
      },
      {
        label: "Rechazadas",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        //borderDash: [5, 5]
      },
      {
        label: "Target [>80%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_ex =  ['#0000ff', '#00D015', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_ex = {

      
      title: {
        display: true,
        text: "Excepciones Rechazadas - Aceptadas"
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Excepciones [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if ((dataset.data[i] != 80) && (dataset.data[i] !=0))
                  ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +5);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }



    };

  });


    GetDataExcepcionesGrupos.async().then(function (datos) {
   
    $scope.data_csv_exg =  [];
    $scope.data_csv_exg_temp =  [];
    $scope.labels_csv_exg =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_exg_temp.push(datos.data[i]['%Aceptadas']);
    
    $scope.data_csv_exg.push($scope.data_csv_exg_temp);
   
    $scope.data_csv_exg_temp =  [];
      
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_exg_temp.push(datos.data[i]['%Ignoradas']);
    
    $scope.data_csv_exg.push($scope.data_csv_exg_temp);


     $scope.data_csv_exg_temp =  [];
      
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_exg_temp.push(80);
    
    $scope.data_csv_exg.push($scope.data_csv_exg_temp);
    
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_exg.push(datos.data[i]['Fecha']);
    
    $scope.datasetOverride_csv_exg = [
      {
        label: "Aceptadas",
        borderWidth: 1,
        type: 'line',
        lineTension: 0,
        fill: false
      },
      {
        label: "Rechazadas",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        //borderDash: [5, 5]
      },
      {
        label: "Target [<80%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_exg =  ['#0000ff', '#00D015', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_exg = {

      
      title: {
        display: true,
        text: "Excepciones Rechazadas - Aceptadas"
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: '  [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if ((dataset.data[i] != 80) && (dataset.data[i] !=0))
                  ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +5);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }



    };

  });


  var fecha_hoy = $filter('date')(new Date(), 'yyyy-MM-dd');
  $scope.param1 = {
      fechaini: fecha_hoy,
      fechater: fecha_hoy,
      flota : null
    }

  GetDataComunicaciones.async($scope.param1).then(function (datos) {
   
    $scope.data_csv_co =  [];
    $scope.data_csv_co_temp =  [];
    $scope.labels_csv_co =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_co_temp.push(datos.data[i]['%Comm']);
    
    $scope.data_csv_co.push($scope.data_csv_co_temp);
   
    $scope.data_csv_co_temp =  [];
      
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_co_temp.push(datos.data[i]['%NoComm']);
    
    $scope.data_csv_co.push($scope.data_csv_co_temp);


     $scope.data_csv_co_temp =  [];
      
   
    
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_co.push(datos.data[i]['UnitId']);
    
    $scope.datasetOverride_csv_co = [
      {
        label: " Enviados ",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: " Perdidos ",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false,
        //borderDash: [5, 5]
      },
      
    ];

    $scope.color_csv_co =  ['#0000ff','#ff0000', '#00D015', '#ff0000', '#00D015', '#808080', '#ff0000'];

    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    curr_month++;
    var curr_year = d.getFullYear();
     $scope.fecha_ = curr_date + "-" + curr_month + "-" + curr_year;

    $scope.options_csv_co = {


      
      title: {
        display: true,
        text: " Comunicaciones "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 2 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: true,
          scaleLabel: {
            labelString:  $scope.fecha_,
            //fontColor: '#151515',
            display: true,
          },
          ticks: {
            autoSkip : false,      
            display: true,
            
        }
        }
        ],
        yAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: ' Estado [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  autoSkip : false, 
                  beginAtZero: true,
                  
              }
        }]
      },
       
    
      animation: {
        duration: 1,
        onComplete: function (animation) {
           var chartInstance = this.chart;
    var ctx = chartInstance.ctx;
    ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";                          
    Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
      var meta = chartInstance.controller.getDatasetMeta(i);
      Chart.helpers.each(meta.data.forEach(function (bar, index) {
        ctx.save();
        // Translate 0,0 to the point you want the text
        ctx.translate(bar._model.x +5 , bar._model.y + 30);

        // Rotate context by -90 degrees
        ctx.rotate(-0.5 * Math.PI);

        // Draw text
         if (dataset.data[index] > 10)
        ctx.fillText(dataset.data[index] + '%', 0, 0);
        ctx.restore();
      }),this)
    }),this);


  }}
    }

  });


   GetDataSinComunicaciones.async().then(function (datos) {
   
    $scope.data_csv_sinco =  [];
    $scope.data_csv_sinco_temp =  [];
    $scope.labels_csv_sinco =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_sinco_temp.push(datos.data[i]['NoTalks']);
    
    $scope.data_csv_sinco.push($scope.data_csv_sinco_temp);
   
    
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_sinco.push(datos.data[i]['Location']);
    
    $scope.datasetOverride_csv_sinco = [
      {
        label: " Perdidas de Comunicación ",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: " Perdidos ",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false,
        //borderDash: [5, 5]
      },
      
    ];

     var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    curr_month++;
    var curr_year = d.getFullYear();
     $scope.fecha_ = curr_date + "-" + curr_month + "-" + curr_year;

    $scope.color_csv_sinco =  ['#0000ff','#ff0000', '#00D015', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_sinco = {

      
      title: {
        display: true,
        text: " Ubicaciones sin comunicación "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 2 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: $scope.fecha_,
            //fontColor: '#151515',
            display: true,
          },
          
         ticks: {
            fontSize: 10,
            autoSkip : false,      
            display: true,
           
            
        }
        }
        ],
        yAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: ' [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  autoSkip : false, 
                  beginAtZero: true,
                  
              }
        }]
      },
       

      animation: {
        duration: 1,
        onComplete: function (animation) {
          var chartInstance = this.chart;
          var ctx = chartInstance.ctx;
          ctx.font = "bold 7pt Arial";
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';
          ctx.fillStyle = "#000000";



        }
      }
    }

  });



  GetDataNoTalks.async().then(function (datos) {
   
    $scope.data_csv_notalk =  [];
    $scope.data_csv_notalk_temp =  [];
    $scope.labels_csv_notalk =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_notalk_temp.push(datos.data[i]['Porc_NoTalks']);
    
    $scope.data_csv_notalk.push($scope.data_csv_notalk_temp);
   
    
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_notalk.push(datos.data[i]['Equipment']);
    
    $scope.datasetOverride_csv_notalk = [
      {
        label: "  Equipos ",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: " Perdidos ",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false,
        //borderDash: [5, 5]
      },
      
    ];

     var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    curr_month++;
    var curr_year = d.getFullYear();
     $scope.fecha_ = curr_date + "-" + curr_month + "-" + curr_year;

    $scope.color_csv_notalk =  ['#ff0000', '#00D015', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_notalk = {

      
      title: {
        display: true,
        text: " No Talk por Equipos "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 2 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: $scope.fecha_,
            //fontColor: '#151515',
            display: true,
          },
          
         ticks: {
            fontSize: 10,
            autoSkip : false,      
            display: true,
           
            
        }
        }
        ],
        yAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: ' [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  autoSkip : false, 
                  beginAtZero: true,
                  
              }
        }]
      },
       

      animation: {
        duration: 1,
        onComplete: function (animation) {
          var chartInstance = this.chart;
          var ctx = chartInstance.ctx;
          ctx.font = "bold 7pt Arial";
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';
          ctx.fillStyle = "#000000";



        }
      }
    }

  });



  $scope.data_dirty_line = [[Math.random()*10,Math.random()*10,Math.random()*100,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*30,Math.random()],[Math.random()*10,Math.random()*100,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*100,Math.random()*10,Math.random()],[Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()],[Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()],[Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()*10,Math.random()]];     

    $scope.colors_dirty_line = ['#424242', '#3ADF00', '#808080', '#ffff00', '#ff0000']
    $scope.labels_dirty_line = ["0", "1","2","3","4","5", "6"];

    $scope.options_dirty_line = {

      
      title: {
        display: true,
        text: " Frecuencia de Palas "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          position: 'top',
          stacked: true,
          scaleLabel: {
            labelString: ' Tiempo Estimado',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: ' Tiempo Real [min] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  //beginAtZero: true
              }
        }]
      }     
    



    }

      $scope.datasetOverride_dirty_line = [
        {
          label: " Operativo ",
          borderWidth: 3,        
          type: 'line',
          lineTension: 0,
          fill: false,
          backgroundColor: "#424242",
          steppedLine: true,
          pointRadius: 0
        },
        {
        label: " Demora ",
        borderWidth: 3,        
        type: 'line',
        lineTension: 0,
        fill: false,
        backgroundColor: "#3ADF00",
        steppedLine: true,
        pointRadius: 0
        },
        {
          label: " Disponible ",
          borderWidth: 3,        
          type: 'line',
          lineTension: 0,
          fill: false,
          backgroundColor: "#808080",
          steppedLine: true,
          pointRadius: 0
          },

          {
            label: " Para ",
            borderWidth: 3,        
            type: 'line',
            lineTension: 0,
            fill: false,
            backgroundColor: "#ffff00",
            steppedLine: true,
            pointRadius: 0
            },

            {
              label: " Cambio Turno ",
              borderWidth: 3,        
              type: 'line',
              lineTension: 0,
              fill: false,
              backgroundColor: "#ff0000",
              steppedLine: true,
              pointRadius: 0
              }
        
      ];





  getDataFrecuenciaEsperaPala.async($scope.param1).then(function (datos) {   

    $scope.data_dirty_frp = datos.data[1].data2;     

    $scope.colors_dirty_frp = ['#000000', '#B2B2B2' ];
   

    $scope.options_dirty_frp = {

      
      title: {
        display: true,
        text: " Frecuencia de Palas "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: false, fontColor: '#fcfeff' },
  
      tooltips: {
        display: false,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
         // position: 'top',
          stacked: false,
          scaleLabel: {
            labelString: ' Tiempo Estimado',
            //fontColor: '#151515',
            display: false,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Tiempo Real [min] ',
            // fontColor: '#151515',
            display: false,
          },
          ticks: {
                  //beginAtZero: true
              }
        }]
      }     
    



    }

      $scope.datasetOverride_dirty_frp = [
        {
          label: " Estimado ",
          borderWidth: 3,        
          type: 'line',
          lineTension: 0,
          fill: false,
          backgroundColor: "rgba(0,0,0,1)",
          //borderDash: [5, 5],
          pointRadius: 0
        },
        {
        label: " Estimado ",
        borderWidth: 3,        
        type: 'line',
        lineTension: 0,
        fill: false,
        backgroundColor: "rgba(0,0,0,3)",
        //borderDash: [5, 5],
        pointRadius: 0
        }
        
      ];


  });

  getDataEstimadaVSReal.async($scope.param1).then(function (datos) {   
      
    $scope.data_dirty_tvr = datos.data[1].data2;     

    $scope.colors_dirty_tvr = ['#37eeae', '#f30938' ];
   

    $scope.options_dirty_tvr = {

      
      title: {
        display: true,
        text: " Tiempo estimado vs tiempo real "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: ' Tiempo Estimado',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: ' Tiempo Real [min] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  //beginAtZero: true
              }
        }]
      }

      
       
    



    }

      $scope.datasetOverride_dirty_tvr = [
        {
          type: 'bubble',
          label: ' Real ',
          //data: [30,40,50,60,70],
          fill: false,
          backgroundColor: "rgba(112,202,171)",
          borderColor: "transparent",
          pointRadius: 0
        },
        {
          label: " Estimado ",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        backgroundColor: "rgba(243,7,50)",
        borderDash: [5, 5],
        pointRadius: 2
        }
        
      ];


    
      

      });




  var hoy_ = $filter('date')(new Date(), 'yyyy-MM-dd');
  $scope.param1 = {
      fechaini: hoy_,
      fechater: hoy_,
      flota : null
    }

 GetDataGPSAlta.async($scope.param1).then(function (datos) {
        $scope.data_csv_gps_alta = [];
        $scope.data_csv_gps_alta_temp = [];
        $scope.labels_csv_gps_alta = [];
        for (i = 0; i < datos.data.length; i++)
          $scope.data_csv_gps_alta_temp.push(datos.data[i]['%Good']);

        $scope.data_csv_gps_alta.push($scope.data_csv_gps_alta_temp);
        $scope.data_csv_gps_alta_temp = [];
        for (i = 0; i < datos.data.length; i++)
          $scope.data_csv_gps_alta_temp.push(datos.data[i]['%Bad']);

        $scope.data_csv_gps_alta.push($scope.data_csv_gps_alta_temp);
        $scope.data_csv_gps_alta_temp = [];
        for (i = 0; i < datos.data.length; i++)
          $scope.labels_csv_gps_alta.push(datos.data[i]['UnitId']);

        
        $scope.datasetOverride_csv_gps_alta = [
      {
        label: " Buenos ",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: " Malos ",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false,
        //borderDash: [5, 5]
      }
      
    ];



    $scope.color_csv_gps_alta =  ['#00FF00','#ff0000', '#00D015', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_gps_alta = {      
      title: {
        display: true,
        text: " GPS "       
      },
      elements: { point: { radius: 2 } },  
      legend: { display: true, fontColor: '#fcfeff' },  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12  
      },
      responsive: true,    
  
      scales: {
        xAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: $scope.fecha_,           
            display: true,
          },
          ticks: {
            autoSkip : false,      
            display: true
            
        }
        }
        ],
        yAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: ' Estado [%] ',            
            display: true,
          },
          ticks: {
                  autoSkip : false,
                  beginAtZero: true,
                  
              }
        }]
      },       
    
      animation: {
        duration: 1,
        onComplete: function (animation) {
           var chartInstance = this.chart;
    var ctx = chartInstance.ctx;
    ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";                          
    Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
      var meta = chartInstance.controller.getDatasetMeta(i);
      Chart.helpers.each(meta.data.forEach(function (bar, index) {
        ctx.save();
        // Translate 0,0 to the point you want the text
        ctx.translate(bar._model.x +5 , bar._model.y + 30);

        // Rotate context by -90 degrees
        ctx.rotate(-0.5 * Math.PI);

        // Draw text
         if (dataset.data[index] > 10)
        ctx.fillText(dataset.data[index] + '%', 0, 0);
        ctx.restore();
      }),this)
    }),this);


  }}
    }




      });

 GetDataGPS.async($scope.param1).then(function (datos) {
   
    $scope.data_csv_gps =  [];
    $scope.data_csv_gps_temp =  [];
    $scope.labels_csv_gps =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_gps_temp.push(datos.data[i]['%Good']);
    
    $scope.data_csv_gps.push($scope.data_csv_gps_temp);
   
    $scope.data_csv_gps_temp =  [];
      
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_gps_temp.push(datos.data[i]['%Bad']);
    
    $scope.data_csv_gps.push($scope.data_csv_gps_temp);


     $scope.data_csv_gps_temp =  [];
      
    
    
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_gps.push(datos.data[i]['UnitId']);
    
    $scope.datasetOverride_csv_gps = [
      {
        label: " Buenos ",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: " Malos ",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false,
        //borderDash: [5, 5]
      },
      
    ];

     var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    curr_month++;
    var curr_year = d.getFullYear();
     $scope.fecha_ = curr_date + "-" + curr_month + "-" + curr_year;

    $scope.color_csv_gps =  ['#00FF00','#ff0000', '#00D015', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_gps = {      
      title: {
        display: true,
        text: " GPS "       
      },
      elements: { point: { radius: 2 } },  
      legend: { display: true, fontColor: '#fcfeff' },  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12  
      },
      responsive: true,    
  
      scales: {
        xAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: $scope.fecha_,           
            display: true,
          },
          ticks: {
            autoSkip : false,      
            display: true
            
        }
        }
        ],
        yAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: ' Estado [%] ',            
            display: true,
          },
          ticks: {
                  autoSkip : false,
                  beginAtZero: true,
                  
              }
        }]
      },       
    
      animation: {
        duration: 1,
        onComplete: function (animation) {
           var chartInstance = this.chart;
    var ctx = chartInstance.ctx;
    ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";                          
    Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
      var meta = chartInstance.controller.getDatasetMeta(i);
      Chart.helpers.each(meta.data.forEach(function (bar, index) {
        ctx.save();
        // Translate 0,0 to the point you want the text
        ctx.translate(bar._model.x +5 , bar._model.y + 30);

        // Rotate context by -90 degrees
        ctx.rotate(-0.5 * Math.PI);

        // Draw text
         if (dataset.data[index] > 10)
        ctx.fillText(dataset.data[index] + '%', 0, 0);
        ctx.restore();
      }),this)
    }),this);


  }}
    }

  });








  GetDataCSVLlegadasTardes.async().then(function (datos) {
    $scope.data_csv_3 =  [];
    $scope.data_csv_3_temp =  [];
    $scope.labels_csv_3 =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_3_temp.push(datos.data[i]['LlegadasTarde']);
    
    
    $scope.data_csv_3.push($scope.data_csv_3_temp);

    $scope.data_csv_3_temp =  [];

    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_3_temp.push(datos.data[i]['Target[<10%]']);
   
    $scope.data_csv_3.push($scope.data_csv_3_temp);
    //datos[0].data[1] ;
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_3.push(datos.data[i]['Periodo']);
   
    $scope.datasetOverride_csv_3 = [
      {
        label: "Llegadas tardes",
        borderWidth: 1,
        type: 'line',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [<10%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_3 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_3 = {

      
      title: {
        display: true,
        text: " Llegadas Tardes "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Llegadas Tardes [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                  ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +5);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }
       
    



    };

  });



  GetDataCSVBalizasPerdidas.async().then(function (datos) {
    $scope.data_csv_4 =  [];
    $scope.data_csv_4_temp =  [];
    $scope.labels_csv_4 =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_4_temp.push(datos.data[i]['BalizasPerdidas']);
    //$scope.data_csv_4_temp.push(datos.data[1]['BalizasPerdidas']);
    //$scope.data_csv_4_temp.push(datos.data[2]['BalizasPerdidas']);
   // $scope.data_csv_4_temp.push(datos[0].data[4][1]);
    //$scope.data_csv_4_temp.push(datos[0].data[5][1]);
    
    $scope.data_csv_4.push($scope.data_csv_4_temp);

    $scope.data_csv_4_temp =  [];

   for (i=0; i< datos.data.length; i++)
   $scope.data_csv_4_temp.push(datos.data[i]['Target[<5]']);
   // $scope.data_csv_4_temp.push(datos.data[1]['Target[<5]']);
   // $scope.data_csv_4_temp.push(datos.data[2]['Target[<5]']);
    //$scope.data_csv_4_temp.push(datos[0].data[4][2]);
    //$scope.data_csv_4_temp.push(datos[0].data[5][2]);

    $scope.data_csv_4.push($scope.data_csv_4_temp);
    //datos[0].data[1] ;
   for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_4.push(datos.data[i]['Periodo']);
    //$scope.labels_csv_4.push(datos.data[1]['Periodo']);
    //$scope.labels_csv_4.push(datos.data[2]['Periodo']);
    //$scope.labels_csv_4.push(datos.data[4][0]);
    //$scope.labels_csv_4.push(datos[0].data[5][0]);
    //$scope.labels_csv_1.push(datos[0].data[4][0]);
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_4 = [
      {
        label: "Balizas Perdidas",
        borderWidth: 1,
        type: 'line',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [<5%]",
        borderWidth: 1,        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_4 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_4 = {

      
      title: {
        display: true,
        text: " Balizas Perdidas "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Balizas Perdidas ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                  ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +8);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }
       
    



    };

  });


  GetDataCSVCargasExtras.async().then(function (datos) {
    $scope.data_csv_5 =  [];
    $scope.data_csv_5_temp =  [];
    $scope.labels_csv_5 =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_5_temp.push(datos.data[i]['CargasExtras']);
    //$scope.data_csv_5_temp.push(datos.data[1]['CargasExtras']);
    //$scope.data_csv_5_temp.push(datos.data[2]['CargasExtras']);
    //$scope.data_csv_5_temp.push(datos[0].data[4][1]);
    //$scope.data_csv_5_temp.push(datos[0].data[5][1]);
    
    $scope.data_csv_5.push($scope.data_csv_5_temp);

    $scope.data_csv_5_temp =  [];

     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_5_temp.push(datos.data[i]['Target[<2%]']);
    //$scope.data_csv_5_temp.push(datos.data[1]['Target[<2%]']);
    //$scope.data_csv_5_temp.push(datos.data[2]['Target[<2%]']);
    //$scope.data_csv_5_temp.push(datos[0].data[4][2]);
    //$scope.data_csv_5_temp.push(datos[0].data[5][2]);

    $scope.data_csv_5.push($scope.data_csv_5_temp);
    //datos[0].data[1] ;
     for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_5.push(datos.data[i]['Periodo']);
    //$scope.labels_csv_5.push(datos.data[1]['Periodo']);
    //$scope.labels_csv_5.push(datos.data[2]['Periodo']);
    //$scope.labels_csv_5.push(datos.data[4][0]);
    //$scope.labels_csv_5.push(datos[0].data[5][0]);
    //$scope.labels_csv_1.push(datos[0].data[4][0]);
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_5 = [
      {
        label: "Cargas Extras",
        borderWidth: 1,
        type: 'line',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [<2%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_5 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_5 = {

      
      title: {
        display: true,
        text: " Cargas Extras "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Cargas Extras [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                  ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +15);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }
       
    



    };

  });




  GetDataCSVUsoDinamico.async().then(function (datos) {
    $scope.data_csv_6 =  [];
    $scope.data_csv_6_temp =  [];
    $scope.labels_csv_6 =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_6_temp.push(datos.data[i]['UsoDinamico']);
    //$scope.data_csv_6_temp.push(datos.data[1]['UsoDinamico']);
    //$scope.data_csv_6_temp.push(datos.data[2]['UsoDinamico']);
    //$scope.data_csv_6_temp.push(datos[0].data[4][1]);
    //$scope.data_csv_6_temp.push(datos[0].data[5][1]);
    
    $scope.data_csv_6.push($scope.data_csv_6_temp);

    $scope.data_csv_6_temp =  [];

    for (i=0; i< datos.data.length; i++) 
    $scope.data_csv_6_temp.push(datos.data[i]['Target[>80%]']);
    //$scope.data_csv_6_temp.push(datos.data[1]['Target[>80%]']);
    //$scope.data_csv_6_temp.push(datos.data[2]['Target[>80%]']);
    //$scope.data_csv_6_temp.push(datos[0].data[4][2]);
    //$scope.data_csv_6_temp.push(datos[0].data[5][2]);

    $scope.data_csv_6.push($scope.data_csv_6_temp);
    //datos[0].data[1] ;
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_6.push(datos.data[i]['Periodo']);
    //$scope.labels_csv_6.push(datos.data[1]['Periodo']);
    //$scope.labels_csv_6.push(datos.data[2]['Periodo']);
    //$scope.labels_csv_6.push(datos[0].data[4][0]);
    //$scope.labels_csv_6.push(datos[0].data[5][0]);
    //$scope.labels_csv_1.push(datos[0].data[4][0]);
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_6 = [
      {
        label: "Uso Dinamico",
        borderWidth: 1,
        type: 'line',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [>80%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_6 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_6 = {

      
      title: {
        display: true,
        text: " Uso Dinamico "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Uso Dinamico [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if (dataset.data[i] != 80) 
                  ctx.fillText(dataset.data[i], model.x, model.y + 15);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }
       
    



    };

  });





  GetDataCSVBalizasPerdidasGrupos.async().then(function (datos) {
    
    $scope.data_csv_7 =  [];
    $scope.data_csv_7_temp =  [];
    $scope.labels_csv_7 =  [];
     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_7_temp.push(datos.data[i]['Grupo 1']);
   
    
    $scope.data_csv_7.push($scope.data_csv_7_temp);

    $scope.data_csv_7_temp =  [];
     for (i=0; i< datos.data.length; i++) 
    $scope.data_csv_7_temp.push(datos.data[i]['Grupo 2']);
    

    $scope.data_csv_7.push($scope.data_csv_7_temp);

    $scope.data_csv_7_temp =  [];

     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_7_temp.push(datos.data[i]['Grupo 3']);
    

    $scope.data_csv_7.push($scope.data_csv_7_temp);

    $scope.data_csv_7_temp =  [];

     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_7_temp.push(datos.data[i]['Grupo 4']);
   

    $scope.data_csv_7.push($scope.data_csv_7_temp);


    $scope.data_csv_7_temp =  [];

     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_7_temp.push(datos.data[i]['Target[<5]']);
    

    $scope.data_csv_7.push($scope.data_csv_7_temp);
    
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_7.push(datos.data[i].Periodo);
    
    $scope.datasetOverride_csv_7 = [
      {
        label: "Grupo 1",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: "Grupo 2",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Grupo 3",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Grupo 4",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Target[<5]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_7 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_7 = {

      
      title: {
        display: true,
        text: " Balizas Perdidas "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Balizas Perdidas ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  //beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if (dataset.data[i] != 0) 
                  ctx.fillText(dataset.data[i], model.x, model.y + 15);
              }
          });

         // document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }
       
    



    };

  });


  GetDataCSVCiclosCortosGrupos.async().then(function (datos) {
    $scope.data_csv_8 =  [];
    $scope.data_csv_8_temp =  [];
    $scope.labels_csv_8 =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_8_temp.push(datos.data[i]['Grupo 1']);
    //$scope.data_csv_8_temp.push(datos.data[0]['Grupo 1']);
    //$scope.data_csv_8_temp.push(datos.data[0]['Grupo 1']);
    //$scope.data_csv_8_temp.push(datos[0].data[4][1]);
    //$scope.data_csv_8_temp.push(datos[0].data[5][1]);
    
    $scope.data_csv_8.push($scope.data_csv_8_temp);

    $scope.data_csv_8_temp =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_8_temp.push(datos.data[i]['Grupo 2']);
    //$scope.data_csv_8_temp.push(datos.data[1]['Grupo 2']);
    //$scope.data_csv_8_temp.push(datos.data[2]['Grupo 2']);
    //$scope.data_csv_8_temp.push(datos[0].data[4][2]);
    //$scope.data_csv_8_temp.push(datos[0].data[5][2]);

    $scope.data_csv_8.push($scope.data_csv_8_temp);

    $scope.data_csv_8_temp =  [];
  for (i=0; i< datos.data.length; i++)
    $scope.data_csv_8_temp.push(datos.data[i]['Grupo 3']);
    //$scope.data_csv_8_temp.push(datos.data[1]['Grupo 3']);
    //$scope.data_csv_8_temp.push(datos.data[2]['Grupo 3']);
    //$scope.data_csv_8_temp.push(datos[0].data[4][3]);
    //$scope.data_csv_8_temp.push(datos[0].data[5][3]);

    $scope.data_csv_8.push($scope.data_csv_8_temp);

    $scope.data_csv_8_temp =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_8_temp.push(datos.data[i]['Grupo 4']);
    //$scope.data_csv_8_temp.push(datos.data[1]['Grupo 4']);
    //$scope.data_csv_8_temp.push(datos.data[2]['Grupo 4']);
    //$scope.data_csv_8_temp.push(datos[0].data[4][4]);
    //$scope.data_csv_8_temp.push(datos[0].data[5][4]);

    $scope.data_csv_8.push($scope.data_csv_8_temp);


    $scope.data_csv_8_temp =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_8_temp.push(datos.data[i]['Target[<0.5%]']);
    //$scope.data_csv_8_temp.push(datos.data[1]['Target[<0.5%]']);
    //$scope.data_csv_8_temp.push(datos.data[2]['Target[<0.5%]']);
    //$scope.data_csv_8_temp.push(datos[0].data[4][6]);
    //$scope.data_csv_8_temp.push(datos[0].data[5][6]);

    $scope.data_csv_8.push($scope.data_csv_8_temp);
    //datos[0].data[1] ;
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_8.push(datos.data[i]['Periodo']);
    //$scope.labels_csv_8.push(datos.data[1]['Periodo']);
    //$scope.labels_csv_8.push(datos.data[2]['Periodo']);
    //$scope.labels_csv_8.push(datos[0].data[4][0]);
    //$scope.labels_csv_8.push(datos[0].data[5][0]);
    //$scope.labels_csv_1.push(datos[0].data[4][0]);
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_8 = [
      {
        label: "Grupo 1",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: "Grupo 2",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Grupo 3",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Grupo 4",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Target[<5]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_8 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_8 = {

      
      title: {
        display: true,
        text: " Ciclos Cortos "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Ciclos Cortos [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  //beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if (dataset.data[i] != 0) 
                  ctx.fillText(dataset.data[i], model.x, model.y + 15);
              }
          });

         // document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }
       
    



    };

  });



  GetDataCSVIntegridadGrupos.async().then(function (datos) {
    $scope.data_csv_9 =  [];
    $scope.data_csv_9_temp =  [];
    $scope.labels_csv_9 =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_9_temp.push(datos.data[i]['Grupo 1']);
   // $scope.data_csv_9_temp.push(datos.data[1]['Grupo 1']);
   // $scope.data_csv_9_temp.push(datos.data[2]['Grupo 1']);
    //$scope.data_csv_9_temp.push(datos[0].data[4][1]);
    //$scope.data_csv_9_temp.push(datos[0].data[5][1]);
    
    $scope.data_csv_9.push($scope.data_csv_9_temp);

    $scope.data_csv_9_temp =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_9_temp.push(datos.data[i]['Grupo 2']);
    //$scope.data_csv_9_temp.push(datos.data[1]['Grupo 2']);
    //$scope.data_csv_9_temp.push(datos.data[2]['Grupo 2']);
    //$scope.data_csv_9_temp.push(datos[0].data[4][2]);
    //$scope.data_csv_9_temp.push(datos[0].data[5][2]);

    $scope.data_csv_9.push($scope.data_csv_9_temp);

    $scope.data_csv_9_temp =  [];

     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_9_temp.push(datos.data[i]['Grupo 3']);
   // $scope.data_csv_9_temp.push(datos.data[1]['Grupo 3']);
   // $scope.data_csv_9_temp.push(datos.data[2]['Grupo 3']);
    //$scope.data_csv_9_temp.push(datos[0].data[4][3]);
    //$scope.data_csv_9_temp.push(datos[0].data[5][3]);

    $scope.data_csv_9.push($scope.data_csv_9_temp);

    $scope.data_csv_9_temp =  [];
     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_9_temp.push(datos.data[i]['Grupo 4']);
    //$scope.data_csv_9_temp.push(datos.data[1]['Grupo 4']);
    //$scope.data_csv_9_temp.push(datos.data[2]['Grupo 4']);
    //$scope.data_csv_9_temp.push(datos[0].data[4][4]);
    //$scope.data_csv_9_temp.push(datos[0].data[5][4]);

    $scope.data_csv_9.push($scope.data_csv_9_temp);


    $scope.data_csv_9_temp =  [];
     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_9_temp.push(datos.data[i]['Target[>68%]']);
    //$scope.data_csv_9_temp.push(datos.data[1]['Target[>68%]']);
    //$scope.data_csv_9_temp.push(datos.data[2]['Target[>68%]']);
    //$scope.data_csv_9_temp.push(datos[0].data[4][6]);
    //$scope.data_csv_9_temp.push(datos[0].data[5][6]);

    $scope.data_csv_9.push($scope.data_csv_9_temp);
    //datos[0].data[1] ;
     for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_9.push(datos.data[i]['Periodo']);
    //$scope.labels_csv_9.push(datos.data[1]['Periodo']);
    //$scope.labels_csv_9.push(datos.data[2]['Periodo']);
    //$scope.labels_csv_9.push(datos[0].data[4][0]);
    //$scope.labels_csv_9.push(datos[0].data[5][0]);
    //$scope.labels_csv_1.push(datos[0].data[4][0]);
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_9 = [
      {
        label: "Grupo 1",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: "Grupo 2",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Grupo 3",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Grupo 4",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Target[>68]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_9 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_9 = {

      
      title: {
        display: true,
        text: " Integridad "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Integridad [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  //beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if (dataset.data[i] != 0) 
                  ctx.fillText(dataset.data[i], model.x, model.y + 15);
              }
          });

         // document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }
       
    



    };

  });




  GetDataCSVLlegadasTardesGrupos.async().then(function (datos) {
    $scope.data_csv_10 =  [];
    $scope.data_csv_10_temp =  [];
    $scope.labels_csv_10 =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_10_temp.push(datos.data[i]['Grupo 1']);
    //$scope.data_csv_10_temp.push(datos.data[0]['Grupo 1']);
    //$scope.data_csv_10_temp.push(datos.data[0]['Grupo 1']);
    //$scope.data_csv_10_temp.push(datos[0].data[4][1]);
    //$scope.data_csv_10_temp.push(datos[0].data[5][1]);
    
    $scope.data_csv_10.push($scope.data_csv_10_temp);

    $scope.data_csv_10_temp =  [];
     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_10_temp.push(datos.data[i]['Grupo 2']);
    //$scope.data_csv_10_temp.push(datos.data[1]['Grupo 2']);
    //$scope.data_csv_10_temp.push(datos.data[2]['Grupo 2']);
    //$scope.data_csv_10_temp.push(datos[0].data[4][2]);
    //$scope.data_csv_10_temp.push(datos[0].data[5][2]);

    $scope.data_csv_10.push($scope.data_csv_10_temp);

    $scope.data_csv_10_temp =  [];
     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_10_temp.push(datos.data[i]['Grupo 3']);
    //$scope.data_csv_10_temp.push(datos.data[1]['Grupo 3']);
    //$scope.data_csv_10_temp.push(datos.data[2]['Grupo 3']);
    //$scope.data_csv_10_temp.push(datos[0].data[4][3]);
    //$scope.data_csv_10_temp.push(datos[0].data[5][3]);

    $scope.data_csv_10.push($scope.data_csv_10_temp);

    $scope.data_csv_10_temp =  [];
     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_10_temp.push(datos.data[i]['Grupo 4']);
    //$scope.data_csv_10_temp.push(datos.data[1]['Grupo 4']);
    //$scope.data_csv_10_temp.push(datos.data[2]['Grupo 4']);
    //$scope.data_csv_10_temp.push(datos[0].data[4][4]);
    //$scope.data_csv_10_temp.push(datos[0].data[5][4]);

    $scope.data_csv_10.push($scope.data_csv_10_temp);


    $scope.data_csv_10_temp =  [];
     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_10_temp.push(datos.data[i]['Target[<10%]']);
    //$scope.data_csv_10_temp.push(datos.data[1]['Target[<10%]']);
    //$scope.data_csv_10_temp.push(datos.data[2]['Target[<10%]']);
    //$scope.data_csv_10_temp.push(datos[0].data[4][6]);
    //$scope.data_csv_10_temp.push(datos[0].data[5][6]);

    $scope.data_csv_10.push($scope.data_csv_10_temp);
    //datos[0].data[1] ;
     for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_10.push(datos.data[i]['Periodo']);
    //$scope.labels_csv_10.push(datos.data[1]['Periodo']);
   // $scope.labels_csv_10.push(datos.data[2]['Periodo']);
    //$scope.labels_csv_10.push(datos[0].data[4][0]);
    //$scope.labels_csv_10.push(datos[0].data[5][0]);
    //$scope.labels_csv_1.push(datos[0].data[4][0]);
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_10 = [
      {
        label: "Grupo 1",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: "Grupo 2",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Grupo 3",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Grupo 4",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Target[<10%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_10 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_10 = {

      
      title: {
        display: true,
        text: " Llegadas Tardes "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Llegadas Tardes [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  //beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if ((dataset.data[i] != 10) && (dataset.data[i] != 0))
                  ctx.fillText(dataset.data[i], model.x, model.y + 15);
              }
          });

         // document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }
       
    



    };

  });



  GetDataCSVCargasExtrasGrupos.async().then(function (datos) {
    $scope.data_csv_11 =  [];
    $scope.data_csv_11_temp =  [];
    $scope.labels_csv_11 =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_11_temp.push(datos.data[i]['Grupo 1']);
    //$scope.data_csv_11_temp.push(datos.data[1]['Grupo 1']);
    //$scope.data_csv_11_temp.push(datos.data[2]['Grupo 1']);
    //$scope.data_csv_11_temp.push(datos[0].data[4][1]);
    //$scope.data_csv_11_temp.push(datos[0].data[5][1]);
    
    $scope.data_csv_11.push($scope.data_csv_11_temp);

    $scope.data_csv_11_temp =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_11_temp.push(datos.data[i]['Grupo 2']);
    //$scope.data_csv_11_temp.push(datos.data[1]['Grupo 2']);
   // $scope.data_csv_11_temp.push(datos.data[2]['Grupo 2']);
    //$scope.data_csv_11_temp.push(datos[0].data[4][2]);
    //$scope.data_csv_11_temp.push(datos[0].data[5][2]);

    $scope.data_csv_11.push($scope.data_csv_11_temp);

    $scope.data_csv_11_temp =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_11_temp.push(datos.data[i]['Grupo 3']);
    //$scope.data_csv_11_temp.push(datos.data[1]['Grupo 3']);
    //$scope.data_csv_11_temp.push(datos.data[2]['Grupo 3']);
    //$scope.data_csv_11_temp.push(datos[0].data[4][3]);
    //$scope.data_csv_11_temp.push(datos[0].data[5][3]);

    $scope.data_csv_11.push($scope.data_csv_11_temp);

    $scope.data_csv_11_temp =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_11_temp.push(datos.data[i]['Grupo 4']);
    //$scope.data_csv_11_temp.push(datos.data[1]['Grupo 4']);
    //$scope.data_csv_11_temp.push(datos.data[2]['Grupo 4']);
    //$scope.data_csv_11_temp.push(datos[0].data[4][4]);
    //$scope.data_csv_11_temp.push(datos[0].data[5][4]);

    $scope.data_csv_11.push($scope.data_csv_11_temp);


    $scope.data_csv_11_temp =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_11_temp.push(datos.data[i]['Target[<2%]']);
    //$scope.data_csv_11_temp.push(datos.data[1]['Target[<2%]']);
    //$scope.data_csv_11_temp.push(datos.data[2]['Target[<2%]']);
    //$scope.data_csv_11_temp.push(datos[0].data[4][6]);
    //$scope.data_csv_11_temp.push(datos[0].data[5][6]);

    $scope.data_csv_11.push($scope.data_csv_11_temp);
    //datos[0].data[1] ;
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_11.push(datos.data[i]['Periodo']);
    //$scope.labels_csv_11.push(datos.data[1]['Periodo']);
    //$scope.labels_csv_11.push(datos.data[2]['Periodo']);
    //$scope.labels_csv_11.push(datos[0].data[4][0]);
    //$scope.labels_csv_11.push(datos[0].data[5][0]);
    //$scope.labels_csv_1.push(datos[0].data[4][0]);
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_11 = [
      {
        label: "Grupo 1",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: "Grupo 2",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Grupo 3",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Grupo 4",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Target[<2%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_11 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_11 = {

      
      title: {
        display: true,
        text: " Cargas Extras "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Cargas Extras [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  //beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  if (dataset.data[i] == 2) 
                  ctx.fillText(dataset.data[i], model.x, model.y + 5);//}else{
                  if ((dataset.data[i] != 2) && (dataset.data[i] != 0))
                  ctx.fillText(dataset.data[i], model.x, model.y );
              }
          });

         // document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }
       
    



    };

  });


  GetDataCSVUsoDinamicoGrupos.async().then(function (datos) {
    $scope.data_csv_12 =  [];
    $scope.data_csv_12_temp =  [];
    $scope.labels_csv_12 =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_12_temp.push(datos.data[i]['Grupo 1']);
    //$scope.data_csv_12_temp.push(datos.data[1]['Grupo 1']);
    //$scope.data_csv_12_temp.push(datos.data[2]['Grupo 1']);
    //$scope.data_csv_12_temp.push(datos[0].data[4][1]);
    //$scope.data_csv_12_temp.push(datos[0].data[5][1]);
    
    $scope.data_csv_12.push($scope.data_csv_12_temp);

    $scope.data_csv_12_temp =  [];

     for (i=0; i< datos.data.length; i++)
    $scope.data_csv_12_temp.push(datos.data[i]['Grupo 2']);
   // $scope.data_csv_12_temp.push(datos.data[1]['Grupo 2']);
   // $scope.data_csv_12_temp.push(datos.data[2]['Grupo 2']);
    //$scope.data_csv_12_temp.push(datos[0].data[4][2]);
    //$scope.data_csv_12_temp.push(datos[0].data[5][2]);

    $scope.data_csv_12.push($scope.data_csv_12_temp);

    $scope.data_csv_12_temp =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_12_temp.push(datos.data[i]['Grupo 3']);
   // $scope.data_csv_12_temp.push(datos.data[1]['Grupo 3']);
   // $scope.data_csv_12_temp.push(datos.data[2]['Grupo 3']);
    //$scope.data_csv_12_temp.push(datos[0].data[4][3]);
    //$scope.data_csv_12_temp.push(datos[0].data[5][3]);

    $scope.data_csv_12.push($scope.data_csv_12_temp);

    $scope.data_csv_12_temp =  [];
    for (i=0; i< datos.data.length; i++)
    $scope.data_csv_12_temp.push(datos.data[i]['Grupo 4']);
    //$scope.data_csv_12_temp.push(datos.data[1]['Grupo 4']);
    //$scope.data_csv_12_temp.push(datos.data[2]['Grupo 4']);
    //$scope.data_csv_12_temp.push(datos[0].data[4][4]);
    //$scope.data_csv_12_temp.push(datos[0].data[5][4]);

    $scope.data_csv_12.push($scope.data_csv_12_temp);


    $scope.data_csv_12_temp =  [];

   for (i=0; i< datos.data.length; i++)
     $scope.data_csv_12_temp.push(datos.data[i]['Target[>80%]']);
   // $scope.data_csv_12_temp.push(datos.data[1]['Target[>80%]']);
   // $scope.data_csv_12_temp.push(datos.data[2]['Target[>80%]']);
    //$scope.data_csv_12_temp.push(datos[0].data[4][4]);
    //$scope.data_csv_12_temp.push(datos[0].data[5][4]);

    $scope.data_csv_12.push($scope.data_csv_12_temp);

    //$scope.data_csv_12_temp.push(datos[0].data[1][6]);
    //$scope.data_csv_12_temp.push(datos[0].data[2][6]);
    //$scope.data_csv_12_temp.push(datos[0].data[3][6]);
    //$scope.data_csv_12_temp.push(datos[0].data[4][6]);
    //$scope.data_csv_12_temp.push(datos[0].data[5][6]);

    //$scope.data_csv_12.push($scope.data_csv_12_temp);
    //datos[0].data[1] ;
    for (i=0; i< datos.data.length; i++)
    $scope.labels_csv_12.push(datos.data[i]['Periodo']);
    //$scope.labels_csv_12.push(datos.data[1]['Periodo']);
    //$scope.labels_csv_12.push(datos.data[2]['Periodo']);
    //$scope.labels_csv_12.push(datos[0].data[4][0]);
    //$scope.labels_csv_12.push(datos[0].data[5][0]);
    //$scope.labels_csv_1.push(datos[0].data[4][0]);
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_12 = [
      {
        label: "Grupo 1",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: "Grupo 2",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Grupo 3",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Grupo 4",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false
      },

      {
        label: "Target[>80%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    $scope.color_csv_12 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_12 = {

      
      title: {
        display: true,
        text: " Uso Dinamico "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 0 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Dias',
            //fontColor: '#151515',
            display: true,
          }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Uso Dinamico [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  //beginAtZero: true
              }
        }]
      },

      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                 // if (dataset.data[i] == 0) {
                 // ctx.fillText(dataset.data[i], model.x, model.y - 5);}else{
                  if ((dataset.data[i] != 2) && (dataset.data[i] != 0))
                  ctx.fillText(dataset.data[i], model.x, model.y + 15);
              }
          });

         // document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
       
      }
       
    



    };

  });



 $scope.param1 = {
        fechaini: $scope.fechaIniPlm,
        fechater: $scope.fechaTerPlm,
       // flota: $scope.flota.id
      }

  GetDataEfectividadPLM.async($scope.param1).then(function (datos) {

     
    $scope.data_csv_13 =  [];
    $scope.data_csv_13_temp =  [];
    $scope.labels_csv_13 =  [];
    for (i=0;i < datos.data[0].length; i++)    
    $scope.data_csv_13_temp.push(datos.data[0][i].result);
    
    
    $scope.data_csv_13.push($scope.data_csv_13_temp);

   
    
    for (i=0;i < datos.data[0].length; i++)
       $scope.labels_csv_13.push(datos.data[0][i].truck);
    
    
   
    $scope.datasetOverride_csv_13 = [
      {
        label: " N° Cargas Pesometro / N° Cargas",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [<0.5%]",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

      var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    curr_month++;
    var curr_year = d.getFullYear();
     $scope.fecha_ = curr_date + "-" + curr_month + "-" + curr_year;

    $scope.color_csv_13 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_13 = {

      
      title: {
        display: true,
        text: " Pesometro por Camión "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 2 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString:  $scope.fecha_,
            //fontColor: '#151515',
            display: true,
          },
          ticks: {
                  
            display: true,
            
        }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Efectividad [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  
                  beginAtZero: true,
                  
              }
        }]
      },
       
    
      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  //if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                  //ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +15);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
    }



    };

  });




  GetDataIntegridadTurno.async().then(function (datos) {

     
    $scope.data_csv_14 =  [];
    $scope.data_csv_14_temp =  [];
    $scope.labels_csv_14 =  [];
    for (i=0;i < datos.data[0].length; i++)    
    $scope.data_csv_14_temp.push(datos.data[0][i].PercentGood);
    //$scope.data_csv_13_temp.push(datos[0].data[2][1]);
    //$scope.data_csv_13_temp.push(datos[0].data[3][1]);
    //$scope.data_csv_13_temp.push(datos[0].data[4][1]);
    //$scope.data_csv_13_temp.push(datos[0].data[5][1]);
    
    $scope.data_csv_14.push($scope.data_csv_14_temp);

    /*$scope.data_csv_2_temp =  [];

    $scope.data_csv_2_temp.push(datos[0].data[1][2]);
    $scope.data_csv_2_temp.push(datos[0].data[2][2]);
    $scope.data_csv_2_temp.push(datos[0].data[3][2]);
    $scope.data_csv_2_temp.push(datos[0].data[4][2]);
    $scope.data_csv_2_temp.push(datos[0].data[5][2]);*/
   
    //$scope.data_csv_13.push($scope.data_csv_2_temp);
    //datos[0].data[1] ;
    
    for (i=0;i < datos.data[0].length; i++)
       $scope.labels_csv_14.push(datos.data[0][i].fecha_turno);
    
    
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_14 = [
      {
        label: "Cicclos Correctos",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [<0.5%]",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    curr_month++;
    var curr_year = d.getFullYear();
     $scope.fecha_ = curr_date + "-" + curr_month + "-" + curr_year;

    $scope.color_csv_14 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_14 = {

      
      title: {
        display: true,
        text: " Integridad total por turno "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 2 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: $scope.fecha_,
            //fontColor: '#151515',
            display: true,
          },
          ticks: {
                  
            display: true,
            
        }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Integridad [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  
                  beginAtZero: true,
                  
              }
        }]
      },
       
    
      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  //if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                  //ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +15);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
    }



    };

  });



  GetDataIntegridadCamion.async().then(function (datos) {     
    $scope.data_csv_15 =  [];
    $scope.data_csv_15_temp =  [];
    $scope.labels_csv_15 =  [];
    for (i=0;i < datos.data[0].length; i++)    
    $scope.data_csv_15_temp.push(datos.data[0][i].result);    
    $scope.data_csv_15.push($scope.data_csv_15_temp);    
    for (i=0;i < datos.data[0].length; i++)
       $scope.labels_csv_15.push(datos.data[0][i].truck);    
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_15 = [
      {
        label: "Ciclos Correctos",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [<0.5%]",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

     var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    curr_month++;
    var curr_year = d.getFullYear();
     $scope.fecha_ = curr_date + "-" + curr_month + "-" + curr_year;

    $scope.color_csv_15 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_15 = {

      
      title: {
        display: true,
        text: " Integridad total por Camion "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 2 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: $scope.fecha_,
            //fontColor: '#151515',
            display: true,
          },
          ticks: {
                  
            display: true,
            
        }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Integridad [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  
                  beginAtZero: true,
                  
              }
        }]
      },
       
    
      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  //if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                  //ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +15);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
    }



    };

  });



  GetDataIntegridadPala.async().then(function (datos) {     
    $scope.data_csv_16 =  [];
    $scope.data_csv_16_temp =  [];
    $scope.labels_csv_16 =  [];
    for (i=0;i < datos.data[0].length; i++)    
    $scope.data_csv_16_temp.push(datos.data[0][i].result);    
    $scope.data_csv_16.push($scope.data_csv_16_temp);    
    for (i=0;i < datos.data[0].length; i++)
       $scope.labels_csv_16.push(datos.data[0][i].excav);    
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_16 = [
      {
        label: " Ciclos Correctos",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [<0.5%]",
        borderWidth: 1,
        
        type: 'bar',
        lineTension: 0,
        fill: false,
        borderDash: [5, 5]
      }
    ];

    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    curr_month++;
    var curr_year = d.getFullYear();
     $scope.fecha_ = curr_date + "-" + curr_month + "-" + curr_year;

    $scope.color_csv_16 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_16 = {

      
      title: {
        display: true,
        text: " Integridad total por Pala "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 2 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: $scope.fecha_,
            //fontColor: '#151515',
            display: true,
          },
          ticks: {
                  
            display: true,
            
        }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Integridad [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  
                  beginAtZero: true,
                  
              }
        }]
      },
       
    
      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  //console.log(dataset.data[i]);
                  //if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                  //ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +15);
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
    }



    };

  });



  GetDataIntegridadOperador.async().then(function (datos) {     
    $scope.data_csv_17 =  [];
    $scope.data_csv_17_temp =  [];
    $scope.labels_csv_17 =  [];
    for (i=0;i < datos.data[0].length; i++)    
    $scope.data_csv_17_temp.push(datos.data[0][i].result);    
    $scope.data_csv_17.push($scope.data_csv_17_temp);    
    for (i=0;i < datos.data[0].length; i++)
       $scope.labels_csv_17.push(datos.data[0][i].operator);    
    //console.log( $scope.labels_csv_1);
    $scope.datasetOverride_csv_17 = [
      {
        label: "Ciclos correctos",
        borderWidth: 1,
        type: 'bar',
        lineTension: 0,
        fill: false
      },
      {
        label: "Target [<0.5%]",
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false
      }
    ];

    $scope.color_csv_17 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_17 = {

      
      title: {
        display: true,
        text: " Integridad total por Operador "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 2 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Operador',
            //fontColor: '#151515',
            display: true,
          },
          ticks: {
            autoSkip: false,      
            display: true,
            
        }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Integridad [%] ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  
                  beginAtZero: true,
                  
              }
        }]
      },
       
    
      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
    }

    };

  });

  var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    curr_month++;
    var mes_ = ''
     if (curr_month<10){
       mes_= '0'+curr_month;
     }else{
       mes_= curr_month;
     }

     if (curr_date<10){
       dia_= '0'+curr_date;
     }else{
       dia_= curr_date;
     }

     

    var curr_year = d.getFullYear();
     $scope.fecha_ =curr_year  + "-" + mes_ + "-" + dia_;
  console.log($scope.fecha_);
  $scope.param1 = {
        fechaini: $scope.fecha_,
        fechater: $scope.fecha_,
       // flota: $scope.flota.id
      }


   $scope.gridEquipos = {};

   GetDataInfluxDB.async($scope.param1).then(function (datos) { 
    $scope.datagridequipos = datos;
      
    $scope.gridEquipos = {
      data: 'datagridequipos',
      columnDefs:[{name:'IpSource',displayName : 'IP Adress'},
                 {name:'DeviceMonitorVersion',displayName:'DM Version'},
                  {name:'DispatchLiteVersion',displayName:'Dispatch Lite'},
                  {name:'DispatchVersion',displayName:'Dispatch Version'},
                   {name:'ProvisionVersion',displayName:'Provision Version'},
                   {name:'ImageVersion',displayName:'Image Version'},
                    {name:'OemDriversVersion',displayName:'OEM Drivers'},
                     {name:'OemInterfacesVersion',displayName:'OEM Interface Drivers'},
                     
                       {name:'SerialNumber',displayName:'Serial Number'},
                       {name:'MinecareVersion',displayName:'Minecare Version'},
                       {name:'Platform',displayName:'Platform'},
                  ]
    };
    $scope.gridEquipos.onRegisterApi = function(gridApi){
      $scope.gridApi = gridApi;
    }
  });


   GetDataInfluxDBFreezer.async($scope.param1).then(function (datos) {

      $scope.data_free = [];
      $scope.labels_free = [];

       $scope.data_free = datos.data[1].data2;
      $scope.labels_free = datos.data[0].labels;

      
      
     // $scope.data_queuetime = [];
     // $scope.data_hangtime = [];

      $scope.colors_free = ['#008000', '#ff0000'];
      $scope.series_free =  ['Congelamiento', 'Reinicio Aplicacion'];
      

      $scope.options_free = {

      
      title: {
        display: true,
        text: " Congelamiento de PTX "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 2 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: 'Equipos',
            //fontColor: '#151515',
            display: true,
          },
          ticks: {
            autoSkip: false,      
            display: true,
            
        }
        }
        ],
        yAxes: [{
          stacked: true,
          scaleLabel: {
            labelString: ' Cantidad ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  
                  beginAtZero: true,
                  
              }
        }]
      },
       
    
      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
    }

    };


    });
  



  GetDataToneladasPalaHora.async().then(function (datos) {     
    $scope.data_csv_18 =  [];
    $scope.data_csv_18_temp =  [];
    $scope.labels_csv_18 =  [];
    
    $scope.data_csv_18_temp_2 =  [];
    $scope.data_csv_18_temp_2 = Object.values(datos.data[0][0]);   
    for (i=2;i < $scope.data_csv_18_temp_2.length; i++)   
    $scope.data_csv_18_temp.push($scope.data_csv_18_temp_2[i]);    
    $scope.data_csv_18.push($scope.data_csv_18_temp);

    $scope.data_csv_18_temp_2 =  [];
    $scope.data_csv_18_temp =  [];
    $scope.data_csv_18_temp_2 = Object.values(datos.data[0][1]);   
    for (i=2;i < $scope.data_csv_18_temp_2.length; i++)   
    $scope.data_csv_18_temp.push($scope.data_csv_18_temp_2[i]);    
    $scope.data_csv_18.push($scope.data_csv_18_temp);

    $scope.labels_csv_18 =  []; 
    $scope.labels_csv_18_temp = Object.keys(datos.data[0][0]);

    for (i=2;i < $scope.labels_csv_18_temp.length; i++)
       $scope.labels_csv_18.push($scope.labels_csv_18_temp[i]); 
       


    $scope.datasetOverride_csv_18 = [
      {
        label: 'Pala ' + datos.data[0][0].ExcavName,
        borderWidth: 1,
        type: 'line',
        lineTension: 0,
        fill: false
      },
      {
        label: 'Pala ' + datos.data[0][1].ExcavName,
        borderWidth: 1,
        
        type: 'line',
        lineTension: 0,
        fill: false
      }
    ];

    $scope.color_csv_18 =  ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];

    $scope.options_csv_18 = {

      
      title: {
        display: true,
        text: " Toneladas de Pala por hora. "
        // fontColor: '#f9fdff'
      },

      elements: { point: { radius: 2 } },
  
      legend: { display: true, fontColor: '#fcfeff' },
  
      tooltips: {
        display: true,
        mode: 'label',
        titleFontSize: 12
  
      },
      responsive: true,
      //maintainAspectRatio: false,
  
      scales: {
        xAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: 'Turno',
            //fontColor: '#151515',
            display: true,
          },
          ticks: {
            autoSkip: false,      
            display: true,
            
        }
        }
        ],
        yAxes: [{
          stacked: false,
          scaleLabel: {
            labelString: ' Tons ',
            // fontColor: '#151515',
            display: true,
          },
          ticks: {
                  
                  beginAtZero: true,
                  
              }
        }]
      },
       
    
      animation: {
        duration: 1,
        onComplete: function (animation) {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            ctx.font = "bold 7pt Arial";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#000000";
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  
              }
          });

          document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
        }
    }

    };

  });

  //initializeChart('Lomas Bayas','mlb'); //pavez 
   var initializeChart = function (cliente, id_cliente) {

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }



    /*GetDataReport1.async($scope.param1).then(function (datos) {


      $scope.alldata = {};
      $scope.data1 = [];
      $scope.series = [];
      $scope.labels = [];

      if (datos === 0) {
        $scope.dataall = $rootScope.report1;
      } else {
        $scope.dataall = datos;
        $rootScope.report1 = datos;
      }

      $scope.labels = $scope.dataall.data[0].labels;

      $scope.data = $scope.dataall.data[1].data2;

    });*/














    

  

    GetDataFlotaReport.async($scope.param1).then(function (datos_flota) {
       
      $rootScope.flota = datos_flota.data;
       
      
    // console.log(datos_flota.data[2].qualif);
    
    


    $scope.creados = [];
    $scope.turnos = 2;
    if ((id_cliente == 'cmh') || (id_cliente=='mlc')){
      $scope.turnos = 3;
    }

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente,
      turnos: $scope.turnos
    }

    GetDataChart1Montly.async($scope.param1).then(function (datos) {
      $scope.data_dirty_1 = [];
      $scope.labels_dirty_1 = [];
      $scope.colors_dirty_1 = ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];
      angular.forEach(datos.data, function (value, key) {
        this.push(value.Turno);        
      }, $scope.labels_dirty_1);
      $scope.data1 = [];
      $scope.data2 = [];
      $scope.data3 = [];
      $scope.data4 = [];
      angular.forEach(datos.data, function (value, key) {
        this.push(value.Grupo1);        
      }, $scope.data1);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Grupo2);        
      }, $scope.data2);
      angular.forEach(datos.data, function (value, key) {
        this.push(value.Grupo3);        
      }, $scope.data3);
      angular.forEach(datos.data, function (value, key) {
        this.push(value.Grupo4);        
      }, $scope.data4);

      
      $scope.data_dirty_1.push($scope.data1);
      $scope.data_dirty_1.push($scope.data2);
      $scope.data_dirty_1.push($scope.data3);
      $scope.data_dirty_1.push($scope.data4);
     
      $scope.series_dirty_1 = ['1','2','3','4'];

      $scope.series_dirty_1_ = ['1','2','3','4'];

       $scope.options_dirty_1 = {
        title: {
          display: true,
          text: "Movimiento Mina promedio por Hora - Ultimas 3 Semanas"
    
        },
          
        elements: { point: { radius: 0 } },

        legend: { display: true, fontColor: '#fcfeff' },
        tooltips: {
          display: true,
          mode: 'label',
          titleFontSize: 12
        },
        responsive: true,
        scales: {
          xAxes: [{
            stacked: false,
            scaleLabel: {
              labelString: 'Hora',
              display: true,
            }
          }
          ],
          yAxes: [{
            stacked: false,
            scaleLabel: {
              labelString: 'Tons',
              display: true,
            },
            ticks: {
              beginAtZero: true
            }
          }]
        },
        animation: {
          onComplete: function (animation) {
            document.getElementById("chart25").value = animation.chartInstance.toBase64Image();           
          }
        }
    
      };


      $scope.datasetOverride_dirty_1 = [
        {
          label: "Grupo 1",
          borderWidth: 1,
          type: 'line',
          fill: false,
          lineTension: 0
        },
        {
          label: "Grupo 2",
          borderWidth: 1,
          
          type: 'line',
          fill: false,
          lineTension: 0
        },
        {
          label: "Grupo 3",
          borderWidth: 1,        
          type: 'line',
          fill: false,
          lineTension: 0
        },
        {
          label: "Grupo 4",
          borderWidth: 1,         
          type: 'line',
          fill: false,
          lineTension: 0
        }
      ];
    });


    

    $scope.param1 = {
      cliente: id_cliente,
      id_cliente: id_cliente,
      flota : datos_flota.data[0].qualif
    }



    GetDataChart2Montly.async($scope.param1).then(function (datos) {

      $scope.data_dirty_2 = [];
      $scope.labels_dirty_2 = [];

      $scope.data_dirty_2 = datos.data[1].data2;
      $scope.labels_dirty_2 = datos.data[0].labels;

      
      
      $scope.data_queuetime = [];
      $scope.data_hangtime = [];

      $scope.colors_dirty_2 = ['#008000', '#ff0000'];
      $scope.series_dirty_2 =  ['Tiempo de Cola CAEX', 'Tiempo Espera de Pala'];
      //$scope.datasetOverride_dirty_2 = {};

      $scope.options_dirty_2 = {

        

        title: {
          display: true,
          text: "Pérdidas Operacionales por Grupo - " +datos_flota.data[0].qualif
          // fontColor: '#f9fdff'
        },
    
        legend: { display: true, fontColor: '#fcfeff' },
    
        tooltips: {
          display: true,
          mode: 'label',
          titleFontSize: 12
    
        },
        responsive: true,
    
        scales: {
          xAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Grupos',
              //fontColor: '#151515',
              display: true,
            }
          }
          ],
          yAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Perdidas Operacionales [min]',
              // fontColor: '#151515',
              display: true,
            },
            ticks: {
              //callback: function(value, index, values) {
              //    return parseFloat(value).toFixed(2);
              //},
              min: 0,
              max: 8,
              stepSize: 1
                }
          }]
        },
        animation: {
          duration: 1,
          onComplete: function (animation) {
              var chartInstance = this.chart,
                  ctx = chartInstance.ctx;
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
              ctx.textAlign = 'center';
              ctx.textBaseline = 'bottom';
              ctx.fillStyle = "#000000";
              this.data.datasets.forEach(function (dataset) {
                for (var i = 0; i < dataset.data.length; i++) {
                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                    //console.log(model);
                    //if (dataset.data[i] != 80)
                    ctx.fillText(dataset.data[i], model.x, model.y + 15);
                }
            });
            document.getElementById("chart23").value = animation.chartInstance.toBase64Image();      
          }
      }
      };




    });

   

    $scope.param1 = {
      cliente: id_cliente,
      id_cliente: id_cliente,
      flota : datos_flota.data[1].qualif
    };


    GetDataChart2Montly.async($scope.param1).then(function (datos) {

      $scope.data_dirty_3 = [];
      $scope.labels_dirty_3 = [];

      $scope.data_dirty_3 = datos.data[1].data2;
      $scope.labels_dirty_3 = datos.data[0].labels;

      
      
      $scope.data_queuetime = [];
      $scope.data_hangtime = [];

      $scope.colors_dirty_3 = ['#008000', '#ff0000'];
      $scope.series_dirty_3 =  ['Tiempo de Cola CAEX', 'Tiempo Espera de Pala'];
      

      $scope.options_dirty_3 = {


        title: {
          display: true,
          text: "Pérdidas Operacionales por Grupo - " +  datos_flota.data[1].qualif
          // fontColor: '#f9fdff'
        },
    
        legend: { display: true, fontColor: '#fcfeff' },
    
        tooltips: {
          display: true,
          mode: 'label',
          titleFontSize: 12
    
        },
        responsive: true,
    
        scales: {
          xAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Grupos',
              //fontColor: '#151515',
              display: true,
            }
          }
          ],
          yAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Perdidas Operacionales [min]',
              // fontColor: '#151515',
              display: true,
            },
            ticks: {
              //callback: function(value, index, values) {
              //    return parseFloat(value).toFixed(2);
              //},
              min: 0,
              max: 8,
              stepSize: 1
                }
          }]
        },

        animation: {
          duration: 1,
          onComplete: function (animation) {
              var chartInstance = this.chart,
                  ctx = chartInstance.ctx;
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
              ctx.textAlign = 'center';
              ctx.textBaseline = 'bottom';
              ctx.fillStyle = "#000000";
              this.data.datasets.forEach(function (dataset) {
                for (var i = 0; i < dataset.data.length; i++) {
                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                    //console.log(model);
                    //if (dataset.data[i] != 80)
                    ctx.fillText(dataset.data[i], model.x, model.y + 15);
                }
            });
            document.getElementById("chart24").value = animation.chartInstance.toBase64Image();      
          }
      }
      };




    });

    if ( datos_flota.data.length > 2) {

    $scope.param1 = {
      cliente: id_cliente,
      id_cliente: id_cliente,
      flota : datos_flota.data[2].qualif
    };

  


    GetDataChart2Montly.async($scope.param1).then(function (datos) {

      $scope.data_dirty_4 = [];
      $scope.labels_dirty_4 = [];

      $scope.data_dirty_4 = datos.data[1].data2;
      $scope.labels_dirty_4 = datos.data[0].labels;

      
      
      $scope.data_queuetime = [];
      $scope.data_hangtime = [];

      
      $scope.colors_dirty_4 = ['#008000', '#ff0000'];
      $scope.series_dirty_4 =  ['Tiempo de Cola CAEX', 'Tiempo Espera de Pala'];
      $scope.options_dirty_4 = {


        title: {
          display: true,
          text: "Pérdidas Operacionales por Grupo - "+datos_flota.data[2].qualif
          // fontColor: '#f9fdff'
        },
    
        legend: { display: true, fontColor: '#fcfeff' },
    
        tooltips: {
          display: true,
          mode: 'label',
          titleFontSize: 12
    
        },
        responsive: true,
    
        scales: {
          xAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Grupos',
              //fontColor: '#151515',
              display: true,
            }
          }
          ],
          yAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Perdidas Operacionales [min]',
              // fontColor: '#151515',
              display: true,
            },
            ticks: {
              //callback: function(value, index, values) {
              //    return parseFloat(value).toFixed(2);
              //},
              min: 0,
              max: 8,
              stepSize: 1
                }
          }]
        },

        animation: {
          duration: 1,
          onComplete: function (animation) {
              var chartInstance = this.chart,
                  ctx = chartInstance.ctx;
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
              ctx.textAlign = 'center';
              ctx.textBaseline = 'bottom';
              ctx.fillStyle = "#000000";
              this.data.datasets.forEach(function (dataset) {
                for (var i = 0; i < dataset.data.length; i++) {
                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                    //console.log(model);
                    //if (dataset.data[i] != 80)
                    ctx.fillText(dataset.data[i], model.x, model.y + 15);
                }
            });
            document.getElementById("chart21").value = animation.chartInstance.toBase64Image();      
          }
      }
      };




    });

    }

    if ( datos_flota.data.length > 3) {

    $scope.param1 = {
      cliente: id_cliente,
      id_cliente: id_cliente,
      flota : datos_flota.data[3].qualif
    };


    GetDataChart2Montly.async($scope.param1).then(function (datos) {

      $scope.data_dirty_5 = [];
      $scope.labels_dirty_5 = [];

      $scope.data_dirty_5 = datos.data[1].data2;
      $scope.labels_dirty_5 = datos.data[0].labels;

      
      
      $scope.data_queuetime = [];
      $scope.data_hangtime = [];

      $scope.colors_dirty_5 = ['#008000', '#ff0000'];
      $scope.series_dirty_5 = ['Tiempo de Cola CAEX', 'Tiempo Espera de Pala'];
      

      $scope.options_dirty_5 = {
        title: {
          display: true,
          text: "Pérdidas Operacionales por Grupo - "+datos_flota.data[3].qualif
          // fontColor: '#f9fdff'
        },
    
        legend: { display: true, fontColor: '#fcfeff' },
    
        tooltips: {
          display: true,
          mode: 'label',
          titleFontSize: 12
    
        },
        responsive: true,
    
        scales: {
          xAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Grupos',
              //fontColor: '#151515',
              display: true,
            }
          }
          ],
          yAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Perdidas Operacionales [min]',
              // fontColor: '#151515',
              display: true,
            },
            ticks: {
              //callback: function(value, index, values) {
              //    return parseFloat(value).toFixed(2);
              //},
              min: 0,
              max: 8,
              stepSize: 1
                }
          }]
        },

        animation: {
          duration: 1,
          onComplete: function (animation) {
              var chartInstance = this.chart,
                  ctx = chartInstance.ctx;
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
              ctx.textAlign = 'center';
              ctx.textBaseline = 'bottom';
              ctx.fillStyle = "#000000";
              this.data.datasets.forEach(function (dataset) {
                for (var i = 0; i < dataset.data.length; i++) {
                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                    //console.log(model);
                    if (dataset.data[i] != 80)
                    ctx.fillText(dataset.data[i], model.x, model.y + 15);
                }
            });
            document.getElementById("chart22").value = animation.chartInstance.toBase64Image();      
          }
      }
      }

    }); 

     }

     

      $scope.param1 = {
        cliente: id_cliente,
        id_cliente: id_cliente,
        flota : datos_flota.data[0].qualif
      };

      





      GetDataChartMontlyScatter.async($scope.param1).then(function (datos) {   
      
      $scope.data_dirty_6 = datos.data[1].data2[0];     

      $scope.colors_dirty_6 = [];
      for (i=0; i< 100 ; i++)
         $scope.colors_dirty_6.push('#0000ff'); 

      $scope.options_dirty_6 ={
        title: {
          display: true,
          text: "Distribución Pérdidas Operacionales - "+ datos_flota.data[0].qualif
          // fontColor: '#f9fdff'
        },
    
        legend: false,
    
        tooltips: false,

        annotation: {
          annotations: [{
             type: 'box',
             drawTime: 'beforeDatasetsDraw',
             xScaleID: 'x-axis-0',
             yScaleID: 'y-axis-0',
             xMin: 0,
             xMax: 2,
             yMin: 0,
             yMax: 2,
             backgroundColor: 'rgba(20, 20, 20, 0.2)'
          }]
       },
        responsive: true,
    
        scales: {
          xAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Promedio Espera Pala [min]',
              //fontColor: '#151515',
              display: true,
            },
            ticks: {
           //   callback: function(value, index, values) {
           //       return parseFloat(value).toFixed(2);
            //  },
              min: 0,
              max: 8,
              stepSize: 1
          }
          }
          ],
          yAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Promedio Cola CAEX [min]',
              // fontColor: '#151515',
              display: true,
            },
            ticks: {
              //callback: function(value, index, values) {
              //    return parseFloat(value).toFixed(2);
              //},
              min: 0,
              max: 8,
              stepSize: 1
          }
          }]
        },
        animation: {
          onComplete: function (animation) {
            document.getElementById("chart19").value = animation.chartInstance.toBase64Image();           
          }
        }
      };

        $scope.datasetOverride_dirty_6 = [
          {
            backgroundColor:"#ff6384",
          hoverBackgroundColor: "#ff6384"
          },
          {
            backgroundColor:"#ff6384",
            hoverBackgroundColor: "#ff6384"
          },
          {
            backgroundColor:"#ff6384",
            hoverBackgroundColor: "#ff6384"
          },
          {
            backgroundColor:"#ff6384",
            hoverBackgroundColor: "#ff6384"
          }
        ];
      
        

        });


        $scope.param1 = {
          cliente: id_cliente,
          id_cliente: id_cliente,
          flota : datos_flota.data[1].qualif
        };
  
        GetDataChartMontlyScatter.async($scope.param1).then(function (datos) {  
          
          $scope.colors_dirty_7 = [];
          for (i = 0; i < 200; i++)
            $scope.colors_dirty_7.push('#0000ff');
          $scope.data_dirty_7 = datos.data[1].data2[0];     
  
        $scope.options_dirty_7 ={
          title: {
            display: true,
            text: "Distribución Pérdidas Operacionales - " + datos_flota.data[1].qualif
            // fontColor: '#f9fdff'
          },
      
          legend: false,
        
          tooltips: false,

          annotation: {
            annotations: [{
               type: 'box',
               drawTime: 'beforeDatasetsDraw',
               xScaleID: 'x-axis-0',
               yScaleID: 'y-axis-0',
               xMin: 0,
               xMax: 2,
               yMin: 0,
               yMax: 2,
               backgroundColor: 'rgba(20, 20, 20, 0.2)'
            }]
         },

          responsive: true,
      
          scales: {
            xAxes: [{
              stacked: true,
              scaleLabel: {
                labelString: 'Promedio Espera Pala [min]',
                //fontColor: '#151515',
                display: true,
              },
              ticks: {
               // callback: function(value, index, values) {
               //     return parseFloat(value).toFixed(2);
               // },
                min: 0,
              max: 8,
              stepSize: 1
            }
            }
            ],
            yAxes: [{
              stacked: true,
              scaleLabel: {
                labelString: 'Promedio Cola CAEX [min]',
                // fontColor: '#151515',
                display: true,
              },
              ticks: {
                //callback: function(value, index, values) {
                //    return parseFloat(value).toFixed(2);
                //},
                min: 0,
              max: 8,
              stepSize: 1
            }
            }]
          },
          animation: {
            onComplete: function (animation) {
              document.getElementById("chart20").value = animation.chartInstance.toBase64Image();           
            }
          }
        };
  
          $scope.datasetOverride_dirty_7 = [
            {
              backgroundColor:"#ff6384",
            hoverBackgroundColor: "#ff6384"
            },
            {
              backgroundColor:"#ff6384",
              hoverBackgroundColor: "#ff6384"
            },
            {
              backgroundColor:"#ff6384",
              hoverBackgroundColor: "#ff6384"
            },
            {
              backgroundColor:"#ff6384",
              hoverBackgroundColor: "#ff6384"
            }
          ];
        
          
  
          });

          if ( datos_flota.data.length > 2) { 

          $scope.param1 = {
            cliente: id_cliente,
            id_cliente: id_cliente,
            flota : datos_flota.data[2].qualif
          };
    
          GetDataChartMontlyScatter.async($scope.param1).then(function (datos) {   
          
          $scope.data_dirty_8 = datos.data[1].data2[0];     
          $scope.colors_dirty_8 = [];
          for (i = 0; i < 200; i++)
            $scope.colors_dirty_8.push('#0000ff');
          $scope.options_dirty_8 ={
            title: {
              display: true,
              text: "Distribución Pérdidas Operacionales - " +  datos_flota.data[2].qualif
              // fontColor: '#f9fdff'
            },
        
            legend: false,
      
            tooltips: false,

            annotation: {
              annotations: [{
                 type: 'box',
                 drawTime: 'beforeDatasetsDraw',
                 xScaleID: 'x-axis-0',
                 yScaleID: 'y-axis-0',
                 xMin: 0,
                 xMax: 2,
                 yMin: 0,
                 yMax: 2,
                 backgroundColor: 'rgba(20, 20, 20, 0.2)'
              }]
           },
        
           
            responsive: true,
        
            scales: {
              xAxes: [{
                stacked: true,
                scaleLabel: {
                  labelString: 'Promedio Espera Pala [min]',
                  //fontColor: '#151515',
                  display: true,
                },

                ticks: {
                 // callback: function(value, index, values) {
                 //     return parseFloat(value).toFixed(2);
                 // },
                  min: 0,
              max: 8,
              stepSize: 1
              }
              }
              
              
              ],
              yAxes: [{
                stacked: true,
                scaleLabel: {
                  labelString: 'Promedio Cola CAEX [min]',
                  // fontColor: '#151515',
                  display: true,
                },
                ticks: {
                 // callback: function(value, index, values) {
                 //     return parseFloat(value).toFixed(2);
                 // },
                  min: 0,
              max: 8,
              stepSize: 1
              }
              }]
            },

            animation: {
              onComplete: function (animation) {
                document.getElementById("chart17").value = animation.chartInstance.toBase64Image();           
              }
            }
          };
    
            $scope.datasetOverride_dirty_8 = [
              {
                backgroundColor:"#ff6384",
              hoverBackgroundColor: "#ff6384",
              displayLegend:false
              },
              {
                backgroundColor:"#ff6384",
                hoverBackgroundColor: "#ff6384",
                displayLegend:false
              },
              {
                backgroundColor:"#ff6384",
                hoverBackgroundColor: "#ff6384",
                displayLegend:false
              },
              {
                backgroundColor:"#ff6384",
                hoverBackgroundColor: "#ff6384"
              }
            ];
          
            
    
            });

          }

          if ( datos_flota.data.length > 3) {           

            $scope.param1 = {
              cliente: id_cliente,
              id_cliente: id_cliente,
              flota : datos_flota.data[3].qualif
            };
      
            GetDataChartMontlyScatter.async($scope.param1).then(function (datos) {   
            
            $scope.data_dirty_9 = datos.data[1].data2[0];     
            $scope.colors_dirty_9 = [];
            for (i = 0; i < 200; i++)
              $scope.colors_dirty_9.push('#0000ff');
            $scope.options_dirty_9 ={
              title: {
                display: true,
                text: "Distribución Pérdidas Operacionales - " +datos_flota.data[3].qualif
                // fontColor: '#f9fdff'
              },
          
              legend: true,
        
              tooltips: {
                mode: 'index',
                intersect: true
              },
              annotation: {
                annotations: [{
                   type: 'box',
                   drawTime: 'beforeDatasetsDraw',
                   xScaleID: 'x-axis-0',
                   yScaleID: 'y-axis-0',
                   xMin: 0,
                   xMax: 2,
                   yMin: 0,
                   yMax: 2,
                   backgroundColor: 'rgba(20, 20, 20, 0.2)'
                }]
             },
          
             
              responsive: true,
          
              scales: {
                xAxes: [{
                  stacked: true,
                  scaleLabel: {
                    labelString: 'Promedio Espera Pala [min]',
                    //fontColor: '#151515',
                    display: true,
                  },
                  ticks: {
                    //callback: function(value, index, values) {
                   //     return parseFloat(value).toFixed(2);
                   // },
                    min: 0,
              max: 8,
              stepSize: 1
                }
                }
                ],
                yAxes: [{
                  stacked: true,
                  scaleLabel: {
                    labelString: 'Promedio Cola CAEX [min]',
                    // fontColor: '#151515',
                    display: true,
                  },
                  ticks: {
                    //callback: function(value, index, values) {
                   //     return parseFloat(value).toFixed(2);
                   // },
                    min: 0,
              max: 8,
              stepSize: 1
                }
                }]
              },

           
              
              animation: {
                onComplete: function (animation) {
                  document.getElementById("chart18").value = animation.chartInstance.toBase64Image();           
                }
              }
            };
      
              $scope.datasetOverride_dirty_9 = [{
                label: "My First dataset",
                backgroundColor: "#ff6384"},{
                  label: "My First dataset",
                  backgroundColor: "#ff6384"},{
                    label: "My First dataset",
                    backgroundColor: "#ff6384"},{
                      label: "My First dataset",
                      backgroundColor: "#ff6384"},{
                        label: "My First dataset",
                        backgroundColor: "#ff6384"},{
                          label: "My First dataset",
                          backgroundColor: "#ff6384"},{
                            label: "My First dataset",
                            backgroundColor: "#ff6384"},{
                              label: "My First dataset",
                              backgroundColor: "#ff6384"},{
                                label: "My First dataset",
                                backgroundColor: "#ff6384"},{
                                  label: "My First dataset",
                                  backgroundColor: "#ff6384"},{
                                    label: "My First dataset",
                                    backgroundColor: "#ff6384"},{
                                      label: "My First dataset",
                                      backgroundColor: "#ff6384"},{
                                        label: "My First dataset",
                                        backgroundColor: "#ff6384"},{
                                          label: "My First dataset",
                                          backgroundColor: "#ff6384"},{
                                            label: "My First dataset",
                                            backgroundColor: "#ff6384"},{
                                              label: "My First dataset",
                                              backgroundColor: "#ff6384"},{
                                                label: "My First dataset",
                                                backgroundColor: "#ff6384"}];
            
              
      
              });

              }

                  

              GetDataChart3Montly.async($scope.param1).then(function (datos) { 

                $scope.data_dirty_10 = datos.data[1].data2;  
                $scope.labels_dirty_10 = datos.data[0].labels;
                $scope.colors_dirty_10 = ['#0000ff', '#008000', '#ff0000', '#808080', '#ff0000'];
                $scope.datasetOverride_dirty_10 = [
                  {
                    label: "Grupo 1",
                    borderWidth: 1,
                    type: 'bar'
                  },
                  {
                    label: "Grupo 2",
                    borderWidth: 1,
                    type: 'bar'
                  },
                  {
                    label: "Grupo 3",
                    borderWidth: 1,
                    type: 'bar'
                  },
                  {
                    label: "Grupo 4",
                    borderWidth: 1,
                    type: 'bar'
                  },
                  //{
                  //  label: "Tendencia",
                    //borderWidth: 1,
                    //borderDash: [5, 5],
                  //  pointBorderWidth: 1,
                    
                  //  type: 'line',
                  //  fill: false
                  //},
                  {
                    label: "Target[>80%]",
                    borderWidth: 1,
                    //pointBorderWidth: 1,
                    borderDash: [5, 5],
                    //hoverBackgroundColor: "rgba(0, 0, 0, 0)",
                    //hoverBorderColor: "rgba(0, 0, 0, 0)",
                    type: 'line',
                    fill: false
                  }
                ];

                $scope.options_dirty_10 = {


                  title: {
                    display: true,
                    text: " Uso Dinamico por Grupo "
                    // fontColor: '#f9fdff'
                  },
              
                  legend: { display: true, fontColor: '#fcfeff' },
              
                  tooltips: {
                    display: true,
                    mode: 'label',
                    titleFontSize: 12
              
                  },
                  responsive: true,
                  //maintainAspectRatio: false,
              
                  scales: {
                    xAxes: [{
                      stacked: false,
                      scaleLabel: {
                        labelString: 'Mes',
                        //fontColor: '#151515',
                        display: true,
                      }
                    }
                    ],
                    yAxes: [{
                      stacked: false,
                      scaleLabel: {
                        labelString: ' Uso Dinamico [%] ',
                        // fontColor: '#151515',
                        display: true,
                      },
                      ticks: {
                              beginAtZero: true
                          }
                    }]
                  },
                  animation: {
                    duration: 1,
                    onComplete: function (animation) {
                        //var chartInstance = this.chart,
                        //    ctx = chartInstance.ctx;
                        //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        //ctx.font="9.5px Arial";
                        //ctx.textAlign = 'center';
                        //ctx.textBaseline = 'bottom';
                        //ctx.fillStyle = "#0000ff";
                        //this.data.datasets.forEach(function (dataset) {
                        //  for (var i = 0; i < dataset.data.length; i++) {
                        //      var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                              //console.log(model);
                        //      if (dataset.data[i] != 80)
                         //     ctx.fillText(dataset.data[i], model.x, model.y + 15);
                         // }
                     // });
                      document.getElementById("chart16").value = animation.chartInstance.toBase64Image();
                    }
                }
                };
              

              });

              GetDataChart4Montly.async($scope.param1).then(function (datos) {

                $scope.data_dirty_11 = datos.data[1].data2;  
                $scope.labels_dirty_11 = datos.data[0].labels;
                $scope.colors_dirty_11 = ['#0000ff', '#ff0000'];

                $scope.datasetOverride_dirty_11 = [
                  {
                    label: "Uso Dinamico [%]",
                    borderWidth: 1,
                    type: 'bar'
                  },
                  {
                    label: "Taget [>80%]",
                    borderWidth: 1,
                    //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
                    //hoverBorderColor: "rgba(255, 0, 0, 1)",
                    type: 'line',
                    fill: false
                  }
                ];

                $scope.options_dirty_11 = {


                  title: {
                    display: true,
                    text: " BenchMark Uso Dinamico  "
                    // fontColor: '#f9fdff'
                  },
              
                  legend: { display: true, fontColor: '#fcfeff' },
              
                  tooltips: {
                    display: true,
                    mode: 'label',
                    titleFontSize: 12
              
                  },
                  responsive: true,
                  //maintainAspectRatio: false,
              
                  scales: {
                    xAxes: [{
                      stacked: false,
                      scaleLabel: {
                        labelString: 'Clientes Performance Assurance',
                        //fontColor: '#151515',
                        display: true,
                      }
                    }
                    ],
                    yAxes: [{
                      stacked: false,
                      scaleLabel: {
                        labelString: ' Uso Dinamico [%] ',
                        // fontColor: '#151515',
                        display: true,
                      },
                      ticks: {
                              beginAtZero: true
                          }
                    }]
                  },

                  animation: {
                    duration: 1,
                    onComplete: function (animation) {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = "#0000ff";
                        this.data.datasets.forEach(function (dataset) {
                          for (var i = 0; i < dataset.data.length; i++) {
                              var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                              if ((dataset.data[i] != 80) && (dataset.data[i] !=0))
                              ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +20);
                          }
                      });
                      document.getElementById("chart15").value = animation.chartInstance.toBase64Image();
                    }
                }
                };
              

              });


              GetDataIAuditor.async($scope.param1).then(function (datos) {
                 console.log("PASO POR AQUI");
                //$scope.data_iauditor_man_1 = datos.data[1].data2;  
                //$scope.data_iauditor_man_1 = [[3,6,8,8],[6,9,0,5],[5,5,6,7]];

                $scope.data_iauditor_man_1 = [];
                $scope.data_iauditor_man_2 = [];
                $scope.data_iauditor_falla = [];



               // $scope.labels_iauditor_man_1 = $filter("filter")(datos, { Value : { Client: "Reporte Incidente o falla  MLB", Group: "Grupo 3" }}).length;
               // console.log($scope.labels_iauditor_man_1);

                $scope.dataejemplo = [];             
                
                 $scope.labels_iauditor_man_1 = [];
                  $scope.labels_iauditor_man_2 = [];
                  $scope.labels_iauditor_falla= [];
                  $scope.data_man = [];
                  $scope.data_man_2 = [];

                  $scope.data_man_3 = [];
                  $scope.data_man_4 = [];

                   $scope.data_falla1 = [];
                    $scope.data_falla2 = [];
                    $scope.data_falla3 = [];  
                for (var i=0; i < 4; i++) {  
                    var trip = new Date();

                    
                    
                    var besok =new Date(trip.getTime() - (i*31 )*(24*60*60*1000));
                    var labels = $filter('date')(besok, 'yyyy-MM');
                     var coun1 =0,coun2=0; coun3=0;count4=0;
                   
                   var  coun1 = $filter("filter")(datos, { Value : { Client: "Reporte Mantenciones MLB", Fecha: labels }});
                   var c1=0,c2=0;
                   angular.forEach(coun1, function(value, key){
                     if (value.Value.Group == "Grupo 1 (Gonzalez/Figueroa)")
                     {
                        c1++;  
                     }else{
                        c2++;
                     }       
                   });
                     
                   $scope.data_man.push(c1);                                 
                   $scope.data_man_2.push(c2);                    
                   $scope.labels_iauditor_man_1.push(labels);
                   $scope.labels_iauditor_man_2.push(labels);


                   var cc1=0,cc2=0;
                   angular.forEach(coun1, function(value, key){
                     if (value.Value.Type == "Programada")
                     {
                        cc1++;  
                     }else{
                        cc2++;
                     }       
                   });

                  $scope.data_man_3.push(cc1);                                 
                   $scope.data_man_4.push(cc2); 

                   var fallas = $filter("filter")(datos, { Value : { Client: "Reporte Incidente o falla  MLB", Fecha: labels }});

                   
                   

                   var f1=0,f2=0,f3=0;
                   angular.forEach(fallas, function(value, key){
                     if (value.Value.Type == "Daños a equipos/materiales")
                         f1++;
                     if (value.Value.Type == "Falla  de Hardware")
                        f2++;
                     if (value.Value.Type == "Falla  de Software")
                        f3++;

                   });
                     
                     $scope.data_falla1.push(f1);
                    $scope.data_falla2.push(f2);
                    $scope.data_falla3.push(f3);
                     $scope.labels_iauditor_falla.push(labels);
                   

                    
                }
                $scope.data_iauditor_man_1.push($scope.data_man);
                $scope.data_iauditor_man_1.push($scope.data_man_2);
                $scope.data_iauditor_man_2.push($scope.data_man_3);
                $scope.data_iauditor_man_2.push($scope.data_man_4);

                 $scope.data_iauditor_falla.push($scope.data_falla1);
                  $scope.data_iauditor_falla.push($scope.data_falla2);
                   $scope.data_iauditor_falla.push($scope.data_falla3);
               
                $scope.series_iauditor_man_1 = ["Grupo 1 (Gonzalez/Figueroa)", "Grupo 2 (Soto/Carvajal)", "%SpotCorrecto/CargaMala", "%SpotMalo/CargaMalo","%Spot/CargaNulo"];
                $scope.colors_iauditor_man_1 = ['#424242', '#3ADF00', '#808080', '#ffff00', '#000000'];
                $scope.options_iauditor_man_1 = {


                  title: {
                    display: true,
                    text: " Mantenciones por Grupos "
                    // fontColor: '#f9fdff'
                  },
              
                  legend: { display: true, fontColor: '#fcfeff' },
              
                  tooltips: {
                    display: true,
                    mode: 'label',
                    titleFontSize: 12
              
                  },
                  responsive: true,
                  //maintainAspectRatio: false,
              
                  scales: {
                    xAxes: [{
                      stacked: true,
                      scaleLabel: {
                        labelString: 'Fechas',
                        //fontColor: '#151515',
                        display: true,
                      }
                    }
                    ],
                    yAxes: [{
                      stacked: true,
                      scaleLabel: {
                        labelString: 'Cantidad',
                        // fontColor: '#151515',
                        display: true,
                      },
                      ticks: {
                              beginAtZero: true
                          }
                    }]
                  },

                  animation: {
                    duration: 1,
                    onComplete: function (animation) {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.font="9.5px Arial";
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = "#0000ff";
                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                //if (data != 7)                         
                                ctx.fillText(data, bar._model.x, bar._model.y + 15);
                            });
                        });
                        document.getElementById("chart14").value = animation.chartInstance.toBase64Image();
                    }
                }
              };
              

              $scope.series_iauditor_man_2 = ["Programada", "No Programada"];
                $scope.colors_iauditor_man_2 = ['#6ba4d1', '#b2379a', '#808080', '#ffff00', '#000000'];
                $scope.options_iauditor_man_2 = {


                  title: {
                    display: true,
                    text: " Mantenciones por Grupos "
                    // fontColor: '#f9fdff'
                  },
              
                  legend: { display: true, fontColor: '#fcfeff' },
              
                  tooltips: {
                    display: true,
                    mode: 'label',
                    titleFontSize: 12
              
                  },
                  responsive: true,
                  //maintainAspectRatio: false,
              
                  scales: {
                    xAxes: [{
                      stacked: true,
                      scaleLabel: {
                        labelString: 'Fechas',
                        //fontColor: '#151515',
                        display: true,
                      }
                    }
                    ],
                    yAxes: [{
                      stacked: true,
                      scaleLabel: {
                        labelString: 'Cantidad',
                        // fontColor: '#151515',
                        display: true,
                      },
                      ticks: {
                              beginAtZero: true
                          }
                    }]
                  },

                  animation: {
                    duration: 1,
                    onComplete: function (animation) {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.font="9.5px Arial";
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = "#0000ff";
                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                if (data != 0)                         
                                ctx.fillText(data, bar._model.x, bar._model.y + 15);
                            });
                        });
                        document.getElementById("chart14").value = animation.chartInstance.toBase64Image();
                    }
                }
                };

                $scope.series_iauditor_falla = ["Daños a equipos/materiales", "Falla  de Hardware", "Falla  de Software"];
                $scope.colors_iauditor_falla = [ '#808080', '#ffff00', '#c66247'];
                $scope.options_iauditor_falla = {


                  title: {
                    display: true,
                    text: " Reporte Incidente o falla  "
                    // fontColor: '#f9fdff'
                  },
              
                  legend: { display: true, fontColor: '#fcfeff' },
              
                  tooltips: {
                    display: true,
                    mode: 'label',
                    titleFontSize: 12
              
                  },
                  responsive: true,
                  //maintainAspectRatio: false,
              
                  scales: {
                    xAxes: [{
                      stacked: true,
                      scaleLabel: {
                        labelString: 'Fechas',
                        //fontColor: '#151515',
                        display: true,
                      }
                    }
                    ],
                    yAxes: [{
                      stacked: true,
                      scaleLabel: {
                        labelString: 'Cantidad',
                        // fontColor: '#151515',
                        display: true,
                      },
                      ticks: {
                              beginAtZero: true
                          }
                    }]
                  },

                  animation: {
                    duration: 1,
                    onComplete: function (animation) {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.font="9.5px Arial";
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = "#0000ff";
                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                 if (data != 0)                         
                                ctx.fillText(data, bar._model.x, bar._model.y + 15);
                            });
                        });
                        document.getElementById("chart14").value = animation.chartInstance.toBase64Image();
                    }
                }
                };



              });

              GetDataChart5Montly.async($scope.param1).then(function (datos) {
                $scope.data_dirty_12 = datos.data[1].data2;  
                $scope.labels_dirty_12 = datos.data[0].labels;
                $scope.series_dirty_12 = ["%SpotNormal/CargaNormal", "%SpotMalo/CargaCorrecta", "%SpotCorrecto/CargaMala", "%SpotMalo/CargaMalo","%Spot/CargaNulo"];
                $scope.colors_dirty_12 = ['#008000', '#ff0000', '#808080', '#ffff00', '#000000'];
                $scope.options_dirty_12 = {


                  title: {
                    display: true,
                    text: "Integridad (Spot & Load) "
                    // fontColor: '#f9fdff'
                  },
              
                  legend: { display: true, fontColor: '#fcfeff' },
              
                  tooltips: {
                    display: true,
                    mode: 'label',
                    titleFontSize: 12
              
                  },
                  responsive: true,
                  //maintainAspectRatio: false,
              
                  scales: {
                    xAxes: [{
                      stacked: true,
                      scaleLabel: {
                        labelString: 'Dias',
                        //fontColor: '#151515',
                        display: true,
                      }
                    }
                    ],
                    yAxes: [{
                      stacked: true,
                      scaleLabel: {
                        labelString: 'Spot & Load [%]',
                        // fontColor: '#151515',
                        display: true,
                      },
                      ticks: {
                              beginAtZero: true
                          }
                    }]
                  },

                  animation: {
                    duration: 1,
                    onComplete: function (animation) {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.font="9.5px Arial";
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = "#0000ff";
                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                if (data >= 7)                         
                                ctx.fillText(data, bar._model.x, bar._model.y + 15);
                            });
                        });
                        document.getElementById("chart14").value = animation.chartInstance.toBase64Image();
                    }
                }
                };


              });

              GetDataChart6Montly.async($scope.param1).then(function (datos) {
                $scope.data_dirty_13 = datos.data[1].data2;  
                $scope.labels_dirty_13 = datos.data[0].labels;

                $scope.datasetOverride_dirty_13 = [
                  {
                    label: "Integridad [%]",
                    borderWidth: 1,
                    type: 'bar'
                  },
                  {
                    label: "Target [>68%]",
                    borderWidth: 1,
                    
                    type: 'line',
                    fill: false
                  }
                ];

                $scope.options_dirty_13 = {


                  title: {
                    display: true,
                    text: " BenchMark Integridad Spot & Load "
                    // fontColor: '#f9fdff'
                  },
              
                  legend: { display: true, fontColor: '#fcfeff' },
              
                  tooltips: {
                    display: true,
                    mode: 'label',
                    titleFontSize: 12
              
                  },
                  responsive: true,
                  //maintainAspectRatio: false,
              
                  scales: {
                    xAxes: [{
                      stacked: false,
                      scaleLabel: {
                        labelString: 'Clientes Performance Assurance',
                        //fontColor: '#151515',
                        display: true,
                      }
                    }
                    ],
                    yAxes: [{
                      stacked: false,
                      scaleLabel: {
                        labelString: ' Spot & Load [%] ',
                        // fontColor: '#151515',
                        display: true,
                      },
                      ticks: {
                              beginAtZero: true
                          }
                    }]
                  },

                  animation: {
                    duration: 1,
                    onComplete: function (animation) {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.font = "bold 10pt Arial";
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = "#0000ff";
                        this.data.datasets.forEach(function (dataset) {
                          for (var i = 0; i < dataset.data.length; i++) {
                              var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                              //console.log(dataset.data[i]);
                              if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                              ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +35);
                          }
                      });

                      document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
                    }
                }



                };

              });
    
      

              




    });



  







    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

   /* GetDataReportCCAChart1.async($scope.param1).then(function (datos_cca) {
      $scope.labels_cca1_ = [];
      $scope.Emergencia = [];
      $scope.NoEmergencia = [];
      $scope.Ticket = [];
      $scope.data_cca1_ = [];


      angular.forEach(datos_cca.data, function (value, key) {
        this.push(value.Mes);
      }, $scope.labels_cca1_);

      angular.forEach(datos_cca.data, function (value, key) {
        this.push(value.Emergencias);
      }, $scope.Emergencia);
      //$scope.data.push($scope.data_)

      angular.forEach(datos_cca.data, function (value, key) {
        this.push(value.No_Emergencias);
      }, $scope.NoEmergencia);

      angular.forEach(datos_cca.data, function (value, key) {
        this.push(value.Tickets_Creados);
      }, $scope.Ticket);

      $scope.data_cca1_.push($scope.NoEmergencia);
      $scope.data_cca1_.push($scope.Emergencia);
      $scope.data_cca1_.push($scope.Ticket);

    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportCCAChart2.async($scope.param1).then(function (datos) {
      $scope.labels2_ = [];
      $scope.data_cca2_ = [];
      $scope.ticketResueltos = [];
      $scope.ticketCreados = [];

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Mes);
      }, $scope.labels2_);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Tickets_Resueltos);
      }, $scope.ticketResueltos);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Tickets_Creados);
      }, $scope.ticketCreados);

      $scope.data_cca2_.push($scope.ticketResueltos);
      $scope.data_cca2_.push($scope.ticketCreados);
    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportCCAChart3.async($scope.param1).then(function (datos) {
      $scope.labels3_ = [];
      $scope.data3_ = [];
      $scope.ticketLlamados = [];

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Mes);
      }, $scope.labels3_);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.ticketLlamados);
      }, $scope.ticketLlamados);
      $scope.data3_.push($scope.ticketLlamados);

    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportTicketRMA1.async($scope.param1).then(function (datos) {
      $scope.labels_cca1 = [];
      $scope.data_cca1 = [];
      $scope.entrada_cca1 = [];
      $scope.salida_cca1 = [];
      $scope.sinop = [];

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Mes);
      }, $scope.labels_cca1);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Ticket_Creados);
      }, $scope.entrada_cca1);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Tickets_Resueltos);
      }, $scope.salida_cca1);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Tickets_Esperando);
      }, $scope.sinop);

      $scope.data_cca1.push($scope.entrada_cca1);
      $scope.data_cca1.push($scope.salida_cca1);
      $scope.data_cca1.push($scope.sinop);

    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportTicketRMA2.async($scope.param1).then(function (datos) {
      $scope.labels_cca2 = [];
      $scope.data_cca2 = [];
      $scope.backlog2 = [];
      $scope.backlog3 = [];
      $scope.sinop2 = [];

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Mes);
      }, $scope.labels_cca2);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Equipos_Ingresados);
      }, $scope.backlog2);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Equipos_Reparados);
      }, $scope.backlog3);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Equipos_esperando);
      }, $scope.sinop2);

      $scope.data_cca2.push($scope.backlog2);
      $scope.data_cca2.push($scope.backlog3);
      $scope.data_cca2.push($scope.sinop2);

    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportTicketRMA3.async($scope.param1).then(function (datos) {
      $scope.labels_cca3 = [];
      $scope.data_cca3 = [];
      angular.forEach(datos.data, function (value, key) {
        this.push(value.Mes);
      }, $scope.labels_cca3);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Tickets);
      }, $scope.data_cca3);

    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportTicketRMA4.async($scope.param1).then(function (datos) {

      $scope.labels_cca4 = [];
      $scope.data_cca4 = [];

      $scope.data_evaluation = [];
      $scope.data_evaluation2 = [];
      $scope.data_requires = [];
      $scope.data_technical = [];

      angular.forEach(datos.data[0], function (value, key) {
        this.push(value.Mes);
      }, $scope.labels_cca4);

      angular.forEach(datos.data[0], function (value, key) {
        this.push(value.Equipos_en_Bodega);
      }, $scope.data_evaluation);

      angular.forEach(datos.data[1].data2, function (value, key) {
        this.push(value);
      }, $scope.data_evaluation2);

      //  angular.forEach(datos.data, function(value, key) {
      //    this.push(value.requires);
      //  }, $scope.data_requires );

      //  angular.forEach(datos.data, function(value, key) {
      //    this.push(value.technical );
      //  }, $scope.data_technical  );

      $scope.data_cca4.push($scope.data_evaluation);
      $scope.data_cca4.push($scope.data_evaluation2);
      // $scope.data_cca4.push($scope.data_requires);
      // $scope.data_cca4.push($scope.data_technical);

    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportTicketCCA.async($scope.param1).then(function (datos) {

      $scope.myData = datos.data;

    });*/
  }


  initializeChart('Lomas Bayas','mlb');



































  $scope.toastPosition = angular.extend({}, last);

  $scope.getToastPosition = function () {
    sanitizePosition();

    return Object.keys($scope.toastPosition)
      .filter(function (pos) { return $scope.toastPosition[pos]; })
      .join(' ');
  };


  $scope.onChange = function (e, fileList) {
    //alert('this is on-change handler!');
  };

  $scope.onLoad = function (e, reader, file, fileList, fileOjects, fileObj) {
    //alert($scope.file1.base64);
  };

  function sanitizePosition() {
    var current = $scope.toastPosition;

    if (current.bottom && last.top) current.top = false;
    if (current.top && last.bottom) current.bottom = false;
    if (current.right && last.left) current.left = false;
    if (current.left && last.right) current.right = false;

    last = angular.extend({}, current);
  }

  $scope.showSimpleToast2 = function () {
    var pinTo = $scope.getToastPosition();

    $mdToast.show(
      $mdToast.simple()
        .textContent('Archivo Subido sin Problemas...!')
        .position(pinTo)
        .hideDelay(3000)
        .theme('error-toast2')
      //.colors('red')
    );
  };

  $scope.showSimpleToast = function () {
    var pinTo = $scope.getToastPosition();

    $mdToast.show(
      $mdToast.simple()
        .textContent('Grabado Exitoso ...!')
        .position(pinTo)
        .hideDelay(3000)
        .theme('error-toast')
      //.colors('red')
    );
  };

  $scope.showActionToast = function () {
    var pinTo = $scope.getToastPosition();
    var toast = $mdToast.simple()
      .textContent('Marked as read')
      .action('UNDO')
      .highlightAction(true)
      .highlightClass('md-accent')// Accent is used by default, this just demonstrates the usage.
      .position(pinTo);

    $mdToast.show(toast).then(function (response) {
      if (response == 'ok') {
        alert('You clicked the \'UNDO\' action.');
      }
    });
  };

  $scope.series = ['Series A', 'Series B'];

  //$scope.flotas = {};

  $scope.uploadFile = function () {

    var file = $scope.myFile;
   // console.log(file);
    //var uploadUrl = "http://localhost:3000/restservicemantos/UploadFile";
    var uploadUrl = "http://192.168.12.64:4000/restservicemodular/uploadFile?cliente=" + $scope.user.id + "&mes=" + $scope.mes.id + "&ano=" + $scope.ano.id;
    var fd = new FormData();
    fd.append('file', file);

    $http.post(uploadUrl, fd, {
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    })
      .then(function () {
        console.log("success!!");
        $scope.showSimpleToast2();
      })
      .catch(function () {
        console.log("error!!");
      });
  };


  var cleanObject = function () {
    $scope.intro = "";
    $scope.intro2 = "";
    $scope.intro3 = "";
    $scope.intro4 = "";
    $scope.intro5 = "";
    $scope.intro6 = "";

    $scope.intro2_spotload  = "";
    $scope.intro2_usodinamico = "";
    $scope.intro2_perdida_1 = "";
    $scope.intro2_perdida_2 = "";
    $scope.intro2_perdida_3 = "";
    $scope.intro2_perdida_4 = "";
    $scope.intro2_moviminento = "";

    $scope.myFile1 = { base64: '' };
    $scope.myFile2 = { base64: '' };
    $scope.myFile3 = { base64: '' };
    $scope.myFile4 = { base64: '' };
    $scope.myFile5 = { base64: '' };
  }

  $scope.data = {
    cb1: true,
    cb4: true,
    cb5: false
  };

  $scope.message = 'false';

  $scope.onChangeInforme = function (cbState) {
    $scope.message = cbState;
  };

  var initializeChart = function (cliente, id_cliente) {

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }



    /*GetDataReport1.async($scope.param1).then(function (datos) {


      $scope.alldata = {};
      $scope.data1 = [];
      $scope.series = [];
      $scope.labels = [];

      if (datos === 0) {
        $scope.dataall = $rootScope.report1;
      } else {
        $scope.dataall = datos;
        $rootScope.report1 = datos;
      }

      $scope.labels = $scope.dataall.data[0].labels;

      $scope.data = $scope.dataall.data[1].data2;

    });*/














    

  

    GetDataFlotaReport.async($scope.param1).then(function (datos_flota) {
       
      $rootScope.flota = datos_flota.data;
       
      
    // console.log(datos_flota.data[2].qualif);
    
    


    $scope.creados = [];
    $scope.turnos = 2;
    if ((id_cliente == 'cmh') || (id_cliente=='mlc')){
      $scope.turnos = 3;
    }

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente,
      turnos: $scope.turnos
    }

    GetDataChart1Montly.async($scope.param1).then(function (datos) {
      $scope.data_dirty_1 = [];
      $scope.labels_dirty_1 = [];
      $scope.colors_dirty_1 = ['#0000ff', '#ff0000', '#00D015', '#808080', '#ff0000'];
      angular.forEach(datos.data, function (value, key) {
        this.push(value.Turno);        
      }, $scope.labels_dirty_1);
      $scope.data1 = [];
      $scope.data2 = [];
      $scope.data3 = [];
      $scope.data4 = [];
      angular.forEach(datos.data, function (value, key) {
        this.push(value.Grupo1);        
      }, $scope.data1);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Grupo2);        
      }, $scope.data2);
      angular.forEach(datos.data, function (value, key) {
        this.push(value.Grupo3);        
      }, $scope.data3);
      angular.forEach(datos.data, function (value, key) {
        this.push(value.Grupo4);        
      }, $scope.data4);

      
      $scope.data_dirty_1.push($scope.data1);
      $scope.data_dirty_1.push($scope.data2);
      $scope.data_dirty_1.push($scope.data3);
      $scope.data_dirty_1.push($scope.data4);
     
      $scope.series_dirty_1 = ['1','2','3','4'];

      $scope.series_dirty_1_ = ['1','2','3','4'];

       $scope.options_dirty_1 = {
        title: {
          display: true,
          text: "Movimiento Mina promedio por Hora "
    
        },
          
        elements: { point: { radius: 0 } },

        legend: { display: true, fontColor: '#fcfeff' },
        tooltips: {
          display: true,
          mode: 'label',
          titleFontSize: 12
        },
        responsive: true,
        scales: {
          xAxes: [{
            stacked: false,
            scaleLabel: {
              labelString: 'Hora',
              display: true,
            }
          }
          ],
          yAxes: [{
            stacked: false,
            scaleLabel: {
              labelString: 'Tons',
              display: true,
            },
            ticks: {
              beginAtZero: true
            }
          }]
        },
        animation: {
          onComplete: function (animation) {
            document.getElementById("chart25").value = animation.chartInstance.toBase64Image();           
          }
        }
    
      };


      $scope.datasetOverride_dirty_1 = [
        {
          label: "Grupo 1",
          borderWidth: 1,
          type: 'line',
          fill: false,
          lineTension: 0
        },
        {
          label: "Grupo 2",
          borderWidth: 1,
          
          type: 'line',
          fill: false,
          lineTension: 0
        },
        {
          label: "Grupo 3",
          borderWidth: 1,        
          type: 'line',
          fill: false,
          lineTension: 0
        },
        {
          label: "Grupo 4",
          borderWidth: 1,         
          type: 'line',
          fill: false,
          lineTension: 0
        }
      ];
    });


    

    $scope.param1 = {
      cliente: id_cliente,
      id_cliente: id_cliente,
      flota : datos_flota.data[0].qualif
    }



    GetDataChart2Montly.async($scope.param1).then(function (datos) {

      $scope.data_dirty_2 = [];
      $scope.labels_dirty_2 = [];

      $scope.data_dirty_2 = datos.data[1].data2;
      $scope.labels_dirty_2 = datos.data[0].labels;

      
      
      $scope.data_queuetime = [];
      $scope.data_hangtime = [];

      $scope.colors_dirty_2 = ['#008000', '#ff0000'];
      $scope.series_dirty_2 =  ['Tiempo de Cola CAEX', 'Tiempo Espera de Pala'];
      //$scope.datasetOverride_dirty_2 = {};

      $scope.options_dirty_2 = {

        

        title: {
          display: true,
          text: "Pérdidas Operacionales por Grupo - " +datos_flota.data[0].qualif
          // fontColor: '#f9fdff'
        },
    
        legend: { display: true, fontColor: '#fcfeff' },
    
        tooltips: {
          display: true,
          mode: 'label',
          titleFontSize: 12
    
        },
        responsive: true,
    
        scales: {
          xAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Grupos',
              //fontColor: '#151515',
              display: true,
            }
          }
          ],
          yAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Perdidas Operacionales [min]',
              // fontColor: '#151515',
              display: true,
            },
            ticks: {
              //callback: function(value, index, values) {
              //    return parseFloat(value).toFixed(2);
              //},
              min: 0,
              max: 8,
              stepSize: 1
                }
          }]
        },
        animation: {
          duration: 1,
          onComplete: function (animation) {
              var chartInstance = this.chart,
                  ctx = chartInstance.ctx;
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
              ctx.textAlign = 'center';
              ctx.textBaseline = 'bottom';
              ctx.fillStyle = "#000000";
              this.data.datasets.forEach(function (dataset) {
                for (var i = 0; i < dataset.data.length; i++) {
                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                    //console.log(model);
                    //if (dataset.data[i] != 80)
                    ctx.fillText(dataset.data[i], model.x, model.y + 15);
                }
            });
            document.getElementById("chart23").value = animation.chartInstance.toBase64Image();      
          }
      }
      };




    });


    $scope.param1 = {
      cliente: id_cliente,
      id_cliente: id_cliente,
      flota : datos_flota.data[1].qualif
    };


    GetDataChart2Montly.async($scope.param1).then(function (datos) {

      $scope.data_dirty_3 = [];
      $scope.labels_dirty_3 = [];

      $scope.data_dirty_3 = datos.data[1].data2;
      $scope.labels_dirty_3 = datos.data[0].labels;

      
      
      $scope.data_queuetime = [];
      $scope.data_hangtime = [];

      $scope.colors_dirty_3 = ['#008000', '#ff0000'];
      $scope.series_dirty_3 =  ['Tiempo de Cola CAEX', 'Tiempo Espera de Pala'];
      

      $scope.options_dirty_3 = {


        title: {
          display: true,
          text: "Pérdidas Operacionales por Grupo - " +  datos_flota.data[1].qualif
          // fontColor: '#f9fdff'
        },
    
        legend: { display: true, fontColor: '#fcfeff' },
    
        tooltips: {
          display: true,
          mode: 'label',
          titleFontSize: 12
    
        },
        responsive: true,
    
        scales: {
          xAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Grupos',
              //fontColor: '#151515',
              display: true,
            }
          }
          ],
          yAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Perdidas Operacionales [min]',
              // fontColor: '#151515',
              display: true,
            },
            ticks: {
              //callback: function(value, index, values) {
              //    return parseFloat(value).toFixed(2);
              //},
              min: 0,
              max: 8,
              stepSize: 1
                }
          }]
        },

        animation: {
          duration: 1,
          onComplete: function (animation) {
              var chartInstance = this.chart,
                  ctx = chartInstance.ctx;
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
              ctx.textAlign = 'center';
              ctx.textBaseline = 'bottom';
              ctx.fillStyle = "#000000";
              this.data.datasets.forEach(function (dataset) {
                for (var i = 0; i < dataset.data.length; i++) {
                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                    //console.log(model);
                    //if (dataset.data[i] != 80)
                    ctx.fillText(dataset.data[i], model.x, model.y + 15);
                }
            });
            document.getElementById("chart24").value = animation.chartInstance.toBase64Image();      
          }
      }
      };




    });

    if ( datos_flota.data.length > 2) {

    $scope.param1 = {
      cliente: id_cliente,
      id_cliente: id_cliente,
      flota : datos_flota.data[2].qualif
    };


    GetDataChart2Montly.async($scope.param1).then(function (datos) {

      $scope.data_dirty_4 = [];
      $scope.labels_dirty_4 = [];

      $scope.data_dirty_4 = datos.data[1].data2;
      $scope.labels_dirty_4 = datos.data[0].labels;

      
      
      $scope.data_queuetime = [];
      $scope.data_hangtime = [];

      
      $scope.colors_dirty_4 = ['#008000', '#ff0000'];
      $scope.series_dirty_4 =  ['Tiempo de Cola CAEX', 'Tiempo Espera de Pala'];
      $scope.options_dirty_4 = {


        title: {
          display: true,
          text: "Pérdidas Operacionales por Grupo - "+datos_flota.data[2].qualif
          // fontColor: '#f9fdff'
        },
    
        legend: { display: true, fontColor: '#fcfeff' },
    
        tooltips: {
          display: true,
          mode: 'label',
          titleFontSize: 12
    
        },
        responsive: true,
    
        scales: {
          xAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Grupos',
              //fontColor: '#151515',
              display: true,
            }
          }
          ],
          yAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Perdidas Operacionales [min]',
              // fontColor: '#151515',
              display: true,
            },
            ticks: {
              //callback: function(value, index, values) {
              //    return parseFloat(value).toFixed(2);
              //},
              min: 0,
              max: 8,
              stepSize: 1
                }
          }]
        },

        animation: {
          duration: 1,
          onComplete: function (animation) {
              var chartInstance = this.chart,
                  ctx = chartInstance.ctx;
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
              ctx.textAlign = 'center';
              ctx.textBaseline = 'bottom';
              ctx.fillStyle = "#000000";
              this.data.datasets.forEach(function (dataset) {
                for (var i = 0; i < dataset.data.length; i++) {
                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                    //console.log(model);
                    //if (dataset.data[i] != 80)
                    ctx.fillText(dataset.data[i], model.x, model.y + 15);
                }
            });
            document.getElementById("chart21").value = animation.chartInstance.toBase64Image();      
          }
      }
      };




    });

    }

    if ( datos_flota.data.length > 3) {

    $scope.param1 = {
      cliente: id_cliente,
      id_cliente: id_cliente,
      flota : datos_flota.data[3].qualif
    };


    GetDataChart2Montly.async($scope.param1).then(function (datos) {

      $scope.data_dirty_5 = [];
      $scope.labels_dirty_5 = [];

      $scope.data_dirty_5 = datos.data[1].data2;
      $scope.labels_dirty_5 = datos.data[0].labels;

      
      
      $scope.data_queuetime = [];
      $scope.data_hangtime = [];

      $scope.colors_dirty_5 = ['#008000', '#ff0000'];
      $scope.series_dirty_5 = ['Tiempo de Cola CAEX', 'Tiempo Espera de Pala'];
      

      $scope.options_dirty_5 = {
        title: {
          display: true,
          text: "Pérdidas Operacionales por Grupo - "+datos_flota.data[3].qualif
          // fontColor: '#f9fdff'
        },
    
        legend: { display: true, fontColor: '#fcfeff' },
    
        tooltips: {
          display: true,
          mode: 'label',
          titleFontSize: 12
    
        },
        responsive: true,
    
        scales: {
          xAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Grupos',
              //fontColor: '#151515',
              display: true,
            }
          }
          ],
          yAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Perdidas Operacionales [min]',
              // fontColor: '#151515',
              display: true,
            },
            ticks: {
              //callback: function(value, index, values) {
              //    return parseFloat(value).toFixed(2);
              //},
              min: 0,
              max: 8,
              stepSize: 1
                }
          }]
        },

        animation: {
          duration: 1,
          onComplete: function (animation) {
              var chartInstance = this.chart,
                  ctx = chartInstance.ctx;
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
              ctx.textAlign = 'center';
              ctx.textBaseline = 'bottom';
              ctx.fillStyle = "#000000";
              this.data.datasets.forEach(function (dataset) {
                for (var i = 0; i < dataset.data.length; i++) {
                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                    //console.log(model);
                    if (dataset.data[i] != 80)
                    ctx.fillText(dataset.data[i], model.x, model.y + 15);
                }
            });
            document.getElementById("chart22").value = animation.chartInstance.toBase64Image();      
          }
      }
      }

    }); 

     }

      $scope.param1 = {
        cliente: id_cliente,
        id_cliente: id_cliente,
        flota : datos_flota.data[0].qualif
      };

      GetDataChartMontlyScatter.async($scope.param1).then(function (datos) {   
      
      $scope.data_dirty_6 = datos.data[1].data2[0];     

      $scope.colors_dirty_6 = [];
      for (i=0; i< 100 ; i++)
         $scope.colors_dirty_6.push('#0000ff'); 

      $scope.options_dirty_6 ={
        title: {
          display: true,
          text: "Distribución Pérdidas Operacionales - "+ datos_flota.data[0].qualif
          // fontColor: '#f9fdff'
        },
    
        legend: false,
    
        tooltips: false,

        annotation: {
          annotations: [{
             type: 'box',
             drawTime: 'beforeDatasetsDraw',
             xScaleID: 'x-axis-0',
             yScaleID: 'y-axis-0',
             xMin: 0,
             xMax: 2,
             yMin: 0,
             yMax: 2,
             backgroundColor: 'rgba(20, 20, 20, 0.2)'
          }]
       },
        responsive: true,
    
        scales: {
          xAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Promedio Espera Pala [min]',
              //fontColor: '#151515',
              display: true,
            },
            ticks: {
           //   callback: function(value, index, values) {
           //       return parseFloat(value).toFixed(2);
            //  },
              min: 0,
              max: 8,
              stepSize: 1
          }
          }
          ],
          yAxes: [{
            stacked: true,
            scaleLabel: {
              labelString: 'Promedio Cola CAEX [min]',
              // fontColor: '#151515',
              display: true,
            },
            ticks: {
              //callback: function(value, index, values) {
              //    return parseFloat(value).toFixed(2);
              //},
              min: 0,
              max: 8,
              stepSize: 1
          }
          }]
        },
        animation: {
          onComplete: function (animation) {
            document.getElementById("chart19").value = animation.chartInstance.toBase64Image();           
          }
        }
      };

        $scope.datasetOverride_dirty_6 = [
          {
            backgroundColor:"#ff6384",
          hoverBackgroundColor: "#ff6384"
          },
          {
            backgroundColor:"#ff6384",
            hoverBackgroundColor: "#ff6384"
          },
          {
            backgroundColor:"#ff6384",
            hoverBackgroundColor: "#ff6384"
          },
          {
            backgroundColor:"#ff6384",
            hoverBackgroundColor: "#ff6384"
          }
        ];
      
        

        });


        $scope.param1 = {
          cliente: id_cliente,
          id_cliente: id_cliente,
          flota : datos_flota.data[1].qualif
        };
  
        GetDataChartMontlyScatter.async($scope.param1).then(function (datos) {  
          
          $scope.colors_dirty_7 = [];
          for (i = 0; i < 200; i++)
            $scope.colors_dirty_7.push('#0000ff');
          $scope.data_dirty_7 = datos.data[1].data2[0];     
  
        $scope.options_dirty_7 ={
          title: {
            display: true,
            text: "Distribución Pérdidas Operacionales - " + datos_flota.data[1].qualif
            // fontColor: '#f9fdff'
          },
      
          legend: false,
        
          tooltips: false,

          annotation: {
            annotations: [{
               type: 'box',
               drawTime: 'beforeDatasetsDraw',
               xScaleID: 'x-axis-0',
               yScaleID: 'y-axis-0',
               xMin: 0,
               xMax: 2,
               yMin: 0,
               yMax: 2,
               backgroundColor: 'rgba(20, 20, 20, 0.2)'
            }]
         },

          responsive: true,
      
          scales: {
            xAxes: [{
              stacked: true,
              scaleLabel: {
                labelString: 'Promedio Espera Pala [min]',
                //fontColor: '#151515',
                display: true,
              },
              ticks: {
               // callback: function(value, index, values) {
               //     return parseFloat(value).toFixed(2);
               // },
                min: 0,
              max: 8,
              stepSize: 1
            }
            }
            ],
            yAxes: [{
              stacked: true,
              scaleLabel: {
                labelString: 'Promedio Cola CAEX [min]',
                // fontColor: '#151515',
                display: true,
              },
              ticks: {
                //callback: function(value, index, values) {
                //    return parseFloat(value).toFixed(2);
                //},
                min: 0,
              max: 8,
              stepSize: 1
            }
            }]
          },
          animation: {
            onComplete: function (animation) {
              document.getElementById("chart20").value = animation.chartInstance.toBase64Image();           
            }
          }
        };
  
          $scope.datasetOverride_dirty_7 = [
            {
              backgroundColor:"#ff6384",
            hoverBackgroundColor: "#ff6384"
            },
            {
              backgroundColor:"#ff6384",
              hoverBackgroundColor: "#ff6384"
            },
            {
              backgroundColor:"#ff6384",
              hoverBackgroundColor: "#ff6384"
            },
            {
              backgroundColor:"#ff6384",
              hoverBackgroundColor: "#ff6384"
            }
          ];
        
          
  
          });

          if ( datos_flota.data.length > 2) { 

          $scope.param1 = {
            cliente: id_cliente,
            id_cliente: id_cliente,
            flota : datos_flota.data[2].qualif
          };
    
          GetDataChartMontlyScatter.async($scope.param1).then(function (datos) {   
          
          $scope.data_dirty_8 = datos.data[1].data2[0];     
          $scope.colors_dirty_8 = [];
          for (i = 0; i < 200; i++)
            $scope.colors_dirty_8.push('#0000ff');
          $scope.options_dirty_8 ={
            title: {
              display: true,
              text: "Distribución Pérdidas Operacionales - " +  datos_flota.data[2].qualif
              // fontColor: '#f9fdff'
            },
        
            legend: false,
      
            tooltips: false,

            annotation: {
              annotations: [{
                 type: 'box',
                 drawTime: 'beforeDatasetsDraw',
                 xScaleID: 'x-axis-0',
                 yScaleID: 'y-axis-0',
                 xMin: 0,
                 xMax: 2,
                 yMin: 0,
                 yMax: 2,
                 backgroundColor: 'rgba(20, 20, 20, 0.2)'
              }]
           },
        
           
            responsive: true,
        
            scales: {
              xAxes: [{
                stacked: true,
                scaleLabel: {
                  labelString: 'Promedio Espera Pala [min]',
                  //fontColor: '#151515',
                  display: true,
                },

                ticks: {
                 // callback: function(value, index, values) {
                 //     return parseFloat(value).toFixed(2);
                 // },
                  min: 0,
              max: 8,
              stepSize: 1
              }
              }
              
              
              ],
              yAxes: [{
                stacked: true,
                scaleLabel: {
                  labelString: 'Promedio Cola CAEX [min]',
                  // fontColor: '#151515',
                  display: true,
                },
                ticks: {
                 // callback: function(value, index, values) {
                 //     return parseFloat(value).toFixed(2);
                 // },
                  min: 0,
              max: 8,
              stepSize: 1
              }
              }]
            },

            animation: {
              onComplete: function (animation) {
                document.getElementById("chart17").value = animation.chartInstance.toBase64Image();           
              }
            }
          };
    
            $scope.datasetOverride_dirty_8 = [
              {
                backgroundColor:"#ff6384",
              hoverBackgroundColor: "#ff6384",
              displayLegend:false
              },
              {
                backgroundColor:"#ff6384",
                hoverBackgroundColor: "#ff6384",
                displayLegend:false
              },
              {
                backgroundColor:"#ff6384",
                hoverBackgroundColor: "#ff6384",
                displayLegend:false
              },
              {
                backgroundColor:"#ff6384",
                hoverBackgroundColor: "#ff6384"
              }
            ];
          
            
    
            });

          }

          if ( datos_flota.data.length > 3) {           

            $scope.param1 = {
              cliente: id_cliente,
              id_cliente: id_cliente,
              flota : datos_flota.data[3].qualif
            };
      
            GetDataChartMontlyScatter.async($scope.param1).then(function (datos) {   
            
            $scope.data_dirty_9 = datos.data[1].data2[0];     
            $scope.colors_dirty_9 = [];
            for (i = 0; i < 200; i++)
              $scope.colors_dirty_9.push('#0000ff');
            $scope.options_dirty_9 ={
              title: {
                display: true,
                text: "Distribución Pérdidas Operacionales - " +datos_flota.data[3].qualif
                // fontColor: '#f9fdff'
              },
          
              legend: true,
        
              tooltips: {
                mode: 'index',
                intersect: true
              },
              annotation: {
                annotations: [{
                   type: 'box',
                   drawTime: 'beforeDatasetsDraw',
                   xScaleID: 'x-axis-0',
                   yScaleID: 'y-axis-0',
                   xMin: 0,
                   xMax: 2,
                   yMin: 0,
                   yMax: 2,
                   backgroundColor: 'rgba(20, 20, 20, 0.2)'
                }]
             },
          
             
              responsive: true,
          
              scales: {
                xAxes: [{
                  stacked: true,
                  scaleLabel: {
                    labelString: 'Promedio Espera Pala [min]',
                    //fontColor: '#151515',
                    display: true,
                  },
                  ticks: {
                    //callback: function(value, index, values) {
                   //     return parseFloat(value).toFixed(2);
                   // },
                    min: 0,
              max: 8,
              stepSize: 1
                }
                }
                ],
                yAxes: [{
                  stacked: true,
                  scaleLabel: {
                    labelString: 'Promedio Cola CAEX [min]',
                    // fontColor: '#151515',
                    display: true,
                  },
                  ticks: {
                    //callback: function(value, index, values) {
                   //     return parseFloat(value).toFixed(2);
                   // },
                    min: 0,
              max: 8,
              stepSize: 1
                }
                }]
              },

           
              
              animation: {
                onComplete: function (animation) {
                  document.getElementById("chart18").value = animation.chartInstance.toBase64Image();           
                }
              }
            };
      
              $scope.datasetOverride_dirty_9 = [{
                label: "My First dataset",
                backgroundColor: "#ff6384"},{
                  label: "My First dataset",
                  backgroundColor: "#ff6384"},{
                    label: "My First dataset",
                    backgroundColor: "#ff6384"},{
                      label: "My First dataset",
                      backgroundColor: "#ff6384"},{
                        label: "My First dataset",
                        backgroundColor: "#ff6384"},{
                          label: "My First dataset",
                          backgroundColor: "#ff6384"},{
                            label: "My First dataset",
                            backgroundColor: "#ff6384"},{
                              label: "My First dataset",
                              backgroundColor: "#ff6384"},{
                                label: "My First dataset",
                                backgroundColor: "#ff6384"},{
                                  label: "My First dataset",
                                  backgroundColor: "#ff6384"},{
                                    label: "My First dataset",
                                    backgroundColor: "#ff6384"},{
                                      label: "My First dataset",
                                      backgroundColor: "#ff6384"},{
                                        label: "My First dataset",
                                        backgroundColor: "#ff6384"},{
                                          label: "My First dataset",
                                          backgroundColor: "#ff6384"},{
                                            label: "My First dataset",
                                            backgroundColor: "#ff6384"},{
                                              label: "My First dataset",
                                              backgroundColor: "#ff6384"},{
                                                label: "My First dataset",
                                                backgroundColor: "#ff6384"}];
            
              
      
              });

              }

                  

              GetDataChart3Montly.async($scope.param1).then(function (datos) { 

                $scope.data_dirty_10 = datos.data[1].data2;  
                $scope.labels_dirty_10 = datos.data[0].labels;
                $scope.colors_dirty_10 = ['#0000ff', '#008000', '#ff0000', '#808080', '#ff0000'];
                $scope.datasetOverride_dirty_10 = [
                  {
                    label: "Grupo 1",
                    borderWidth: 1,
                    type: 'bar'
                  },
                  {
                    label: "Grupo 2",
                    borderWidth: 1,
                    type: 'bar'
                  },
                  {
                    label: "Grupo 3",
                    borderWidth: 1,
                    type: 'bar'
                  },
                  {
                    label: "Grupo 4",
                    borderWidth: 1,
                    type: 'bar'
                  },
                  //{
                  //  label: "Tendencia",
                    //borderWidth: 1,
                    //borderDash: [5, 5],
                  //  pointBorderWidth: 1,
                    
                  //  type: 'line',
                  //  fill: false
                  //},
                  {
                    label: "Target[>80%]",
                    borderWidth: 1,
                    //pointBorderWidth: 1,
                    borderDash: [5, 5],
                    //hoverBackgroundColor: "rgba(0, 0, 0, 0)",
                    //hoverBorderColor: "rgba(0, 0, 0, 0)",
                    type: 'line',
                    fill: false
                  }
                ];

                $scope.options_dirty_10 = {


                  title: {
                    display: true,
                    text: " Uso Dinamico por Grupo "
                    // fontColor: '#f9fdff'
                  },
              
                  legend: { display: true, fontColor: '#fcfeff' },
              
                  tooltips: {
                    display: true,
                    mode: 'label',
                    titleFontSize: 12
              
                  },
                  responsive: true,
                  //maintainAspectRatio: false,
              
                  scales: {
                    xAxes: [{
                      stacked: false,
                      scaleLabel: {
                        labelString: 'Mes',
                        //fontColor: '#151515',
                        display: true,
                      }
                    }
                    ],
                    yAxes: [{
                      stacked: false,
                      scaleLabel: {
                        labelString: ' Uso Dinamico [%] ',
                        // fontColor: '#151515',
                        display: true,
                      },
                      ticks: {
                              beginAtZero: true
                          }
                    }]
                  },
                  animation: {
                    duration: 1,
                    onComplete: function (animation) {
                        //var chartInstance = this.chart,
                        //    ctx = chartInstance.ctx;
                        //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        //ctx.font="9.5px Arial";
                        //ctx.textAlign = 'center';
                        //ctx.textBaseline = 'bottom';
                        //ctx.fillStyle = "#0000ff";
                        //this.data.datasets.forEach(function (dataset) {
                        //  for (var i = 0; i < dataset.data.length; i++) {
                        //      var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                              //console.log(model);
                        //      if (dataset.data[i] != 80)
                         //     ctx.fillText(dataset.data[i], model.x, model.y + 15);
                         // }
                     // });
                      document.getElementById("chart16").value = animation.chartInstance.toBase64Image();
                    }
                }
                };
              

              });


              

              GetDataChart4Montly.async($scope.param1).then(function (datos) {

                $scope.data_dirty_11 = datos.data[1].data2;  
                $scope.labels_dirty_11 = datos.data[0].labels;
                $scope.colors_dirty_11 = ['#0000ff', '#ff0000'];

                $scope.datasetOverride_dirty_11 = [
                  {
                    label: "Uso Dinamico [%]",
                    borderWidth: 1,
                    type: 'bar'
                  },
                  {
                    label: "Taget [>80%]",
                    borderWidth: 1,
                    //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
                    //hoverBorderColor: "rgba(255, 0, 0, 1)",
                    type: 'line',
                    fill: false
                  }
                ];

                $scope.options_dirty_11 = {


                  title: {
                    display: true,
                    text: " BenchMark Uso Dinamico  "
                    // fontColor: '#f9fdff'
                  },
              
                  legend: { display: true, fontColor: '#fcfeff' },
              
                  tooltips: {
                    display: true,
                    mode: 'label',
                    titleFontSize: 12
              
                  },
                  responsive: true,
                  //maintainAspectRatio: false,
              
                  scales: {
                    xAxes: [{
                      stacked: false,
                      scaleLabel: {
                        labelString: 'Clientes Performance Assurance',
                        //fontColor: '#151515',
                        display: true,
                      }
                    }
                    ],
                    yAxes: [{
                      stacked: false,
                      scaleLabel: {
                        labelString: ' Uso Dinamico [%] ',
                        // fontColor: '#151515',
                        display: true,
                      },
                      ticks: {
                              beginAtZero: true
                          }
                    }]
                  },

                  animation: {
                    duration: 1,
                    onComplete: function (animation) {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = "#0000ff";
                        this.data.datasets.forEach(function (dataset) {
                          for (var i = 0; i < dataset.data.length; i++) {
                              var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                              if ((dataset.data[i] != 80) && (dataset.data[i] !=0))
                              ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +20);
                          }
                      });
                      document.getElementById("chart15").value = animation.chartInstance.toBase64Image();
                    }
                }
                };
              

              });

              GetDataChart5Montly.async($scope.param1).then(function (datos) {
                $scope.data_dirty_12 = datos.data[1].data2;  
                $scope.labels_dirty_12 = datos.data[0].labels;
                $scope.series_dirty_12 = ["%SpotNormal/CargaNormal", "%SpotMalo/CargaCorrecta", "%SpotCorrecto/CargaMala", "%SpotMalo/CargaMalo","%Spot/CargaNulo"];
                $scope.colors_dirty_12 = ['#008000', '#ff0000', '#808080', '#ffff00', '#000000'];
                $scope.options_dirty_12 = {


                  title: {
                    display: true,
                    text: "Integridad (Spot & Load) "
                    // fontColor: '#f9fdff'
                  },
              
                  legend: { display: true, fontColor: '#fcfeff' },
              
                  tooltips: {
                    display: true,
                    mode: 'label',
                    titleFontSize: 12
              
                  },
                  responsive: true,
                  //maintainAspectRatio: false,
              
                  scales: {
                    xAxes: [{
                      stacked: true,
                      scaleLabel: {
                        labelString: 'Mes',
                        //fontColor: '#151515',
                        display: true,
                      }
                    }
                    ],
                    yAxes: [{
                      stacked: true,
                      scaleLabel: {
                        labelString: 'Spot & Load [%]',
                        // fontColor: '#151515',
                        display: true,
                      },
                      ticks: {
                              beginAtZero: true
                          }
                    }]
                  },

                  animation: {
                    duration: 1,
                    onComplete: function (animation) {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.font="9.5px Arial";
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = "#0000ff";
                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                if (data >= 7)                         
                                ctx.fillText(data, bar._model.x, bar._model.y + 15);
                            });
                        });
                        document.getElementById("chart14").value = animation.chartInstance.toBase64Image();
                    }
                }
                };


              });

              GetDataChart6Montly.async($scope.param1).then(function (datos) {
                $scope.data_dirty_13 = datos.data[1].data2;  
                $scope.labels_dirty_13 = datos.data[0].labels;

                $scope.datasetOverride_dirty_13 = [
                  {
                    label: "Integridad [%]",
                    borderWidth: 1,
                    type: 'bar'
                  },
                  {
                    label: "Target [>68%]",
                    borderWidth: 1,
                    
                    type: 'line',
                    fill: false
                  }
                ];

                $scope.options_dirty_13 = {


                  title: {
                    display: true,
                    text: " BenchMark Integridad Spot & Load "
                    // fontColor: '#f9fdff'
                  },
              
                  legend: { display: true, fontColor: '#fcfeff' },
              
                  tooltips: {
                    display: true,
                    mode: 'label',
                    titleFontSize: 12
              
                  },
                  responsive: true,
                  //maintainAspectRatio: false,
              
                  scales: {
                    xAxes: [{
                      stacked: false,
                      scaleLabel: {
                        labelString: 'Clientes Performance Assurance',
                        //fontColor: '#151515',
                        display: true,
                      }
                    }
                    ],
                    yAxes: [{
                      stacked: false,
                      scaleLabel: {
                        labelString: ' Spot & Load [%] ',
                        // fontColor: '#151515',
                        display: true,
                      },
                      ticks: {
                              beginAtZero: true
                          }
                    }]
                  },

                  animation: {
                    duration: 1,
                    onComplete: function (animation) {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.font = "bold 10pt Arial";
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = "#0000ff";
                        this.data.datasets.forEach(function (dataset) {
                          for (var i = 0; i < dataset.data.length; i++) {
                              var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                              //console.log(dataset.data[i]);
                              if ((dataset.data[i] != 68) && (dataset.data[i] !=0))
                              ctx.fillText(dataset.data[i], model.x, model.y + (dataset.data[i]/2) +35);
                          }
                      });

                      document.getElementById("chart13").value = animation.chartInstance.toBase64Image();
                    }
                }



                };

              });
    
      

              




    });



  







    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

   /* GetDataReportCCAChart1.async($scope.param1).then(function (datos_cca) {
      $scope.labels_cca1_ = [];
      $scope.Emergencia = [];
      $scope.NoEmergencia = [];
      $scope.Ticket = [];
      $scope.data_cca1_ = [];


      angular.forEach(datos_cca.data, function (value, key) {
        this.push(value.Mes);
      }, $scope.labels_cca1_);

      angular.forEach(datos_cca.data, function (value, key) {
        this.push(value.Emergencias);
      }, $scope.Emergencia);
      //$scope.data.push($scope.data_)

      angular.forEach(datos_cca.data, function (value, key) {
        this.push(value.No_Emergencias);
      }, $scope.NoEmergencia);

      angular.forEach(datos_cca.data, function (value, key) {
        this.push(value.Tickets_Creados);
      }, $scope.Ticket);

      $scope.data_cca1_.push($scope.NoEmergencia);
      $scope.data_cca1_.push($scope.Emergencia);
      $scope.data_cca1_.push($scope.Ticket);

    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportCCAChart2.async($scope.param1).then(function (datos) {
      $scope.labels2_ = [];
      $scope.data_cca2_ = [];
      $scope.ticketResueltos = [];
      $scope.ticketCreados = [];

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Mes);
      }, $scope.labels2_);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Tickets_Resueltos);
      }, $scope.ticketResueltos);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Tickets_Creados);
      }, $scope.ticketCreados);

      $scope.data_cca2_.push($scope.ticketResueltos);
      $scope.data_cca2_.push($scope.ticketCreados);
    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportCCAChart3.async($scope.param1).then(function (datos) {
      $scope.labels3_ = [];
      $scope.data3_ = [];
      $scope.ticketLlamados = [];

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Mes);
      }, $scope.labels3_);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.ticketLlamados);
      }, $scope.ticketLlamados);
      $scope.data3_.push($scope.ticketLlamados);

    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportTicketRMA1.async($scope.param1).then(function (datos) {
      $scope.labels_cca1 = [];
      $scope.data_cca1 = [];
      $scope.entrada_cca1 = [];
      $scope.salida_cca1 = [];
      $scope.sinop = [];

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Mes);
      }, $scope.labels_cca1);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Ticket_Creados);
      }, $scope.entrada_cca1);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Tickets_Resueltos);
      }, $scope.salida_cca1);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Tickets_Esperando);
      }, $scope.sinop);

      $scope.data_cca1.push($scope.entrada_cca1);
      $scope.data_cca1.push($scope.salida_cca1);
      $scope.data_cca1.push($scope.sinop);

    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportTicketRMA2.async($scope.param1).then(function (datos) {
      $scope.labels_cca2 = [];
      $scope.data_cca2 = [];
      $scope.backlog2 = [];
      $scope.backlog3 = [];
      $scope.sinop2 = [];

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Mes);
      }, $scope.labels_cca2);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Equipos_Ingresados);
      }, $scope.backlog2);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Equipos_Reparados);
      }, $scope.backlog3);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Equipos_esperando);
      }, $scope.sinop2);

      $scope.data_cca2.push($scope.backlog2);
      $scope.data_cca2.push($scope.backlog3);
      $scope.data_cca2.push($scope.sinop2);

    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportTicketRMA3.async($scope.param1).then(function (datos) {
      $scope.labels_cca3 = [];
      $scope.data_cca3 = [];
      angular.forEach(datos.data, function (value, key) {
        this.push(value.Mes);
      }, $scope.labels_cca3);

      angular.forEach(datos.data, function (value, key) {
        this.push(value.Tickets);
      }, $scope.data_cca3);

    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportTicketRMA4.async($scope.param1).then(function (datos) {

      $scope.labels_cca4 = [];
      $scope.data_cca4 = [];

      $scope.data_evaluation = [];
      $scope.data_evaluation2 = [];
      $scope.data_requires = [];
      $scope.data_technical = [];

      angular.forEach(datos.data[0], function (value, key) {
        this.push(value.Mes);
      }, $scope.labels_cca4);

      angular.forEach(datos.data[0], function (value, key) {
        this.push(value.Equipos_en_Bodega);
      }, $scope.data_evaluation);

      angular.forEach(datos.data[1].data2, function (value, key) {
        this.push(value);
      }, $scope.data_evaluation2);

      //  angular.forEach(datos.data, function(value, key) {
      //    this.push(value.requires);
      //  }, $scope.data_requires );

      //  angular.forEach(datos.data, function(value, key) {
      //    this.push(value.technical );
      //  }, $scope.data_technical  );

      $scope.data_cca4.push($scope.data_evaluation);
      $scope.data_cca4.push($scope.data_evaluation2);
      // $scope.data_cca4.push($scope.data_requires);
      // $scope.data_cca4.push($scope.data_technical);

    });

    $scope.param1 = {
      cliente: cliente,
      id_cliente: id_cliente
    }

    GetDataReportTicketCCA.async($scope.param1).then(function (datos) {

      $scope.myData = datos.data;

    });*/
  }



  $scope.gridOptions = {};
  $scope.gridOptions2 = {};
  $scope.gridOptions3 = {};
  $scope.gridVentas = {};



  $scope.myData2 = [{ turno: "0", grupo: "0", carga_extra: "0", carga_normal: "0", carga_total: "0", porcentaje: "0" },
  { turno: "0", grupo: "0", carga_extra: "0", carga_normal: "0", carga_total: "0", porcentaje: "0" }
  ];

  $scope.myData3 = [{ grupo: "0", tonelaje: "0", numero: "0" }, { grupo: "0", tonelaje: "0", numero: "0" }
  ];

  $scope.gridOptions = {
    data: 'myData',
    //enableCellEditOnFocus: true,5555
    //infiniteScrollRowsFromEnd: 4,
    //infiniteScrollUp: true,
    //infiniteScrollDown: true,
    columnDefs: [{ name: 'Status', displayName: 'Status' },
    { name: 'Subject', displayName: 'Subject' },
    { name: 'Assignee', displayName: 'Assignee' },
    { name: 'Hours', displayName: 'Hours' },
    { name: 'Tags', displayName: 'Tags' },
    { name: 'Category', displayName: 'Category' },
    { name: 'Created', displayName: 'Created' }

    ]
  };




  $scope.gridOptions.enableCellEditOnFocus = true;


  $scope.gridOptions2 = {
    data: 'myData2',
    enableCellEditOnFocus: true,
    infiniteScrollRowsFromEnd: 4,
    infiniteScrollUp: true,
    infiniteScrollDown: true,

    columnDefs: [{ name: 'turno', displayName: 'Turno' },
    { name: 'grupo', displayName: 'grupo' },
    { name: 'carga_extra', displayName: 'Carga Extra' },
    { name: 'carga_normal', displayName: 'Carga Normal' },
    { name: 'carga_total', displayName: 'Total Carga' },
    { name: 'porcentaje', displayName: 'Porcentaje' }]
  };

 

  $scope.gridOptions3 = {
    data: 'myData3',
    enableCellEditOnFocus: true,
    infiniteScrollRowsFromEnd: 4,
    infiniteScrollUp: true,
    infiniteScrollDown: true,

    columnDefs: [{ name: 'grupo', displayName: 'Grupo' },
    { name: 'tonelaje', displayName: 'Tonelaje Generado por Ciclos Cortos' },
    { name: 'numero', displayName: 'Numero Ciclos Cortos ' }
    ]
  };

  //$scope.gridOptions.data = $scope.myData;

  // $scope.gridOptions.columnDefs = [{ name: 'indicador', displayName: 'Indicador', width: "*" },
  // { name: 'rango', displayName: 'Rango', width: "*" },
  // { name: 'ponderacion', displayName: 'Ponderacion', width: "*" }]
  //  ;



  //$scope.saveRow = function (rowEntity) {

  //console.log($scope.myData);
  //console.log($scope.myData2);
  // create a fake promise - normally you'd use the promise returned by $http or $resource
  //   var promise = $q.defer();
  //   $scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);


  // };


  //$scope.saveRow2 = function (rowEntity) {

  //console.log($scope.myData);
  //console.log($scope.myData2);
  // create a fake promise - normally you'd use the promise returned by $http or $resource
  // var promise = $q.defer();
  //$scope.gridApi2.rowEdit.setSavePromise(rowEntity, promise.promise);

  // fake a delay of 3 seconds whilst the save occurs, return error if gender is "male"

  //};





  $scope.gridOptions.onRegisterApi = function (gridApi) {
    //set gridApi on scope
    $scope.gridApi = gridApi;

    //gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
  };

  $scope.gridOptions2.onRegisterApi = function (gridApi2) {
    //set gridApi on scope
    // alert();
    $scope.gridApi2 = gridApi2;
    //gridApi2.rowEdit.on.saveRow($scope, $scope.saveRow2);
  };

  $scope.gridOptions3.onRegisterApi = function (gridApi3) {
    //set gridApi on scope
    // alert();
    $scope.gridApi3 = gridApi3;
    //gridApi2.rowEdit.on.saveRow($scope, $scope.saveRow2);
  };


  $scope.addData = function () {
    //var n = $scope.gridOpts.data.length + 1;
    $scope.myData2.push({ turno: " ", grupo: "0", carga_extra_turno: "0", carga_normal: "0", total_carga: "", porcentaje: "" });
  };

  $scope.addData3 = function () {
    //var n = $scope.gridOpts.data.length + 1;
    $scope.myData3.push({ grupo: "0", tonelaje: "0", numero: "0" });
  };

  $scope.reset = function () {
    //var n = $scope.gridOpts.data.length + 1;
    $scope.myData2 = [];
  };

  $scope.reset3 = function () {
    //var n = $scope.gridOpts.data.length + 1;
    $scope.myData3 = [];
  };

  $scope.series1 = ["No Emergencia", "Emergencia"];
  $scope.series1_cca = ["Ticket Creados", "Tickets Resueltos", "Tickets Esperando OC"];
  $scope.series2_cca = ["Equipos_Ingresados", "Equipos_Reparados", "Equipos_esperando OC"];
  $scope.series3_cca = ["Tickets"];
  $scope.series4_cca = ["Equipos en Bodega", "Tendencia", "technical"];

  $scope.chart1_base64 = 'eee';

  Chart.defaults.global.title.fontSize = 24;
  $scope.options1 = {
    title: {
      display: true,
      text: "Llamados de Emergencia "

    },
    elements: {
      line: {
          tension: 0
      }
  },
    legend: { display: true, fontColor: '#fcfeff' },
    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12
    },
    responsive: true,
    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Mes',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Tickets',
          display: true,
        },
        ticks: {
          beginAtZero: true
        }
      }]
    },
    animation: {
      onComplete: function (animation) {
        document.getElementById("chart1").value = animation.chartInstance.toBase64Image();
        //console.log(animation.chartInstance.toBase64Image());//this gives the base64 image
      }
    }

  };

  //console.log($scope.chart1_base64);

  $scope.colorsCCA = [{
    backgroundColor: '#0000ff',
    pointBackgroundColor: '#0000ff',
    pointHoverBackgroundColor: '#0000ff',
    borderColor: '#0000ff',
    pointBorderColor: '#0000ff',
    pointHoverBorderColor: '#0000ff',
    lineTension:0,
    fill: false /* this option hide background-color */
  }, {
    backgroundColor: '#ff0000',
    pointBackgroundColor: '#ff0000',
    pointHoverBackgroundColor: '#ff0000',
    borderColor: '#ff0000',
    pointBorderColor: '#ff0000',
    pointHoverBorderColor: '#ff0000',
    fill: false /* this option hide background-color */
  }, {
    backgroundColor: '#808080',
    pointBackgroundColor: '#808080',
    pointHoverBackgroundColor: '#808080',
    borderColor: '#808080',
    pointBorderColor: '#808080',
    pointHoverBorderColor: '#808080',
    fill: false /* this option hide background-color */
  }];

  $scope.colorsCCA3 = ['#0000ff', '#0000ff', '#0000ff', '#0000ff', '#0000ff'];

  $scope.options1_rma = {


    title: {
      display: true,
      text: "Tickets RMA – Tickets Creados / Resueltos / Esperando Orden de Compra (OC) "
      // fontColor: '#f9fdff'
    },

    elements: {
      line: {
          tension: 0
      }
  },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
	bezierCurve : false,
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Mes',
          //fontColor: '#151515',
          display: true,
        },
        gridLines: {
          color: "rgba(0,0,0,0)"
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Equipos',
          // fontColor: '#151515',
          display: true,
        },
        gridLines: {
          color: "rgba(0,0,0,0)"
        },
        ticks: {
          beginAtZero: true
        }
      }]
    },
    animation: {
      onComplete: function (animation) {
        document.getElementById("chart4").value = animation.chartInstance.toBase64Image();
        //console.log(animation.chartInstance.toBase64Image());//this gives the base64 image
      }
    }
  };


  $scope.options2_rma = {


    title: {
      display: true,
      text: "Equipos RMA – Ingresos vs Egresos vs Esperando Orden de Compra (OC) "
      // fontColor: '#f9fdff'
    },

    elements: {
      line: {
          tension: 0
      }
  },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,
	bezierCurve : false,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Mes',
          //fontColor: '#151515',
          display: true,
        },
        gridLines: {
          color: "rgba(0,0,0,0)"
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Equipos',
          // fontColor: '#151515',
          display: true,
        },
        gridLines: {
          color: "rgba(0,0,0,0)"
        },
        ticks: {
          beginAtZero: true
        }
      }]
    },
    animation: {
      onComplete: function (animation) {
        document.getElementById("chart5").value = animation.chartInstance.toBase64Image();
        //console.log(animation.chartInstance.toBase64Image());//this gives the base64 image
      }
    }
  };

  $scope.options3_cca = {


    title: {
      display: true,
      text: "Tickets Sin Orden de Compra "
      // fontColor: '#f9fdff'
    },

    legend: { display: false, fontColor: '#fcfeff' },

    tooltips: {
      display: false,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Mes',
          //fontColor: '#151515',
          display: true,
        },
        gridLines: {
          color: "rgba(0,0,0,0)"
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Tickets',
          // fontColor: '#151515',
          display: true,
        },
        gridLines: {
          color: "rgba(0,0,0,0)"
        },
        ticks: {
          beginAtZero: true
        }
      }]
    },
    animation: {
      onComplete: function (animation) {
        document.getElementById("chart6").value = animation.chartInstance.toBase64Image();
        //console.log(animation.chartInstance.toBase64Image());//this gives the base64 image
      }
    }
  };

  $scope.options4_cca = {


    title: {
      display: true,
      text: "Equipos de Bodega"
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Mes',
          //fontColor: '#151515',
          display: true,
        },
        gridLines: {
          color: "rgba(0,0,0,0)"
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Equipos',
          // fontColor: '#151515',
          display: true,
        },
        gridLines: {
          color: "rgba(0,0,0,0)"
        },
        ticks: {
          beginAtZero: true
        }
      }]
    },
    animation: {
      onComplete: function (animation) {
        document.getElementById("chart7").value = animation.chartInstance.toBase64Image();
        //console.log(animation.chartInstance.toBase64Image());//this gives the base64 image
      }
    }
  };


  $scope.series2 = ["IntegridadSistema", "Target"];
  $scope.options2 = {


    title: {
      display: true,
      text: " Ticket Creados y Resueltos "
      // fontColor: '#f9fdff'
    },

    elements: {
      line: {
          tension: 0
      }
  },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Mes',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Tickets',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
          beginAtZero: true
        }
      }]
    },
    animation: {
      onComplete: function (animation) {
        document.getElementById("chart2").value = animation.chartInstance.toBase64Image();
        //console.log(animation.chartInstance.toBase64Image());//this gives the base64 image
      }
    }

  };



  $scope.options3 = {


    title: {
      display: true,
      text: "Llamadas a Modular Online (MOL) los ultimos 6 meses "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: true,
        scaleLabel: {
          labelString: 'Mes',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: true,
        scaleLabel: {
          labelString: 'Tickets',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
          beginAtZero: true
        }
      }]
    },
    animation: {
      onComplete: function (animation) {
        document.getElementById("chart3").value = animation.chartInstance.toBase64Image();
        //console.log(animation.chartInstance.toBase64Image());//this gives the base64 image
      }
    }
  };


  $scope.colors = ['#0000ff', '#ff0000', '#808080', '#000000', '#ff0000'];

  $scope.colors2 = ['#0000ff', '#ff0000'];

  $scope.colors3 = ['#008000', '#ff0000', '#808080', '#ffff00', '#000000'];

  $scope.datasetOverride2 = [
    {
      label: "Ticket Resueltos",
      borderWidth: 1,
      type: 'line'
    },
    {
      label: "Ticket Creados",
      borderWidth: 1,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line'
      //fill: false
    }
  ];

  $scope.series3 = ["Llamados", "%SpotMalo/CargaCorrecta", "%SpotCorrecto/CargaMala", "%SpotMalo/CargaMalo", "%Spot/CargaNulo"];


  $scope.datasetOverride4 = [
    {
      label: "Grupo 1",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 2",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 3",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 4",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      borderWidth: 3,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    },
    {
      label: "Target [>68%]",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      type: 'line',
      fill: false
    }
  ];


  $scope.datasetOverride_rma4 = [
    {

      label: "Equipos en Bodega",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      type: 'line',
      fill: false
    }
  ];

  $scope.options4 = {


    title: {
      display: true,
      text: "Integridad (Spot & Load) Por Grupo "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Mes',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Spot & Load [%]',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };

  $scope.options5 = {


    title: {
      display: true,
      text: " BenchMark Integridad Spot & Load "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Clientes Performance Assurance',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Spot % Load [%] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };



  $scope.datasetOverride5 = [
    {
      label: "Integridad [%]",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Target [>68%]",
      borderWidth: 1,

      type: 'line',
      fill: false
    }
  ];


  $scope.options6 = {


    title: {
      display: true,
      text: " Uso Dinamico por Grupo "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Mes',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Uso Dinamico [%] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };


  $scope.datasetOverride6 = [
    {
      label: "Grupo 1",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 2",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 3",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 4",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      //borderWidth: 1,
      //borderDash: [5, 5],
      pointBorderWidth: 1,

      type: 'line',
      fill: false
    },
    {
      label: "Target[>80%]",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      //hoverBackgroundColor: "rgba(0, 0, 0, 0)",
      //hoverBorderColor: "rgba(0, 0, 0, 0)",
      type: 'line',
      fill: false
    }
  ];

  $scope.datasetOverride7 = [
    {
      label: "Uso Dinamico [%]",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Taget [>80%]",
      borderWidth: 1,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    }
  ];


  $scope.options7 = {


    title: {
      display: true,
      text: " BenchMark Uso Dinamico  "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Clientes Performance Assurance',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Uso Dinamico [%] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };


  $scope.datasetOverride8 = [
    {
      label: "Grupo 1",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 2",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 3",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 4",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      borderWidth: 1,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    },
    {
      label: "Target [<0.5%]",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      type: 'line',
      fill: false
    }
  ];

  $scope.options8 = {


    title: {
      display: true,
      text: " Ciclos Cortos por Grupo  "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Mes',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Ciclos Cortos [%] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };


  $scope.datasetOverride9 = [
    {
      label: "Grupo 1",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 2",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 3",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 4",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      borderWidth: 1,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    },
    {
      label: "Target [<2%]",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      type: 'line',
      fill: false
    }
  ];


  $scope.options9 = {


    title: {
      display: true,
      text: " Cargas Extras por grupo  "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Mes',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Cargas Extras  [%] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };


  $scope.datasetOverride10 = [
    {
      label: "Grupo 1",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 2",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 3",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 4",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      borderWidth: 1,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    },
    {
      label: "Target[<5]",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      type: 'line',
      fill: false
    }
  ];


  $scope.options10 = {


    title: {
      display: true,
      text: " Balizas Perdidas por grupo  "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Mes',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Balizas Perdidas  [#] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };

  $scope.datasetOverride11 = [
    {
      label: "Grupo 1",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 2",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 3",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 4",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      borderWidth: 1,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    },
    {
      label: "Target [<10%]",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      type: 'line',
      fill: false
    }
  ];


  $scope.options11 = {


    title: {
      display: true,
      text: " Llegadas Tarde por grupo  "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Mes',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Llegadas Tarde  [%] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };



  //$scope.data = [
  //  [65, 59, 80, 81, 56, 55, 40],
  //  [28, 48, 40, 19, 86, 27, 90]
  //];

  this.settings = {
    name: 'settings',
    items: [
      'Home',
      'About',
      'Contact'
    ]
  };
  this.favorite = {
    name: 'favorite',
    items: [
      'Add to Favorites'
    ]
  };
  this.more = {
    name: 'more',
    items: [
      'Account',
      'Sign Out'
    ]
  };
  this.tools = {
    name: 'tools',
    items: [
      'Create',
      'Delete'
    ]
  };
  this.code = {
    name: 'code',
    items: [
      'See Source',
      'See Commits'
    ]
  };



  this.menuTemplate = '' +
    '<div class="menu-panel" md-whiteframe="4">' +
    '  <div class="menu-content">' +
    '    <div class="menu-item" ng-repeat="item in ctrl.items">' +
    '      <button class="md-button">' +
    '        <span>{{item}}</span>' +
    '      </button>' +
    '    </div>' +
    '    <md-divider></md-divider>' +
    '    <div class="menu-item">' +
    '      <button class="md-button" ng-click="ctrl.closeMenu()">' +
    '        <span>Close Menu</span>' +
    '      </button>' +
    '    </div>' +
    '  </div>' +
    '</div>';

  $mdPanel.newPanelGroup('toolbar', {
    maxOpen: 2
  });

  $mdPanel.newPanelGroup('menus', {
    maxOpen: 3
  });

  $scope.SaveAll = function () {

    //$scope.status = 'You decided to get rid of your debt.';

    var grafico1 = angular.element(document.getElementById("chart1"));
    var grafico2 = angular.element(document.getElementById("chart2"));
    var grafico3 = angular.element(document.getElementById("chart3"));
    var grafico4 = angular.element(document.getElementById("chart4"));
    var grafico5 = angular.element(document.getElementById("chart5"));
    var grafico6 = angular.element(document.getElementById("chart6"));
    var grafico7 = angular.element(document.getElementById("chart7"));
    var grafico8 = angular.element(document.getElementById("chart8"));
    var grafico9 = angular.element(document.getElementById("chart9"));
    var grafico10 = angular.element(document.getElementById("chart10"));
    var grafico11 = angular.element(document.getElementById("chart11"));
    var grafico12 = angular.element(document.getElementById("chart12"));
    var grafico13 = angular.element(document.getElementById("chart13"));
    var grafico14 = angular.element(document.getElementById("chart14"));
    var grafico15 = angular.element(document.getElementById("chart15"));
    var grafico16 = angular.element(document.getElementById("chart16"));
    var grafico17 = angular.element(document.getElementById("chart17"));
    var grafico18 = angular.element(document.getElementById("chart18"));
    var grafico19 = angular.element(document.getElementById("chart19"));
    var grafico20 = angular.element(document.getElementById("chart20"));
    var grafico21 = angular.element(document.getElementById("chart21"));
    var grafico22 = angular.element(document.getElementById("chart22"));
    var grafico23 = angular.element(document.getElementById("chart23"));
    var grafico24 = angular.element(document.getElementById("chart24"));
    var grafico25 = angular.element(document.getElementById("chart25"));




    $scope.chr1_base64 = grafico1.val();
    $scope.chr1_base64 = $scope.chr1_base64.substr(22, $scope.chr1_base64.length);

    $scope.chr2_base64 = grafico2.val();
    $scope.chr2_base64 = $scope.chr2_base64.substr(22, $scope.chr2_base64.length);

    $scope.chr3_base64 = grafico3.val();
    $scope.chr3_base64 = $scope.chr3_base64.substr(22, $scope.chr3_base64.length);

    $scope.chr4_base64 = grafico4.val();
    $scope.chr4_base64 = $scope.chr4_base64.substr(22, $scope.chr4_base64.length);

    $scope.chr5_base64 = grafico5.val();
    $scope.chr5_base64 = $scope.chr5_base64.substr(22, $scope.chr5_base64.length);

    $scope.chr6_base64 = grafico6.val();
    $scope.chr6_base64 = $scope.chr6_base64.substr(22, $scope.chr6_base64.length);

    $scope.chr7_base64 = grafico7.val();
    $scope.chr7_base64 = $scope.chr7_base64.substr(22, $scope.chr7_base64.length);


    $scope.chr13_base64 = grafico13.val();
    $scope.chr13_base64 = $scope.chr13_base64.substr(22, $scope.chr13_base64.length);

    $scope.chr14_base64 = grafico14.val();
    $scope.chr14_base64 = $scope.chr14_base64.substr(22, $scope.chr14_base64.length);

    $scope.chr15_base64 = grafico15.val();
    $scope.chr15_base64 = $scope.chr15_base64.substr(22, $scope.chr15_base64.length);

    $scope.chr16_base64 = grafico16.val();
    $scope.chr16_base64 = $scope.chr16_base64.substr(22, $scope.chr16_base64.length);

    $scope.chr17_base64 = grafico17.val();
    $scope.chr17_base64 = $scope.chr17_base64.substr(22, $scope.chr17_base64.length);

    $scope.chr18_base64 = grafico18.val();
    $scope.chr18_base64 = $scope.chr18_base64.substr(22, $scope.chr18_base64.length);

    $scope.chr19_base64 = grafico19.val();
    $scope.chr19_base64 = $scope.chr19_base64.substr(22, $scope.chr19_base64.length);

    $scope.chr20_base64 = grafico20.val();
    $scope.chr20_base64 = $scope.chr20_base64.substr(22, $scope.chr20_base64.length);

    $scope.chr21_base64 = grafico21.val();
    $scope.chr21_base64 = $scope.chr21_base64.substr(22, $scope.chr21_base64.length);

    $scope.chr22_base64 = grafico22.val();
    $scope.chr22_base64 = $scope.chr22_base64.substr(22, $scope.chr22_base64.length);

    $scope.chr23_base64 = grafico23.val();
    $scope.chr23_base64 = $scope.chr23_base64.substr(22, $scope.chr23_base64.length);

    $scope.chr24_base64 = grafico24.val();
    $scope.chr24_base64 = $scope.chr24_base64.substr(22, $scope.chr24_base64.length);

    $scope.chr25_base64 = grafico25.val();
    $scope.chr25_base64 = $scope.chr25_base64.substr(22, $scope.chr25_base64.length);


    $scope.chr8_base64 = grafico8.val();
    $scope.chr9_base64 = grafico9.val();
    $scope.chr10_base64 = grafico10.val();
    $scope.chr11_base64 = grafico11.val();
    $scope.chr12_base64 = grafico12.val();


    $scope.param1 = {
      idcliente: $scope.user.id,
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      intro: $scope.intro,
      intro2: $scope.intro2,
      intro3: $scope.intro3,
      intro4: $scope.intro4,
      intro5: $scope.intro5,
      intro6: $scope.intro6
    }




    InsertDataReportMensual.async($scope.param1).then(function (datos) {
    })

    InsertDataReportDirtyMensual.async($scope.param1).then(function (datos) {
    })

    /* Insert de todos los graficos */

    $scope.param = {
      idcliente: $scope.user.id,
      image: '1',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr1_base64
    }
    InsertDataReportImagesMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '2',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr2_base64
    }
    InsertDataReportImagesMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '3',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr3_base64
    }
    InsertDataReportImagesMensual.async($scope.param).then(function (datos) {
    });


    $scope.param = {
      idcliente: $scope.user.id,
      image: '4',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr4_base64
    }
    InsertDataReportImagesMensual.async($scope.param).then(function (datos) {
    });

    $scope.param = {
      idcliente: $scope.user.id,
      image: '5',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr5_base64
    }
    InsertDataReportImagesMensual.async($scope.param).then(function (datos) {
    });

    $scope.param = {
      idcliente: $scope.user.id,
      image: '6',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr6_base64
    }
    InsertDataReportImagesMensual.async($scope.param).then(function (datos) {
    });


    $scope.param = {
      idcliente: $scope.user.id,
      image: '7',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr7_base64
    }
    InsertDataReportImagesMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '8',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr8_base64
    }
    InsertDataReportImagesMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '9',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr9_base64
    }
    InsertDataReportImagesMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '10',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr10_base64
    }
    InsertDataReportImagesMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '11',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr11_base64
    }
    InsertDataReportImagesMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '12',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr12_base64
    }
    InsertDataReportImagesMensual.async($scope.param).then(function (datos) {
    })

    /********************************************************************** */

     /* Insert de todos los graficos */

    $scope.param = {
      idcliente: $scope.user.id,
      image: '13',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr13_base64,
      intro: $scope.intro2_spotload
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '14',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr14_base64,
      intro: $scope.intro2_spotload
      
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '15',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr15_base64,
      intro: $scope.intro2_usodinamico
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    });


    $scope.param = {
      idcliente: $scope.user.id,
      image: '16',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr16_base64,
      intro: $scope.intro2_usodinamico
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    });

    $scope.param = {
      idcliente: $scope.user.id,
      image: '17',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr17_base64,
      intro: $scope.intro2_perdida_1
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    });

    $scope.param = {
      idcliente: $scope.user.id,
      image: '18',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr18_base64,
      intro: $scope.intro2_perdida_1
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    });


    $scope.param = {
      idcliente: $scope.user.id,
      image: '19',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr19_base64,
      intro: $scope.intro2_perdida_2
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '20',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr20_base64,
      intro: $scope.intro2_perdida_2
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '21',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr21_base64,
      intro: $scope.intro2_perdida_3
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '22',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr22_base64,
      intro: $scope.intro2_perdida_3
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '23',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr23_base64,
      intro: $scope.intro2_perdida_4
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '24',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr24_base64,
      intro: $scope.intro2_perdida_4
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente: $scope.user.id,
      image: '25',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr25_base64,
      intro: $scope.intro2_moviminento
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    })

    /*$scope.param = {
      idcliente: $scope.user.id,
      image: '13',
      mes: $scope.mes.id,
      ano: $scope.ano.id,
      file1: $scope.chr12_base64
    }
    InsertDataImagesDirtyMensual.async($scope.param).then(function (datos) {
    })*/








    $scope.showSimpleToast();
    $scope.active_print = false;

  };


  $scope.user = null;
  $scope.users = null;
  $scope.mes = null;
  $scope.meses = null;

  $scope.ChangeMes = function () {

    cleanObject();

    $scope.param_mes = {
      mes: $scope.mes.id
    }
    $scope.param = {
      idcliente: $scope.user.id,
      mes: $scope.mes.id,
      ano: $scope.ano.id
    }

    GetDataValidateMensual.async($scope.param).then(function (datosValidacion) {

      initializeChart($scope.user.name, $scope.user.id);


      $timeout(function () {

        $scope.colorreply = 'md-primary';
        $scope.color_msg = {color:'primary'};
        $scope.msg_validaction = "Validacion Sin Problemas";
        $scope.selectedIndex = 1;
        $scope.active_print = true;
        $scope.active_ingreso = false;
        $scope.active_resumen = false;
        $scope.active_dirty = false;
        $scope.active_cca = false;
        $scope.active_rma = false;
        $scope.active_ventas = false;
        $scope.active_finanzas = false
      }
        , 4000);

      $scope.intro = datosValidacion.data[0].Intro;
      $scope.intro2 = datosValidacion.data[0].Intro2;
      $scope.intro3 = datosValidacion.data[0].Intro3;
      $scope.intro4 = datosValidacion.data[0].Intro4;
      $scope.intro5 = datosValidacion.data[0].Intro5;
      $scope.intro6 = datosValidacion.data[0].Intro6;

      //console.log(">>>>>>>>>>>>>>>>>>> ENTRA ");
      //console.log(datosValidacion.data[0].Intro7);
      $scope.intro2_spotload  = datosValidacion.data[0].Intro7;
      $scope.intro2_usodinamico = datosValidacion.data[0].Intro8;
      //console.log(datosValidacion.data[0].Intro8);
      $scope.intro2_perdida_1 = datosValidacion.data[0].Intro9;
      //console.log(datosValidacion.data[0].Intro9);
      $scope.intro2_perdida_2 = datosValidacion.data[0].Intro10;
      //console.log(datosValidacion.data[0].Intro10);
      $scope.intro2_perdida_3 = datosValidacion.data[0].Intro11;
      $scope.intro2_perdida_4 = datosValidacion.data[0].Intro12;
      $scope.intro2_moviminento = datosValidacion.data[0].Intro13;

    })

    $scope.refreshImageDirtyDozen();
  };

  $scope.ChangeYear = function () {

    cleanObject();

    $scope.param_years = {
      ano: $scope.ano.id
    }


    $scope.param = {
      idcliente: 'mlb',
      mes: '07',
      ano: '2018'
    }

    GetDataValidateMensual.async($scope.param).then(function (datosValidacion) {

      initializeChart($scope.user.name, $scope.user.id);
      console.log($scope.user.name);
      console.log($scope.user.id);
      $timeout(function () {

        $scope.colorreply = 'md-primary';
        $scope.msg_validaction = "Validacion Sin Problemas";
        $scope.selectedIndex = 1;
        $scope.active_print = true;
        $scope.active_ingreso = false;
        $scope.active_resumen = false;
        $scope.active_dirty = false;
        $scope.active_cca = false;
        $scope.active_rma = false;
        $scope.active_ventas = false;
        $scope.active_finanzas = false
      }
        , 2000);

      $scope.intro = datosValidacion.data[0].Intro;
      $scope.intro2 = datosValidacion.data[0].Intro2;
      $scope.intro3 = datosValidacion.data[0].Intro3;
      $scope.intro4 = datosValidacion.data[0].Intro4;
      $scope.intro5 = datosValidacion.data[0].Intro5;
      $scope.intro6 = datosValidacion.data[0].Intro6;
      $scope.intro2_spotload  = datosValidacion.data[0].Intro7;
      $scope.intro2_usodinamico = datosValidacion.data[0].Intro8;
      //console.log(datosValidacion.data[0].Intro8);
      $scope.intro2_perdida_1 = datosValidacion.data[0].Intro9;
      //console.log(datosValidacion.data[0].Intro9);
      $scope.intro2_perdida_2 = datosValidacion.data[0].Intro10;
      //console.log(datosValidacion.data[0].Intro10);
      $scope.intro2_perdida_3 = datosValidacion.data[0].Intro11;
      $scope.intro2_perdida_4 = datosValidacion.data[0].Intro12;
      $scope.intro2_moviminento = datosValidacion.data[0].Intro13;

    })

    $scope.refreshImageDirtyDozen();

    $scope.param = {
      cliente: $scope.user.id,
      mes: $scope.mes.id,
      ano: $scope.ano.id
    }

    

    $scope.encabezados = [];
    $scope.data_ventas = [];


    GetDataFileVentaFinanzas.async($scope.param).then(function (datosValidacion) {
      for (var i=0; i< datosValidacion.data[0][0].data[0].length ; i++){
        $scope.encabezados.push({ name: datosValidacion.data[0][0].data[0][i].replace(' ', '_'), displayName: datosValidacion.data[0][0].data[0][i]});
      }
      for (var x=0; x < datosValidacion.data[0][0].data.length; x++){
      for (var a=0; a< datosValidacion.data[0][0].data[0].length; a++){
      //for (var b=0; b< datosValidacion.data[0][0].data[a][b].length; b++){
        // Para Recorrer la hoja excele Se debe variar los valores de 
        $scope.myData2 = datosValidacion.data[0][0].data[x][a] ;


      }console.log('-----------------------------------------------------');}
      //}}

    })


   // $scope.myData2 = [{ turno: "0", grupo: "0", carga_extra: "0", carga_normal: "0", carga_total: "0", porcentaje: "0" },
  //{ turno: "0", grupo: "0", carga_extra: "0", carga_normal: "0", carga_total: "0", porcentaje: "0" }
  //];

    $scope.gridVentas = {
      data: 'myData2',
      enableCellEditOnFocus: false,
      infiniteScrollRowsFromEnd: 4,
      infiniteScrollUp: true,
      infiniteScrollDown: true,
      columnDefs:  $scope.encabezados
    };

  };



  $scope.ChangeCliente = function () {

    cleanObject();

    $scope.param_ = {
      cliente: $scope.user.id
    }

    $scope.param = {
      idcliente: $scope.user.id,
      mes: $scope.mes.id,
      ano: $scope.ano.id
    }

    GetDataValidateMensual.async($scope.param).then(function (datosValidacion) {

      initializeChart($scope.user.name, $scope.user.id);


      $timeout(function () {

        $scope.colorreply = 'md-primary';
        $scope.msg_validaction = "Validacion Sin Problemas";
        $scope.selectedIndex = 1;
        $scope.active_print = true;
        $scope.active_ingreso = false;
        $scope.active_resumen = false;
        $scope.active_dirty = false;
        $scope.active_cca = false;
        $scope.active_rma = false;
        $scope.active_ventas = false;
        $scope.active_finanzas = false
      }
        , 2000);

      $scope.intro = datosValidacion.data[0].Intro;
      $scope.intro2 = datosValidacion.data[0].Intro2;
      $scope.intro3 = datosValidacion.data[0].Intro3;
      $scope.intro4 = datosValidacion.data[0].Intro4;
      $scope.intro5 = datosValidacion.data[0].Intro5;
      $scope.intro6 = datosValidacion.data[0].Intro6;
      $scope.intro2_spotload  = datosValidacion.data[0].Intro7;
      $scope.intro2_usodinamico = datosValidacion.data[0].Intro8;
      //console.log(datosValidacion.data[0].Intro8);
      $scope.intro2_perdida_1 = datosValidacion.data[0].Intro9;
      //console.log(datosValidacion.data[0].Intro9);
      $scope.intro2_perdida_2 = datosValidacion.data[0].Intro10;
      //console.log(datosValidacion.data[0].Intro10);
      $scope.intro2_perdida_3 = datosValidacion.data[0].Intro11;
      $scope.intro2_perdida_4 = datosValidacion.data[0].Intro12;
      $scope.intro2_moviminento = datosValidacion.data[0].Intro13;

    })

    $scope.refreshImageDirtyDozen();

  }

  $scope.refreshImageDirtyDozen = function (){

    $scope.param = {
      idcliente: $scope.user.id,
      mes: $scope.mes.id,
      ano: $scope.ano.id
    }


    GetDataChartDirtyDozen.async($scope.param).then(function (datosdirtydozen) {
        
      $scope.myFile1 = {base64 : datosdirtydozen.data[0].File8 };
      $scope.myFile2 = {base64 : datosdirtydozen.data[0].File9 };
      $scope.myFile3 = {base64 : datosdirtydozen.data[0].File10 };
      $scope.myFile4 = {base64 : datosdirtydozen.data[0].File11 };
      $scope.myFile5 = {base64 : datosdirtydozen.data[0].File12 };
  })

  };

  $scope.showConfirm = function (ev) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
      .title('Confirmación')
      .textContent('Se Enviaran los Datos Correpondiente al Informe Mensual de ' + $scope.user.name)
      .ariaLabel('Lucky day')
      .targetEvent(ev)
      .ok('Save')
      .cancel('Cancel');

    $mdDialog.show(confirm).then(function () {
      //$scope.status = 'You decided to get rid of your debt.';
      $scope.SaveAll();
      /*var grafico1 = angular.element(document.getElementById("chart1"));
      var grafico2 = angular.element(document.getElementById("chart2"));
      var grafico3 = angular.element(document.getElementById("chart3"));
      var grafico4 = angular.element(document.getElementById("chart4"));
      var grafico5 = angular.element(document.getElementById("chart5"));
      var grafico6 = angular.element(document.getElementById("chart6"));
      var grafico7 = angular.element(document.getElementById("chart7"));
      console.log($scope.ano.id);
      console.log($scope.mes.id);*/
    }, function () {
      //$scope.status = 'You decided to keep your debt.';
    });
  };

  $scope.foo = function () {



    $scope.param = {
      cliente: $scope.user.id,
      mes: $scope.mes.id,
      ano: $scope.ano.id
    }

    GetInformationKPIMensual.async($scope.param).then(function (datosValidacion) {
        if (datosValidacion.code === 'ENOENT'){
          $scope.color_msg = {color:'red'};
          $scope.msg_validaction = "Debe Subir los archivos de Ventas y Finanzas";
          $scope.selectedIndex = 1;

        }else{
          $window.open('http://192.168.12.64:4000/restservicemodular/getReportServerMensual?id=' + $scope.user.id + '&mes=' + $scope.mes.id + '&ano=' + $scope.ano.id, 'C-Sharpcorner', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=650, height=550,left = 300,top=100');
        }
    });

    //$scope.isLoading = true;
    //$scope.content = '';
    //$scope.content = $sce.trustAsResourceUrl('http://localhost:3000/restservicemantos/getReportServer?id=' + $scope.user.id);
    

    //$timeout(countUp, 5000);
    //$scope.isLoading = false;


  };

  $scope.loadUsers = function () {

    // Use timeout to simulate a 650ms request.
    return $timeout(function () {

      $scope.users = $scope.users || [


        { id: 'dand', name: 'Andina' },
        // { id: 'sand', name: 'Andina Subterranea' },
        { id: 'mlc', name: 'Candelaria' },
        { id: 'cmcc', name: 'Cerro Colorado' },
        //{ id: 'cvg', name: 'Cerro Vanguardia' },
        { id: 'cmdic', name: 'Collahuasi' },
        { id: 'scma', name: 'El Abra' },
        // { id: 'det', name: 'El Teniente' },
        { id: 'mel', name: 'Escondida' },
        //{ id: 'exss', name: 'Excon Toconao' },
        { id: 'gaby', name: 'Gaby' },
        { id: 'cmh', name: 'Huasco' },
        //{ id: 'kch', name: 'Komatsu Chile (MLP)' },
        { id: 'mlb', name: 'Lomas Bayas' },
        { id: 'dlb', name: 'Los Bronces' },
        { id: 'mlp', name: 'Los Pelambres' },
        { id: 'dmbl', name: 'Mantos Blancos' },
        { id: 'smir', name: 'Invierno Mine' },
        // { id: 'mms-chile', name: 'MMS-Chile' },
        // { id: 'nnn', name: 'Nueva Venta' },
        { id: 'msc', name: 'San Cristobal' },
        { id: 'cms', name: 'Spence' },
        { id: 'cmz', name: 'Zaldivar' },
        { id: 'sqm', name: 'SQM' }
      ];

    }, 650);
  };

  $scope.loadyear = function () {
    return $timeout(function () {
      $scope.anos = $scope.anos || [
        { id: '2017', name: '2017' },
        { id: '2018', name: '2018' },
        { id: '2019', name: '2019' },
        { id: '2020', name: '2020' },
        { id: '2021', name: '2021' },
        { id: '2022', name: '2022' },
        { id: '2023', name: '2023' },
        { id: '2024', name: '2024' },
        { id: '2025', name: '2025' },
        { id: '2026', name: '2026' },
        { id: '2027', name: '2027' },
        { id: '2028', name: '2028' }
      ];

    }, 650)
  };

  $scope.loadMountly = function () {
    return $timeout(function () {
      $scope.meses = $scope.meses || [
        { id: '1', name: 'Enero' },
        { id: '2', name: 'Febrero' },
        { id: '3', name: 'Marzo' },
        { id: '4', name: 'Abril' },
        { id: '5', name: 'Mayo' },
        { id: '6', name: 'Junio' },
        { id: '7', name: 'Julio' },
        { id: '8', name: 'Agosto' },
        { id: '9', name: 'Septiembre' },
        { id: '10', name: 'Octubre' },
        { id: '11', name: 'Noviembre' },
        { id: '12', name: 'Diciembre' }
      ];

    }, 650);
  };

  this.showToolbarMenu = function ($event, menu) {
    var template = this.menuTemplate;

    var position = $mdPanel.newPanelPosition()
      .relativeTo($event.srcElement)
      .addPanelPosition(
      $mdPanel.xPosition.ALIGN_START,
      $mdPanel.yPosition.BELOW
      );

    var config = {
      id: 'toolbar_' + menu.name,
      attachTo: angular.element(document.body),
      controller: PanelMenuCtrl,
      controllerAs: 'ctrl',
      template: template,
      position: position,
      panelClass: 'menu-panel-container',
      locals: {
        items: menu.items
      },
      openFrom: $event,
      focusOnOpen: false,
      zIndex: 100,
      propagateContainerEvents: true,
      groupName: ['toolbar', 'menus']
    };

    $mdPanel.open(config);
  };

  this.showContentMenu = function ($event, menu) {
    var template = this.menuTemplate;

    var position = $mdPanel.newPanelPosition()
      .relativeTo($event.srcElement)
      .addPanelPosition(
      $mdPanel.xPosition.ALIGN_START,
      $mdPanel.yPosition.BELOW
      );

    var config = {
      id: 'content_' + menu.name,
      attachTo: angular.element(document.body),
      controller: PanelMenuCtrl,
      controllerAs: 'ctrl',
      template: template,
      position: position,
      panelClass: 'menu-panel-container',
      locals: {
        items: menu.items
      },
      openFrom: $event,
      focusOnOpen: false,
      zIndex: 100,
      propagateContainerEvents: true,
      groupName: 'menus'
    };

    $mdPanel.open(config);
  };
}




//$scope.currentNavItem = 'page1';
function PanelMenuCtrl(mdPanelRef) {
  this.closeMenu = function () {
    mdPanelRef && mdPanelRef.close();
  }
}
