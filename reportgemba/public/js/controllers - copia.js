//angular.module( 'YourApp', [ 'ngMaterial' ] )


angular
  .module('YourApp', ['angularGoogleMapsExample.services', 'ngMaterial', 'chart.js', 'ngMessages', 'naif.base64', 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.cellNav', 'ui.grid.infiniteScroll', 'ui.grid.autoResize'])

  .controller('gridListDemoCtrl', function ($scope) {

    this.tiles = buildGridModel({
      icon: "avatar:svg-",
      title: "Svg-",
      background: ""
    });



    function buildGridModel(tileTmpl) {
      var it, results = [];

      for (var j = 0; j < 11; j++) {

        it = angular.extend({}, tileTmpl);
        it.icon = it.icon + (j + 1);
        it.title = it.title + (j + 1);
        it.span = { row: 1, col: 1 };

        switch (j + 1) {
          case 1:
            it.background = "red";
            it.span.row = it.span.col = 2;
            break;

          case 2: it.background = "green"; break;
          case 3: it.background = "darkBlue"; break;
          case 4:
            it.background = "blue";
            it.span.col = 2;
            break;

          case 5:
            it.background = "yellow";
            it.span.row = it.span.col = 2;
            break;

          case 6: it.background = "pink"; break;
          case 7: it.background = "darkBlue"; break;
          case 8: it.background = "purple"; break;
          case 9: it.background = "deepBlue"; break;
          case 10: it.background = "lightPurple"; break;
          case 11: it.background = "yellow"; break;
        }

        results.push(it);
      }
      return results;
    }
  })

  

  




  .controller('PanelGroupsCtrl', PanelGroupsCtrl)

.directive('apsUploadFile', ['$interval', 'dateFilter', function($interval, dateFilter) {

  var directive = {
    restrict: 'E',
    template: '<input id="fileInput" type="file" class="ng-hide" ng-model="fileName2" base-sixty-four-input> <md-button id="uploadButton" class="md-raised md-primary" aria-label="attach_file">    Choose file </md-button><md-input-container  md-no-float>    <input id="textInput" ng-model="fileName" type="text" placeholder="No file chosen" ng-readonly="true" ></md-input-container>',
    link: apsUploadFileLink
  };

  function apsUploadFileLink(scope, element, attrs) {
  var input = $(element[0].querySelector('#fileInput'));
  var button = $(element[0].querySelector('#uploadButton'));
  var textInput = $(element[0].querySelector('#textInput'));

  if (input.length && button.length && textInput.length) {
    button.click(function(e) {
      input.click();
    });
    textInput.click(function(e) {
      input.click();
    });
  }

  input.on('change', function(e) {
    var files = e.target.files;
    var to =  e.target;
    
    if (files[0]) {
      scope.fileName = files[0].name;
    } else {
      scope.fileName = null;
    }
    scope.$apply();
  });
}

return directive;

}])


  //.directive('apsUploadFile', function ())

  
  .controller('PanelMenuCtrl', PanelMenuCtrl);
  
  
//  function apsUploadFile() {
//  var directive = {
//    restrict: 'E',
//    template: '<input id="fileInput" type="file" base-sixty-four-input class="ng-hide"> <md-button id="uploadButton" class="md-raised md-primary" aria-label="attach_file">    Choose file </md-button><md-input-container  md-no-float>    <input id="textInput" ng-model="fileName" type="text" placeholder="No file chosen" ng-readonly="true"></md-input-container>',
//    link: apsUploadFileLink
//  };
//  return directive;
//}




function PanelGroupsCtrl($mdPanel, $scope, $timeout, $q, $interval, $mdToast, $window, GetDataReport1, GetDataReport2, GetDataReport3, GetDataReport4, GetDataReport5, GetDataReport6, GetDataReport7, GetDataReport8, GetDataReport9, GetDataReport10, GetDataReport11, InsertDataReport, GetPdfReport, InsertDataReportImages, $sce, $rootScope,$mdDialog, GetDataInput,GetDataValidate) {

  var last = {
    bottom: false,
    top: true,
    left: false,
    right: true
  };

  $scope.active_print = true;
  $scope.active_ingreso = true;
  $scope.active_resumen = true;
  $scope.isDisabled = true;

  $scope.file1 = {};
  $scope.file2 = {};
  $scope.file3 = {};


  $scope.toastPosition = angular.extend({}, last);

  $scope.getToastPosition = function () {
    sanitizePosition();

    return Object.keys($scope.toastPosition)
      .filter(function (pos) { return $scope.toastPosition[pos]; })
      .join(' ');
  };


  $scope.onChange = function (e, fileList) {
    //alert('this is on-change handler!');
  };

  $scope.onLoad = function (e, reader, file, fileList, fileOjects, fileObj) {
    //alert($scope.file1.base64);
  };

  function sanitizePosition() {
    var current = $scope.toastPosition;

    if (current.bottom && last.top) current.top = false;
    if (current.top && last.bottom) current.bottom = false;
    if (current.right && last.left) current.left = false;
    if (current.left && last.right) current.right = false;

    last = angular.extend({}, current);
  }

  $scope.showSimpleToast = function () {
    var pinTo = $scope.getToastPosition();

    $mdToast.show(
      $mdToast.simple()
        .textContent('Grabado Exitoso ...!')
        .position(pinTo)
        .hideDelay(3000)
        .theme('error-toast')
      //.colors('red')
    );
  };

  $scope.showActionToast = function () {
    var pinTo = $scope.getToastPosition();
    var toast = $mdToast.simple()
      .textContent('Marked as read')
      .action('UNDO')
      .highlightAction(true)
      .highlightClass('md-accent')// Accent is used by default, this just demonstrates the usage.
      .position(pinTo);

    $mdToast.show(toast).then(function (response) {
      if (response == 'ok') {
        alert('You clicked the \'UNDO\' action.');
      }
    });
  };

  $scope.series = ['Series A', 'Series B'];


  var cleanObject = function(){

         //$scope.intro = "";
         $scope.estado1 = "";
         $scope.comentario1 = "";
         $scope.estado2 = "";
         $scope.comentario2 = "";
         $scope.estado3 = "";
         $scope.comentario3 = "";
         $scope.estado4 = "";
         $scope.comentario4 = "";
         $scope.estado5 = "";
         $scope.comentario5 = "";
         $scope.estado6 = "";
         $scope.comentario6 = "";
         $scope.estado7 = "";
         $scope.comentario7 = "";
         $scope.estado8 = "";
         $scope.comentario8 = "";
         $scope.estado9 = "";
         $scope.comentario9 = "";
         $scope.estado10 = "";
         $scope.comentario10 = "";
         $scope.estado11 = "";
         $scope.comentario11 = "";
         $scope.estado12 = "";
         $scope.comentario12 = "";
         $scope.estado13 = "";
         $scope.comentario13 = "";
         $scope.estado14 = "";
         $scope.comentario14 = "";
         $scope.estado15 = "";
         $scope.comentario15 = "";
         $scope.estado16 = "";
         $scope.comentario16 = "";
         $scope.file1.base64 = "";
         $scope.file2.base64 = "";
         $scope.file3.base64 = "";

         $scope.myData = [{ indicador: "Integridad (Spot % Load) [%]", rango: "[68-100%]", ponderacion: "0.3" },
                          { indicador: "Llegadas Tarde [%]", rango: "[0-10%[", ponderacion: "0.175" },
                          { indicador: "Balizas Perdidas [#]", rango: "[0-5[", ponderacion: "0.175" },
                          { indicador: "Uso Dinamico  [%]", rango: "[68-100%]", ponderacion: "0" },                          
                          { indicador: "Cargas Extras  [%]", rango: "[0-2%[", ponderacion: "0.175" },                         
                          { indicador: "Ciclos Cortos [%]", rango: "[0-0.5%[", ponderacion: "0.175" }];

  $scope.myData2 = [{ turno: "0", grupo: "0", carga_extra: "0", carga_normal: "0", carga_total: "0", porcentaje: "0" },
  { turno: "0", grupo: "0", carga_extra: "0", carga_normal: "0", carga_total: "0", porcentaje: "0" }
  ];

  $scope.myData3 =[ { grupo:"0",tonelaje:"0",numero:"0" },{ grupo:"0",tonelaje:"0",numero:"0" }
           ];

  }

  $scope.data = {
    cb1: true,
    cb4: true,
    cb5: false
  };

  $scope.message = 'false';

  $scope.onChangeInforme = function(cbState) {
  	$scope.message = cbState;
  };

  var initializeChart = function (cliente) {

    $scope.param1 = {
      cliente: cliente
    }
    


    GetDataReport1.async($scope.param1).then(function (datos) {


      $scope.alldata = {};
      $scope.data1 = [];
      $scope.series = [];
      $scope.labels = [];

      if (datos === 0) {
        $scope.dataall = $rootScope.report1;
      } else {
        $scope.dataall = datos;
        $rootScope.report1 = datos;
      }

      $scope.labels = $scope.dataall.data[0].labels;

      $scope.data = $scope.dataall.data[1].data2;

    });


    GetDataReport2.async($scope.param1).then(function (datos) {


      $scope.alldata = {};
      $scope.data1 = [];
      $scope.series = [];
      $scope.labels = [];

      if (datos === 0) {
        $scope.dataall22 = $rootScope.report1;
      } else {
        $scope.dataall2 = datos;
        $rootScope.report1 = datos;
      }

      $scope.labels2 = $scope.dataall2.data[0].labels;

      $scope.data2 = $scope.dataall2.data[1].data2;

    });


    GetDataReport3.async($scope.param1).then(function (datos) {


      $scope.alldata = {};
      $scope.data1 = [];
      $scope.series = [];
      $scope.labels = [];

      if (datos === 0) {
        $scope.dataall3 = $rootScope.report1;
      } else {
        $scope.dataall3 = datos;
        $rootScope.report1 = datos;
      }

      $scope.labels3 = $scope.dataall3.data[0].labels;

      $scope.data3 = $scope.dataall3.data[1].data2;

    });



    GetDataReport4.async($scope.param1).then(function (datos) {




      if (datos === 0) {
        $scope.dataall4 = $rootScope.report1;
      } else {
        $scope.dataall4 = datos;
        //$rootScope.report1 = datos;
      }
      //console.log($scope.dataall3);
      $scope.labels4 = $scope.dataall4.data[0].labels;
      //console.log($scope.dataall2.data[1].IntegridadSistema);
      $scope.data4 = $scope.dataall4.data[1].data2;

    });


    GetDataReport5.async($scope.param1).then(function (datos) {




      if (datos === 0) {
        $scope.dataall5 = $rootScope.report1;
      } else {
        $scope.dataall5 = datos;
        //$rootScope.report1 = datos;
      }
      //console.log($scope.dataall3);
      $scope.labels5 = $scope.dataall5.data[0].labels;
      //console.log($scope.dataall2.data[1].IntegridadSistema);
      $scope.data5 = $scope.dataall5.data[1].data2;

    });


    GetDataReport6.async($scope.param1).then(function (datos) {


      //$scope.alldata = {};
      //$scope.data1 = [];
      //$scope.series = [];
      //$scope.labels = [];

      if (datos === 0) {
        $scope.dataall6 = $rootScope.report1;
      } else {
        $scope.dataall6 = datos;
        //$rootScope.report1 = datos;
      }
      //console.log($scope.dataall3);
      $scope.labels6 = $scope.dataall6.data[0].labels;
      //console.log($scope.dataall2.data[1].IntegridadSistema);
      $scope.data6 = $scope.dataall6.data[1].data2;

    });

    GetDataReport7.async($scope.param1).then(function (datos) {


      //$scope.alldata = {};
      //$scope.data1 = [];
      //$scope.series = [];
      //$scope.labels = [];

      if (datos === 0) {
        $scope.dataall7 = $rootScope.report1;
      } else {
        $scope.dataall7 = datos;
        //$rootScope.report1 = datos;
      }
      //console.log($scope.dataall3);
      $scope.labels7 = $scope.dataall7.data[0].labels;
      //console.log($scope.dataall2.data[1].IntegridadSistema);
      $scope.data7 = $scope.dataall7.data[1].data2;

    });

    GetDataReport8.async($scope.param1).then(function (datos) {


      //$scope.alldata = {};
      //$scope.data1 = [];
      //$scope.series = [];
      //$scope.labels = [];

      if (datos === 0) {
        $scope.dataall8 = $rootScope.report1;
      } else {
        $scope.dataall8 = datos;
        //$rootScope.report1 = datos;
      }
      //console.log($scope.dataall3);
      $scope.labels8 = $scope.dataall8.data[0].labels;
      //console.log($scope.dataall2.data[1].IntegridadSistema);
      $scope.data8 = $scope.dataall8.data[1].data2;

    });

    GetDataReport9.async($scope.param1).then(function (datos) {


      //$scope.alldata = {};
      //$scope.data1 = [];
      //$scope.series = [];
      //$scope.labels = [];

      if (datos === 0) {
        $scope.dataall9 = $rootScope.report1;
      } else {
        $scope.dataall9 = datos;
        //$rootScope.report1 = datos;
      }
      //console.log($scope.dataall3);
      $scope.labels9 = $scope.dataall9.data[0].labels;
      //console.log($scope.dataall2.data[1].IntegridadSistema);
      $scope.data9 = $scope.dataall9.data[1].data2;

    });

    GetDataReport10.async($scope.param1).then(function (datos) {


      //$scope.alldata = {};
      //$scope.data1 = [];
      //$scope.series = [];
      //$scope.labels = [];

      if (datos === 0) {
        $scope.dataall10 = $rootScope.report1;
      } else {
        $scope.dataall10 = datos;
        //$rootScope.report1 = datos;
      }
      //console.log($scope.dataall3);
      $scope.labels10 = $scope.dataall10.data[0].labels;
      //console.log($scope.dataall2.data[1].IntegridadSistema);
      $scope.data10 = $scope.dataall10.data[1].data2;

    });


    GetDataReport11.async($scope.param1).then(function (datos) {


      //$scope.alldata = {};
      //$scope.data1 = [];
      //$scope.series = [];
      //$scope.labels = [];

      if (datos === 0) {
        $scope.dataall11 = $rootScope.report1;
      } else {
        $scope.dataall11 = datos;
        //$rootScope.report1 = datos;
      }
      //console.log($scope.dataall3);
      $scope.labels11 = $scope.dataall11.data[0].labels;
      //console.log($scope.dataall2.data[1].IntegridadSistema);
      $scope.data11 = $scope.dataall11.data[1].data2;

    });

  }

  

  $scope.gridOptions = {};
  $scope.gridOptions2 = {};
  $scope.gridOptions3 = {};

  $scope.myData = [{ indicador: "Integridad (Spot % Load) [%]", rango: "[68-100%]", ponderacion: "0.3" },
  { indicador: "Llegadas Tarde [%]", rango: "[0-10%[", ponderacion: "0.175" },
  { indicador: "Balizas Perdidas [#]", rango: "[0-5[", ponderacion: "0.175" },
  { indicador: "Uso Dinamico  [%]", rango: "[68-100%]", ponderacion: "0" },                          
  { indicador: "Cargas Extras  [%]", rango: "[0-2%[", ponderacion: "0.175" },                         
  { indicador: "Ciclos Cortos [%]", rango: "[0-0.5%[", ponderacion: "0.175" }];

  $scope.myData2 = [{ turno: "0", grupo: "0", carga_extra: "0", carga_normal: "0", carga_total: "0", porcentaje: "0" },
  { turno: "0", grupo: "0", carga_extra: "0", carga_normal: "0", carga_total: "0", porcentaje: "0" }
  ];

  $scope.myData3 =[ { grupo:"0",tonelaje:"0",numero:"0" },{ grupo:"0",tonelaje:"0",numero:"0" }
           ];

  $scope.gridOptions = {
    data: 'myData',
    //enableCellEditOnFocus: true,
    //infiniteScrollRowsFromEnd: 4,
    //infiniteScrollUp: true,
    //infiniteScrollDown: true,
    columnDefs: [{ name: 'indicador', displayName: 'Indicador' },
    { name: 'rango', displayName: 'Rango' },
    { name: 'ponderacion', displayName: 'Ponderacion' }]
  };


 

  $scope.gridOptions.enableCellEditOnFocus = true;


  $scope.gridOptions2 = {
    data: 'myData2',
    enableCellEditOnFocus: true,
    infiniteScrollRowsFromEnd: 4,
    infiniteScrollUp: true,
    infiniteScrollDown: true,

    columnDefs: [{ name: 'turno', displayName: 'Turno' },
    { name: 'grupo', displayName: 'grupo' },
    { name: 'carga_extra', displayName: 'Carga Extra' },
    { name: 'carga_normal', displayName: 'Carga Normal' },
    { name: 'carga_total', displayName: 'Total Carga' },
    { name: 'porcentaje', displayName: 'Porcentaje' }]
  };

  $scope.gridOptions3 = {
    data: 'myData3',
    enableCellEditOnFocus: true,
    infiniteScrollRowsFromEnd: 4,
    infiniteScrollUp: true,
    infiniteScrollDown: true,

    columnDefs: [{ name: 'grupo', displayName: 'Grupo' },
    { name: 'tonelaje', displayName: 'Tonelaje Generado por Ciclos Cortos' },
    { name: 'numero', displayName: 'Numero Ciclos Cortos ' }
    ]
  };

  //$scope.gridOptions.data = $scope.myData;

 // $scope.gridOptions.columnDefs = [{ name: 'indicador', displayName: 'Indicador', width: "*" },
 // { name: 'rango', displayName: 'Rango', width: "*" },
 // { name: 'ponderacion', displayName: 'Ponderacion', width: "*" }]
  //  ;



  //$scope.saveRow = function (rowEntity) {

    //console.log($scope.myData);
    //console.log($scope.myData2);
    // create a fake promise - normally you'd use the promise returned by $http or $resource
 //   var promise = $q.defer();
 //   $scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);


 // };


  //$scope.saveRow2 = function (rowEntity) {

    //console.log($scope.myData);
    //console.log($scope.myData2);
    // create a fake promise - normally you'd use the promise returned by $http or $resource
   // var promise = $q.defer();
    //$scope.gridApi2.rowEdit.setSavePromise(rowEntity, promise.promise);

    // fake a delay of 3 seconds whilst the save occurs, return error if gender is "male"

  //};





  $scope.gridOptions.onRegisterApi = function (gridApi) {
    //set gridApi on scope
    $scope.gridApi = gridApi;

    //gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
  };

  $scope.gridOptions2.onRegisterApi = function (gridApi2) {
    //set gridApi on scope
    // alert();
    $scope.gridApi2 = gridApi2;
    //gridApi2.rowEdit.on.saveRow($scope, $scope.saveRow2);
  };

  $scope.gridOptions3.onRegisterApi = function (gridApi3) {
    //set gridApi on scope
    // alert();
    $scope.gridApi3 = gridApi3;
    //gridApi2.rowEdit.on.saveRow($scope, $scope.saveRow2);
  };


  $scope.addData = function () {
    //var n = $scope.gridOpts.data.length + 1;
    $scope.myData2.push({ turno: " ", grupo: "0", carga_extra_turno: "0", carga_normal: "0", total_carga: "", porcentaje: "" });
  };

  $scope.addData3 = function () {
    //var n = $scope.gridOpts.data.length + 1;
    $scope.myData3.push({ grupo:"0",tonelaje:"0",numero:"0" });
  };

  $scope.reset = function () {
    //var n = $scope.gridOpts.data.length + 1;
    $scope.myData2 = [];
  };

  $scope.reset3 = function () {
    //var n = $scope.gridOpts.data.length + 1;
    $scope.myData3 = [];
  };

  $scope.series1 = ["Grupo1", "Grupo2", "Grupo3", "Grupo4"];
  $scope.options1 = {


    title: {
      display: true,
      text: "Ranking Integridad sistema "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Integridad sistema',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Semana',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
                beginAtZero: true
            }
      }]
    }
  };


   $scope.series2 = ["IntegridadSistema", "Target"];
   $scope.options2 = {


    title: {
      display: true,
      text: " BenchMark Integridad Sistema "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Integridad sistema %',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Clientes Performance Assurance',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
                beginAtZero: true
            }
      }]
    }
  };



  $scope.options3 = {


    title: {
      display: true,
      text: "Equipos de Transporte - Equipo - "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: true,
        scaleLabel: {
          labelString: 'Semana',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: true,
        scaleLabel: {
          labelString: 'Spot & Load [%]',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
                beginAtZero: true
            }
      }]
    }
  };


  $scope.colors = ['#0000ff', '#008000', '#ff0000', '#808080', '#000000', '#ff0000'];

  $scope.colors2 = ['#0000ff', '#ff0000'];

  $scope.colors3 = ['#008000', '#ff0000', '#808080', '#ffff00', '#000000'];

  $scope.datasetOverride2 = [
    {
      label: "IntegridadSistema [%]",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Target [>90]",
      borderWidth: 1,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    }
  ];

  $scope.series3 = ["%SpotNormal/CargaNormal", "%SpotMalo/CargaCorrecta", "%SpotCorrecto/CargaMala", "%SpotMalo/CargaMalo","%Spot/CargaNulo"];


  $scope.datasetOverride4 = [
    {
      label: "Grupo 1",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 2",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 3",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 4",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      borderWidth: 3,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    },
    {
      label: "Target [>68%]",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      type: 'line',
      fill: false
    }
  ];

  $scope.options4 = {


    title: {
      display: true,
      text: "Integridad (Spot & Load) Por Grupo "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Semana',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Spot & Load [%]',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
                beginAtZero: true
            }
      }]
    }
  };

  $scope.options5 = {


    title: {
      display: true,
      text: " BenchMark Integridad Spot & Load "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Clientes Performance Assurance',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Spot % Load [%] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
                beginAtZero: true
            }
      }]
    }
  };



  $scope.datasetOverride5 = [
    {
      label: "Integridad [%]",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Target [>68%]",
      borderWidth: 1,
      
      type: 'line',
      fill: false
    }
  ];


  $scope.options6 = {


    title: {
      display: true,
      text: " Uso Dinamico por Grupo "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Semana',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Uso Dinamico [%] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
                beginAtZero: true
            }
      }]
    }
  };


  $scope.datasetOverride6 = [
    {
      label: "Grupo 1",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 2",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 3",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 4",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      //borderWidth: 1,
      //borderDash: [5, 5],
      pointBorderWidth: 1,
      
      type: 'line',
      fill: false
    },
    {
      label: "Target[>80%]",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      //hoverBackgroundColor: "rgba(0, 0, 0, 0)",
      //hoverBorderColor: "rgba(0, 0, 0, 0)",
      type: 'line',
      fill: false
    }
  ];

  $scope.datasetOverride7 = [
    {
      label: "Uso Dinamico [%]",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Taget [>80%]",
      borderWidth: 1,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    }
  ];


  $scope.options7 = {


    title: {
      display: true,
      text: " BenchMark Uso Dinamico  "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Clientes Performance Assurance',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Uso Dinamico [%] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
                beginAtZero: true
            }
      }]
    }
  };


  $scope.datasetOverride8 = [
    {
      label: "Grupo 1",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 2",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 3",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 4",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      borderWidth: 1,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    },
    {
      label: "Target [<0.5%]",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      type: 'line',
      fill: false
    }
  ];

  $scope.options8 = {


    title: {
      display: true,
      text: " Ciclos Cortos por Grupo  "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Semana',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Ciclos Cortos [%] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
                beginAtZero: true
            }
      }]
    }
  };


  $scope.datasetOverride9 = [
    {
      label: "Grupo 1",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 2",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 3",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 4",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      borderWidth: 1,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    },
    {
      label: "Target [<2%]",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      type: 'line',
      fill: false
    }
  ];


  $scope.options9 = {


    title: {
      display: true,
      text: " Cargas Extras por grupo  "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Semana',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Cargas Extras  [%] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
                beginAtZero: true
            }
      }]
    }
  };


  $scope.datasetOverride10 = [
    {
      label: "Grupo 1",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 2",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 3",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 4",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      borderWidth: 1,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    },
    {
      label: "Target[<5]",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      type: 'line',
      fill: false
    }
  ];


  $scope.options10 = {


    title: {
      display: true,
      text: " Balizas Perdidas por grupo  "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Semana',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Balizas Perdidas  [#] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
                beginAtZero: true
            }
      }]
    }
  };

  $scope.datasetOverride11 = [
    {
      label: "Grupo 1",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 2",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 3",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Grupo 4",
      borderWidth: 1,
      type: 'bar'
    },
    {
      label: "Tendencia",
      borderWidth: 1,
      //hoverBackgroundColor: "rgba(255, 0, 0, 1)",
      //hoverBorderColor: "rgba(255, 0, 0, 1)",
      type: 'line',
      fill: false
    },
    {
      label: "Target [<10%]",
      borderWidth: 1,
      //pointBorderWidth: 1,
      borderDash: [5, 5],
      type: 'line',
      fill: false
    }
  ];


  $scope.options11 = {


    title: {
      display: true,
      text: " Llegadas Tarde por grupo  "
      // fontColor: '#f9fdff'
    },

    legend: { display: true, fontColor: '#fcfeff' },

    tooltips: {
      display: true,
      mode: 'label',
      titleFontSize: 12

    },
    responsive: true,

    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: 'Semana',
          //fontColor: '#151515',
          display: true,
        }
      }
      ],
      yAxes: [{
        stacked: false,
        scaleLabel: {
          labelString: ' Llegadas Tarde  [%] ',
          // fontColor: '#151515',
          display: true,
        },
        ticks: {
                beginAtZero: true
            }
      }]
    }
  };



  //$scope.data = [
  //  [65, 59, 80, 81, 56, 55, 40],
  //  [28, 48, 40, 19, 86, 27, 90]
  //];

  this.settings = {
    name: 'settings',
    items: [
      'Home',
      'About',
      'Contact'
    ]
  };
  this.favorite = {
    name: 'favorite',
    items: [
      'Add to Favorites'
    ]
  };
  this.more = {
    name: 'more',
    items: [
      'Account',
      'Sign Out'
    ]
  };
  this.tools = {
    name: 'tools',
    items: [
      'Create',
      'Delete'
    ]
  };
  this.code = {
    name: 'code',
    items: [
      'See Source',
      'See Commits'
    ]
  };



  this.menuTemplate = '' +
    '<div class="menu-panel" md-whiteframe="4">' +
    '  <div class="menu-content">' +
    '    <div class="menu-item" ng-repeat="item in ctrl.items">' +
    '      <button class="md-button">' +
    '        <span>{{item}}</span>' +
    '      </button>' +
    '    </div>' +
    '    <md-divider></md-divider>' +
    '    <div class="menu-item">' +
    '      <button class="md-button" ng-click="ctrl.closeMenu()">' +
    '        <span>Close Menu</span>' +
    '      </button>' +
    '    </div>' +
    '  </div>' +
    '</div>';

  $mdPanel.newPanelGroup('toolbar', {
    maxOpen: 2
  });

  $mdPanel.newPanelGroup('menus', {
    maxOpen: 3
  });

  $scope.SaveAll = function () { 

    

    $scope.param1 = {
      idcliente : $scope.user.id,
      intro: $scope.intro,
      estado1: $scope.estado1,
      comentario1: $scope.comentario1,
      estado2: $scope.estado2,
      comentario2: $scope.comentario2,
      estado3: $scope.estado3,
      comentario3: $scope.comentario3,
      estado4: $scope.estado4,
      comentario4: $scope.comentario4,
      estado5: $scope.estado5,
      comentario5: $scope.comentario5,
      estado6: $scope.estado6,
      comentario6: $scope.comentario6,
      estado7: $scope.estado7,
      comentario7: $scope.comentario7,
      estado8: $scope.estado8,
      comentario8: $scope.comentario8,
      estado9: $scope.estado9,
      comentario9: $scope.comentario9,
      estado10: $scope.estado10,
      comentario10: $scope.comentario10,
      estado11: $scope.estado11,
      comentario11: $scope.comentario11,
      label1: $scope.user.name,
      data1 : $scope.myData,
      data2 : $scope.myData2,
      data3 : $scope.myData3       
    }
    



    InsertDataReport.async($scope.param1).then(function (datos) {
    })

    $scope.param = {
      idcliente : $scope.user.id,
      image: '1',
      file1: $scope.file1.base64
    }
    InsertDataReportImages.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente : $scope.user.id,
      image: '2',
      file1: $scope.file2.base64
    }
    InsertDataReportImages.async($scope.param).then(function (datos) {
    })

    $scope.param = {
      idcliente : $scope.user.id,
      image: '3',
      file1: $scope.file3.base64
    }
    InsertDataReportImages.async($scope.param).then(function (datos) {
    })

    $scope.showSimpleToast();
    $scope.active_print = false;

  };


  $scope.user = null;
  $scope.users = null;

  $scope.ChangeCliente = function () {

    $scope.param_ = {
      cliente: $scope.user.id
    }
    
     
     GetDataValidate.async($scope.param_).then(function (datosValidacion) {
        
          
    if (datosValidacion.reply === true ){
     $scope.colorreply = 'md-primary';      
     $scope.msg_validaction = "Validacion Sin Problemas";
    $scope.selectedIndex = 1;

     $scope.active_ingreso = false;
     $scope.active_resumen = false;
     $scope.active_print = true;
     cleanObject();
     $scope.intro = "<p>Al revisar el gráfico de ranking semanal, Gráfico 1, se observa que los grupos resultados impares en sus indicadores en comparación a su turno anterior (semana 28). El grupo G presentó una disminución de 13%, mientras que el grupo H subió en 8%.</p> <p>La explicación del aumento del grupo H se fundamenta en el desempeño mostrado en los indicadores Ciclos Cortos y Spot y Load, ya que este grupo logró disminuir los Ciclos Cortos en 0,2%, mientras que aumentó la cantidad de registros Spot y Load Correctos, logrando el target de 68%.</p>  <p>El escenario es distinto para el grupo G, ya que este grupo presentó una baja de 0,4% en Ciclos Cortos, quitándole una ponderación de 14%. Si bien este grupo aumentó su indicador de Spot y Load Correcto, este aumento no es significativo frente a lo bajado en ciclos cortos, por lo que se vio reflejado en magnitud la caída en Ciclos Cortos.</p> <p>El indicador Balizas Perdidas ha mejorado en las últimas semanas, gracias al seguimiento en las recomendaciones realizadas al cliente de la mantención de la gráfica de mina. Se felicita esta labor y se promueve su continuación diariamente. Durante esta semana se han detectado nuevas ubicaciones con problemas que requieren de la atención del cliente.</p> <p>A nivel de comparación con los otros clientes con Performance Assurance, CMCC se encuentra en sexto lugar con un 49%.</p> Este ranking es construido en base a las ponderaciones presentadas en la Tabla 1 para los 4 grupos. Estas ponderaciones son distintas a los otros clientes, debido a que CMCC no tiene activada la medición de uso dinámico.";
     initializeChart($scope.user.id);

    $scope.param_ = {
      cliente: $scope.user.id
    }
    


    GetDataInput.async($scope.param_).then(function (datos) {
       if (datos.data.labels[0] == undefined){
         cleanObject();
       }else{
         $scope.myData = {};
         
         if (datos.data.ranking.length == 0) {
           
           $scope.myData = [{ indicador: "Integridad (Spot % Load) [%]", rango: "[68-100%]", ponderacion: "0.3" },
           { indicador: "Llegadas Tarde [%]", rango: "[0-10%[", ponderacion: "0.175" },
           { indicador: "Balizas Perdidas [#]", rango: "[0-5[", ponderacion: "0.175" },
           { indicador: "Uso Dinamico  [%]", rango: "[68-100%]", ponderacion: "0" },                          
           { indicador: "Cargas Extras  [%]", rango: "[0-2%[", ponderacion: "0.175" },                         
           { indicador: "Ciclos Cortos [%]", rango: "[0-0.5%[", ponderacion: "0.175" }];

         } else {
           
           $scope.myData = datos.data.ranking;
         }
         
         if (datos.data.cargas == 0) {
           $scope.myData2 = [{ turno: "0", grupo: "0", carga_extra: "0", carga_normal: "0", carga_total: "0", porcentaje: "0" },
           { turno: "0", grupo: "0", carga_extra: "0", carga_normal: "0",  carga_total: "0", porcentaje: "0" }
           ];

         }
         else {
           $scope.myData2 = datos.data.cargas;
         }

         if (datos.data.ciclos == 0) {
           $scope.myData3 =[ { grupo:"0",tonelaje:"0",numero:"0" },{ grupo:"0",tonelaje:"0",numero:"0" }
           ];

         }
         else {
           $scope.myData3 = datos.data.ciclos;
         }

        







         //$scope.intro = datos.data.labels[0].Intro;
         $scope.estado1 = datos.data.labels[0].Estado1;
         $scope.comentario1 = datos.data.labels[0].Comentario1;
         $scope.estado2 = datos.data.labels[0].Estado2;
         $scope.comentario2 = datos.data.labels[0].Comentario2;
         $scope.estado3 = datos.data.labels[0].Estado3;
         $scope.comentario3 = datos.data.labels[0].Comentario3;
         $scope.estado4 = datos.data.labels[0].Estado4;
         $scope.comentario4 = datos.data.labels[0].Comentario4;
         $scope.estado5 = datos.data.labels[0].Estado5;
         $scope.comentario5 = datos.data.labels[0].Comentario5;
         $scope.estado6 = datos.data.labels[0].Estado6;
         $scope.comentario6 = datos.data.labels[0].Comentario6;
         $scope.estado7 = datos.data.labels[0].Estado7;
         $scope.comentario7 = datos.data.labels[0].Comentario7;
         $scope.estado8 = datos.data.labels[0].Estado8;
         $scope.comentario8 = datos.data.labels[0].Comentario8;
         $scope.estado9 = datos.data.labels[0].Estado9;
         $scope.comentario9 = datos.data.labels[0].Comentario9;
         $scope.estado10 = datos.data.labels[0].Estado10;
         $scope.comentario10 = datos.data.labels[0].Comentario10;
         $scope.estado11 = datos.data.labels[0].Estado11;
         $scope.comentario11 = datos.data.labels[0].Comentario11;
         $scope.estado12 = datos.data.labels[0].Estado12;
         $scope.comentario12 = datos.data.labels[0].Comentario12;
         $scope.estado13 = datos.data.labels[0].Estado13;
         $scope.comentario13 = datos.data.labels[0].Comentario13;
         $scope.estado14 = datos.data.labels[0].Estado14;
         $scope.comentario14 = datos.data.labels[0].Comentario14;
         $scope.estado15 = datos.data.labels[0].Estado15;
         $scope.comentario15 = datos.data.labels[0].Comentario15;
         $scope.estado16 = datos.data.labels[0].Estado16;
         $scope.comentario16 = datos.data.labels[0].Comentario16;
         $scope.file1.base64 = datos.data.labels[0].File1;
         $scope.file2.base64 = datos.data.labels[0].File2;
         $scope.file3.base64 = datos.data.labels[0].File3;




       }
    })

    }else{
      $scope.colorreply = 'md-warn';
      $scope.active_ingreso = true;
     $scope.active_resumen = true;
     $scope.active_print = true;
      $scope.selectedIndex = 2;
      $scope.msg_validaction = "Para el Cliente " + $scope.user.name + " No se encuantra carga la Informacion Para La semana en curso.";

    }
    })  

  }

  

  $scope.showConfirm = function(ev) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('Confirmación')
          .textContent('Se enviaran los Datos Correpondiente al informe Semanal de '+ $scope.user.name)
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Save')
          .cancel('Cancel');

    $mdDialog.show(confirm).then(function() {
      //$scope.status = 'You decided to get rid of your debt.';
      $scope.SaveAll();
    }, function() {
      //$scope.status = 'You decided to keep your debt.';
    });
  };

  $scope.foo = function () {


    //$scope.isLoading = true;
    //$scope.content = '';
    //$scope.content = $sce.trustAsResourceUrl('http://localhost:3000/restservicemantos/getReportServer?id=' + $scope.user.id);
    $window.open('http://localhost:3000/restservicemantos/getReportServer?id=' + $scope.user.id, 'C-Sharpcorner', 'toolbar=0,scrollbars=1,location=0,status=0,menubar=0,resizable=1,width=650, height=550,left = 300,top=100');

    //$timeout(countUp, 5000);
    //$scope.isLoading = false;


  };

  $scope.loadUsers = function () {

    // Use timeout to simulate a 650ms request.
    return $timeout(function () {

      $scope.users = $scope.users || [


        { id: 'dand', name: 'Andina Rajo' },
       // { id: 'sand', name: 'Andina Subterranea' },
        { id: 'mlc', name: 'Candelaria' },
        { id: 'cmcc', name: 'Cerro Colorado' },
        //{ id: 'cvg', name: 'Cerro Vanguardia' },
        { id: 'cmdic', name: 'Collahuasi' },
        //{ id: 'scma', name: 'El Abra' },
       // { id: 'det', name: 'El Teniente' },
        { id: 'mel', name: 'Escondida' },
        //{ id: 'exss', name: 'Excon Toconao' },
        //{ id: 'gaby', name: 'Gaby' },
        { id: 'cmh', name: 'Huasco' },
        //{ id: 'kch', name: 'Komatsu Chile (MLP)' },
        { id: 'mlb', name: 'Lomas Bayas' },
        { id: 'dlb', name: 'Los Bronces' },
        { id: 'mlp', name: 'Los Pelambres' },
        { id: 'dmbl', name: 'Mantos Blancos' },
        //{ id: 'smir', name: 'Mina Invierno' },
       // { id: 'mms-chile', name: 'MMS-Chile' },
       // { id: 'nnn', name: 'Nueva Venta' },
        { id: 'msc', name: 'San Cristobal' },
        { id: 'cms', name: 'Spence' },
        { id: 'cmz', name: 'Zaldivar' }
      ];

    }, 650);
  };

  this.showToolbarMenu = function ($event, menu) {
    var template = this.menuTemplate;

    var position = $mdPanel.newPanelPosition()
      .relativeTo($event.srcElement)
      .addPanelPosition(
      $mdPanel.xPosition.ALIGN_START,
      $mdPanel.yPosition.BELOW
      );

    var config = {
      id: 'toolbar_' + menu.name,
      attachTo: angular.element(document.body),
      controller: PanelMenuCtrl,
      controllerAs: 'ctrl',
      template: template,
      position: position,
      panelClass: 'menu-panel-container',
      locals: {
        items: menu.items
      },
      openFrom: $event,
      focusOnOpen: false,
      zIndex: 100,
      propagateContainerEvents: true,
      groupName: ['toolbar', 'menus']
    };

    $mdPanel.open(config);
  };

  this.showContentMenu = function ($event, menu) {
    var template = this.menuTemplate;

    var position = $mdPanel.newPanelPosition()
      .relativeTo($event.srcElement)
      .addPanelPosition(
      $mdPanel.xPosition.ALIGN_START,
      $mdPanel.yPosition.BELOW
      );

    var config = {
      id: 'content_' + menu.name,
      attachTo: angular.element(document.body),
      controller: PanelMenuCtrl,
      controllerAs: 'ctrl',
      template: template,
      position: position,
      panelClass: 'menu-panel-container',
      locals: {
        items: menu.items
      },
      openFrom: $event,
      focusOnOpen: false,
      zIndex: 100,
      propagateContainerEvents: true,
      groupName: 'menus'
    };

    $mdPanel.open(config);
  };
}




//$scope.currentNavItem = 'page1';
function PanelMenuCtrl(mdPanelRef) {
  this.closeMenu = function () {
    mdPanelRef && mdPanelRef.close();
  }
}
