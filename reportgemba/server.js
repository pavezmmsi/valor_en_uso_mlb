var express = require("express"),
    app = express(),
    bodyParser = require("body-parser"),
    methodOverride = require("method-override");
    mongoose = require('mongoose');
    fs = require('fs');
    morgan = require('morgan');    
    winston = require('winston/config');
    app.use(morgan('combined', { stream: winston.stream }));
    winston.info('Servicio Lomas Bayas Iniciado ......');
    app.use(express.static(__dirname + '/public')); 
    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
    app.use(bodyParser.json());
    app.use(methodOverride());
    


var router = express.Router();

var data = '';
var myarray = [];
var datos = [];
var datos2 = [];


var metadata = {} // empty Object
var metadata2 = {} // empty Object
var key = 'metadata';
metadata[key] = []; // empty Array, which you can push() values into
metadata2[key] = [];



function handle_GetMetaData_request(request, response) {

    var get_data_report = require('./modules/get-meta-data');
    get_data_report.get_meta_data(request, response);

}
function handle_GetDataReport_I_request(request, response) {
    var get_data_report = require('./modules/get-data-report-i');
    get_data_report.get_data_report_i(request, response);

}

function handle_GetDataReport_II_request(request, response) {
    var get_data_report = require('./modules/get-data-report-ii');
    get_data_report.get_data_report_ii(request, response);

}
function handle_GetDataReport_III_request(request, response) {
    var get_data_report = require('./modules/get-data-report-iii');
    get_data_report.get_data_report_iii(request, response);

}

function handle_GetAllDataReport_IV_request(request, response) {
    var get_data_report = require('./modules/get-all-data-report-iv');
    get_data_report.get_all_data_report_iv(request, response);
}

function handle_GetDataReport_IV_request(request, response) {
    var get_data_report = require('./modules/get-data-report-iv');
    get_data_report.get_data_report_iv(request, response);
}

function handle_GetDataReport_V_request(request, response) {
    var get_data_report = require('./modules/get-data-report-v');
    get_data_report.get_data_report_v(request, response);
}

function handle_GetDataReport_VI_request(request, response) {
    var get_data_report = require('./modules/get-data-report-vi');
    get_data_report.get_data_report_vi(request, response);
}

function handle_GetAllDataReport_V_request(request, response) {
    var get_data_report = require('./modules/get-all-data-report-v');
    get_data_report.get_all_data_report_v(request, response);
}

function handle_GetAllDataReport_VI_request(request, response) {
    var get_data_report = require('./modules/get-all-data-report-vi');
    get_data_report.get_all_data_report_vi(request, response);
}

function handle_GetDataReportAllEquipoment_IV_request(request, response) {
    var get_data_report = require('./modules/get-all-equipment-report-iv');
    get_data_report.get_all_equipment_report_iv(request, response);

}

function handle_GetDataReportAllEquipoment_V_request(request, response) {
    var get_data_report = require('./modules/get-all-equipment-report-v');
    get_data_report.get_all_equipment_report_v(request, response);

}

function handle_GetDataReportAllEquipoment_VI_request(request, response) {
    var get_data_report = require('./modules/get-all-equipment-report-vi');
    get_data_report.get_all_equipment_report_vi(request, response);

}

function handle_GetGridStatusEquipment_request(request, response) {
    var get_data_report = require('./modules/get-grid-status-equipment');
    get_data_report.get_grid_status_equipment(request, response);
}

function handle_GetUpdate_request(request, response) {
    var get_data_report = require('./modules/get-update-data');
    get_data_report.update(request, response);
}

function handle_GetNotificationList_request(request, response){
    var get_data_report = require('./modules/get-notification-data');
    get_data_report.get_notification_data(request, response);
}
function handle_GetPitgraphics_request(request, response){
    var get_data_report = require('./modules/get-minegraphics-data');
    get_data_report.get_minegraphics_data(request, response);
}

function handle_GetinfoMineGraphicsById_request(request, response){
    var get_data_report = require('./modules/get-info-mine-graphics-by-id');
    get_data_report.get_info_mine_graphics_by_id(request, response);
}

function handle_getDataChar1_request(request, response){
    var get_data_report = require('./modules/get-data-char1');
    get_data_report.get_data_char1(request, response);
}

function handle_getDataInput_request(request, response){
    var get_data_report = require('./modules/get-data-input');
    get_data_report.get_data_input(request, response);
}

function handle_getDataCharCms_request(request, response){
    var get_data_report = require('./modules/get-data-char-cms');
    get_data_report.get_data_char_cms(request, response);
}

function handle_getDataCharCmz_request(request, response){
    var get_data_report = require('./modules/get-data-char-cmz');
    get_data_report.get_data_char_cmz(request, response);
}

function handle_getDataChar1Angular_request(request, response){
    var get_data_report = require('./modules/get-data-char1-angular');
    get_data_report.get_data_char1_angular(request, response);
}

function handle_getDataCharValidate_request(request, response){
    var get_data_report = require('./modules/get-data-char-validate');
    get_data_report.get_data_validate(request, response);
}
function handle_getDataCharValidateMensual_request(request, response){
    var get_data_report = require('./modules/get-data-char-validate-mensual');
    get_data_report.get_data_validate_mensual(request, response);
}

function handle_getDataChar2Angular_request(request, response){
    var get_data_report = require('./modules/get-data-char2-angular');
    get_data_report.get_data_char2_angular(request, response);
}

function handle_getDataChar3Angular_request(request, response){
    var get_data_report = require('./modules/get-data-char3-angular');
    get_data_report.get_data_char3_angular(request, response);
}

function handle_getDataChar4Angular_request(request, response){
    var get_data_report = require('./modules/get-data-char4-angular');
    get_data_report.get_data_char4_angular(request, response);
}

function handle_getDataChar5Angular_request(request, response){
    var get_data_report = require('./modules/get-data-char5-angular');
    get_data_report.get_data_char5_angular(request, response);
}

function handle_getDataChar6Angular_request(request, response){
    var get_data_report = require('./modules/get-data-char6-angular');
    get_data_report.get_data_char6_angular(request, response);
}

function handle_getDataChar7Angular_request(request, response){
    var get_data_report = require('./modules/get-data-char7-angular');
    get_data_report.get_data_char7_angular(request, response);
}

function handle_getDataChar8Angular_request(request, response){
    var get_data_report = require('./modules/get-data-char8-angular');
    get_data_report.get_data_char8_angular(request, response);
}

function handle_getDataChar9Angular_request(request, response){
    var get_data_report = require('./modules/get-data-char9-angular');
    get_data_report.get_data_char9_angular(request, response);
}

function handle_getDataChar10Angular_request(request, response){
    var get_data_report = require('./modules/get-data-char10-angular');
    get_data_report.get_data_char10_angular(request, response);
}

function handle_getDataChar11Angular_request(request, response){
    var get_data_report = require('./modules/get-data-char11-angular');
    get_data_report.get_data_char11_angular(request, response);
}

function handle_getInsertData_request(request, response){
    var get_data_report = require('./modules/get-insert-data');
    get_data_report.get_insert_data(request, response);

}

function handle_getInsertDataMensual_request(request, response){
    var get_data_report = require('./modules/get-insert-data-mensual');
    get_data_report.get_insert_data_mesual(request, response);

}

function handle_getInsertDataDirtyMensual_request(request, response){
    var get_data_report = require('./modules/get-insert-data-mensual-dirty');
    get_data_report.get_insert_data_mesual_dirty(request, response);

}

function handle_getInsertDataImages_request(request, response){
    var get_data_report = require('./modules/get-insert-data-images');
    get_data_report.get_insert_data_images(request, response);

}

function handle_getInsertDataImagesMensual_request(request, response){
    var get_data_report = require('./modules/get-insert-data-images-mensual');
    get_data_report.get_insert_data_images_mensual(request, response);

}

function handle_getInsertDataDirtyImagesMensual_request(request, response){
    var get_data_report = require('./modules/get-insert-data-images-dirty-mensual');
    get_data_report.get_insert_data_dirty_images_mensual(request, response);

}

function handle_getPDF_request(request, response){
    var get_data_report = require('./modules/get-pdf-example');
    get_data_report.get_pdf_example(request, response);
}

function handle_getReportServer_request(request, response){
    var get_data_report = require('./modules/get-report-server');
    get_data_report.get_report_server(request, response);
}

function handle_getReportServerMensual_request(request, response){
    var get_data_report = require('./modules/get-report-server-mensual');
    get_data_report.get_report_server_mensual(request, response);
}
function handle_uploadFile_request(request, response){
  var get_data_report = require ('./modules/upload_file');
  get_data_report.upload_file(request, response);
}
function handle_getDataReportCCAChart1(request, response){
    var get_data_report = require ('./modules/get-data-report-cca-chart1');
    get_data_report.get_data_report_cca_chart1(request, response);
}

function handle_getDataReportCCAChart2(request, response){
    var get_data_report = require ('./modules/get-data-report-cca-chart2');
    get_data_report.get_data_report_cca_chart2(request, response);
}

function handle_getDataReportCCAChart3(request, response){
    var get_data_report = require ('./modules/get-data-report-cca-chart3');
    get_data_report.get_data_report_cca_chart3(request, response);
}
function handle_getDataReportTicketRMA1(request, response){
    var get_data_report = require ('./modules/get-data-report-ticket-rma1');
    get_data_report.get_data_report_ticket_rma1(request, response);
}

function handle_getDataReportTicketRMA2(request, response){
    var get_data_report = require ('./modules/get-data-report-ticket-rma2');
    get_data_report.get_data_report_ticket_rma2(request, response);
}



function handle_getDataReportTicketRMA3(request, response){
    var get_data_report = require ('./modules/get-data-report-ticket-rma3');
    get_data_report.get_data_report_ticket_rma3(request, response);
}

function handle_getDataReportTicketRMA4(request, response){
    var get_data_report = require ('./modules/get-data-report-ticket-rma4');
    get_data_report.get_data_report_ticket_rma4(request, response);
}

function handle_getDataReportTicketCCA(request, response){
    var get_data_report = require ('./modules/get-data-report-ticket-cca');
    get_data_report.get_data_report_ticket_cca(request, response);
}

function handle_getDataReportTicketCCA2(request, response){
    var get_data_report = require ('./modules/get-data-report-ticket-cca2');
    get_data_report.get_data_report_ticket_cca2(request, response);
}

function handle_getDataReportTicketCCA3(request, response){
    var get_data_report = require ('./modules/get-data-report-ticket-cca3');
    get_data_report.get_data_report_ticket_cca3(request, response);
}

function handle_getInformationKPIMensual(request, response){
    var get_data_report = require ('./modules/get-information-kpi-mensual');
    get_data_report.get_information_kpi_mensual(request, response);
}

function handle_getFileExcel(request, response){
    var get_data_report = require ('./modules/read_file_excel');
    get_data_report.read_file_excel(request, response);
}
function handle_getDataCharDirtyDozen(request, response){
    var get_data_report = require ('./modules/get-data-char-dirty-dozen');
    get_data_report.get_data_char_dirty_dozen(request, response);
}

function handle_GetDataBaseMongo_request(request, response){
    var get_data_report = require('./modules/get-db-mongo');
    get_data_report.get_db_mongo(request, response);
}

function handle_GetDataDictionaryClient(request, response){
    var get_data_report = require('./modules/dictionary_client');
    get_data_report.get_dictionary_client(request, response);
}
function handle_GetAllStatusEquipment_request(request, response){
    var get_data_report = require('./modules/get-all-status-equipment');
    get_data_report.get_all_status_equipment(request, response);
}

function handle_GetDataChart1Montly_request(request, response){
    var get_data_report = require('./modules/get-data-char1-montly');
    get_data_report.get_data_char1_montly(request, response);
}

function handle_GetDataChart2Montly_request(request, response){
    var get_data_report = require('./modules/get-data-char2-montly');
    get_data_report.get_data_char2_montly(request, response);
}

function handle_GetDataChart3Montly_request(request, response){
    var get_data_report = require('./modules/get-data-char3-montly');
    get_data_report.get_data_char3_montly(request, response);
}

function handle_GetDataChart4Montly_request(request, response){
    var get_data_report = require('./modules/get-data-char4-montly');
    get_data_report.get_data_char4_montly(request, response);
}

function handle_GetDataChart5Montly_request(request, response){
    var get_data_report = require('./modules/get-data-char5-montly');
    get_data_report.get_data_char5_montly(request, response);
}

function handle_GetDataPerdidaOperacional_request(request, response){
    var get_data_report = require('./modules/get-data-perdida-operacional');
    get_data_report.get_data_perdida_operacional(request, response);
}

function handle_GetDataPerdidaOperacionalScatter_request(request, response){
    var get_data_report = require('./modules/get-data-perdida-operacional-scatter');
    get_data_report.get_data_perdida_operacional_scatter(request, response);
}

function handle_GetDataFlota_request(request, response){
    var get_data_report = require('./modules/get-data-flota-report');
    get_data_report.get_data_flota_report(request, response);
}

function handle_GetDataCSVCiclosCortos(request, response){
    var get_data_report = require('./modules/get-data-csv-ciclos-cortos');
    get_data_report.get_data_csv_ciclos_cortos(request, response);
}

function handle_GetDataCSVCiclosCortosGrupos(request, response){
    var get_data_report = require('./modules/get-data-csv-ciclos-cortos-grupos');
    get_data_report.get_data_csv_ciclos_cortos_grupos(request, response);
}


function handle_GetDataCSVBalizasPerdidas(request, response){
    var get_data_report = require('./modules/get-data-csv-balizas-perdidas');
    get_data_report.get_data_csv_balizas_perdidas(request, response);

}

function handle_GetDataCSVBalizasPerdidasGrupos(request, response){
    var get_data_report = require('./modules/get-data-csv-balizas-perdidas-grupos');
    get_data_report.get_data_csv_balizas_perdidas_grupos(request, response);

}


function handle_GetDataCSVCargasExtras(request, response){
    var get_data_report = require('./modules/get-data-csv-cargas-extras');
    get_data_report.get_data_csv_cargas_extras(request, response);

}

function handle_GetDataCSVCargasExtrasGrupos(request, response){
    var get_data_report = require('./modules/get-data-csv-cargas-extras-grupos');
    get_data_report.get_data_csv_cargas_extras_grupos(request, response);

}


function handle_GetDataCSVLlegadasTardes(request, response){
    var get_data_report = require('./modules/get-data-csv-llegadas-tardes');
    get_data_report.get_data_csv_llegadas_tardes(request, response);

}

function handle_GetDataCSVLlegadasTardesGrupos(request, response){
    var get_data_report = require('./modules/get-data-csv-llegadas-tardes-grupos');
    get_data_report.get_data_csv_llegadas_tardes_grupos(request, response);

}

function handle_GetDataCSVIntegridad(request, response){
    var get_data_report = require('./modules/get-data-csv-integridad');
    get_data_report.get_data_csv_integridad(request, response);

}

function handle_GetDataCSVIntegridadCiclosNulos(request, response){
    var get_data_report = require('./modules/get-data-csv-integridad-ciclos-nulos');
    get_data_report.get_data_csv_integridad_ciclos_nulos(request, response);

}



function handle_GetDataCSVIntegridadGrupos(request, response){
    var get_data_report = require('./modules/get-data-csv-integridad-grupos');
    get_data_report.get_data_csv_integridad_grupos(request, response);

}

function handle_GetDataCSVUsoDinamico(request, response){
    var get_data_report = require('./modules/get-data-csv-uso-dinamico');
    get_data_report.get_data_csv_uso_dinamico(request, response);

}

function handle_GetDataCSVUsoDinamicoGrupos(request, response){
    var get_data_report = require('./modules/get-data-csv-uso-dinamico-grupos');
    get_data_report.get_data_csv_uso_dinamico_grupos(request, response);

}

function handle_GetDataEfectividadPLM(request, response){
    var get_data_report = require('./modules/get-data-efectividad-plm');
    get_data_report.get_data_efectividad_plm(request, response);
}
function handle_GetDataCSVIntegridadTurno(request, response){
    var get_data_report = require('./modules/get-data-integridad-porturno');
    get_data_report.get_data_integridad_porturno(request, response);
}

function handle_GetDataCSVIntegridadCamion(request, response){
    var get_data_report = require('./modules/get-data-integridad-porcamion');
    get_data_report.get_data_integridad_porcamion(request, response);
}

function handle_GetDataCSVIntegridadPala(request, response){
    var get_data_report = require('./modules/get-data-integridad-porpala');
    get_data_report.get_data_integridad_porpala(request, response);
}

function handle_GetDataCSVIntegridadOperador(request, response){
    var get_data_report = require('./modules/get-data-integridad-poroperador');
    get_data_report.get_data_integridad_poroperador(request, response);
}
function handle_GetDataToneladasPalaHora(request, response){
    var get_data_report = require('./modules/get-data-toneladas-pala-hora');
    get_data_report.get_data_toneladas_pala_hora(request, response);
}
function handle_GetDataAsignacionesDespacho(request, response){
    var get_data_report = require('./modules/get-data-asignaciones-despacho');
    get_data_report.get_data_asignaciones_despacho(request, response);
}
function handle_GetDataExcepciones(request, response){
    var get_data_report = require('./modules/get-data-excepciones');
    get_data_report.get_data_excepciones(request, response);
}

function handle_GetDataExcepcionesGrupos(request, response){
    var get_data_report = require('./modules/get-data-excepciones-grupos');
    get_data_report.get_data_excepciones_grupos(request, response);
}

function handle_GetDataComunicaciones(request, response){
    var get_data_report = require('./modules/get-data-comunicaciones');
    get_data_report.get_data_comunicaciones(request, response);
}
function handle_GetDataSinComunicaciones(request, response){
    var get_data_report = require('./modules/get-data-sin-comunicaciones');
    get_data_report.get_data_sin_comunicaciones(request, response);
}

function handle_GetDataGPSAlta(request, response){
    var get_data_report = require('./modules/get-data-gps-alta');
    get_data_report.get_data_gps_alta(request, response);
}

function handle_GetDataGPS(request, response){
    var get_data_report = require('./modules/get-data-gps');
    get_data_report.get_data_gps(request, response);
}
function handle_getDataInfluxDB(request, response){
     var get_data_report = require('./modules/get-data-influxdb');
     get_data_report.get_data_influxdb(request, response)
}
function handle_getDataInfluxDB_Freezer(request, response){
     var get_data_report = require('./modules/get-data-influxdb-freezercount');
     get_data_report.get_data_influxdb_freezecount(request, response)
}
function handle_getDataNoTalks(request,response){
    var get_data_report = require('./modules/get-data-notalks');
    get_data_report.get_data_notalks(request, response);
}

function handle_getDataIAuditor(request,response){
    var get_data_report = require('./modules/read_file_json');
    get_data_report.read_file_json(request, response);
}

function handle_getDataTiempoEstimadoVSTiempoReal(request, response){
    var get_data_report = require('./modules/get-data-tiempo-estimado-tiempo-real');
    get_data_report.get_data_tiempo_estimado_tiempo_real(request, response);
}

function handle_getDataFrecuenciaEsperaPala(request, response){
    var get_data_report = require('./modules/get-data-frecuencia-espera-pala')
    get_data_report.get_data_frecuencia_espera_pala(request, response);
}

router.get('/restservicemodular/services/getDataFrecuenciaEsperaPala',function(request,response){
    handle_getDataFrecuenciaEsperaPala(request, response);
})

router.get ('/restservicemodular/services/getDataEstimadaVSReal',function(request,response){
    handle_getDataTiempoEstimadoVSTiempoReal(request,response);

})



router.get ('/restservicemodular/services/getDataIAuditor',function(request,response){
     handle_getDataIAuditor(request,response);

})

router.get ('/restservicemodular/services/GetDataNoTalks',function(request,response){
     handle_getDataNoTalks(request,response);

})
router.get('/restservicemodular/services/GetDataInfluxDBFreezer',function(request, response){
    handle_getDataInfluxDB_Freezer(request, response)
})

router.get('/restservicemodular/services/GetDataInfluxDB',function(request, response){
    handle_getDataInfluxDB(request, response)
})

router.get('/restservicemodular/services/GetDataGPSAlta',function(request, response){
    handle_GetDataGPSAlta(request, response);
});


router.get('/restservicemodular/services/GetDataGPS',function(request, response){
    handle_GetDataGPS(request, response);
});

router.get('/restservicemodular/services/GetDataSinComunicaciones',function(request, response){
    handle_GetDataSinComunicaciones(request, response);
});

router.get('/restservicemodular/services/GetDataComunicaciones',function(request, response){
    handle_GetDataComunicaciones(request, response);
});

router.get('/restservicemodular/services/GetDataExcepcionesGrupos',function(request, response){
    handle_GetDataExcepcionesGrupos(request, response);
});

router.get('/restservicemodular/services/GetDataExcepciones',function(request, response){
    handle_GetDataExcepciones(request, response);
});    

router.get('/restservicemodular/services/GetDataAsignacionesDespacho',function(request, response){
    handle_GetDataAsignacionesDespacho(request, response);
});

router.get('/restservicemodular/services/GetDataToneladasPalaHora',function(request, response){
    handle_GetDataToneladasPalaHora(request, response);
});

router.get('/restservicemodular/services/GetDataIntegridadOperador',function(request, response){
    handle_GetDataCSVIntegridadOperador(request, response);
});

router.get('/restservicemodular/services/GetDataIntegridadPala',function(request, response){
    handle_GetDataCSVIntegridadPala(request, response);
});

router.get('/restservicemodular/services/GetDataIntegridadCamion',function(request, response){
    handle_GetDataCSVIntegridadCamion(request, response);
});

router.get('/restservicemodular/services/GetDataIntegridadTurno',function(request, response){
    handle_GetDataCSVIntegridadTurno(request, response);
});


router.get('/restservicemodular/services/GetDataEfectividadPLM',function(request, response){
    handle_GetDataEfectividadPLM(request, response);
});

router.get('/restservicemodular/services/GetDataCSVUsoDinamico',function(request, response){
    handle_GetDataCSVUsoDinamico(request, response);
});

router.get('/restservicemodular/services/GetDataCSVUsoDinamicoGrupos',function(request, response){
    handle_GetDataCSVUsoDinamicoGrupos(request, response);
});


router.get('/restservicemodular/services/GetDataCSVIntegridad',function(request, response){
    handle_GetDataCSVIntegridad(request, response);
});

router.get('/restservicemodular/services/GetDataCSVIntegridadCiclosCortos',function(request, response){
    handle_GetDataCSVIntegridadCiclosNulos(request, response);
});

router.get('/restservicemodular/services/GetDataCSVIntegridadGrupos',function(request, response){
    handle_GetDataCSVIntegridadGrupos(request, response);
});

router.get('/restservicemodular/services/GetDataCSVLlegadasTardes',function(request, response){
    handle_GetDataCSVLlegadasTardes(request, response);
});

router.get('/restservicemodular/services/GetDataCSVLlegadasTardesGrupos',function(request, response){
    handle_GetDataCSVLlegadasTardesGrupos(request, response);
});

router.get('/restservicemodular/services/GetDataCSVCargasExtras',function(request, response){
    handle_GetDataCSVCargasExtras(request, response);
});

router.get('/restservicemodular/services/GetDataCSVCargasExtraGrupos',function(request, response){
    handle_GetDataCSVCargasExtrasGrupos(request, response);
});

router.get('/restservicemodular/services/GetDataCSVBalizasPerdidas',function(request, response){
    handle_GetDataCSVBalizasPerdidas(request, response);
});

router.get('/restservicemodular/services/GetDataCSVBalizasPerdidasGrupos',function(request, response){
    handle_GetDataCSVBalizasPerdidasGrupos(request, response);
});

router.get('/restservicemodular/services/GetDataCSVCiclosCortos',function(request, response){
    handle_GetDataCSVCiclosCortos(request, response);
});

router.get('/restservicemodular/services/GetDataCSVCiclosCortosGrupos',function(request, response){
    handle_GetDataCSVCiclosCortosGrupos(request, response);
});

router.get('/restservicemodular/services/GetDataFlotaReport',function(request, response){
    handle_GetDataFlota_request(request, response);
});

router.get('/restservicemodular/services/GetDataPerdidaOperacional',function(request, response){
    handle_GetDataPerdidaOperacional_request(request, response);
});

router.get('/restservicemodular/services/GetDataPerdidaOperacionalScatter',function(request, response){
    handle_GetDataPerdidaOperacionalScatter_request(request, response);
});

router.get('/restservicemodular/services/GetDataChart1Montly',function(request, response){
    handle_GetDataChart1Montly_request(request, response);
});

router.get('/restservicemodular/services/GetDataChart2Montly',function(request, response){
    handle_GetDataChart2Montly_request(request, response);
});

router.get('/restservicemodular/services/GetDataChart3Montly',function(request, response){
    handle_GetDataChart3Montly_request(request, response);
});

router.get('/restservicemodular/services/GetDataChart4Montly',function(request, response){
    handle_GetDataChart4Montly_request(request, response);
});

router.get('/restservicemodular/services/GetDataChart5Montly',function(request, response){
    handle_GetDataChart5Montly_request(request, response);
});

router.get('/restservicemodular/services/GetAllStatusEquipment/:type_equipment/:status_equipment/',function(request, response){
    handle_GetAllStatusEquipment_request(request, response);
});

router.get('/restservicemodular/services/GetDataDictionaryClient/', function (request, response) {
    handle_GetDataDictionaryClient(request, response);

});

router.get('/restservicemodular/services/GetDataMongo/', function (request, response) {
    handle_GetDataBaseMongo_request(request, response);

});

router.get('/restservicemodular/getDataCharDirtyDozen',function (request, response) {
    handle_getDataCharDirtyDozen(request, response);
});

router.get('/restservicemodular/getFileExcel',function (request, response) {
    handle_getFileExcel(request, response);
});

router.get('/restservicemodular/getDataReportTicketRMA1', function (request, response) {
    handle_getDataReportTicketRMA1(request, response);
});

router.get('/restservicemodular/getDataReportTicketRMA2', function (request, response) {
    handle_getDataReportTicketRMA2(request, response);
});

router.get('/restservicemodular/getDataReportTicketRMA3', function (request, response) {
    handle_getDataReportTicketRMA3(request, response);
});

router.get('/restservicemodular/getDataReportTicketRMA4', function (request, response) {
    handle_getDataReportTicketRMA4(request, response);
});

router.get('/restservicemodular/getDataReportTicketCCA', function (request, response) {
    handle_getDataReportTicketCCA(request, response);
});

router.get('/restservicemodular/getDataReportTicketCCA2', function (request, response) {
    handle_getDataReportTicketCCA2(request, response);
});

router.get('/restservicemodular/getDataReportTicketCCA3', function (request, response) {
    handle_getDataReportTicketCCA3(request, response);
});

router.get('/restservicemodular/getDataReportTicketCCA4', function (request, response) {
    handle_getDataReportTicketCCA4(request, response);
});

router.get('/restservicemodular/getDataReportCCAChart1', function (request, response) {
    handle_getDataReportCCAChart1(request, response);
});

router.get('/restservicemodular/getDataReportCCAChart2', function (request, response) {
    handle_getDataReportCCAChart2(request, response);
});

router.get('/restservicemodular/getDataReportCCAChart3', function (request, response) {
    handle_getDataReportCCAChart3(request, response);
});

router.get('/restservicemodular/services/MetaDataRestService.svc/GetMetaData/', function (request, response) {
    handle_GetMetaData_request(request, response);
});

router.get('/restservicemodular/services/ReportRestService.svc/GetDataReport_I/', function (request, response) {
    handle_GetDataReport_I_request(request, response);
});
router.get('/restservicemodular/services/ReportRestService.svc/GetDataReport_II/', function (request, response) {
    handle_GetDataReport_II_request(request, response);
});
router.get('/restservicemodular/services/ReportRestService.svc/GetDataReport_III/', function (request, response) {
    handle_GetDataReport_III_request(request, response)
});
router.get('/restservicemodular/services/ReportRestService.svc/GetAllDataReport_IV/:equipment', function (request, response) {
    handle_GetAllDataReport_IV_request(request, response)
});
router.get('/restservicemodular/services/ReportRestService.svc/GetAllDataReport_V/:equipment', function (request, response) {
    handle_GetAllDataReport_V_request(request, response)
});
router.get('/restservicemodular/services/ReportRestService.svc/GetAllDataReport_VI/:equipment', function (request, response) {
    handle_GetAllDataReport_VI_request(request, response);
});
router.get('/restservicemodular/services/ReportRestService.svc/GetDataReportAllEquipoment_IV/', function (request, response) {
    handle_GetDataReportAllEquipoment_IV_request(request, response);
});

router.get('/restservicemodular/services/ReportRestService.svc/GetDataReportAllEquipoment_V/', function (request, response) {
    handle_GetDataReportAllEquipoment_V_request(request, response);
});

router.get('/restservicemodular/services/ReportRestService.svc/GetDataReportAllEquipoment_VI/', function (request, response) {
    handle_GetDataReportAllEquipoment_VI_request(request, response);
});

router.get('/restservicemodular/services/EquipmentRestService.svc/GetGridStatusEquipment/', function (request, response) {
    handle_GetGridStatusEquipment_request(request, response);
});
router.get('/restservicemodular/services/MetaDataRestService.svc/GetUpdateData/', function (request, response) {
    handle_GetUpdate_request(request, response);
});


router.get('/restservicemodular/services/ReportRestService.svc/GetDataReport_IV/:equipment', function (request, response) {
    handle_GetDataReport_IV_request(request, response);
});

router.get('/restservicemodular/services/ReportRestService.svc/GetDataReport_V/:equipment', function (request, response) {
    handle_GetDataReport_V_request(request, response);
});

router.get('/restservicemodular/services/ReportRestService.svc/GetDataReport_VI/:equipment', function (request, response) {
    handle_GetDataReport_VI_request(request, response);
});

router.get('/restservicemodular/services/MessageRestService.svc/GetNotificationList/', function (request, response) {
    handle_GetNotificationList_request(request, response);
});

router.get('/restservicemodular/services/MineGraphicsRestService.svc/GetPitgraphics/', function (request, response) {
    handle_GetPitgraphics_request(request, response);
});

router.get('/restservicemodular/services/MineGraphicsRestService.svc/GetinfoMineGraphicsById/:equipment', function (request, response) {
    handle_GetinfoMineGraphicsById_request(request, response);
});

router.get('/restservicemodular/getDataChar1', function (request, response) {
    handle_getDataChar1_request(request, response);
});

router.get('/restservicemodular/getDataInput', function (request, response) {
    handle_getDataInput_request(request, response);
});

router.get('/restservicemodular/getDataCharCms', function (request, response) {
    handle_getDataCharCms_request(request, response);
});

router.get('/restservicemodular/getDataCharCmz', function (request, response) {
    handle_getDataCharCmz_request(request, response);
});

router.get('/restservicemodular/getDataCharValidate', function (request, response) {
    handle_getDataCharValidate_request(request, response);
});

router.get('/restservicemodular/getDataCharValidateMensual', function (request, response) {
    handle_getDataCharValidateMensual_request(request, response);
});

router.get('/restservicemodular/getDataChar1Angular', function (request, response) {
      handle_getDataChar1Angular_request(request, response);
});

router.get('/restservicemodular/getDataChar2Angular', function (request, response) {
      handle_getDataChar2Angular_request(request, response);
});

router.get('/restservicemodular/getDataChar3Angular', function (request, response) {
      handle_getDataChar3Angular_request(request, response);
});

router.get('/restservicemodular/getDataChar4Angular', function (request, response) {
      handle_getDataChar4Angular_request(request, response);
});

router.get('/restservicemodular/getDataChar5Angular', function (request, response) {
      handle_getDataChar5Angular_request(request, response);
});

router.get('/restservicemodular/getDataChar6Angular', function (request, response) {
      handle_getDataChar6Angular_request(request, response);
});

router.get('/restservicemodular/getDataChar7Angular', function (request, response) {
      handle_getDataChar7Angular_request(request, response);
});

router.get('/restservicemodular/getDataChar8Angular', function (request, response) {
      handle_getDataChar8Angular_request(request, response);
});

router.get('/restservicemodular/getDataChar9Angular', function (request, response) {
      handle_getDataChar9Angular_request(request, response);
});

router.get('/restservicemodular/getDataChar10Angular', function (request, response) {
      handle_getDataChar10Angular_request(request, response);
});

router.get('/restservicemodular/getDataChar11Angular', function (request, response) {
      handle_getDataChar11Angular_request(request, response);
});

router.get('/restservicemodular/getInsertData', function (request, response) {
      handle_getInsertData_request(request, response);
});

router.get('/restservicemodular/getInsertDataMensual', function (request, response) {
    handle_getInsertDataMensual_request(request, response);
});

router.get('/restservicemodular/getInsertDataDirtyMensual', function (request, response) {
    handle_getInsertDataDirtyMensual_request(request, response);
});

router.get('/restservicemodular/getInsertDataImages', function (request, response) {
      handle_getInsertDataImages_request(request, response);
});

router.post('/restservicemodular/getInsertDataImagesMensual', function (request, response) {
    handle_getInsertDataImagesMensual_request(request, response);
});

router.post('/restservicemodular/getInsertDataImagesDirtyMensual', function (request, response) {
    handle_getInsertDataDirtyImagesMensual_request(request, response);
});

router.get('/restservicemodular/getPDFExample', function (request, response) {
      handle_getPDF_request(request, response);
});

router.get('/restservicemodular/getReportServer', function (request, response) {
      handle_getReportServer_request(request, response);
});

router.get('/restservicemodular/getReportServerMensual', function (request, response) {
    handle_getReportServerMensual_request(request, response);
});

router.get('/restservicemodular/getInformationKPIMensual', function (request, response) {
    handle_getInformationKPIMensual(request, response);
});

router.post('/restservicemodular/uploadFile', function (request, response) {
    handle_uploadFile_request(request, response);
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Content-Type", "application/json");
    next();
  });
  app.use(morgan('combined', { stream: winston.stream }));
 // winston.info('You have successfully started working with winston and morgan');
  app.use(express.static(__dirname + '/public')); 
  app.use(bodyParser.json({limit: '50gb'}));
  app.use(bodyParser.urlencoded({limit: '50gb', extended: true}));
  app.use(router);
  


app.listen(6699, function () {
    console.log("Node server running on http://localhost:6699");
    
});

app.get('*', function(req, res) {
        res.sendFile(__dirname + '/public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });

    