exports.get_data_char5_angular = function (request, response) {
    var sql = require("mssql");

     var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    function getEmp() {
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var IntegridadSistema = {};
            IntegridadSistema['IntegridadSistema'] = [];

            var labels = {};
            labels['labels'] = [];

            var Target = {};
            Target['Target'] = [];

            var data2 = {};
            data2['data2'] = [];



            var req = new sql.Request(conn);


           sql = "create table #TempA(Clientes varchar(20),Indicador int,[Target] int) " +
                " insert into #TempA " +
                " exec [MLBOperational].[dbo].["+ option_where(request.query.cliente)+"] " +
                 "" + query10_cliente(request.query.cliente, 5) +
                //" @cliente = '" + request.query.cliente + "',@indicador = 5, @datefirts = 3,@numdate1 =21, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
                " select Clientes, " +
                " Indicador 'Integridad', " +
                " [Target] 'Target' " +
                " from #TempA " +
                " drop table #TempA";


            req.query(sql).then(function (recordset) {

                var datos = {};
                datos = recordset[0]["Clientes"];

                var arr1 = new Array();
                var arr2 = new Array();
                

                for (var i = 0; i < recordset.length; i++) {
                    labels['labels'].push(recordset[i]["Clientes"]);
                    arr1.push(recordset[i]["Integridad"]);
                    arr2.push(recordset[i]["Target"]);
                    //arr3.push(recordset[i]["Grupo3"]);
                    //arr4.push(recordset[i]["Grupo4"]);
                    //arr5.push(recordset[i]["Tendencia"]);
                    //arr6.push(recordset[i]["Target"]);

                }








                data_all['data'].push(labels);
                // data_all['data'].push(IntegridadSistema);
                //data_all['data'].push(Target);

                data2['data2'].push(arr1);
                data2['data2'].push(arr2);
                
                data_all['data'].push(data2);
                var json = JSON.stringify(data_all);

                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })
    }

    function option_where(cliente) {
        var string_where = "";
        var s1 = "Bench_Dinamico_Integridad_Semanal";
        var s2 = "Bench_Dinamico_Integridad_Semanal_2";
        //var s3 = "Bench_KpiChile_SemanalCMCC";

        switch (cliente) {
            case "cmh":
                string_where = s2;
                break;
            case "dmbl":
                string_where = s2;
                break;
            case "mlb":
                string_where = s2;
                break;
            case "mel":
                string_where = s2;
                break;
            case "msc":
                string_where = s2;
                break;     

            default:
                string_where = s1;
        }
            return string_where;

    }


    function query10_cliente(cliente, indicador) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @indicador = " + indicador + " , @datefirts = 4, @numdate1 = 21, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp',  @indicador = " + indicador + ",  @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @indicador = " + indicador + ", @datefirts = 2, @numdate1 = 21, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlb')
             string_query = " @cliente = 'mlb', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mel')
             string_query = " @cliente = 'mel', @indicador = " + indicador + ",  @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 ";
        return string_query;

    }

    getEmp();
}