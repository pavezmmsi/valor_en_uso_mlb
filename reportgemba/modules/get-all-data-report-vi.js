exports.get_all_data_report_vi = function (request, response) {
     /* var fs = require('fs');
    var contents = fs.readFileSync('C:/ftp_modular/kpi_drills_byhour.txt').toString();
    response.writeHead(200, { "Content-Type": "application/json" });

    var metadata = {} // empty Object    
    var key = 'data';
    metadata[key] = []; // empty Array, which you can push() values into   
    var datos = [];
    var linea = [];
    datos = contents.split('\n');
    for (a = 0; a < datos.length; a++) {
        linea = datos[a].split('|');
        //console.log(linea);

        var data = {
            equipment: linea[0],
            hour1: linea[3],
            hour2: linea[4],
            hour3: linea[5],
            hour4: linea[6],
            hour5: linea[7],
            hour6: linea[8],
            hour7: linea[9],
            hour8: linea[10],
            hour9: linea[11],
            hour10: linea[12],
            hour11: linea[13],
            hour12: linea[14],
            kpi: linea[1],
            total: linea[2]
            
        };
        metadata[key].push(data);

    }
    var json = JSON.stringify(metadata);
    response.end(json);*/


    var data_all = {};
    data_all['data'] = [];
    var data = {};
    data['data'] = [];
    var labels = {};
    labels['labels'] = [];
    var data_all_data = [];
    var series = {};
    series['series'] = [];
    var data_labels = [];
    var data_series = [];// empty Array, which you can push() values into   

    var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://modular:12345678@localhost:27017/modular";

    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var query = {};
        //query = { equipment: { $nin: ['equipo', 'Total Cargado'] } }
        query = { equipment: request.params.equipment  };
        db.collection("kpi_drills_byhour").find(query).sort( { kpi: -1 } ).toArray(function (err, result) {

            query = { equipment: 'equipo' };
            db.collection("kpi_drills_byhour").find(query).toArray(function (err2, result2) {
                if (err) throw err;
                for (a = 0; a < result2.length; a = a + 1) {
                   
                    data_labels.push(result2[0].hour1);
                    data_labels.push(result2[0].hour2);
                    data_labels.push(result2[0].hour3);
                    data_labels.push(result2[0].hour4);
                    data_labels.push(result2[0].hour5);
                    data_labels.push(result2[0].hour6);
                    data_labels.push(result2[0].hour7);
                    data_labels.push(result2[0].hour8);
                    data_labels.push(result2[0].hour9);
                    data_labels.push(result2[0].hour10);
                    data_labels.push(result2[0].hour11);
                    data_labels.push(result2[0].hour12);
                }
                labels['labels'].push(data_labels);

                 var data_all_ = [];
                for (a = 0; a < result.length; a++) {
                    data_all_data = [];
                    data_all_data.push(result[a].hour1);
                    data_all_data.push(result[a].hour2);
                    data_all_data.push(result[a].hour3);
                    data_all_data.push(result[a].hour4);
                    data_all_data.push(result[a].hour5);
                    data_all_data.push(result[a].hour6);
                    data_all_data.push(result[a].hour7);
                    data_all_data.push(result[a].hour8);
                    data_all_data.push(result[a].hour9);
                    data_all_data.push(result[a].hour10);
                    data_all_data.push(result[a].hour11);
                    data_all_data.push(result[a].hour12);
                    data['data'].push(data_all_data);
                }
                db.close();

                data_all['data'].push(data);
                data_all['data'].push(labels);

                var json = JSON.stringify(data_all);
                response.end(json);
            });
        });
    });


}