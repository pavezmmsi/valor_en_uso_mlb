exports.get_data_report_cca_chart2 = function (request, response) {
    var sql = require("mssql");

    var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var log = require('debug-logger')('AppReport');

    var json = {};

    
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var data2 = {};
            data2['data2'] = [];

            var labels = {};
            labels['labels'] = [];

            


            var req = new sql.Request(conn);

            //Ranking Integridad sistema
            /*sql = " SELECT K.Organization, "+
                  " CASE K.MES "+
                " WHEN 12 THEN 'Diciembre'  "+
               " WHEN 11 THEN 'Noviembre' "+
               " WHEN 10 THEN 'Octubre' "+
               " WHEN 9 THEN 'Septiembre' "+
               " WHEN 8 THEN 'Agosto'  "+
               " WHEN 7 THEN 'Julio' "+
               " WHEN 6 THEN 'Junio' "+
               " WHEN 5 THEN 'Mayo' "+
               " WHEN 4 THEN 'Abril' "+
               " WHEN 3 THEN 'Marzo' "+
               " WHEN 2 THEN 'Febrero' "+
               " WHEN 1 THEN 'Enero' "+
               " ELSE '' "+
             " END as Mes ,   "+          
            " K.ticketResueltos"+
       " FROM ((SELECT    COUNT(*) AS ticketResueltos ,"+
            "  [Organization], MONTH(SYSDATETIME()) AS MES"+
     " FROM      ZendeskImport.dbo.tickets "+
     " WHERE   "+  
      "     YEAR([Solved at]) = YEAR(SYSDATETIME())"+
     " AND       MONTH([Solved at]) = MONTH(SYSDATETIME()) "+
     " AND    [Category  list ] in  ('Customer Care Agreement','Fee for Service', 'Fee for Service - Not Charged') "+
     " AND       [Organization] LIKE '%MANTOS%'"+
     " GROUP BY  YEAR([Created at]), [Organization])"+
     " UNION"+
     " (SELECT    COUNT(*) AS ticketResueltos ,    "+    
     "  [Organization], MONTH(SYSDATETIME())-1 AS MES"+
     " FROM      ZendeskImport.dbo.tickets "+
     " WHERE    "+ 
      "     YEAR([Solved at]) = YEAR(SYSDATETIME())"+
     " AND       MONTH([Solved at]) = MONTH(SYSDATETIME()) -1"+
     " AND    [Category  list ] in  ('Customer Care Agreement','Fee for Service', 'Fee for Service - Not Charged')"+
     " AND       [Organization] LIKE '%MANTOS%'"+
     " GROUP BY  YEAR([Created at]), [Organization])"+
     " UNION"+
     " (SELECT    COUNT(*) AS ticketResueltos ,"+
      "       [Organization], MONTH(SYSDATETIME())-2 AS MES"+
     " FROM      ZendeskImport.dbo.tickets "+
     " WHERE  "+   
     "      YEAR([Solved at]) = YEAR(SYSDATETIME())"+
     " AND       MONTH([Solved at]) = MONTH(SYSDATETIME()) -2 "+
     " AND    [Category  list ] in  ('Customer Care Agreement','Fee for Service', 'Fee for Service - Not Charged')"+
     " AND       [Organization] LIKE '%MANTOS%'"+
     " GROUP BY  YEAR([Created at]), [Organization])"+
     "      UNION "+
     " (SELECT    COUNT(*) AS ticketResueltos ,"+
      "      [Organization], MONTH(SYSDATETIME())-3 AS MES "+
     " FROM      ZendeskImport.dbo.tickets "+
     " WHERE    "+ 
     "      YEAR([Solved at]) = YEAR(SYSDATETIME())"+
     " AND       MONTH([Solved at]) = MONTH(SYSDATETIME()) -3"+
     " AND    [Category  list ] in  ('Customer Care Agreement','Fee for Service', 'Fee for Service - Not Charged')"+
     " AND       [Organization] LIKE '%MANTOS%'"+
     " GROUP BY  YEAR([Created at]), [Organization])"+
     "  UNION "+
     " (SELECT    COUNT(*) AS ticketResueltos , "+
     "  [Organization], MONTH(SYSDATETIME())-4 AS MES "+
     " FROM      ZendeskImport.dbo.tickets  "+
     " WHERE   "+ 
     "      YEAR([Solved at]) = YEAR(SYSDATETIME()) "+
     " AND       MONTH([Solved at]) = MONTH(SYSDATETIME()) -4 "+
     " AND    [Category  list ] in  ('Customer Care Agreement','Fee for Service', 'Fee for Service - Not Charged')"+
     " AND       [Organization] LIKE '%MANTOS%'"+
     " GROUP BY  YEAR([Created at]), [Organization])"+
     "  UNION"+
     " (SELECT    COUNT(*) AS ticketResueltos ,"+
     "  [Organization], MONTH(SYSDATETIME())-5 AS MES"+
     " FROM      ZendeskImport.dbo.tickets "+
     " WHERE "+    
     "      YEAR([Solved at]) = YEAR(SYSDATETIME())"+
     " AND       MONTH([Solved at]) = MONTH(SYSDATETIME()) -5"+
     " AND    [Category  list ] in  ('Customer Care Agreement','Fee for Service', 'Fee for Service - Not Charged')"+
     " AND       [Organization] LIKE '%MANTOS%'"+
     " GROUP BY  YEAR([Created at]), [Organization])) AS K"+
     "      ORDER BY K.MES desc  ";*/

                sql =  " select a.month as Mes, b.tickets as [Tickets_Creados], ISNULL(c.tickets, 0) as [Tickets_Resueltos], [Organization] " +
                   " FROM"+
                   " (select distinct CAST([created at] AS varchar(7))as [Month],datepart(mm,[Created at]) as [mo],datepart(yy,[Created at]) as [yr]"+
                   " from ZenDeskImport.dbo.tickets"+
                   " where [Assignee] not like '%Arti Pathrudkar%' and [Created at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-6, 0)  "+
                  " and [Organization] like '%" + request.query.cliente +"%' "+
                   " )  A"+
                   " LEFT JOIN"+
                  " (select CAST([Created at] AS varchar(7))as [Month],count(id) as [Tickets], [Organization] "+
                  " from zendeskimport.dbo.tickets"+
                 "  where [Assignee] not like '%Arti Pathrudkar%' and [group] = 'First tier - Chile'"+
                  " and [category  list ] = 'Customer Care Agreement' "+
                  " and [Organization] like '%" + request.query.cliente +"%' "+
                "   group by Organization,CAST([Created at] as varchar(7))) B "+
                  " ON A.Month = B.Month"+
                 "  LEFT JOIN"+
                  " (select CAST([solved at] AS varchar(7))as [Month],count(id) as [Tickets]"+
                  " from zendeskimport.dbo.tickets"+
                 "  where [Assignee] not like '%Arti Pathrudkar%' and [group] = 'First tier - Chile'"+
                 "  and [category  list ] = 'Customer Care Agreement' "+
                  " and [Organization] like '%" + request.query.cliente +"%' "+
                  " group by CAST([solved at] as varchar(7))) C"+
                  " ON A.Month = C.Month"+
                  " order by a.Month";

                //log.trace(sql);
                //console.log(sql);

            req.query(sql).then(function (recordset) {                 
               
                data['data'] = recordset;
                 json = JSON.stringify(data);

                 
                // console.log(json);
                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })

     // return json;


    

   
}