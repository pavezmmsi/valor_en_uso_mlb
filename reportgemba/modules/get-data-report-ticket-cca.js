exports.get_data_report_ticket_cca = function (request, response) {
    var sql = require("mssql");

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var log = require('debug-logger')('AppReport');

    var json = {};

    
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var data2 = {};
            data2['data2'] = [];

            var labels = {};
            labels['labels'] = [];

            


            var req = new sql.Request(conn);

            //Ranking Integridad sistema
            sql = " SELECT     Status, Subject, [Category  list ] AS Category, Assignee, replace(convert(NVARCHAR,[Created at],106),' ', '/') AS Created, [Hours in Assigned  dec ] AS Hours, Tags, 'CCA' AS Type " +
            " FROM         ZenDeskImport.dbo.tickets " +
            " where [group] = 'First tier - Chile'"+
          "  and [category  list ] = 'Customer Care Agreement' "+
          "  and [Status]  not in ('Closed','Deleted','Solved')"+
          "  and [Organization] like '%" + request.query.cliente +"%'   "+
          "  order by [Created at] desc";
            
                //log.trace(sql);
               // console.log(request.query.cliente);

            req.query(sql).then(function (recordset) {                 
               
                data['data'] = recordset;
                 json = JSON.stringify(data);

                 
                 //console.log(json);
                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })

     // return json;


    

   
}