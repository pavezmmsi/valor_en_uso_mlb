exports.get_data_report_ticket_rma2 = function (request, response) {
    var sql = require("mssql");

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var log = require('debug-logger')('AppReport');

    var json = {};

    
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var data2 = {};
            data2['data2'] = [];

            var labels = {};
            labels['labels'] = [];

            
             console.log(request.query.id_cliente);

            var req = new sql.Request(conn);

         sql=  "  select a.month as Mes, isnull(b.Equipos,0) as [Equipos_Ingresados], isnull(c.Equipos,0) as [Equipos_Reparados], isnull(d.Equipos,0) as [Equipos_esperando]"+
          "  FROM"+
          "  (select CAST([created at] AS varchar(7))as [Month],datepart(mm,[Created at]) as [mo],datepart(yy,[Created at]) as [yr]"+
          "  from ZenDeskImport.dbo.Tickets"+
          "  where [Created at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0)"+
          " and ([Subject] like '%"+ request.query.id_cliente + "%' or [Organization] like '%"+ request.query.cliente +"%')  " +
          "  and [Category  list ] = 'RMA Request' and [GROUP] = 'RMA - Chile' "+
          "  )  A"+
          " LEFT JOIN"+
          "  (select CAST([Created at] AS varchar(7))as [Month],sum(cast([Quantity Returning] as int)) as [Equipos]"+
          "  from zendeskimport.dbo.Tickets"+
          "  where [Created at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0)"+
          " and ([Subject] like '%"+ request.query.id_cliente + "%' or [Organization] like '%"+ request.query.cliente +"%')  " +
          "  and [Category  list ] = 'RMA Request' and [GROUP] = 'RMA - Chile' "+
          "  group by CAST([Created at] as varchar(7))) B "+
          "  ON A.Month = B.Month"+
          "  LEFT JOIN"+
           " (select CAST([solved at] AS varchar(7))as [Month],sum(cast([Quantity Returning] as int)) as [Equipos]"+
          "  from zendeskimport.dbo.Tickets"+
          "  where [solved at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0)"+
          " and ([Subject] like '%"+ request.query.id_cliente + "%' or [Organization] like '%"+ request.query.cliente +"%')  " +
          "  and [Category  list ] = 'RMA Request' and [GROUP] = 'RMA - Chile' "+
         "   group by CAST([solved at] as varchar(7))) C"+
         "   ON A.Month = C.Month"+
          "  LEFT JOIN"+
         "   (select CAST([Created at] AS varchar(7))as [Month],sum(cast([Quantity Returning] as int)) as [Equipos]"+
         "   from zendeskimport.dbo.Tickets"+
         "   where [Created at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0)"+
         " and ([Subject] like '%"+ request.query.id_cliente + "%' or [Organization] like '%"+ request.query.cliente +"%')  " +
          "  and [Category  list ] = 'RMA Request' and [GROUP] = 'RMA - Chile' "+
          "  and [Stage  list ] = 'Requires PO'"+
          "  group by CAST([created at] as varchar(7))) D"+
          "  ON A.Month = D.Month"+
          "  group by A.Month,B.Equipos,C.Equipos,D.Equipos"+
          "  order by a.Month";
               // log.trace(sql); 
               // console.log(sql);

            req.query(sql).then(function (recordset) {                 
               
                data['data'] = recordset;
                 json = JSON.stringify(data);

                 
                // console.log(json);
                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })

     // return json;


    

   
}