exports.get_data_char3_montly = function (request, response) {
    var sql = require("mssql");

     var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    function getEmp() {
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var IntegridadSistema = {};
            IntegridadSistema['IntegridadSistema'] = [];

            var labels = {};
            labels['labels'] = [];

            var Target = {};
            Target['Target'] = [];

            var data2 = {};
            data2['data2'] = [];
            var req = new sql.Request(conn);
            sql = " SET ARITHABORT OFF "+
           " SET ANSI_WARNINGS OFF"+
           " create table #TempB(Clientes varchar(20),Indicador int,[Target] int)"+
           " insert into #TempB"+
           " exec [MLBOperational].[dbo].[Bench_Dinamico_Integridad_Mensual] @cliente = '"+ request.query.id_cliente +"',@indicador = 6"+
            
           " select Clientes,"+
           " Indicador 'UsoDinamico',"+
           " [Target] 'Target'"+
           " from #TempB"+            
          "  drop table #TempB";    

          req.query(sql).then(function (recordset) {
            var datos = {};
            //datos = recordset[0]["Clientes"];
            var arr1 = new Array();
            var arr2 = new Array();   

            for (var i = 0; i < recordset.length; i++) {
                labels['labels'].push(recordset[i]["Clientes"]);
                arr1.push(recordset[i]["UsoDinamico"]);
                arr2.push(recordset[i]["Target"]);
               
            }
            data_all['data'].push(labels);            
            data2['data2'].push(arr1);
            data2['data2'].push(arr2);
                      
            data_all['data'].push(data2);
            var json = JSON.stringify(data_all);
            response.end(json);
            conn.close();
        })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });
        })
    }
    getEmp();
}