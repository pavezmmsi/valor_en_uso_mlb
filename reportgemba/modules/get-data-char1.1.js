exports.get_data_char1 = function (request, response) {
    var sql = require("mssql");

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var cliente_ = request.query.cliente;
    //var get_data_report_json = require('./get-data-char1-json');
    //var aaaa = {};
    //aaaa = get_data_report_json.get_data_char1_json(request, response);

    //console.log(get_data_report_json.get_data_char1_json(request, response));

    function query10_cliente(cliente, indicador) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @indicador = " + indicador + " , @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp',  @indicador = " + indicador + ",  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @indicador = " + indicador + ", @datefirts = 2, @numdate1 = 35, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }


    function query11_cliente(cliente, indicador) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms',@periodo = 'semana',@datefirts = 3, @indicador = " + indicador + ", @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";



        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz', @periodo = 'semana', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @periodo = 'semana' , @indicador = " + indicador + " , @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh', @periodo = 'semana' , @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic',@periodo = 'semana', @indicador = " + indicador + ", @datefirts = 2, @numdate1 = 35, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc',@periodo = 'semana',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }

    function query3_cliente(cliente, indicador) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms',@periodo = 'semana', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz', @periodo = 'semana', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @periodo = 'semana' , @indicador = " + indicador + " , @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh', @periodo = 'semana' , @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic',@periodo = 'semana', @indicador = " + indicador + ", @datefirts = 2, @numdate1 = 35, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc',@periodo = 'semana',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }


    function query15_cliente(cliente, indicador) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";


        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @indicador = " + indicador + " , @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp',  @indicador = " + indicador + ",  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @indicador = " + indicador + ", @datefirts = 2, @numdate1 = 35, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }

    function query_cliente2(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms',  @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz',  @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc',  @datefirts = 4, @numdate1 = 21, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl',  @datefirts = 1, @numdate1 = 35, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp',  @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb',  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh',  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @datefirts = 2, @numdate1 = 21, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc', @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc',  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand',  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlb')
            string_query = " @cliente = 'dand',  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }

    function option_where2(cliente) {
        var string_where = "";
        var s1 = "Bench_KpiChile_Semanal";
        var s2 = "Bench_KpiChile_Semanal_2";
        var s3 = "Bench_KpiChile_SemanalCMCC";

        switch (cliente) {
            case "cmh":
                string_where = s2;
                break;
            case "dmbl":
                string_where = s2;
                break;
            case "mlb":
                string_where = s2;
                break;
            case "cmcc":
                string_where = s3;
                break;
            case "cmz":
                string_where = s3;
                break;

            default:
                string_where = s1;
        }
        return string_where;

    }


    function query_cliente4(cliente, indicador) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms',@periodo = 'semana', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz', @periodo = 'semana', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @periodo = 'semana' , @indicador = " + indicador + " , @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh', @periodo = 'semana' , @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic',@periodo = 'semana', @indicador = " + indicador + ", @datefirts = 2, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc',@periodo = 'semana',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlb')
            string_query = " @cliente = 'dand',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }


    function query_cliente5(cliente, indicador) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @indicador = " + indicador + " , @datefirts = 4, @numdate1 = 21, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp',  @indicador = " + indicador + ",  @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @indicador = " + indicador + ", @datefirts = 2, @numdate1 = 21, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlb')
            string_query = " @cliente = 'dand', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }

    function option_where5(cliente) {
        var string_where = "";
        var s1 = "Bench_Dinamico_Integridad_Semanal";
        var s2 = "Bench_Dinamico_Integridad_Semanal_2";
        //var s3 = "Bench_KpiChile_SemanalCMCC";

        switch (cliente) {
            case "cmh":
                string_where = s2;
                break;
            case "dmbl":
                string_where = s2;
                break;
            case "mlb":
                string_where = s2;
                break;

            default:
                string_where = s1;
        }
        return string_where;

    }


    function query_cliente6(cliente, indicador) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms',@periodo = 'semana', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";
        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz', @periodo = 'semana', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @periodo = 'semana' , @indicador = " + indicador + " , @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh', @periodo = 'semana' , @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic',@periodo = 'semana', @indicador = " + indicador + ", @datefirts = 2, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc',@periodo = 'semana',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlb')
            string_query = " @cliente = 'dand',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;
    }


    function query_cliente7(cliente, indicador) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";
        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @indicador = " + indicador + " , @datefirts = 4, @numdate1 = 21, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp',  @indicador = " + indicador + ",  @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @indicador = " + indicador + ", @datefirts = 2, @numdate1 = 21, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 14, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc',  @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlb')
            string_query = " @cliente = 'dand', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 21, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }

    function option_where7(cliente) {
        var string_where = "";
        var s1 = "Bench_Dinamico_Integridad_Semanal";
        var s2 = "Bench_Dinamico_Integridad_Semanal_2";
        //var s3 = "Bench_KpiChile_SemanalCMCC";

        switch (cliente) {
            case "cmh":
                string_where = s2;
                break;
            case "dmbl":
                string_where = s2;
                break;
            case "mlb":
                string_where = s2;
                break;

            default:
                string_where = s1;
        }
        return string_where;

    }

    function query_cliente8(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms', @periodo = 'semana', @datefirts = 3, @indicador = 1,@numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";
        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz', @periodo = 'semana', @datefirts = 3, @indicador = 1,@numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @periodo = 'semana', @datefirts = 4, @indicador = 1,@numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl', @periodo = 'semana', @datefirts = 1,@indicador = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp', @periodo = 'semana', @datefirts = 3,@indicador = 1, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb', @periodo = 'semana', @datefirts = 1,@indicador = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh', @periodo = 'semana', @datefirts = 1, @indicador = 1,@numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @periodo = 'semana', @datefirts = 2,@indicador = 1, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc', @periodo = 'semana', @datefirts = 3, @indicador = 1,@numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc', @periodo = 'semana', @datefirts = 1, @indicador = 1,@numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand', @periodo = 'semana', @datefirts = 1, @indicador = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlb')
            string_query = " @cliente = 'mlb', @periodo = 'semana', @datefirts = 1, @indicador = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }

    function option_where8(cliente) {
        var string_where = "";
        var s1 = "Indicadores_Semanales_Grupos_Proyeccion";
        var s2 = "Indicadores_Semanales_Grupos_Proyeccion2";
        //var s3 = "Bench_KpiChile_SemanalCMCC";

        switch (cliente) {
            case "dlb":
                string_where = s2;
                break;
            case "cmh":
                string_where = s2;
                break;
            case "dand":
                string_where = s2;
                break;
            case "dmbl":
                string_where = s2;
                break;
            case "mlc":
                string_where = s2;
                break;
            case "mlb":
                string_where = s2;
                break;

            default:
                string_where = s1;
        }
        return string_where;

    }


    function query_cliente9(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms', @periodo = 'semana', @datefirts = 3, @indicador = 3,@numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";
        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz', @periodo = 'semana', @datefirts = 3, @indicador = 3,@numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @periodo = 'semana', @datefirts = 4, @indicador = 3,@numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl', @periodo = 'semana', @datefirts = 1,@indicador = 3, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp', @periodo = 'semana', @datefirts = 3,@indicador = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb', @periodo = 'semana', @datefirts = 1,@indicador = 3, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh', @periodo = 'semana', @datefirts = 1, @indicador = 3,@numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @periodo = 'semana', @datefirts = 2,@indicador = 3, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc', @periodo = 'semana', @datefirts = 3, @indicador = 3,@numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc', @periodo = 'semana', @datefirts = 1, @indicador = 3,@numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand', @periodo = 'semana', @datefirts = 1, @indicador = 3, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlb')
            string_query = " @cliente = 'mlb', @periodo = 'semana', @datefirts = 1, @indicador = 3, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }

    function option_where9(cliente) {
        var string_where = "";
        var s1 = "Indicadores_Semanales_Grupos_Proyeccion";
        var s2 = "Indicadores_Semanales_Grupos_Proyeccion2";
        //var s3 = "Bench_KpiChile_SemanalCMCC";

        switch (cliente) {
            case "dlb":
                string_where = s2;
                break;
            case "cmh":
                string_where = s2;
                break;
            case "dand":
                string_where = s2;
                break;
            case "dmbl":
                string_where = s2;
                break;
            case "mlc":
                string_where = s2;
                break;
            case "mlb":
                string_where = s2;
                break;

            default:
                string_where = s1;
        }
        return string_where;

    }

    function query_cliente10(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms', @periodo = 'semana', @datefirts = 3, @indicador = 4,@numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";
        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz', @periodo = 'semana', @datefirts = 3, @indicador = 4,@numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @periodo = 'semana', @datefirts = 4, @indicador = 4,@numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl', @periodo = 'semana', @datefirts = 1,@indicador = 4, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp', @periodo = 'semana', @datefirts = 3,@indicador = 4, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb', @periodo = 'semana', @datefirts = 1,@indicador = 4, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh', @periodo = 'semana', @datefirts = 1, @indicador = 4,@numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @periodo = 'semana', @datefirts = 2,@indicador = 4, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc', @periodo = 'semana', @datefirts = 3, @indicador = 4,@numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc', @periodo = 'semana', @datefirts = 1, @indicador = 4,@numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand', @periodo = 'semana', @datefirts = 1, @indicador = 4, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlb')
            string_query = " @cliente = 'mlb', @periodo = 'semana', @datefirts = 1, @indicador = 4, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }

    function option_where10(cliente) {
        var string_where = "";
        var s1 = "Indicadores_Semanales_Grupos_Proyeccion";
        var s2 = "Indicadores_Semanales_Grupos_Proyeccion2";
        //var s3 = "Bench_KpiChile_SemanalCMCC";

        switch (cliente) {
            case "dlb":
                string_where = s2;
                break;
            case "cmh":
                string_where = s2;
                break;
            case "dand":
                string_where = s2;
                break;
            case "dmbl":
                string_where = s2;
                break;
            case "mlc":
                string_where = s2;
                break;
            case "mlb":
                string_where = s2;
                break;

            default:
                string_where = s1;
        }
        return string_where;

    }

    function query_cliente11(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms', @periodo = 'semana', @datefirts = 3, @indicador = 2,@numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";
        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz', @periodo = 'semana', @datefirts = 3, @indicador = 2,@numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @periodo = 'semana', @datefirts = 4, @indicador = 2,@numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl', @periodo = 'semana', @datefirts = 1,@indicador = 2, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp', @periodo = 'semana', @datefirts = 3,@indicador = 2, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb', @periodo = 'semana', @datefirts = 1,@indicador = 2, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh', @periodo = 'semana', @datefirts = 1, @indicador = 2,@numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @periodo = 'semana', @datefirts = 2,@indicador = 2, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc', @periodo = 'semana', @datefirts = 3, @indicador = 2,@numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc', @periodo = 'semana', @datefirts = 1, @indicador = 2,@numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand', @periodo = 'semana', @datefirts = 1, @indicador = 2, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlb')
            string_query = " @cliente = 'mlb', @periodo = 'semana', @datefirts = 1, @indicador = 2, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }

    function option_where11(cliente) {
        var string_where = "";
        var s1 = "Indicadores_Semanales_Grupos_Proyeccion";
        var s2 = "Indicadores_Semanales_Grupos_Proyeccion2";
        //var s3 = "Bench_KpiChile_SemanalCMCC";

        switch (cliente) {
            case "dlb":
                string_where = s2;
                break;
            case "cmh":
                string_where = s2;
                break;
            case "dand":
                string_where = s2;
                break;
            case "dmbl":
                string_where = s2;
                break;
            case "mlc":
                string_where = s2;
                break;
            case "mlb":
                string_where = s2;
                break;

            default:
                string_where = s1;
        }
        return string_where;

    }



    function query_cliente1(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana] @cliente = 'cms', @periodo = 'semana', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";
        if (cliente == 'cmz')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemanaCMCC]  @cliente = 'cmz', @periodo = 'semana', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'msc', @periodo = 'semana', @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'dmbl', @periodo = 'semana', @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'mlp', @periodo = 'semana', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'dlb', @periodo = 'semana', @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'cmh', @periodo = 'semana', @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'cmdic', @periodo = 'semana', @datefirts = 2, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'cmcc', @periodo = 'semana', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'mlc', @periodo = 'semana', @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'dand', @periodo = 'semana', @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
         if (cliente == 'mlb')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana] @cliente = 'dand', @periodo = 'semana', @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }


    function turno1(cliente) {
        var turno = "";
        var cuatro = " round(sum(G1/4),0) 'G1',	" +
            " round(sum(G2/4),0) 'G2', " +
            " round(sum(G3/4),0) 'G3', " +
            " round(sum(G4/4),0) 'G4' ";

        var dos = " round(sum(G1/2),0) 'G1', " +
            " round(sum(G2/2),0) 'G2', " +
            " round(sum(G3/2),0) 'G3', " +
            " round(sum(G4/2),0) 'G4'  ";

        switch (cliente) {
            case "cmdic":
                turno = cuatro;
                break;
            case "cms":
                turno = cuatro;
                break;
            case "cmcc":
                turno = cuatro;
                break;
            case "mlp":
                turno = cuatro;
                break;
            case "cmz":
                turno = cuatro;
                break;
            case "msc":
                turno = cuatro;
                break;
            case "mel":
                turno = cuatro;
                break;
            default:
                turno = dos;
        }
        return turno;
    }

    function query_cliente3(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc',  @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @datefirts = 2, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlb')
            string_query = " @cliente = 'dand',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        return string_query;

    }


    var conn = new sql.Connection(dbConfig);
    console.log('Cliente ===>' + cliente_);
    conn.connect().then(function () {
        //console.log('ENTRA CONCECION');
        var data_all = {};
        data_all['data'] = [];
        var data = {};
        data['data'] = [];
        var data2 = {};
        data2['data2'] = [];
        var data3 = {};
        data3['data3'] = [];
        var data4 = {};
        data4['data4'] = [];
        var data5 = {};
        data5['data5'] = [];

        var data6 = {};
        data6['data6'] = [];

        var data7 = {};
        data7['data7'] = [];

        var data8 = {};
        data8['data8'] = [];

        var data9 = {};
        data9['data9'] = [];

        var data10 = {};
        data10['data10'] = [];

        var data11 = {};
        data11['data11'] = [];

        var labels = {};
        labels['labels'] = [];

        var ranking = {};
        ranking['ranking'] = [];

        var indicador = {}
        indicador['indicador'] = [];

        var rango = {}
        rango['rango'] = [];

        var ponderacion = {};
        ponderacion['ponderacion'] = [];



        var cargas = {};
        cargas['cargas'] = [];


        var turno = {};
        turno['turno'] = [];
        var grupo = {};
        grupo['grupo'] = [];
        var carga_extra = {};
        carga_extra['carga_extra'] = [];
        var carga_normal = {};
        carga_normal['carga_normal'] = [];
        var carga_total = {};
        carga_total['carga_total'] = [];
        var porcentaje = {};
        porcentaje['porcentaje'] = [];






        //console.log("sssssssssssssssssssss")
        var req = new sql.Request(conn);
       //console.log("cccccccccccccccccccc")
        //Ranking Integridad sistema


        /*sql = "create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float) " +
            "  create table #TempD (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float) 				     " +
            " insert into #TempC												     " +
            " exec [MLBOperational].[dbo].[RankingClientesSemana]								     " + query_cliente(cliente_) +

            " insert into #TempD												     " +
            " select 'Proyección' Periodo,											     " +
            " avg(G1) 'G1',													     " +
            " avg(G2) 'G2',													     " +
            " avg(G3) 'G3',													     " +
            " avg(G4) 'G4'													     " +
            " from														     " +
            " (														     " +
            " select 													     " +
            " round(sum(G1/2),0) 'G1',											     " +
            " round(sum(G2/2),0) 'G2',											     " +
            " round(sum(G3/2),0) 'G3',											     " +
            " round(sum(G4/2),0) 'G4'											     " +
            " from #TempC													     " +
            " )TEMP1													     " +
            " 														     " +
            " insert into #TempC												     " +
            " select * from #TempD												     " +
            " 														     " +
            " select Periodo,												     " +
            " G1 'Grupo1',													     " +
            " G2 'Grupo2',													     " +
            " G3 'Grupo3',													     " +
            " G4 'Grupo4'													     " +
            "  from #Tempc C												     " +
            " 														     " +
            " drop table #TempC,#TempD											     ";*/

        sql = "create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float) " +
            "  create table #TempD (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float) " +
            " insert into #TempC   " + query_cliente1(cliente_) +
            //" @cliente = '" + request.query.cliente + "', @periodo = 'semana', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  " +
            " insert into #TempD " +
            " select 'Proyeccion' Periodo, " +
            " avg(G1) 'G1', " +
            " avg(G2) 'G2', " +
            " avg(G3) 'G3', " +
            " avg(G4) 'G4'  " +
            " from	 " +
            " (	  " +
            " select  " + turno1(cliente_) +
            //" round(sum(G1/2),0) 'G1',											     " +
            //" round(sum(G2/2),0) 'G2',											     " +
            //" round(sum(G3/2),0) 'G3',											     " +
            //" round(sum(G4/2),0) 'G4'											     " +
            " from #TempC " +
            " )TEMP1 " +

            " insert into #TempC  " +
            " select * from #TempD " +
            " select Periodo, " +
            " G1 'Grupo1',  " +
            " G2 'Grupo2',  " +
            " G3 'Grupo3',   " +
            " G4 'Grupo4' " +
            "  from #Tempc C   " +
            " drop table #TempC,#TempD   ";


           //console.log("sql1");


        /*sql2 = "create table #TempA(Clientes varchar(20),Indicador int) " +
            " insert into #TempA " +
            " exec [MLBOperational].[dbo].[Bench_KpiChile_Semanal] " + query_cliente2(cliente_) +
            " select Clientes, " +
            " Indicador 'IntegridadSistema', " +
            " 90 'Target' " +
            " from #TempA " +
            " drop table #TempA ";*/


        sql2 = "create table #TempA(Clientes varchar(20),Indicador int) " +
            " insert into #TempA " +
            " exec [MLBOperational].[dbo].[" + option_where2(cliente_) + "] " + query_cliente2(cliente_) +
            // " exec [MLBOperational].[dbo].[Bench_KpiChile_Semanal] "+   
            //" @cliente = '" + request.query.cliente + "', @datefirts = 3,@numdate1 =21, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 "+
            " select Clientes, " +
            " Indicador 'IntegridadSistema', " +
            " 90 'Target' " +
            " from #TempA " +
            " drop table #TempA ";

          //console.log("sql2");

        //Integridad (Spot & Load)    
        //sql3 = " exec [MLBOperational].[dbo].[Integridad_detalle_Semanal_]" + query_cliente2(cliente_); //@cliente = 'cms', @datefirts = 3,@numdate1 =35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1";
        sql3 = " exec [MLBOperational].[dbo].[Integridad_detalle_Semanal_]" + query_cliente3(cliente_);

        //console.log("sql3");
        //Integridad (Spot & Load) por grupo
        /*sql4 = "create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempD (Periodo varchar(20),Indicador float, [Target] float) " +
            " insert into #TempC " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] " + query3_cliente(cliente_, 6) +
            " insert into #TempD " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] " + query3_cliente(cliente_, 6) +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4],  " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'TargetTarget' " +
            " from #TempC A " +
            " inner join #TempD B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempC, #TempD";*/

        sql4 = "create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempD (Periodo varchar(20),Indicador float, [Target] float) " +
            " insert into #TempC " +
            //" exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = '" + request.query.cliente + "',  " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] " + query_cliente4(cliente_, 5) +
            //" @periodo = 'semana', @datefirts = 3, @indicador = 5, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " insert into #TempD " +
            //" exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = '" + request.query.cliente + "', " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] " + query_cliente4(cliente_, 5) +
            //" @periodo = 'semana', @datefirts = 3, @indicador = 5, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4],  " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempC A " +
            " inner join #TempD B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempC, #TempD";

         //console.log("sql4");

        //BenchMark Integridad Spot & Load

        // console.log(sql4);   

        /*sql5 = "create table #TempA(Clientes varchar(20),Indicador int,[Target] int) " +
            " insert into #TempA " +
            " exec [MLBOperational].[dbo].[Bench_Dinamico_Integridad_Semanal] " +
            "" + query10_cliente(cliente_, 5) +
            " select Clientes, " +
            " Indicador 'Integridad', " +
            " [Target] 'Target' " +
            " from #TempA " +
            " drop table #TempA";*/

        sql5 = "create table #TempA(Clientes varchar(20),Indicador int,[Target] int) " +
            " insert into #TempA " +
            " exec [MLBOperational].[dbo].[" + option_where5(cliente_) + "] " +
            " " + query_cliente5(cliente_, 5) +
            //" @cliente = '" + request.query.cliente + "',@indicador = 5, @datefirts = 3,@numdate1 =21, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " select Clientes, " +
            " Indicador 'Integridad', " +
            " [Target] 'Target' " +
            " from #TempA " +
            " drop table #TempA";

         //console.log("sql5");   

        //console.log(sql5);      
        //Uso dinamico por grupo
        /*sql6 = "create table #TempE (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempF (Periodo varchar(20),Indicador float, [Target] float) " +
            " insert into #TempE " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] " + query11_cliente(cliente_, 6) +
            " insert into #TempF " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend]" + query11_cliente(cliente_, 6) +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4], " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'TargetTarget' " +
            " from #TempE A " +
            " inner join #TempF B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempE, #TempF";*/
        sql6 = "create table #TempE (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempF (Periodo varchar(20),Indicador float, [Target] float) " +
            " insert into #TempE " +
            //" exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = '" + request.query.cliente + "', " +
            //" @periodo = 'semana', @datefirts = 3, @indicador = 6, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] " + query_cliente6(cliente_, 6) +
            " insert into #TempF " +
            //" exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = '" + request.query.cliente + "', " +
            //" @periodo = 'semana', @datefirts = 3, @indicador = 6, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend]" + query_cliente6(cliente_, 6) +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4], " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempE A " +
            " inner join #TempF B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempE, #TempF";
         
        //console.log("sql6");

        //BenchMark Uso Dinámico 



        /*sql7 = " create table #TempA(Clientes varchar(20),Indicador int,[Target] int) " +
            "  insert into #TempA " +
            "  exec [MLBOperational].[dbo].[Bench_Dinamico_Integridad_Semanal]   " + query15_cliente(cliente_, 6) +
            //"  @cliente = 'cms',@indicador = 6, @datefirts = 3,@numdate1 =21, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            "  select Clientes, " +
            "  Indicador 'UsoDinamico', " +
            "  [Target] 'Target' " +
            "  from #TempA " +
            "  drop table #TempA";*/
        sql7 = " create table #TempA(Clientes varchar(20),Indicador int,[Target] int) " +
            "  insert into #TempA " +
            "  exec [MLBOperational].[dbo].[" + option_where7(cliente_) + "]   " + query_cliente7(cliente_, 6) +
            //"  exec [MLBOperational].[dbo].[Bench_Dinamico_Integridad_Semanal]   " +
            //"  @cliente = '" + request.query.cliente + "',@indicador = 6, @datefirts = 3,@numdate1 =21, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            "  select Clientes, " +
            "  Indicador 'UsoDinamico', " +
            "  [Target] 'Target' " +
            "  from #TempA " +
            "  drop table #TempA";


        //ciclos corto por grupo

        /*sql8 = "create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempD (Periodo varchar(20),Indicador float, [Target] float) " +
            " insert into #TempC " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] " + query_cliente4(cliente_) +
            " insert into #TempD exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] " + query_cliente4(cliente_) +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4], " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempC A " +
            " inner join #TempD B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempC, #TempD";*/

        sql8 = "create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempD (Periodo varchar(20),Indicador float, [Target] float) " +
            " insert into #TempC " +
            " exec [MLBOperational].[dbo].[" + option_where8(cliente_) + "] " + query_cliente8(cliente_) +
            " insert into #TempD exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] " + query_cliente8(cliente_) +
            //" exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = '" + request.query.cliente + "',  " +
            //" @periodo = 'semana', @datefirts = 3, @indicador = 1, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            //" insert into #TempD exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = '" + request.query.cliente + "', " +
            //" @periodo = 'semana', @datefirts = 3, @indicador = 1, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4], " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempC A " +
            " inner join #TempD B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempC, #TempD";

        //Cargas Extras por grupo

        /*sql9 = " create table #TempE (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempF (Periodo varchar(20),Indicador float, [Target] float) " +
            "  insert into #TempE " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] " + query_cliente5(cliente_) +
            " insert into #TempF " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] " + query_cliente5(cliente_) +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4], " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempE A " +
            "  inner join #TempF B " +
            "  on A.Periodo = B.Periodo " +
            " drop table #TempE, #TempF";*/

        sql9 = " create table #TempE (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempF (Periodo varchar(20),Indicador float, [Target] float) " +
            "  insert into #TempE " +
            " exec [MLBOperational].[dbo].[" + option_where9(cliente_) + "] " + query_cliente9(cliente_) +
            //" exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = '" + request.query.cliente + "', " +
            //" @periodo = 'semana', @datefirts = 3, @indicador = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " insert into #TempF " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] " + query_cliente9(cliente_) +
            //" exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = '" + request.query.cliente + "', " +
            //" @periodo = 'semana', @datefirts = 3, @indicador = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4], " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempE A " +
            "  inner join #TempF B " +
            "  on A.Periodo = B.Periodo " +
            " drop table #TempE, #TempF";

        //Balizas Perdidas por Grupo

        /*sql10 = " create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempD (Periodo varchar(20),Indicador float, [Target] float) " +
            " insert into #TempC " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] " + query_cliente6(cliente_) +
            " insert into #TempD " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] " + query_cliente6(cliente_) +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4], " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempC A " +
            " inner join #TempD B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempC, #TempD";*/

        sql10 = " create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempD (Periodo varchar(20),Indicador float, [Target] float) " +
            " insert into #TempC " +
            " exec [MLBOperational].[dbo].[" + option_where10(cliente_) + "] " + query_cliente10(cliente_) +
            //" exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = '" + request.query.cliente + "', " +
            //" @periodo = 'semana', @datefirts = 3, @indicador = 4, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " insert into #TempD " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] " + query_cliente10(cliente_) +
            //" exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = '" + request.query.cliente + "', " +
            //" @periodo = 'semana', @datefirts = 3, @indicador = 4, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4], " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempC A " +
            " inner join #TempD B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempC, #TempD";


        //Llegadas Tarde por grupo
        /*sql11 = "create table #TempE (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            "  create table #TempF (Periodo varchar(20),Indicador float, [Target] float) " +
            "  insert into #TempE " +
            "  exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] " + query_cliente7(cliente_) +
            "  insert into #TempF " +
            "  exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] " + query_cliente7(cliente_) +
            "  select A.Periodo, " +
            "  G1 [Grupo1], " +
            "  G2 [Grupo2], " +
            "  G3 [Grupo3], " +
            "  G4 [Grupo4], " +
            "  Indicador 'Tendencia', " +
            "  A.[Target] as 'Target' " +
            "  from #TempE A " +
            " inner join #TempF B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempE, #TempF";*/

        sql11 = "create table #TempE (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            "  create table #TempF (Periodo varchar(20),Indicador float, [Target] float) " +
            "  insert into #TempE " +
            "  exec [MLBOperational].[dbo].[" + option_where11(cliente_) + "] " + query_cliente11(cliente_) +
            //"  exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = '" + request.query.cliente + "', " +
            //"  @periodo = 'semana', @datefirts = 3, @indicador = 2, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            "  insert into #TempF " +
            "  exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] " + query_cliente11(cliente_) +
            //"  exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = '" + request.query.cliente + "', " +
            //"  @periodo = 'semana', @datefirts = 3, @indicador = 2, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            "  select A.Periodo, " +
            "  G1 [Grupo1], " +
            "  G2 [Grupo2], " +
            "  G3 [Grupo3], " +
            "  G4 [Grupo4], " +
            "  Indicador 'Tendencia', " +
            "  A.[Target] as 'Target' " +
            "  from #TempE A " +
            " inner join #TempF B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempE, #TempF";
        
        //console.log("fin SQL");
        req.query(sql).then(function (recordset) {
              //console.log("1");
            sqlLabel = "SELECT  [Id_Cliente] " +
                " , [Id_Informe] " +
                " , [Id_type] " +
                " , [Label1] " +
                " , [Label2] " +
                " , [Label3] " +
                " , [Intro] " +
                " , [Estado1] " +
                " , [Comentario1] " +
                " , [Estado2] " +
                " , [Comentario2] " +
                " , [Estado3] " +
                " , [Comentario3] " +
                " , [Estado4]  " +
                " , [Comentario4] " +
                " , [Estado5] " +
                " , [Comentario5] " +
                " , [Estado6] " +
                " , [Comentario6] " +
                " , [Estado7] " +
                " , [Comentario7] " +
                " , [Estado8] " +
                " , [Comentario8] " +
                " , [Estado9] " +
                " , [Comentario9] " +
                " , [Estado10] " +
                " , [Comentario10] " +
                " , [Estado11] " +
                " , [Comentario11] " +
                " , [File1] " +
                " , [File2] " +
                " , [File3] " +
                " FROM [TableLabel] " +
                " WHERE [id_cliente] = '" + cliente_ + "'";

            sqlRanking = " SELECT RTRIM([id_cliente]) " +
                ",RTRIM([id_informe])" +
                ",RTRIM([indicador]) as indicador" +
                ",RTRIM([rango]) as rango" +
                ",RTRIM([ponderacion]) as ponderacion" +
                " FROM [MLBOperational].[dbo].[Ranking]" +
                " WHERE [id_cliente] = '" + cliente_ + "'";

            sqlCargasExtras = "SELECT RTRIM([id_cliente])" +
                ",RTRIM([id_informe])" +
                ",RTRIM([turno]) as turno " +
                ",RTRIM([grupo]) as grupo" +
                ",RTRIM([carga_extra]) as carga_extra " +
                ",RTRIM([carga_normal]) as carga_normal " +
                ",RTRIM([carga_total]) as carga_total " +
                ",RTRIM([porcentaje]) as porcentaje " +
                "FROM [MLBOperational].[dbo].[CargasExtras]" +
                " WHERE [id_cliente] = '" + cliente_ + "'";

            req.query(sqlRanking).then(function (recordsetRanking) {
                //console.log("2");
                var arr1 = new Array();
                var arr2 = new Array();
                var arr3 = new Array();


                for (var i = 0; i < recordsetRanking.length; i++) {
                    var obj = {
                        indicador: recordsetRanking[i]["indicador"],
                        rango: recordsetRanking[i]["rango"],
                        ponderacion: recordsetRanking[i]["ponderacion"]
                    }
                    arr1.push(obj);
                    //arr2.push(recordsetRanking[i]["rango"]);
                    //arr3.push(recordsetRanking[i]["ponderacion"]);




                }

                req.query(sqlCargasExtras).then(function (recordsetCargasExtras) {
                    //console.log("3");
                    var carga1 = new Array();



                    for (var i = 0; i < recordsetCargasExtras.length; i++) {

                        var obj2 = {
                            turno: recordsetCargasExtras[i]["turno"],
                            grupo: recordsetCargasExtras[i]["grupo"],
                            carga_extra: recordsetCargasExtras[i]["carga_extra"],
                            carga_normal: recordsetCargasExtras[i]["carga_normal"],
                            carga_total: recordsetCargasExtras[i]["carga_total"],
                            porcentaje: recordsetCargasExtras[i]["porcentaje"]
                        }
                        carga1.push(obj2);
                    }

                    req.query(sqlLabel).then(function (recordsetLabel) {
                        //console.log("4");
                        req.query(sql2).then(function (recordset2) {
                               //console.log("5");
                            req.query(sql3).then(function (recordset3) {
                                  //console.log("6");
                                req.query(sql4).then(function (recordset4) {
                                       //console.log("7");
                                    req.query(sql5).then(function (recordset5) {
                                        req.query(sql6).then(function (recordset6) {

                                            req.query(sql7).then(function (recordset7) {
                                                req.query(sql8).then(function (recordset8) {
                                                    req.query(sql9).then(function (recordset9) {
                                                        req.query(sql10).then(function (recordset10) {

                                                            req.query(sql11).then(function (recordset11) {
                                                                var datos = {};
                                                                datos = recordset[0]["key"];

                                                                data_all['data'].push(recordset);
                                                                labels['labels'] = recordsetLabel;

                                                                //indicador['indicador'] = arr1;                                                              
                                                                //rango ['rango'] = arr2;                                                 
                                                                //ponderacion['ponderacion'] = arr3;

                                                                var ranking2 = {
                                                                    indicador: arr1,
                                                                    rango: arr2,
                                                                    ponderacion: arr3
                                                                }


                                                                ranking['ranking'].push(arr1);
                                                                // ranking['ranking'].push(rango);
                                                                // ranking['ranking'].push(ponderacion);


                                                                //turno['turno'] = carga1;                                                              
                                                                //grupo['grupo'] = carga2;                                                             
                                                                //carga_extra['carga_extra'] = carga3;                                                             
                                                                //carga_normal['carga_normal'] = carga4;                                                              
                                                                //carga_total['carga_total'] = carga5;                                                             
                                                                //porcentaje['porcentaje'] = carga6;

                                                                //cargas['cargas'].push(turno);
                                                                //cargas['cargas'].push(grupo);
                                                                //cargas['cargas'].push(carga_extra);
                                                                //cargas['cargas'].push(carga_normal);
                                                                //cargas['cargas'].push(carga_total);
                                                                cargas['cargas'].push(carga1);

                                                                data2['data2'] = recordset2;
                                                                data3['data3'] = recordset3;
                                                                data4['data4'] = recordset4;
                                                                data5['data5'] = recordset5;
                                                                data6['data6'] = recordset6;
                                                                data7['data7'] = recordset7;
                                                                data8['data8'] = recordset8;
                                                                data9['data9'] = recordset9;
                                                                data10['data10'] = recordset10;
                                                                data11['data11'] = recordset11;
                                                                data_all['data'].push(labels);
                                                                data_all['data'].push(data2);
                                                                data_all['data'].push(data3);
                                                                data_all['data'].push(data4);
                                                                data_all['data'].push(data5);
                                                                data_all['data'].push(data6);
                                                                data_all['data'].push(data7);
                                                                data_all['data'].push(data8);
                                                                data_all['data'].push(data9);
                                                                data_all['data'].push(data10);
                                                                data_all['data'].push(data11);
                                                                data_all['data'].push(ranking);
                                                                data_all['data'].push(cargas);
                                                                var json = JSON.stringify(data_all);

                                                                response.end(json);
                                                                conn.close();
                                                            })
                                                        })
                                                    })
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                    throw err;
                    //process.exit(0);
                });
        })
            .catch(function (err) {
                console.log(err);
                //process.exit(0);
                throw err;
            });


    })


}