exports.get_data_report_ticket_rma1 = function (request, response) {
    var sql = require("mssql");

    var dbConfig = {
      server: properties.get('main.server.appdata.ip'),
      database: properties.get('main.server.appdata.database'),
      user: properties.get('main.server.appdata.user'),
      password: properties.get('main.server.appdata.password'),
      port: properties.get('main.server.appdata.port')
  };

    var log = require('debug-logger')('AppReport');

    var json = {};

    
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var data2 = {};
            data2['data2'] = [];

            var labels = {};
            labels['labels'] = [];

            


            var req = new sql.Request(conn);

            //Ranking Integridad sistema
           /* sql = " SELECT CASE final.mes "+
           " WHEN 12 THEN 'Diciembre' "+
           " WHEN 11 THEN 'Noviembre'  "+
           " WHEN 10 THEN 'Octubre' "+
           " WHEN 9 THEN 'Septiembre' "+
           " WHEN 8 THEN 'Agosto' "+
           " WHEN 7 THEN 'Julio' "+
           " WHEN 6 THEN 'Junio' "+
           " WHEN 5 THEN 'Mayo' "+
           " WHEN 4 THEN 'Abril' "+
           " WHEN 3 THEN 'Marzo'  "+
           " WHEN 2 THEN 'Febrero'  "+ 
           " WHEN 1 THEN 'Enero'  "+
           " ELSE ''  "+
        " END AS Mes,  "+
        " final.entrada,  "+ 
        " final.salida, "+ 
        " final.sinop  "+
 " FROM "+
   " (SELECT MONTH(SYSDATETIME()) AS mes,  "+
         "  sum(CASE  "+  
          "         WHEN t1.des ='entrada' THEN t1.cantidad  "+ 
          "         ELSE 0  "+
          "     END) entrada,  "+
         "  sum(CASE "+
         "          WHEN t1.des ='salida' THEN t1.cantidad "+
         "          ELSE 0 "+
         "      END) salida, "+
         "  sum(CASE  "+
         "          WHEN t1.des ='sinop' THEN t1.cantidad "+
         "          ELSE 0 "+
         "      END) sinop      "+
   " FROM "+
    "  (SELECT sum(CAST ([Quantity Returning] AS int)) AS cantidad, "+ 
            "   'entrada' AS des "+
       " FROM [ZenDeskImport].[dbo].[tickets] "+ 
      " WHERE YEAR([Created at]) = YEAR(SYSDATETIME()) "+
      "   AND MONTH([Created at]) = MONTH(SYSDATETIME())  "+
      "   AND [Subject] LIKE ('%MEL%')  "+ 
      "   AND [Category  list ] = 'RMA Request'  "+
      "   AND [Group] = 'RMA - Chile' "+
      " UNION SELECT sum(CAST ([Quantity Returning] AS int)) AS cantidad, "+
      "              'salida' AS des "+
      " FROM [ZenDeskImport].[dbo].[tickets] "+ 
      " WHERE YEAR([Solved at]) = YEAR(SYSDATETIME()) "+ 
      "   AND MONTH([Solved at]) = MONTH(SYSDATETIME()) "+
         
      "   AND [Category  list ] = 'RMA Request' "+
      "   AND [Group] = 'RMA - Chile' "+
     "  UNION "+
     "  SELECT  "+
     "  sum(cast ([Quantity Returning] as int)) as cantidad, "+
     " 'sinop' as des  "+
       
  " FROM [ZenDeskImport].[dbo].[tickets] "+
   "  where YEAR([Created at]) = YEAR(SYSDATETIME()) "+
   "  AND MONTH([Created at]) = MONTH(SYSDATETIME()) "+
   "  AND [Solved at] IS NULL  "+
    
  " and [Category  list ] = 'RMA Request'   "+
  " and [Group] = 'RMA - Chile'   "+
   "      ) AS t1 "+
   " UNION SELECT MONTH(SYSDATETIME())-1 AS mes, "+
    "             sum(CASE "+
     "                    WHEN t1.des ='entrada' THEN t1.cantidad "+
     "                    ELSE 0 "+
     "                END) entrada, "+
     "            sum(CASE "+
     "                    WHEN t1.des ='salida' THEN t1.cantidad "+
     "                    ELSE 0 "+
     "                END) salida,  "+
     "            sum(CASE "+
     "              WHEN t1.des ='sinop' THEN t1.cantidad "+
     "              ELSE 0 "+
     "          END) sinop      "+
   " FROM  "+
    "  (SELECT sum(CAST ([Quantity Returning] AS int)) AS cantidad, "+
     "         'entrada' AS des "+
     "  FROM [ZenDeskImport].[dbo].[tickets] "+ 
     "  WHERE YEAR([Created at]) = YEAR(SYSDATETIME()) "+
     "    AND MONTH([Created at]) = MONTH(SYSDATETIME())-1 "+ 
         
      "   AND [Category  list ] = 'RMA Request' "+
      "   AND [Group] = 'RMA - Chile' "+
      " UNION SELECT sum(CAST ([Quantity Returning] AS int)) AS cantidad, "+
      "              'salida' AS des "+ 
      " FROM [ZenDeskImport].[dbo].[tickets] "+
      " WHERE YEAR([Solved at]) = YEAR(SYSDATETIME()) "+
      "   AND MONTH([Solved at]) = MONTH(SYSDATETIME())-1 "+
       
      "   AND [Category  list ] = 'RMA Request' "+
      "   AND [Group] = 'RMA - Chile'UNION "+
      " SELECT  "+
      " sum(cast ([Quantity Returning] as int)) as cantidad, "+
     " 'sinop' as des "+
       
  " FROM [ZenDeskImport].[dbo].[tickets] "+
   "  where YEAR([Created at]) = YEAR(SYSDATETIME()) "+
   "  AND MONTH([Created at]) = MONTH(SYSDATETIME())-1 "+
   "  AND [Solved at] IS NULL "+
    
   " and [Category  list ] = 'RMA Request'   "+
  " and [Group] = 'RMA - Chile'  ) AS t1 "+ 
  "  UNION SELECT MONTH(SYSDATETIME())-2 AS mes, "+
   "              sum(CASE "+
     "                    WHEN t1.des ='entrada' THEN t1.cantidad "+
      "                   ELSE 0 "+
       "              END) entrada, "+
        "         sum(CASE "+
         "                WHEN t1.des ='salida' THEN t1.cantidad "+
          "               ELSE 0 "+
          "           END) salida, "+
          "        sum(CASE "+
          "         WHEN t1.des ='sinop' THEN t1.cantidad "+
          "         ELSE 0 "+
          "     END) sinop    "+ 
   " FROM "+ 
     " (SELECT sum(CAST ([Quantity Returning] AS int)) AS cantidad, "+
       "       'entrada' AS des "+
      " FROM [ZenDeskImport].[dbo].[tickets] "+ 
      " WHERE YEAR([Created at]) = YEAR(SYSDATETIME()) "+ 
      "   AND MONTH([Created at]) = MONTH(SYSDATETIME())-2 "+
         
      "   AND [Category  list ] = 'RMA Request'  "+
      "   AND [Group] = 'RMA - Chile' "+ 
     "  UNION SELECT sum(CAST ([Quantity Returning] AS int)) AS cantidad,  "+
      "              'salida' AS des "+
      " FROM [ZenDeskImport].[dbo].[tickets] "+
      " WHERE YEAR([Solved at]) = YEAR(SYSDATETIME()) "+ 
      "   AND MONTH([Solved at]) = MONTH(SYSDATETIME())-2 "+
     "    AND [Category  list ] = 'RMA Request' "+
      "   AND [Group] = 'RMA - Chile' UNION  "+
     "  SELECT  "+
     "  sum(cast ([Quantity Returning] as int)) as cantidad, "+
     " 'sinop' as des  "+
       
  " FROM [ZenDeskImport].[dbo].[tickets] "+
  "   where YEAR([Created at]) = YEAR(SYSDATETIME())  "+
  "   AND MONTH([Created at]) = MONTH(SYSDATETIME())-2  "+
  "   AND [Solved at] IS NULL   "+
    
  " and [Category  list ] = 'RMA Request'   "+
  " and [Group] = 'RMA - Chile'  ) AS t1 "+
   " UNION SELECT MONTH(SYSDATETIME())-3 AS mes, "+
    "             sum(CASE  "+
     "                    WHEN t1.des ='entrada' THEN t1.cantidad  "+
      "                   ELSE 0  "+
      "               END) entrada,  "+
       "          sum(CASE  "+
       "                  WHEN t1.des ='salida' THEN t1.cantidad  "+
       "                  ELSE 0  "+
       "              END) salida,  "+
       "           sum(CASE  "+
       "            WHEN t1.des ='sinop' THEN t1.cantidad  "+
       "            ELSE 0  "+
       "        END) sinop     "+
     " FROM "+
      " (SELECT sum(CAST ([Quantity Returning] AS int)) AS cantidad, "+
      "        'entrada' AS des "+
      " FROM [ZenDeskImport].[dbo].[tickets] "+
      " WHERE YEAR([Created at]) = YEAR(SYSDATETIME())  "+
      "   AND MONTH([Created at]) = MONTH(SYSDATETIME())-3  "+
         
       "  AND [Category  list ] = 'RMA Request'  "+
       "  AND [Group] = 'RMA - Chile'  "+
      " UNION SELECT sum(CAST ([Quantity Returning] AS int)) AS cantidad, "+
       "             'salida' AS des  "+ 
      " FROM [ZenDeskImport].[dbo].[tickets]  "+
     "  WHERE YEAR([Solved at]) = YEAR(SYSDATETIME())  "+
      "   AND MONTH([Solved at]) = MONTH(SYSDATETIME())-3  "+
         
       "  AND [Category  list ] = 'RMA Request'  "+
       "  AND [Group] = 'RMA - Chile' UNION  "+
      " SELECT  "+
      " sum(cast ([Quantity Returning] as int)) as cantidad,  "+
     " 'sinop' as des  "+
       
  " FROM [ZenDeskImport].[dbo].[tickets]  "+
   "  where YEAR([Created at]) = YEAR(SYSDATETIME())  "+
   "  AND MONTH([Created at]) = MONTH(SYSDATETIME())-3  "+
   "  AND [Solved at] IS NULL  "+
    
  " and [Category  list ] = 'RMA Request'   "+
  " and [Group] = 'RMA - Chile'  ) AS t1  "+
  "  UNION SELECT MONTH(SYSDATETIME())-4 AS mes, "+
          "       sum(CASE  "+
          "               WHEN t1.des ='entrada' THEN t1.cantidad "+
          "               ELSE 0 "+
          "           END) entrada, "+
          "       sum(CASE "+
          "               WHEN t1.des ='salida' THEN t1.cantidad "+
          "               ELSE 0 "+
          "           END) salida, "+
          "        sum(CASE  "+
          "         WHEN t1.des ='sinop' THEN t1.cantidad  "+
          "         ELSE 0  "+ 
          "     END) sinop   "+    
   " FROM "+
    "  (SELECT sum(CAST ([Quantity Returning] AS int)) AS cantidad,  "+
    "          'entrada' AS des  "+
    "   FROM [ZenDeskImport].[dbo].[tickets]  "+
    "   WHERE YEAR([Created at]) = YEAR(SYSDATETIME())  "+
    "     AND MONTH([Created at]) = MONTH(SYSDATETIME())-4  "+
         
    "     AND [Category  list ] = 'RMA Request' "+
    "     AND [Group] = 'RMA - Chile' "+
    "   UNION SELECT sum(CAST ([Quantity Returning] AS int)) AS cantidad, "+
    "                'salida' AS des "+
    "   FROM [ZenDeskImport].[dbo].[tickets]  "+
    "   WHERE YEAR([Solved at]) = YEAR(SYSDATETIME()) "+
    "     AND MONTH([Solved at]) = MONTH(SYSDATETIME())-4 "+
         
    "     AND [Category  list ] = 'RMA Request'  "+
    "     AND [Group] = 'RMA - Chile' UNION  "+
    "   SELECT  "+
    "   sum(cast ([Quantity Returning] as int)) as cantidad,  "+
    "  'sinop' as des  "+
       
   " FROM [ZenDeskImport].[dbo].[tickets]  "+
   "  where YEAR([Created at]) = YEAR(SYSDATETIME())   "+
   "  AND MONTH([Created at]) = MONTH(SYSDATETIME())-4  "+
   "  AND [Solved at] IS NULL  "+
   
 "  and [Category  list ] = 'RMA Request'   "+
 "  and [Group] = 'RMA - Chile'  ) AS t1 "+
  "  UNION SELECT MONTH(SYSDATETIME())-5 AS mes, "+
   "              sum(CASE  "+
    "                     WHEN t1.des ='entrada' THEN t1.cantidad  "+
     "                    ELSE 0  "+
     "                END) entrada,  "+
     "            sum(CASE  "+
      "                   WHEN t1.des ='salida' THEN t1.cantidad  "+
      "                   ELSE 0  "+
      "               END) salida,  "+
      "           sum(CASE  "+
      "             WHEN t1.des ='sinop' THEN t1.cantidad  "+
      "             ELSE 0  "+
      "         END) sinop      "+
   " FROM "+
    "  (SELECT sum(CAST ([Quantity Returning] AS int)) AS cantidad,  "+
      "        'entrada' AS des  "+
      " FROM [ZenDeskImport].[dbo].[tickets]  "+
      " WHERE YEAR([Created at]) = YEAR(SYSDATETIME())  "+
      "   AND MONTH([Created at]) = MONTH(SYSDATETIME())-5  "+
         
       "  AND [Category  list ] = 'RMA Request'  "+
       "  AND [Group] = 'RMA - Chile'  "+
     "  UNION SELECT sum(CAST ([Quantity Returning] AS int)) AS cantidad,  "+
       "             'salida' AS des  "+
      " FROM [ZenDeskImport].[dbo].[tickets]   "+
      " WHERE YEAR([Solved at]) = YEAR(SYSDATETIME())  "+
      "   AND MONTH([Solved at]) = MONTH(SYSDATETIME())-5  "+
         
       "  AND [Category  list ] = 'RMA Request'  "+
       "  AND [Group] = 'RMA - Chile' UNION  "+
      " SELECT  "+
      " sum(cast ([Quantity Returning] as int)) as cantidad,  "+
     " 'sinop' as des  "+
       
  " FROM [ZenDeskImport].[dbo].[tickets]  "+
   "  where YEAR([Created at]) = YEAR(SYSDATETIME())  "+
   "  AND MONTH([Created at]) = MONTH(SYSDATETIME()) -5  "+
   "  AND [Solved at] IS NULL  "+
    
  " and [Category  list ] = 'RMA Request'   "+
  " and [Group] = 'RMA - Chile'  ) AS t1) AS FINAL ";*/
console.log(request.query.id_cliente);
console.log(request.query.cliente);

sql =  " select a.month as Mes, isnull(b.tickets,0) as [Ticket_Creados], isnull(c.tickets,0) as [Tickets_Resueltos], isnull(d.tickets,0) as [Tickets_Esperando]"+
 " FROM"+
 " (select distinct CAST([created at] AS varchar(7))as [Month],datepart(mm,[Created at]) as [mo],datepart(yy,[Created at]) as [yr]"+
 " from ZenDeskImport.dbo.tickets"+
 " where [Created at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0)"+

  " and ([Subject] like '%"+ request.query.id_cliente + "%' or [Organization] like '%"+ request.query.cliente +"%')  " +
  
 " and [Category  list ] = 'RMA Request' and [GROUP] = 'RMA - Chile' "+
 " )  A"+
"  LEFT JOIN"+
 " (select CAST([Created at] AS varchar(7))as [Month],count(id) as [Tickets]"+
 " from zendeskimport.dbo.tickets"+
"  where [Created at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0)"+
" and ([Subject] like '%"+ request.query.id_cliente + "%' or [Organization] like '%"+ request.query.cliente +"%')  " +
"  and [Category  list ] = 'RMA Request' and [GROUP] = 'RMA - Chile' "+
"  group by CAST([Created at] as varchar(7))) B "+
"  ON A.Month = B.Month"+
 " LEFT JOIN"+
 " (select CAST([solved at] AS varchar(7))as [Month],count(id) as [Tickets]"+
 " from zendeskimport.dbo.tickets"+
 " where [solved at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0)"+
 " and ([Subject] like '%"+ request.query.id_cliente + "%' or [Organization] like '%"+ request.query.cliente +"%')  " +
 " and [Category  list ] = 'RMA Request' and [GROUP] = 'RMA - Chile' "+
"  group by CAST([solved at] as varchar(7))) C"+
 " ON A.Month = C.Month"+
 " LEFT JOIN"+
 " (select CAST([Created at] AS varchar(7))as [Month],count(id) as [Tickets]"+
 " from zendeskimport.dbo.tickets"+
 " where [Created at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0)"+
 " and ([Subject] like '%"+ request.query.id_cliente + "%' or [Organization] like '%"+ request.query.cliente +"%')  " +
 "  and [Category  list ] = 'RMA Request' and [GROUP] = 'RMA - Chile' "+
 " and [Stage  list ] = 'Requires PO'"+
 " group by CAST([created at] as varchar(7))) D"+
 " ON A.Month = D.Month"+
 " group by A.Month,B.Tickets,C.Tickets,D.Tickets"+
  " order by a.Month";
  
               // log.trace(sql);
               // console.log(sql);

            req.query(sql).then(function (recordset) {                 
               
                data['data'] = recordset;
                 json = JSON.stringify(data);

                 
                // console.log(json);
                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })

     // return json;


    

   
}