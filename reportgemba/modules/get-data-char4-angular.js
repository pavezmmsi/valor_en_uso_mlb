exports.get_data_char4_angular = function (request, response) {
    var sql = require("mssql");

     var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    function getEmp() {
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var IntegridadSistema = {};
            IntegridadSistema['IntegridadSistema'] = [];

            var labels = {};
            labels['labels'] = [];

            var Target = {};
            Target['Target'] = [];

            var data2 = {};
            data2['data2'] = [];



            var req = new sql.Request(conn);


            sql = "create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
                " create table #TempD (Periodo varchar(20),Indicador float, [Target] float) " +
                " insert into #TempC " +
                //" exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = '" + request.query.cliente + "',  " +
                " exec [MLBOperational].[dbo].["+ option_where4(request.query.cliente) +"] " + query3_cliente(request.query.cliente, 5) +
                //" @periodo = 'semana', @datefirts = 3, @indicador = 5, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
                " insert into #TempD " +
                //" exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = '" + request.query.cliente + "', " +
                " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] " + query3_cliente(request.query.cliente, 5) +
                //" @periodo = 'semana', @datefirts = 3, @indicador = 5, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
                " select A.Periodo, " +
                " G1 [Grupo1], " +
                " G2 [Grupo2], " +
                " G3 [Grupo3], " +
                " G4 [Grupo4],  " +
                " Indicador 'Tendencia', " +
                " A.[Target] as 'Target' " +
                " from #TempC A " +
                " inner join #TempD B " +
                " on A.Periodo = B.Periodo " +
                " drop table #TempC, #TempD";


            req.query(sql).then(function (recordset) {

                var datos = {};
                datos = recordset[0]["Periodo"];

                var arr1 = new Array();
                var arr2 = new Array();
                var arr3 = new Array();
                var arr4 = new Array();
                var arr5 = new Array();
                var arr6 = new Array();

                for (var i = 0; i < recordset.length; i++) {
                    labels['labels'].push(recordset[i]["Periodo"]);
                    arr1.push(recordset[i]["Grupo1"]);
                    arr2.push(recordset[i]["Grupo2"]);
                    arr3.push(recordset[i]["Grupo3"]);
                    arr4.push(recordset[i]["Grupo4"]);
                    arr5.push(recordset[i]["Tendencia"]);
                    arr6.push(recordset[i]["Target"]);

                }








                data_all['data'].push(labels);
                // data_all['data'].push(IntegridadSistema);
                //data_all['data'].push(Target);

                data2['data2'].push(arr1);
                data2['data2'].push(arr2);
                data2['data2'].push(arr3);
                data2['data2'].push(arr4);
                data2['data2'].push(arr5);
                data2['data2'].push(arr6);
                data_all['data'].push(data2);
                var json = JSON.stringify(data_all);

                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })
    }


    function option_where4(cliente) {
        var string_where = "";
        var s1 = "Indicadores_Semanales_Grupos_Proyeccion";
        var s2 = "Indicadores_Semanales_Grupos_Proyeccion2";
        //var s3 = "Bench_KpiChile_SemanalCMCC";

        switch (cliente) {
            case "dand":
                string_where = s2;
                break;
            case "mlc":
                string_where = s2;
                break;
             case "cmh":
                string_where = s2;
                break;
            case "dlb":
                string_where = s2;
                break;
            case "dmbl":
                string_where = s2;
                break;
            case "mlb":
                string_where = s2;
                break;                

            default:
                string_where = s1;
        }
            return string_where;

    }

    function query3_cliente(cliente, indicador) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms',@periodo = 'semana', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz', @periodo = 'semana', @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc', @periodo = 'semana' , @indicador = " + indicador + " , @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh', @periodo = 'semana' , @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic',@periodo = 'semana', @indicador = " + indicador + ", @datefirts = 2, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc',@periodo = 'semana',  @indicador = " + indicador + ", @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc', @periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
         if (cliente == 'mlb')
            string_query = " @cliente = 'mlb',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
         if (cliente == 'mel')
            string_query = " @cliente = 'mel',@periodo = 'semana', @indicador = " + indicador + ",  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 ";
        return string_query;

    }

    getEmp();
}