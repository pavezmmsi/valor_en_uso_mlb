exports.get_data_comunicaciones = function (request, response) {
    var sql = require("mssql");

    var PropertiesReader = require('properties-reader');
    var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    function getEmp() {
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var IntegridadSistema = {};
            IntegridadSistema['IntegridadSistema'] = [];

            var labels = {};
            labels['labels'] = [];

             var Target = {};
            Target['Target'] = [];

            var data2 = {};
            data2['data2'] = [];



            var req = new sql.Request(conn);

           
           /* sql =" DECLARE @startdate datetime "
            +" DECLARE @enddate datetime;"
         + "  set @startdate = (SELECT max(ShiftStartDateTime)  from Common.ShiftInfo with(nolock))"
         + "  SELECT format(convert(date,cast(DateStamp as datetime2),105), 'MM-dd-yyyy') as Fecha, UnitId, NoTalks, ReceivedCount, SentCount, EQ.Unit ,cast(((ReceivedCount*100-NoTalks*100)/ReceivedCount)as decimal(10,1)) as '%Comm' ,cast((NoTalks*100.0/ReceivedCount) as decimal(10,1)) as '%NoComm'"
         + "  from ("
         +  " SELECT CONVERT(varchar(10),Timestamp,111) AS DateStamp, EQMPTID AS UnitId,"
          + " SUM("
            + "   CASE"
               + "    WHEN MESSAGETYPE = 3 THEN 1"
                + "   ELSE 0"
             + "  END) AS NoTalks,"
          + " SUM(RECEIVED) AS ReceivedCount, SUM(SENT) AS SentCount"
         + "  FROM DispatchLog.CommLog WITH(NOLOCK)"
         + "  WHERE Timestamp > @startdate"
         + "  and EQMPTID is not NULL"
         + "  GROUP BY EQMPTID, CONVERT(varchar(10),Timestamp,111)"
          + " )temp1"
          +"  INNER JOIN [MLBOperational].[rpt].[Equipments] as EQ WITH(NOLOCK)"
         + "  ON temp1.UnitId=eq.FieldId "
         + " where eq.Unit<>'*' and ReceivedCount<>0 and NoTalks < ReceivedCount"
          + " ORDER BY DateStamp,eq.Unit, UnitId ASC";*/

         sql = "   DECLARE @startdate datetime "
          +"   DECLARE @enddate datetime;"
         +"  set @startdate = convert(datetime,'" + request.query.fechaini +"')"
		+"	set @enddate =  convert(datetime,'"+ request.query.fechater +"')"
          +"  SELECT F.UnitId, cast(avg(F.[%Comm]) as decimal(10,1)) as '%Comm', cast(avg(F.[%NoComm]) as decimal(10,1)) as '%NoComm'  FROM (SELECT  UnitId, NoTalks, ReceivedCount, SentCount, EQ.Unit ,cast(((ReceivedCount*100-NoTalks*100)/ReceivedCount)as decimal(10,1)) as '%Comm' ,cast((NoTalks*100.0/ReceivedCount) as decimal(10,1)) as '%NoComm'"
        + "   from ("
          + " SELECT CONVERT(varchar(10),Timestamp,111) AS DateStamp, EQMPTID AS UnitId,"
          + " SUM("
          + "     CASE"
          + "         WHEN MESSAGETYPE = 3 THEN 1"
          + "         ELSE 0"
          + "     END) AS NoTalks,"
          + " SUM(RECEIVED) AS ReceivedCount, SUM(SENT) AS SentCount"
          + " FROM DispatchLog.CommLog WITH(NOLOCK)"
          + " WHERE Timestamp between @startdate  and @enddate"
         + "  and EQMPTID is not NULL"
          + " GROUP BY EQMPTID, CONVERT(varchar(10),Timestamp,111)"
          + " )temp1"
          + " INNER JOIN [MLBOperational].[rpt].[Equipments] as EQ WITH(NOLOCK)"
         + "  ON temp1.UnitId=eq.FieldId "
        + "  where eq.Unit<>'*' and ReceivedCount<>0 and NoTalks < ReceivedCount) as F"
		 + " group by F.UnitId"
    


          req.query(sql).then(function (recordset) {
               
                data['data'] = recordset;
                 json = JSON.stringify(data);

                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })
    }

    function query_cliente2(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc',  @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @datefirts = 2, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
         if (cliente == 'mlb')
            string_query = " @cliente = 'mlb',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
          if (cliente == 'mel')
            string_query = " @cliente = 'mel',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 ";
        return string_query;

    }

    getEmp();
}