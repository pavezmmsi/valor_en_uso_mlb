exports.get_notification_data = function (request, response) {
    /*   var fs = require('fs');
    var contents = fs.readFileSync('C:/ftp_modular/unit_status.txt').toString();
    response.writeHead(200, { "Content-Type": "application/json" });

    var metadata = {} // empty Object    
    var key = 'messages';
    metadata[key] = []; // empty Array, which you can push() values into   
    var datos = [];
    var linea = [];
    datos = contents.split('\n');
    for (a = 0; a < datos.length; a++) {
        linea = datos[a].split('|');
        //console.log('<' + linea[0] + '>');
        var typemsg = -1;
        switch (linea[0]) {
            case 'Camion' :
            typemsg = 0;
              break;
            case 'Carguio' :
               typemsg = 1;
               break; 
            case 'Perforadora' :
               typemsg = 2;
              break;     
            default:
             typemsg = 3;
              break; 
               
        }
        if (linea[0] !==  undefined){
        var data = {
            datemsg: linea[4],
            id_menssage: linea[3],
            text: linea[0] + ' ' + linea[1] + ' ' +  linea[2],
            title: 'AVISO',
            type_menssage:typemsg 
            
            
        };
        }
        metadata[key].push(data);

    }
    var json = JSON.stringify(metadata);
    response.end(json);*/

    var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://modular:12345678@localhost:27017/modular";

    var metadata = {} // empty Object    
    var key = 'messages';
    metadata[key] = []; // empty Array, which you can push() values into   
    
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var query = {};
      db.collection("ftp_estados").find(query).toArray(function(err, result) {
        if (err) throw err;
          for (a = 0; a < result.length; a++) {
              var typemsg = -1;
              switch (result[a].type) {
                  case 'Camion':
                      typemsg = 0;
                      break;
                  case 'Pala':
                      typemsg = 1;
                      break;
                  case 'Perforadora':
                      typemsg = 2;
                      break;
                  default:
                      typemsg = 3;
                      break;
              }
             //console.log(result[a].name);
              var data = {
                datemsg:  result[a].date_status,
                id_menssage:  result[a].id_menssage,
                text: result[a].type + ' ' + result[a].name + ' ' +  result[a].status,
                title: 'AVISO',
                type_menssage:typemsg                
                
            };
            metadata[key].push(data);      

        }       
        db.close();
        var json = JSON.stringify(metadata);
        response.end(json);
      });
    });
}