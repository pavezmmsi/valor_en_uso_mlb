exports.get_data_influxdb = function(request, response){
    var PropertiesReader = require('properties-reader');
    var properties = PropertiesReader('./properties/app.properties');

    var server =  properties.get('influxDB.server.ip'),
        database= properties.get('influxDB.server.database'),
        user= properties.get('influxDB.server.user'),
        password= properties.get('influxDB.server.password'),
        port= properties.get('influxDB.server.port')
   // console.log('http://'+user+':'+password +'@'+server+':'+port+'/'+database+'?auth=basic');    
    const Influx = require('influx');
    const client = new Influx.InfluxDB('http://'+user+':'+password +'@'+server+':'+port+'/'+database+'?auth=basic');
    client.query(`
 SELECT last("CpuUsage"), DeviceMonitorVersion, DispatchLiteVersion, DispatchVersion, MinecareVersion, ProvisionVersion, ImageVersion, OemDriversVersion, OemInterfacesVersion, Platform, SerialNumber from device_monitor_V1 WHERE  DispatchVersion != '0.0.0.0'  or DispatchLiteVersion!= '0.0.0.0' or ProvisionVersion!= '0.0.0.0' or  MinecareVersion!= '0.0.0.0' GROUP BY IpSource limit 3

  `)
  .then( result => response.status(200).json(result) )
  .catch( error => response.status(500).json({ error }) );
}

/*SELECT last("CpuUsage"), DeviceMonitorVersion, DispatchLiteVersion, DispatchVersion, MinecareVersion, ProvisionVersion, ImageVersion, OemDriversVersion, OemInterfacesVersion, Platform, SerialNumber from device_monitor_V1 WHERE  DispatchVersion != '0.0.0.0'  or DispatchLiteVersion!= '0.0.0.0' or ProvisionVersion!= '0.0.0.0' or  MinecareVersion!= '0.0.0.0' GROUP BY IpSource limit 3*/
