exports.get_data_char2_angular = function (request, response) {
    var sql = require("mssql");

     var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    function getEmp() {
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var IntegridadSistema = {};
            IntegridadSistema['IntegridadSistema'] = [];

            var labels = {};
            labels['labels'] = [];

             var Target = {};
            Target['Target'] = [];

            var data2 = {};
            data2['data2'] = [];



            var req = new sql.Request(conn);

            //Ranking Integridad sistema
            sql = "create table #TempA(Clientes varchar(20),Indicador int) "+
                   " insert into #TempA "+
                   " exec [MLBOperational].[dbo].[" + option_where(request.query.cliente) +"] " + query_cliente2(request.query.cliente) +
                  // " exec [MLBOperational].[dbo].[Bench_KpiChile_Semanal] "+   
                   //" @cliente = '" + request.query.cliente + "', @datefirts = 3,@numdate1 =21, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 "+
                   " select Clientes, "+
                   " Indicador 'IntegridadSistema', "+
                   " 90 'Target' "+
                   " from #TempA "+
                   " drop table #TempA ";

            //console.log('++++++++++++++++++++++++++++++++++' +request.query.cliente ); 
            req.query(sql).then(function (recordset) {
               
                var datos = {};
                datos = recordset[0]["Periodo"];
                 
                var arr1 = new Array();
                var arr2 = new Array();

                  for (var i =0; i <  recordset.length; i++){
                 labels['labels'].push(recordset[i]["Clientes"]);
                 arr1.push(recordset[i]["IntegridadSistema"]);
                 arr2.push(recordset[i]["Target"]);
                 //IntegridadSistema['IntegridadSistema'].push(recordset[i]["IntegridadSistema"]);
                 //Target['Target'].push(recordset[i]["Target"]);
                }

                /* var items = [ recordset[0]["Grupo1"], recordset[1]["Grupo1"],recordset[2]["Grupo1"],recordset[3]["Grupo1"],recordset[4]["Grupo1"]];
                 // console.log(items);
                  data2['data2'].push(items);

                  var items = [ recordset[0]["Grupo2"], recordset[1]["Grupo2"],recordset[2]["Grupo2"],recordset[3]["Grupo2"],recordset[4]["Grupo2"]];
                 // console.log(items);
                  data2['data2'].push(items);

                  var items = [ recordset[0]["Grupo3"], recordset[1]["Grupo3"],recordset[2]["Grupo3"],recordset[3]["Grupo3"],recordset[4]["Grupo3"]];
                 // console.log(items);
                  data2['data2'].push(items);

                  var items = [ recordset[0]["Grupo4"], recordset[1]["Grupo4"],recordset[2]["Grupo4"],recordset[3]["Grupo4"],recordset[4]["Grupo4"]];
                 // console.log(items);
                  data2['data2'].push(items);*/

                 

               

                 
                 data_all['data'].push(labels);
                 // data_all['data'].push(IntegridadSistema);
                 //data_all['data'].push(Target);

                 data2['data2'].push(arr1);
                 data2['data2'].push(arr2);
                  data_all['data'].push(data2);
                var json = JSON.stringify(data_all);

                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })
    }

    function option_where(cliente) {
        var string_where = "";
        var s1 = "Bench_KpiChile_Semanal";
        var s2 = "Bench_KpiChile_Semanal_2";
        var s3 = "Bench_KpiChile_SemanalCMCC";

        switch (cliente) {
            case "cmh":
                string_where = s2;
                break;
            case "dmbl":
                string_where = s2;
                break;
             case "mlb":
                string_where = s2;
                break;
              case "cmcc":
                string_where = s3;
                break;
              case "mel":
                string_where = s2;
                break;  
              case "cmz":
                string_where = s3;
                break;  

            default:
                string_where = s1;
        }
            return string_where;

    }

    function query_cliente2(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms',  @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz',  @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc',  @datefirts = 4, @numdate1 = 21, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl',  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp',  @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb',  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh',  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @datefirts = 2, @numdate1 = 21, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc', @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc',  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand',  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlb')
            string_query = " @cliente = 'mlb',  @datefirts = 1, @numdate1 = 14, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mel')
            string_query = " @cliente = 'mel',  @datefirts = 3, @numdate1 = 21, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 ";
        return string_query;

    }

    getEmp();
}