exports.get_data_char2_montly = function (request, response) {
    var sql = require("mssql");

 
  var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port'),
        connectionTimeout: 999999999,
        requestTimeout: 999999999
    };

    function getEmp() {
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var IntegridadSistema = {};
            IntegridadSistema['IntegridadSistema'] = [];

            var labels = {};
            labels['labels'] = [];

            var Target = {};
            Target['Target'] = [];

            var data2 = {};
            data2['data2'] = [];



            var req = new sql.Request(conn);


            sql = " create table #TempX (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) "
            + " create table #TempY (Periodo varchar(20),Indicador float, [Target] float) "
             
             
           + " insert into #TempX "
          +  " exec [MLBOperational].[dbo].Indicadores_diarios_Grupos_mlb "
         +  "  @cliente = '"+ request.query.id_cliente +"',@periodo='mes',@indicador=6,@numdate1=12 "  
           + " insert into #TempY "
          +  " exec [MLBOperational].[dbo].Indicadores_diarios_Trend_mlb "
          + "  @cliente = '"+ request.query.id_cliente +"',@periodo='mes',@indicador=6,@numdate1=12 "            
           + " select A.Periodo,"
          + "  G1 [Grupo1],"
           + " G2 [Grupo2],"
           + " G3 [Grupo3],"
          +  " G4 [Grupo4],"
          +  " Indicador 'Tendencia',"
          +  " A.[Target] as 'Target'"
          +  " from #TempX A"
         +  "  inner join #TempY B"
          + "  on A.Periodo = B.Periodo"
          + "  order by A.Periodo"            
          + "  drop table #TempX, #TempY";
              


            req.query(sql).then(function (recordset) {

                var datos = {};
                //datos = recordset[0]["Periodo"];

                var arr1 = new Array();
                var arr2 = new Array();
                var arr3 = new Array();
                var arr4 = new Array();
                var arr5 = new Array();
                var arr6 = new Array();
                

                for (var i = 0; i < recordset.length; i++) {
                    labels['labels'].push(recordset[i]["Periodo"]);
                    arr1.push(recordset[i]["Grupo1"]);
                    arr2.push(recordset[i]["Grupo2"]);
                    arr3.push(recordset[i]["Grupo3"]);
                    arr4.push(recordset[i]["Grupo4"]);
                   // arr5.push(recordset[i]["Tendencia"]);
                    arr6.push(recordset[i]["Target"]);

                }








                data_all['data'].push(labels);
                // data_all['data'].push(IntegridadSistema);
                //data_all['data'].push(Target);

                data2['data2'].push(arr1);
                data2['data2'].push(arr2);
                 data2['data2'].push(arr3);
                data2['data2'].push(arr4);
               //  data2['data2'].push(arr5);
                data2['data2'].push(arr6);
                
                data_all['data'].push(data2);
                var json = JSON.stringify(data_all);

                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })
    }






    getEmp();
}