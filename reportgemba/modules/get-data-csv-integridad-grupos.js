exports.get_data_csv_integridad_grupos = function (request, response) {

/*var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties/app.properties');
var xlsx = require('node-xlsx').default;
//var  workSheetsFromBuffer = xlsx.parse(fs.readFileSync(`./csv/grafico_Integridad_mlb.csv`));
// Parse a file
var path = properties.get('csv.path.file');
var workSheetsFromFile = xlsx.parse(path+`grafico_Integridad_mlb.csv`);


json = JSON.stringify(workSheetsFromFile);

            
response.end(json);*/

var sql = require("mssql");

    var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var log = require('debug-logger')('AppReport');

    var json = {};

    //console.log(request.query.cliente);

        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var data2 = {};
            data2['data2'] = [];

            var labels = {};
            labels['labels'] = [];

            


            var req = new sql.Request(conn);

            

                sql =" create table #TempE (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) "
                     +"   create table #TempF (Periodo varchar(20),Indicador float, [Target] float)"


                     + "  insert into #TempE"
                     + "  exec [MLBOperational].[dbo].Indicadores_diarios_Grupos_mlb "
                     + "  @cliente = 'mlb',@periodo='mes',@indicador=5,@numdate1=3"


                      + " insert into #TempF"
                     + "  exec [MLBOperational].[dbo].Indicadores_diarios_Trend_mlb "
                     + "  @cliente = 'mlb',@periodo='mes',@indicador=5,@numdate1=3"

                    + "   select A.Periodo,"
                    +  "  G1 [Grupo 1],"
                    +  "  G2 [Grupo 2],"
                    +  "  G3 [Grupo 3],"
                    +  "  G4 [Grupo 4],"
                    +  "  Indicador 'Tendencia',"
                    +  "  A.[Target] as 'Target[>68%]'"
                    + "   from #TempE A"
                     +  " inner join #TempF B"
                     + "  on A.Periodo = B.Periodo"
                     +  " order by A.Periodo"

                      + " drop table #TempE, #TempF ";

                //log.trace(sql);
               // console.log(sql);

            req.query(sql).then(function (recordset) {                 
               
                 data['data'] = recordset;
                 json = JSON.stringify(data);

                 
                // console.log(json);
                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })

}
