exports.get_data_char_cms = function (request, response) {
    var sql = require("mssql");

    var PropertiesReader = require('properties-reader');
    var properties = PropertiesReader('./properties/app.properties');

    //var dato = properties.get('main.some.thing');

    //console.log(dato);

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var cliente_ = request.query.cliente;


    var conn = new sql.Connection(dbConfig);
    console.log('entra => get_data_char_cms');
    
    conn.connect().then(function () {
        var data_all = {};
        data_all['data'] = [];
        var data = {};
        data['data'] = [];
        var data2 = {};
        data2['data2'] = [];
        var data3 = {};
        data3['data3'] = [];
        var data4 = {};
        data4['data4'] = [];
        var data5 = {};
        data5['data5'] = [];

        var data6 = {};
        data6['data6'] = [];

        var data7 = {};
        data7['data7'] = [];

        var data8 = {};
        data8['data8'] = [];

        var data9 = {};
        data9['data9'] = [];

        var data10 = {};
        data10['data10'] = [];

        var data11 = {};
        data11['data11'] = [];

        var labels = {};
        labels['labels'] = [];




        var req = new sql.Request(conn);

        //Ranking Integridad sistema


        sql = "create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float) " +
            "  create table #TempD (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float) 				     " +
            " insert into #TempC												     " +
            " exec [MLBOperational].[dbo].[RankingClientesSemana] 									     " +
            " @cliente = 'cms', @periodo = 'semana', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  " +
            " insert into #TempD												     " +
            " select 'Proyeccion' Periodo,											     " +
            " avg(G1) 'G1',													     " +
            " avg(G2) 'G2',													     " +
            " avg(G3) 'G3',													     " +
            " avg(G4) 'G4'													     " +
            " from														     " +
            " (														     " +
            " select 													     " +
            " round(sum(G1/2),0) 'G1',											     " +
            " round(sum(G2/2),0) 'G2',											     " +
            " round(sum(G3/2),0) 'G3',											     " +
            " round(sum(G4/2),0) 'G4'											     " +
            " from #TempC													     " +
            " )TEMP1													     " +
            " 														     " +
            " insert into #TempC												     " +
            " select * from #TempD												     " +
            " 														     " +
            " select Periodo,												     " +
            " G1 'Grupo1',													     " +
            " G2 'Grupo2',													     " +
            " G3 'Grupo3',													     " +
            " G4 'Grupo4'													     " +
            "  from #Tempc C												     " +
            " 														     " +
            " drop table #TempC,#TempD											     ";



        //BenchMark Integridad Sistema    
        //sql2 = " create table #TempB(Clientes varchar(20),Indicador int) insert into #TempB " +
        //    " exec [MLBOperational].[dbo].[Bench_KpiChile_Mensual] @cliente = 'cms' " +
        //    " select Clientes, " +
        //    " Indicador 'IntegridadSistema', " +
        //    " 90 'Target' " +
        //    " from #TempB " +
        //    " drop table #TempB";

        sql2 = "create table #TempA(Clientes varchar(20),Indicador int) " +
            " insert into #TempA " +
            " exec [MLBOperational].[dbo].[Bench_KpiChile_Semanal] " +
            " @cliente = 'cms', @datefirts = 3,@numdate1 =21, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " select Clientes, " +
            " Indicador 'IntegridadSistema', " +
            " 90 'Target' " +
            " from #TempA " +
            " drop table #TempA ";



        //Integridad (Spot & Load)    
        sql3 = "exec [MLBOperational].[dbo].[Integridad_detalle_Semanal_] @cliente = 'cms', @datefirts = 3,@numdate1 =35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1";


        //Integridad (Spot & Load) por grupo
        sql4 = "create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempD (Periodo varchar(20),Indicador float, [Target] float) " +
            " insert into #TempC " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = 'cms',  " +
            " @periodo = 'semana', @datefirts = 3, @indicador = 5, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " insert into #TempD " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = 'cms', " +
            " @periodo = 'semana', @datefirts = 3, @indicador = 5, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4],  " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempC A " +
            " inner join #TempD B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempC, #TempD";
        //BenchMark Integridad Spot & Load

        // console.log(sql4);   

        sql5 = "create table #TempA(Clientes varchar(20),Indicador int,[Target] int) " +
            " insert into #TempA " +
            " exec [MLBOperational].[dbo].[Bench_Dinamico_Integridad_Semanal] " +
            " @cliente = 'cms',@indicador = 5, @datefirts = 3,@numdate1 =21, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " select Clientes, " +
            " Indicador 'Integridad', " +
            " [Target] 'Target' " +
            " from #TempA " +
            " drop table #TempA";

        //console.log(sql5);      
        //Uso dinamico por grupo
        sql6 = "create table #TempE (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempF (Periodo varchar(20),Indicador float, [Target] float) " +
            " insert into #TempE " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = 'cms', " +
            " @periodo = 'semana', @datefirts = 3, @indicador = 6, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " insert into #TempF " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = 'cms', " +
            " @periodo = 'semana', @datefirts = 3, @indicador = 6, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4], " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempE A " +
            " inner join #TempF B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempE, #TempF";
        //BenchMark Uso Dinámico 



        sql7 = " create table #TempA(Clientes varchar(20),Indicador int,[Target] int) " +
            "  insert into #TempA " +
            "  exec [MLBOperational].[dbo].[Bench_Dinamico_Integridad_Semanal]   " +
            "  @cliente = 'cms',@indicador = 6, @datefirts = 3,@numdate1 =21, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            "  select Clientes, " +
            "  Indicador 'UsoDinamico', " +
            "  [Target] 'Target' " +
            "  from #TempA " +
            "  drop table #TempA";
        //ciclos corto por grupo

        sql8 = "create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempD (Periodo varchar(20),Indicador float, [Target] float) " +
            " insert into #TempC " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = 'cms',  " +
            " @periodo = 'semana', @datefirts = 3, @indicador = 1, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " insert into #TempD exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = 'cms', " +
            " @periodo = 'semana', @datefirts = 3, @indicador = 1, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4], " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempC A " +
            " inner join #TempD B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempC, #TempD";

        //Cargas Extras por grupo

        sql9 = " create table #TempE (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempF (Periodo varchar(20),Indicador float, [Target] float) " +
            "  insert into #TempE " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = 'cms', " +
            " @periodo = 'semana', @datefirts = 3, @indicador = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " insert into #TempF " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = 'cms', " +
            " @periodo = 'semana', @datefirts = 3, @indicador = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4], " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempE A " +
            "  inner join #TempF B " +
            "  on A.Periodo = B.Periodo " +
            " drop table #TempE, #TempF";

        //Balizas Perdidas por Grupo

        sql10 = " create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            " create table #TempD (Periodo varchar(20),Indicador float, [Target] float) " +
            " insert into #TempC " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = 'cms', " +
            " @periodo = 'semana', @datefirts = 3, @indicador = 4, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " insert into #TempD " +
            " exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = 'cms', " +
            " @periodo = 'semana', @datefirts = 3, @indicador = 4, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            " select A.Periodo, " +
            " G1 [Grupo1], " +
            " G2 [Grupo2], " +
            " G3 [Grupo3], " +
            " G4 [Grupo4], " +
            " Indicador 'Tendencia', " +
            " A.[Target] as 'Target' " +
            " from #TempC A " +
            " inner join #TempD B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempC, #TempD";
        //Llegadas Tarde por grupo
        sql11 = "create table #TempE (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float, [Target] float) " +
            "  create table #TempF (Periodo varchar(20),Indicador float, [Target] float) " +
            "  insert into #TempE " +
            "  exec [MLBOperational].[dbo].[Indicadores_Semanales_Grupos_Proyeccion] @cliente = 'cms', " +
            "  @periodo = 'semana', @datefirts = 3, @indicador = 2, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            "  insert into #TempF " +
            "  exec [MLBOperational].[dbo].[Indicadores_Semanales_Trend] @cliente = 'cms', " +
            "  @periodo = 'semana', @datefirts = 3, @indicador = 2, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 " +
            "  select A.Periodo, " +
            "  G1 [Grupo1], " +
            "  G2 [Grupo2], " +
            "  G3 [Grupo3], " +
            "  G4 [Grupo4], " +
            "  Indicador 'Tendencia', " +
            "  A.[Target] as 'Target' " +
            "  from #TempE A " +
            " inner join #TempF B " +
            " on A.Periodo = B.Periodo " +
            " drop table #TempE, #TempF";

        req.query(sql).then(function (recordset) {

            sqlLabel = "SELECT  [Id_Cliente] " +
                " , [Id_Informe] " +
                " , [Id_type] " +
                " , [Label1] " +
                " , [Label2] " +
                " , [Label3] " +
                " , [Estado1] " +
                " , [Comentario1] " +
                " , [Estado2] " +
                " , [Comentario2] " +
                " , [Estado3] " +
                " , [Comentario3] " +
                " , [Estado4]  " +
                " , [Comentario4] " +
                " , [Estado5] " +
                " , [Comentario5] " +
                " , [Estado6] " +
                " , [Comentario6] " +
                " , [Estado7] " +
                " , [Comentario7] " +
                " , [Estado8] " +
                " , [Comentario8] " +
                " , [Estado9] " +
                " , [Comentario9] " +
                " , [Estado10] " +
                " , [Comentario10] " +
                " , [Estado11] " +
                " , [Comentario11] " +
                " FROM [TableLabel] ";
            req.query(sqlLabel).then(function (recordsetLabel) {

                req.query(sql2).then(function (recordset2) {

                    req.query(sql3).then(function (recordset3) {

                        req.query(sql4).then(function (recordset4) {

                            req.query(sql5).then(function (recordset5) {
                                req.query(sql6).then(function (recordset6) {

                                    req.query(sql7).then(function (recordset7) {
                                        req.query(sql8).then(function (recordset8) {
                                            req.query(sql9).then(function (recordset9) {
                                                req.query(sql10).then(function (recordset10) {

                                                    req.query(sql11).then(function (recordset11) {
                                                        var datos = {};
                                                        datos = recordset[0]["key"];

                                                        data_all['data'].push(recordset);
                                                        labels['labels'] = recordsetLabel;
                                                        data2['data2'] = recordset2;
                                                        data3['data3'] = recordset3;
                                                        data4['data4'] = recordset4;
                                                        data5['data5'] = recordset5;
                                                        data6['data6'] = recordset6;
                                                        data7['data7'] = recordset7;
                                                        data8['data8'] = recordset8;
                                                        data9['data9'] = recordset9;
                                                        data10['data10'] = recordset10;
                                                        data11['data11'] = recordset11;
                                                        data_all['data'].push(labels);
                                                        data_all['data'].push(data2);
                                                        data_all['data'].push(data3);
                                                        data_all['data'].push(data4);
                                                        data_all['data'].push(data5);
                                                        data_all['data'].push(data6);
                                                        data_all['data'].push(data7);
                                                        data_all['data'].push(data8);
                                                        data_all['data'].push(data9);
                                                        data_all['data'].push(data10);
                                                        data_all['data'].push(data11);
                                                        var json = JSON.stringify(data_all);

                                                        response.end(json);
                                                        conn.close();
                                                    })
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });
        })
            .catch(function (err) {
                console.log(err);
            });


    })


}