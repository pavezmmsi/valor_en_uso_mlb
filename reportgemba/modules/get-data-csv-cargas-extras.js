exports.get_data_csv_cargas_extras = function (request, response) {
 
/*var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties/app.properties');
var xlsx = require('node-xlsx').default;
//var  workSheetsFromBuffer = xlsx.parse(fs.readFileSync(`./csv/grafico_CargasExtras_tend_mlb.csv`));
// Parse a file
var path = properties.get('csv.path.file');
var workSheetsFromFile = xlsx.parse(path+`grafico_CargasExtras_tend_mlb.csv`);


json = JSON.stringify(workSheetsFromFile);

            
response.end(json);*/

var sql = require("mssql");

    var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var log = require('debug-logger')('AppReport');

    var json = {};

    //console.log(request.query.cliente);

        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var data2 = {};
            data2['data2'] = [];

            var labels = {};
            labels['labels'] = [];

            


            var req = new sql.Request(conn);

            

                sql =" create table #TempA (Periodo varchar(20),Indicador float, [Target] float) "
              + " insert into #TempA "
             + "  exec [MLBOperational].[dbo].Indicadores_diarios_Trend_mlb @cliente = 'mlb',@periodo='mes',@indicador=3,@numdate1=3"
             +"  select "
             + "   Periodo,"
             + "  Indicador 'CargasExtras',"
              + " [Target] 'Target[<2%]'"
            +  "  from #TempA"
              + " order by Periodo"
              + " drop table #TempA";

                //log.trace(sql);
               // console.log(sql);

            req.query(sql).then(function (recordset) {                 
               
                 data['data'] = recordset;
                 json = JSON.stringify(data);

                 
                // console.log(json);
                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })

}
