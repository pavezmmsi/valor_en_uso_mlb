exports.get_data_validate_mensual = function (request, response) {
    var sql = require("mssql");
    
    var PropertiesReader = require('properties-reader');
    var properties = PropertiesReader('./properties/app.properties');

    //var dato = properties.get('main.some.thing');

    //console.log(dato);

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };
    
        var log = require('debug-logger')('AppReport');
    
        var json = {};
    
        
            var conn = new sql.Connection(dbConfig);
    
            conn.connect().then(function () {
                var data_all = {};
                data_all['data'] = [];
                var data = {};
                data['data'] = [];
    
                var data2 = {};
                data2['data2'] = [];
    
                var labels = {};
                labels['labels'] = [];
    
                
    
    
                var req = new sql.Request(conn);
    
                //Ranking Integridad sistema
               /* sql = " SELECT  [Id_Cliente] "+
                "  ,[Id_Informe]  "+
                "  ,[Id_type]  "+
                "  ,[Mes]  "+
                "  ,[Ano]   "+
                
                "   ,[Intro] ," + 
                " [Intro2] , "+
                "  [Intro3],"+
                 " [Intro4],"+
                 " [Intro5],"+
                 " [Intro6] "+
                " FROM [MLBOperational].[dbo].[TableChartMensual]   "+
                " WHERE Id_Cliente = '"+ request.query.idcliente + "'" +
                  " AND [Mes] = '"+ request.query.mes + "'" +
                   "  AND [Ano] = '"+ request.query.ano + "'";*/
                
                    //log.trace(sql);
                   // console.log(request.query.cliente);

                   sql = "  SELECT            "+
                   "   TableChartMensual.Id_Cliente,"+
                   "   TableChartMensual.Id_Informe,"+
                    "  TableChartMensual.Id_type,"+
                    "  TableChartMensual.Mes,"+
                    "  TableChartMensual.Ano,"+                   
                     " TableChartMensual.Intro,"+
                     "   TableChartMensual.Intro2,	"+
                       " TableChartMensual.Intro3,	"+
                       "  TableChartMensual.Intro4, "+
                        "  TableChartMensual.Intro5, "+
                      "  TableChartMensual.Intro6,"+
                      "   TableChartDirtyMensual.Intro as Intro7,"+
                 "          TableChartDirtyMensual.Intro2 as Intro8,	"+
                         "  TableChartDirtyMensual.Intro3 as Intro9,	"+
                         "   TableChartDirtyMensual.Intro4 as Intro10, "+
                          "   TableChartDirtyMensual.Intro5 as Intro11, "+
                        "   TableChartDirtyMensual.Intro6 as Intro12,	"+		
                          " TableChartDirtyMensual.Intro7 as Intro13	"+	         
                      
           " FROM         TableChartDirtyMensual CROSS JOIN"+
                               "   TableChartMensual" +
           " WHERE     (TableChartMensual.Id_Cliente = TableChartMensual.Id_Cliente) AND "+
                  "    (TableChartMensual.Id_Informe = TableChartMensual.Id_Informe) AND "+
                    "   (TableChartMensual.Id_type = TableChartMensual.Id_type) AND "+
                   " (TableChartMensual.Mes = TableChartDirtyMensual.Mes) AND "+
                     "  (TableChartMensual.Ano = TableChartDirtyMensual.Ano) AND "+
                  "  (TableChartMensual.Id_Cliente = '"+ request.query.idcliente + "'  ) AND "+
                  "  (TableChartMensual.Ano = "+ request.query.ano + ") AND"+
                 "    (TableChartMensual.Mes =" + request.query.mes +")   ";
    
                req.query(sql).then(function (recordset) {                 
                   
                    data['data'] = recordset;
                     json = JSON.stringify(data);
    
                     
                     //console.log(json);
                    response.end(json);
                    conn.close();
    
    
                })
                    .catch(function (err) {
                        console.log(err);
                        conn.close();
                    });
    
    
    
            })
}