exports.get_data_report_ticket_cca4 = function (request, response) {
    var sql = require("mssql");

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var log = require('debug-logger')('AppReport');

    var json = {};

    
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var data2 = {};
            data2['data2'] = [];

            var labels = {};
            labels['labels'] = [];

            


            var req = new sql.Request(conn);

            //Ranking Integridad sistema
           sql = " select [Status] as [Estado],[ID] as [Numero_Ticket], [Subject] as [Asunto],[Category  list ] as [Categoria],[Assignee] as [Ingeniero_Asignado],"+
            " [Created at] as [Fecha_Solicitud],[Updated at]  as [Ultimo_Update] ,[Severity Level  list ] as [Nivel_de_Severidad]"+
           " from zendeskimport.dbo.tickets"+
           " where [group] = 'First tier - Chile'"+
          "  and [category  list ] not in ('Internal')"+
          "  and [Stage  list ] in ('Waiting for Customer Input')"+
          "  and [Status] not in ('Deleted','closed')"+
          "  and [Organization] like '%Escondida%' "+
          "  order by Categoria,[Created at]";
            
            
            
                //log.trace(sql);
                //console.log(sql);

            req.query(sql).then(function (recordset) {                 
               
                data['data'] = recordset;
                 json = JSON.stringify(data);

                 
                // console.log(json);
                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })

     // return json;


    

   
}