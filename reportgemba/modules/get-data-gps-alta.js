exports.get_data_gps_alta = function (request, response) {
    var sql = require("mssql");

    var PropertiesReader = require('properties-reader');
    var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: 'IntelliMineNextGen',
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port'),
        connectionTimeout: 300000,
        requestTimeout: 300000,
        pool: {
            idleTimeoutMillis: 300000,
            max: 100
        }
    };

    function getEmp() {
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var IntegridadSistema = {};
            IntegridadSistema['IntegridadSistema'] = [];

            var labels = {};
            labels['labels'] = [];

             var Target = {};
            Target['Target'] = [];

            var data2 = {};
            data2['data2'] = [];



            var req = new sql.Request(conn);

           
            /*sql =" DECLARE @sqlCommand varchar(1000) "
                 + "  DECLARE @startdate  varchar(8)"
                 + "  SET @startdate =(SELECT format(convert(date,getdate(),105), 'yyyyMMdd'))"
                 +  " SET @sqlCommand = 'SELECT tm.UnitId,"
                  + "   tm.Good,"
                  +  " tm.Bad,"
                  + " cast(((tm.Good*100)/(tm.Good+tm.Bad )) as decimal(10,1)) as ''%Good'',"
                 + " cast(((tm.Bad*100)/(tm.Good+tm.Bad )) as decimal(10,1)) as ''%Bad''"
                 + "   FROM (SELECT CONVERT(varchar(10),Timestamp,111) AS DateStamp, EquipmentId AS UnitId,"
                 + "   SUM( CASE WHEN  FieldStatTypeCode in (''D3'',''N3'') THEN 1"
                  + "  ELSE 0  END) AS Good,  SUM(  CASE  WHEN  FieldStatTypeCode in (''OT'') THEN 1"
                  + "         ELSE 0"
                   + "    END) AS Bad   "             
                  +"  FROM [MLBHistorical].[PositionTracking].[EquipmentPosition_'+ @startdate +'] WITH(NOLOCK)"
                 + "  GROUP BY EquipmentId, CONVERT(varchar(10),Timestamp,111)) as tm  order by tm.UnitId asc'"
                 +  " EXEC (@sqlCommand)";*/

                 
                 sql = "  declare @inicio datetime"
                  +"  declare @fin datetime"                    
                  +"  set @inicio= '2019-03-05 00:00:00'"
                  +"  set @fin = '2019-03-06 23:59:59'"
                  +"  SELECT  UnitId, cast(((DD.Buenos*100)/(DD.Registros )) as decimal(10,1)) as '%Good',"
                 + "  cast(((DD.Malos*100)/(DD.Registros )) as decimal(10,1)) as '%Bad' "
                  +"  FROM (SELECT   tt.Name as UnitId"
                 +"   , (COUNT(cast( tt.IsElevationOK as int)) - sum( cast(tt.IsElevationOK as int) )) as Malos"
                 + "  , sum( cast(tt.IsElevationOK as int) ) as Buenos"
                 + "  , COUNT(cast( tt.IsElevationOK as int)) as Registros"
                + "   FROM("
                 + "  SELECT Timestamp,Name,IsPositionOk,IsElevationOK"
                 + "  FROM [PositionTrackingHistorical].[EquipmentPosition] t1"
                  +   "   INNER JOIN [IdentityManagement].[ManagedEntity] t2"
                   +   "  ON t1.ManagedEntityID =  t2.ID"                        
                   +   "  WHERE Timestamp BETWEEN @inicio AND @fin "
                 + "  )tt"
                   + " GROUP BY tt.Name ) as DD"






               // if (request.query.flota === undefined) {
               //  sql =" EXEC [dbo].[Bad_Good_GPS]  @starttime ='"+ request.query.fechaini +"', @endtime = '"+ request.query.fechater +"', @flota = null"  
               // }else{
               //    sql =" EXEC [dbo].[Bad_Good_GPS]  @starttime ='"+ request.query.fechaini +"', @endtime = '"+ request.query.fechater +"', @flota = " + request.query.flota  
               // }

                 console.log(sql);
               
          req.query(sql).then(function (recordset) {
               
                data['data'] = recordset;
                 json = JSON.stringify(data);

                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })
    }

    function query_cliente2(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc',  @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @datefirts = 2, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
         if (cliente == 'mlb')
            string_query = " @cliente = 'mlb',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
          if (cliente == 'mel')
            string_query = " @cliente = 'mel',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 ";
        return string_query;

    }

    getEmp();
}