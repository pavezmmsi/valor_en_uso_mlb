exports.get_information_kpi_mensual = function (request, response) {
    var sql = require("mssql");

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port'),    
        connectionTimeout: 300000,
        requestTimeout: 300000,
        // pool: {
        //     idleTimeoutMillis: 300000,
        max: 100
        // }
    };

    function getEmp() {
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {


            var image = request.body.image;

            var xlsx = require('node-xlsx').default;
            //var  workSheetsFromBuffer = xlsx.parse(fs.readFileSync(`./uploads/cmz_10_2017_ventas.xlsx`));
            // Parse a file
            var fecha = new Date();

            var workSheetsFromFile = xlsx.parse('./uploads/' + request.query.cliente + '_' + request.query.mes + '_' + request.query.ano + '_' + 'ventas.xlsx');
            var workSheetsFromFile2 = xlsx.parse('./uploads/' + request.query.cliente + '_' + request.query.mes + '_' + request.query.ano + '_' + 'finanzas.xlsx');

            var jsonFile1 = JSON.stringify(workSheetsFromFile);
            var jsonFile2 = JSON.stringify(workSheetsFromFile2);

           
            var data = {};
            data['data'] = [];
            

            var req = new sql.Request(conn);

            var cliente_p = "";

            switch (request.query.cliente) {
                case 'dand':
                    cliente_p = "Andina";
                    break;
                case 'mlc':
                    cliente_p = "Candelaria";
                    break;
                case 'cmcc':
                    cliente_p = "Cerro Colorado";
                    break;
                case 'cmdic':
                    cliente_p = "Collahuasi";
                    break;
                case 'scma':
                    cliente_p = "El Abra";
                    break;
                case 'mel':
                    cliente_p = "Escondida";
                    break;
                case 'gaby':
                    cliente_p = "Gaby";
                    break;
                case 'cmh':
                    cliente_p = "Huasco";
                    break;
                case 'mlb':
                    cliente_p = "Lomas Bayas";
                    break;
                case 'dlb':
                    cliente_p = "Los Bronces";
                    break;
                case 'mlp':
                    cliente_p = "Los Pelambres";
                    break;
                case 'dmbl':
                    cliente_p = "Mantos Blancos";
                    break;
                case 'smir':
                    cliente_p = "Invierno Mine";
                    break;
                case 'msc':
                    cliente_p = "San Cristobal";
                    break;
                case 'cms':
                    cliente_p = "Spence";
                    break;
                case 'cmz':
                    cliente_p = "Zaldivar";
                    break;
                case 'sqm':
                    cliente_p = "SQM";
                    break;
            }


            sql = "  SELECT            " +
                "   TableChartMensual.Id_Cliente," +
                "   TableChartMensual.Id_Informe," +
                "  TableChartMensual.Id_type," +
                "  TableChartMensual.Mes," +
                "  TableChartMensual.Ano," +
                " TableChartMensual.File1," +
                " TableChartMensual.File2, " +
                "  TableChartMensual.File3," +
                "  TableChartMensual.File4," +
                " TableChartMensual.File5," +
                " TableChartMensual.File6," +
                " TableChartMensual.File7," +
                "  TableChartMensual.File8," +
                " TableChartMensual.File9, " +
                " TableChartMensual.File10," +
                "  TableChartMensual.File11," +
                " TableChartMensual.File12, " +
                "  TableChartDirtyMensual.File13," +
                " TableChartDirtyMensual.File14," +
                " TableChartDirtyMensual.File15," +
                " TableChartDirtyMensual.File16," +
                " TableChartDirtyMensual.File17," +
                " TableChartDirtyMensual.File18, " +
                " TableChartDirtyMensual.File19," +
                " TableChartDirtyMensual.File20," +
                " TableChartDirtyMensual.File21," +
                " TableChartDirtyMensual.File22, " +
                " TableChartDirtyMensual.File23,   " +
                " TableChartDirtyMensual.File24," +
                " TableChartDirtyMensual.File25," +
                " TableChartMensual.Intro," +
                "   TableChartMensual.Intro2,	" +
                " TableChartMensual.Intro3,	" +
                "  TableChartMensual.Intro4, " +
                "  TableChartMensual.Intro5, " +
                "  TableChartMensual.Intro6," +
                "   TableChartDirtyMensual.Intro as Intro7," +
                "          TableChartDirtyMensual.Intro2 as Intro8,	" +
                "  TableChartDirtyMensual.Intro3 as Intro9,	" +
                "   TableChartDirtyMensual.Intro4 as Intro10, " +
                "   TableChartDirtyMensual.Intro5 as Intro11, " +
                "   TableChartDirtyMensual.Intro6 as Intro12,	" +
                " TableChartDirtyMensual.Intro7 as Intro13	" +

                " FROM         TableChartDirtyMensual CROSS JOIN" +
                "   TableChartMensual" +
                " WHERE     (TableChartMensual.Id_Cliente = TableChartDirtyMensual.Id_Cliente) AND " +
                "    (TableChartMensual.Id_Informe = TableChartDirtyMensual.Id_Informe) AND " +
                "   (TableChartMensual.Id_type = TableChartDirtyMensual.Id_type) AND " +
                " (TableChartMensual.Mes = TableChartDirtyMensual.Mes) AND " +
                "  (TableChartMensual.Ano = TableChartDirtyMensual.Ano) AND " +
                "  (TableChartMensual.Id_Cliente = '" + request.query.cliente + "'" + " ) AND " +
                " (TableChartMensual.Ano = '" + request.query.ano + "') AND" +
                " (TableChartMensual.Mes = '" + request.query.mes + "')   ";

;

            sql3 = " select [Status] as [Estado],[ID] as [Numero_Ticket], [Subject] as [Asunto],[Category  list ] as [Categoria],[Assignee] as [Ingeniero_Asignado],CONVERT(varchar, [Created at], 105) as [Fecha_Solicitud],[Severity Level  list ] as [Nivel_de_Severidad]" +
                " from zendeskimport.dbo.tickets" +
                " where [group] = 'First tier - Chile'" +
                "  and [category  list ] = 'Customer Care Agreement' " +
                "            and [Status]  not in ('Closed','Deleted','Solved')" +
                "  and [Organization] like '%" + cliente_p + "%'" +
                " order by [Created at] desc";

            sql4 = " select [Status] as [Estado],[ID] as [Numero_Ticket], [Subject] as [Asunto],[Category  list ] as [Categoria],[Assignee] as [Ingeniero_Asignado],CONVERT(varchar, [Created at], 105) as [Fecha_Solicitud],[Severity Level  list ] as [Nivel_de_Severidad]" +
                " from zendeskimport.dbo.tickets" +
                "  where [group] = 'First tier - Chile'" +
                " and [category  list ] in ('Fee for Service','Fee for Service - Not Charged')" +
                " and [Status]  not in ('Closed','Deleted','Solved')" +
                "  and [Organization] like '%" + cliente_p + "%'" +
                " order by Categoria,[Created at]";

            sql5 = "select COUNT(*) as flotas from Flotas where Client = '" + request.query.cliente + "'";

            sql6 = "exec [ZenDeskImport].[dbo].[CCATickets] @cliente = '"+ cliente_p +"', @siglacliente ='"+ request.query.cliente +"' " ;
            
            

            sql7 = " exec [ZenDeskImport].[dbo].[RMATickets] @cliente = '"+ cliente_p +"', @siglacliente ='"+ request.query.cliente +"' " ;

            sql8 = " exec [ZenDeskImport].[dbo].[SLAFreeport-LUNDINMensual] @cliente = '"+ cliente_p +"'";


            sql9 = " exec [ZenDeskImport].[dbo].[SLAFreeport-LUNDINQuarter] @cliente = '"+ cliente_p +"'";

            sql10 = " SELECT 0 as Campo1, 0 as Campo2, 0 as Campo3, 0 as Campo4, 0 as Campo5";

            sql11 =  " SELECT  [client] ,[sla]FROM [MLBOperational].[dbo].[sla_client] where [client] = '" + request.query.cliente +"' ";

            sql12 = " SELECT 0 as Campo1, 0 as Campo2, 0 as Campo3, 0 as Campo4, 0 as Campo5";

            sql13 = " SELECT 0 as Campo1, 0 as Campo2, 0 as Campo3, 0 as Campo4, 0 as Campo5";

            sql14 = "SELECT  [client] ,[Nombre] ,[sla],[turnos] FROM [MLBOperational].[dbo].[sla_client] where [client] = '" + request.query.cliente +"' ";
            
            req.query(sql).then(function (recordset2) {

                // console.log(sql);
                req.query(sql3).then(function (recordset3) {
                    req.query(sql4).then(function (recordset4) {
                        req.query(sql5).then(function (recordset5) {
                            req.query(sql6).then(function (recordset6) {
                                req.query(sql7).then(function (recordset7) {
                                    req.query(sql8).then(function (recordset8) {
                                        req.query(sql9).then(function (recordset9) {
                                            req.query(sql10).then(function (recordset10) {
                                                req.query(sql11).then(function (recordset11) {
                                                    req.query(sql12).then(function (recordset12) {
                                                        req.query(sql13).then(function (recordset13) {
                                                            req.query(sql14).then(function (recordset14) {
                                                data['data'].push(recordset2);
                                                data['data'].push(recordset12);
                                                data['data'].push(recordset13);
                                                data['data'].push(recordset3);
                                                data['data'].push(recordset4);
                                                data['data'].push(recordset5);
                                                data['data'].push(recordset6);
                                                data['data'].push(recordset7);
                                                data['data'].push(recordset8);
                                                data['data'].push(recordset9);
                                                data['data'].push(recordset10);
                                                data['data'].push(recordset11);
                                                data['data'].push(recordset14);
                                                json = JSON.stringify(data);


                                                response.end(json);
                                                conn.close();

                                            })
                                            .catch(function (err) {

                                                console.log(err);
                                                conn.close();
                                                json = JSON.stringify(err);
                                                response.end(json);
                                            });
                                            })
                                            .catch(function (err) {

                                                console.log(err);
                                                conn.close();
                                                json = JSON.stringify(err);
                                                response.end(json);
                                            });
                                            })
                                            .catch(function (err) {

                                                console.log(err);
                                                conn.close();
                                                json = JSON.stringify(err);
                                                response.end(json);
                                            });

                                            })
                                            .catch(function (err) {

                                                console.log(err);
                                                conn.close();
                                                json = JSON.stringify(err);
                                                response.end(json);
                                            });

                                            })
                                                .catch(function (err) {

                                                    console.log(err);
                                                    conn.close();
                                                    json = JSON.stringify(err);
                                                    response.end(json);
                                                });
                                        })
                                            .catch(function (err) {

                                                console.log(err);
                                                conn.close();
                                                json = JSON.stringify(err);
                                                response.end(json);
                                            });
                                    })
                                        .catch(function (err) {

                                            console.log(err);
                                            conn.close();
                                            json = JSON.stringify(err);
                                            response.end(json);
                                        });
                                })
                                    .catch(function (err) {

                                        console.log(err);
                                        conn.close();
                                        json = JSON.stringify(err);
                                        response.end(json);
                                    });
                            })
                                .catch(function (err) {

                                    console.log(err);
                                    conn.close();
                                    json = JSON.stringify(err);
                                    response.end(json);
                                });
                        })
                            .catch(function (err) {

                                console.log(err);
                                conn.close();
                                json = JSON.stringify(err);
                                response.end(json);
                            });
                    })
                        .catch(function (err) {

                            console.log(err);
                            conn.close();
                            json = JSON.stringify(err);
                            response.end(json);
                        });
                })
                    .catch(function (err) {
                        console.log(err);
                        conn.close();
                        json = JSON.stringify(err);
                        response.end(json);
                    });
            })

                .catch(function (err) {
                    console.log(err);
                    conn.close();
                    json = JSON.stringify(err);
                    response.end(json);
                });
        })
            .catch(function (err) {
                console.log(err);
                conn.close();
                json = JSON.stringify(err);
                response.end(json);
            });


    }

    getEmp();
}