exports.get_data_report_cca_chart1 = function (request, response) {
    var sql = require("mssql");

    var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var log = require('debug-logger')('AppReport');

    var json = {};

    //console.log(request.query.cliente);

        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var data2 = {};
            data2['data2'] = [];

            var labels = {};
            labels['labels'] = [];

            


            var req = new sql.Request(conn);

            

                sql = "  SELECT CAST([solved at] AS varchar(7))as Mes, [Organization] , " +
                      "  count(ID) as Tickets, " +
                      "  isnull(sum(Case when [Severity Level  list ] = '1 - Emergency' then 1 end),0)  as Emergencias, " +
                      "  isnull(sum(Case when [Severity Level  list ] <> '1 - Emergency' then 1 end),0) as No_Emergencias " +
                      "  FROM ZendeskImport.dbo.tickets  " +
                      "  WHERE [Created at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-6, 0)     " +
                      "  and SUBSTRING([Subject],1,4)='[TT]'  " +
                      "  and [Organization] like '%" +request.query.cliente + "%' " +
                      "  GROUP BY CAST([solved at] AS varchar(7)),Organization  " +
                      "  order by Mes ";

                //log.trace(sql);
               // console.log(sql);

            req.query(sql).then(function (recordset) {                 
               
                data['data'] = recordset;
                 json = JSON.stringify(data);

                 
                // console.log(json);
                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })

     // return json;


    

   
}