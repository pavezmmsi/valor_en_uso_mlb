exports.get_data_input = function (request, response) {
    var sql = require("mssql");

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    function getEmp() {
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var Cargas = {};
            Cargas['Cargas'] = [];

            var labels = {};
            labels['labels'] = [];

             var Ranking = {};
            Ranking['Ranking'] = [];

           


            var req = new sql.Request(conn);

            //Ranking Integridad sistema
            sql = "SELECT " + 
                "[Id_Informe] "+
                ",[Id_type] "+
                ",[Label1] "+
                ",[Label2] "+
                ",[Label3]"+
                ",[Estado1]"+
                ",[Comentario1]"+
                ",[Estado2]"+
                ",[Comentario2]"+
                ",[Estado3]"+
                ",[Comentario3]"+
                ",[Estado4]"+
                ",[Comentario4]"+
                ",[Estado5]"+
                ",[Comentario5]"+
               " ,[Estado6]"+
                ",[Comentario6]"+
                ",[Estado7]"+
                ",[Comentario7]"+
                ",[Estado8]"+
                ",[Comentario8]"+
                ",[Estado9]"+
                ",[Comentario9]"+
                ",[Estado10]"+
                ",[Comentario10]"+
                ",[Estado11]"+
                ",[Comentario11]"+
                ",[Intro]"+
                ",[File1]"+
                ",[File2]"+
                ",[File3]"+
            " FROM [MLBOperational].[dbo].[TableLabel] " +
            " WHERE [id_cliente] = '" + request.query.cliente + "'";

            sql2 = " SELECT  " +
                   " RTRIM([indicador]) as  indicador"+
                   " ,RTRIM([rango]) as rango"+
                   " ,RTRIM([ponderacion]) as ponderacion "+
               " FROM [MLBOperational].[dbo].[Ranking] "+
            " WHERE [id_cliente] = '" + request.query.cliente + "'";

            sql3 =  " SELECT " + 
                    " RTRIM([turno]) as turno "+
                    ",RTRIM([grupo]) as grupo "+
                    ",RTRIM([carga_extra]) as carga_extra"+
                    ",RTRIM([carga_normal]) as carga_normal"+
                    ",RTRIM([carga_total]) as carga_total"+
                    ",RTRIM([porcentaje]) as porcentaje"+
                    " FROM [MLBOperational].[dbo].[CargasExtras] "+
                    " WHERE [id_cliente] = '" + request.query.cliente + "'";

            sql4 = "SELECT [id_cliente] " +
                    " ,[grupo]" + 
                   " ,[tonelaje]" +
                   " ,[numero]" +
                  "  FROM [MLBOperational].[dbo].[CiclosCortos]   " +
                   " WHERE [id_cliente] = '" + request.query.cliente + "'";  


           
            
            req.query(sql).then(function (recordset) {

                 req.query(sql2).then(function (recordset2) {

                     req.query(sql3).then(function (recordset3) {

                        req.query(sql4).then(function (recordset4) {

                     var obj = {
                         labels : recordset, 
                         ranking : recordset2,
                         cargas: recordset3,
                         ciclos : recordset4
                     }
               
                

               

                 
                 data_all['data'] = obj;
                var json = JSON.stringify(data_all);
                response.end(json);
                 
                conn.close();

              })
              })
            })
            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })
    }

    getEmp();
}