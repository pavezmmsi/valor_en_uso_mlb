exports.get_data_efectividad_plm = function (request, response) {
    var sql = require("mssql");

    var PropertiesReader = require('properties-reader');
    var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    function getEmp() {
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var IntegridadSistema = {};
            IntegridadSistema['IntegridadSistema'] = [];

            var labels = {};
            labels['labels'] = [];

             var Target = {};
            Target['Target'] = [];

            var data2 = {};
            data2['data2'] = [];



            var req = new sql.Request(conn);

           
           /* sql =" SET ARITHABORT OFF"
           + " SET ANSI_WARNINGS OFF"
         + "  DECLARE @fecha date"
          +" set @fecha=(select max(date) from DatosPowerview ) "
          +  " select * , isnull(cast(cast(datos.loadsPayload as decimal(2)) / cast(datos.loads as decimal(2)) as decimal(15,2)),0)*100  as result  from (select [year],[month#],[day],[truck], COUNT(truck) as loads,"
          + " SUM(case when measureton>0 then 1 else 0 end) as loadsPayload,"
          + "  case "
          +  "   when (SUM(case when measureton>0 then 1 else 0 end))=0"
          +  " then"
          +  "   0"
         +  " else "
          +  "    cast(COUNT(truck)/SUM(case when measureton>0 then 1 else 0 end) as decimal(2)) end as percentPayload"
         + "  from DatosPowerview "
         + "  where client='mlb' and [date]=@fecha"
         + "  group by [year],[month#],[day],[truck]) as datos"
        +  "  order by result desc"*/
            
           sql ="  SET ARITHABORT OFF "
                + " SET ANSI_WARNINGS OFF"
                + " DECLARE @fecha date"
                + "	declare @fechaIni date"
                + "	declare @fechaTer date"
                +"	set  @fechaIni = '" + request.query.fechaini +"'"
                +"	set @fechaTer = '"+ request.query.fechater +"'"         
                + "    select truck, cast(avg(result) as decimal(15,2)) as result from (select truck , isnull(cast(cast(datos.loadsPayload as decimal(2)) / cast(datos.loads as decimal(2)) as decimal(15,2)),0)*100  as result  from (select [year],[month#],[day],[truck], COUNT(truck) as loads,"
                + "  SUM(case when measureton>0 then 1 else 0 end) as loadsPayload,"
                + "   case "
                + "    when (SUM(case when measureton>0 then 1 else 0 end))=0"
                + "  then"
                +  "   0"
                + " else "
                    + "  cast(COUNT(truck)/SUM(case when measureton>0 then 1 else 0 end) as decimal(2)) end as percentPayload"
                + " from DatosPowerview "
                + "  where client='mlb' and"
                +	" [date] between @fechaIni and  @fechaTer"
                + "  group by [year],[month#],[day],[truck]) as datos) as rest"
                +"	group by rest.truck"
            +	"	order by 2 desc"
                

            req.query(sql).then(function (recordset) {
               
                data['data'].push(recordset)
                var json = JSON.stringify(data);

                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })
    }

    function query_cliente2(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc',  @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @datefirts = 2, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
         if (cliente == 'mlb')
            string_query = " @cliente = 'mlb',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
          if (cliente == 'mel')
            string_query = " @cliente = 'mel',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 ";
        return string_query;

    }

    getEmp();
}