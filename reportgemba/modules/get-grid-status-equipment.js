exports.get_grid_status_equipment = function (request, response) {
  /*    var fs = require('fs');
    var contents = fs.readFileSync('C:/ftp_modular/dmbl_mobile_data.txt').toString();
    response.writeHead(200, { "Content-Type": "application/json" });

    var metadata = {} // empty Object    
    var key = 'equipments';
    metadata[key] = []; // empty Array, which you can push() values into   
    var datos = [];
    var linea = [];
    datos = contents.split('\n');
    for (a = 0; a < datos.length; a++) {
        linea = datos[a].split('|');
        //console.log(linea);

        var data = {
            name: linea[0],
            status: linea[5],
            type_equipment: linea[1]
            
        };
        metadata[key].push(data);

    }
    var json = JSON.stringify(metadata);
    response.end(json);*/

    var meta_final = {};
    meta_final = []; 

    var metadata = {} // empty Object    
    var key = 'equipments';
    metadata[key] = []; // empty Array, which you can push() values into  

    var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://modular:12345678@localhost:27017/modular";

    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var query = {};
        db.collection("mlp_mobile_data").find(query).toArray(function (err, result) {
            if (err) throw err;
            for (a = 0; a < result.length; a++) {

                var data = {
                    name: result[a].name,
                    status: result[a].status,
                    type_equipment: result[a].type
                    
                };
                metadata[key].push(data);
               
            }
            db.close();
           
            var json = JSON.stringify(metadata);
            response.end(json);
        });
    });
}