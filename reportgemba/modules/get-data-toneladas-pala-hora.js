exports.get_data_toneladas_pala_hora = function (request, response) {
    var sql = require("mssql");
    var PropertiesReader = require('properties-reader');
    var properties = PropertiesReader('./properties/app.properties');

    //var dato = properties.get('main.some.thing');

    //console.log(dato);

    var dbConfig = {
        server: properties.get('dispatch.server.ip'),
        database: properties.get('dispatch.server.database'),
        user: properties.get('dispatch.server.user'),
        password: properties.get('dispatch.server.password'),
        port: properties.get('dispatch.server.port')
    };

    function getEmp() {
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var IntegridadSistema = {};
            IntegridadSistema['IntegridadSistema'] = [];

            var labels = {};
            labels['labels'] = [];

             var Target = {};
            Target['Target'] = [];

            var data2 = {};
            data2['data2'] = [];



            var req = new sql.Request(conn);

           
            sql =" SELECT 180102001 as ShiftId, "
           +" [excav] as ExcavName, "
           +" coalesce([1],0) as Loads00, "
           +" coalesce([2],0) as Loads01,"
          + " coalesce([3],0) as Loads02, "
           +" coalesce([4],0) as Loads03," 
           +" coalesce([5],0) as Loads04, "
          + " coalesce([6],0) as Loads05, "
          + " coalesce([7],0) as Loads06, "
          + " coalesce([8],0) as Loads07, "
          +"  coalesce([9],0) as Loads08, "
          + " coalesce([10],0) as Loads09," 
          +"  coalesce([11],0) as Loads10, "
          + " coalesce([12],0) as Loads11, "
          + " 0 as Loads12, "
         + "  0 as Loads13,"
          +"  0 as Loads14, "
         + "  0 as Loads15, "
          + " 0 as Loads16, "
          +"  0 as Loads17, "
         + "  0 as Loads18, "
         + "  0 as Loads19, "
          +"  0 as Loads20, "
          +"  0 as Loads21, "
          + " 0 as Loads22, "
          +"  0 as Loads23 "
          +"  FROM( "
              + "    SELECT "
                + "  CASE DATEPART(hh, LoadingTimestamp) "
                + "  WHEN 8 THEN 1"
                + "  WHEN 9 THEN 2"
               + "   WHEN 10 THEN 3"
                 + " WHEN 11 THEN 4"
                + "  WHEN 12 THEN 5"
                + "  WHEN 13 THEN 6"
                + "  WHEN 14 THEN 7"
                +  " WHEN 15 THEN 8"
                + "  WHEN 16 THEN 9"
                + "  WHEN 17 THEN 10"
                + "  WHEN 18 THEN 11"
                + "  WHEN 19 THEN 12"
                + "  WHEN 20 THEN 1"
                + "  WHEN 21 THEN 2"
                + "  WHEN 22 THEN 3"
                + "  WHEN 23 THEN 4"
                + "  WHEN 0 THEN 5"
                + "  WHEN 1 THEN 6 "
               + "   WHEN 2 THEN 7"
                + "  WHEN 3 THEN 8"
               + "   WHEN 4 THEN 9"
                + "  WHEN 5 THEN 10"
                + "  WHEN 6 THEN 11"
                + "  WHEN 7 THEN 12"
                 + " END AS Hos, "
                  
                 + " excav,"
                 + " SUM(LoadfactorValue) AS LoadTons"
                 + " FROM std.StdShiftLoads"
                 + " WHERE  excav in ('201','202','203','204','304','305','306','307','308') AND ShiftId IN (SELECT MAX(ShiftId) FROM std.StdShiftLoads)"
                +  " GROUP BY DATEPART(hh, LoadingTimestamp),Excav"
          + " ) AS t1"
          +  " PIVOT "
          + " ("
                + "  SUM(t1.LoadTons)"
                 + " FOR t1.Hos in ([1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12])"
          + " ) AS t    "
          + " ORDER BY excav asc";
            
            

            req.query(sql).then(function (recordset) {
               
                data['data'].push(recordset)
                var json = JSON.stringify(data);

                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })
    }

    function query_cliente2(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " @cliente = 'cms',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";

        if (cliente == 'cmz')
            string_query = " @cliente = 'cmz',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " @cliente = 'msc',  @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " @cliente = 'dmbl',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " @cliente = 'mlp',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " @cliente = 'dlb',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " @cliente = 'cmh',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " @cliente = 'cmdic', @datefirts = 2, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " @cliente = 'cmcc', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " @cliente = 'mlc',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " @cliente = 'dand',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
         if (cliente == 'mlb')
            string_query = " @cliente = 'mlb',  @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
          if (cliente == 'mel')
            string_query = " @cliente = 'mel',  @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 ";
        return string_query;

    }

    getEmp();
}