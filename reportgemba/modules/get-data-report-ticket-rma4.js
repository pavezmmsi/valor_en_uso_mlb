exports.get_data_report_ticket_rma4 = function (request, response) {
    var sql = require("mssql");

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var log = require('debug-logger')('AppReport');

    var json = {};

    
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var data2 = {};
            data2['data2'] = [];

            var labels = {};
            labels['labels'] = [];

            


            var req = new sql.Request(conn);

            //Ranking Integridad sistema
           /* sql = " SELECT CASE final.mes "+
          "  WHEN 12 THEN 'Diciembre' "+
          "  WHEN 11 THEN 'Noviembre' "+
          "  WHEN 10 THEN 'Octubre' "+
          "  WHEN 9 THEN 'Septiembre' "+
          "  WHEN 8 THEN 'Agosto' "+
          "  WHEN 7 THEN 'Julio' "+
          "  WHEN 6 THEN 'Junio' "+
          "  WHEN 5 THEN 'Mayo' "+
          "  WHEN 4 THEN 'Abril' "+
          "  WHEN 3 THEN 'Marzo' "+
          "  WHEN 2 THEN 'Febrero' "+
          "  WHEN 1 THEN 'Enero' "+
          "  ELSE '' "+
         " END AS Mes, "+
        "  final.evaluation, "+
        "  final.requires, "+
        "  final.technical "+
  " FROM   (SELECT Month(Sysdatetime()) AS mes, "+
                "  Sum(CASE "+
                    "    WHEN t1.des = 'Technical Completion' THEN t1.cantidad "+
                    "    ELSE 0 "+
                    "  END)             Technical, "+
                 " Sum(CASE "+
                     "   WHEN t1.des = 'Requires PO' THEN t1.cantidad "+
                     "   ELSE 0 "+
                    "  END)             Requires, "+
                 " Sum(CASE "+
                      "  WHEN t1.des = 'Evaluation' THEN t1.cantidad "+
                      "  ELSE 0 "+
                    "  END)             Evaluation "+
          " FROM   (SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                        "  'Technical Completion'                  AS des "+
                  " FROM   [ZenDeskImport].[dbo].[tickets] "+
                  " WHERE  [group] = 'RMA - Chile' "+
                       "   AND [stage  list] = 'Technical Completion' "+
                       "   AND Month([created at]) = Month(Sysdatetime()) "+
                  " UNION "+
                  " SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                     "     'Requires PO'                           AS des "+
                  " FROM   [ZenDeskImport].[dbo].[tickets] "+
                  " WHERE  [group] = 'RMA - Chile' "+
                      "    AND [stage  list] = 'Requires PO' "+
                      "    AND Month([created at]) = Month(Sysdatetime()) "+
                  " UNION "+
                  " SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                      "    'Evaluation'                            AS des "+
                 "  FROM   [ZenDeskImport].[dbo].[tickets] "+
                 "  WHERE  [group] = 'RMA - Chile' "+
                        "  AND [stage  list] = 'Evaluation' "+
                        "  AND Month([created at]) = Month(Sysdatetime())) AS t1 "+
          " UNION "+
           " (SELECT Month(Sysdatetime()) - 1 AS mes, "+
                "   Sum(CASE "+
                  "       WHEN t1.des = 'Technical Completion' THEN t1.cantidad "+
                  "       ELSE 0 "+
                  "     END)                 Technical, "+
                  " Sum(CASE "+
                  "       WHEN t1.des = 'Requires PO' THEN t1.cantidad"+
                  "       ELSE 0 "+
                  "     END)                 Requires, "+
                  " Sum(CASE "+
                  "       WHEN t1.des = 'Evaluation' THEN t1.cantidad "+
                  "       ELSE 0 "+
                  "     END)                 Evaluation "+
           " FROM   (SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                  "         'Technical Completion'                  AS des "+
                  "  FROM   [ZenDeskImport].[dbo].[tickets] "+
                  "  WHERE  [group] = 'RMA - Chile' "+
                  "         AND [stage  list] = 'Technical Completion' "+
                  "         AND Month([created at]) = Month(Sysdatetime()) - 1 "+
                  "  UNION "+
                  "  SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                  "         'Requires PO'                           AS des "+
                  "  FROM   [ZenDeskImport].[dbo].[tickets] "+
                  "  WHERE  [group] = 'RMA - Chile' "+
                   "        AND [stage  list] = 'Requires PO' "+
                    "       AND Month([created at]) = Month(Sysdatetime()) - 1 "+
                  "  UNION "+
                  "  SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad,"+
                   "        'Evaluation'                            AS des "+
                  "  FROM   [ZenDeskImport].[dbo].[tickets] "+
                  "  WHERE  [group] = 'RMA - Chile' "+
                      "     AND [stage  list] = 'Evaluation' "+
                      "     AND Month([created at]) = Month(Sysdatetime()) - 1) AS "+
                  " t1 "+
           " UNION "+
           " (SELECT Month(Sysdatetime()) - 2 AS mes, "+
                  "  Sum(CASE "+
                      "    WHEN t1.des = 'Technical Completion' THEN t1.cantidad "+
                      "    ELSE 0 "+
                      "  END)                 Technical, "+
                  "  Sum(CASE "+
                       "   WHEN t1.des = 'Requires PO' THEN t1.cantidad "+
                       "   ELSE 0 "+
                      "  END)                 Requires,"+
                   " Sum(CASE "+
                      "    WHEN t1.des = 'Evaluation' THEN t1.cantidad "+
                      "    ELSE 0 "+
                     "   END)                 Evaluation "+
            " FROM   (SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                         "   'Technical Completion'                  AS des "+
                    " FROM   [ZenDeskImport].[dbo].[tickets] "+
                    " WHERE  [group] = 'RMA - Chile' "+
                       "     AND [stage  list] = 'Technical Completion' "+
                        "    AND Month([created at]) = Month(Sysdatetime()) - 2 "+
                    " UNION "+
                    " SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                     "       'Requires PO'                           AS des "+
                    " FROM   [ZenDeskImport].[dbo].[tickets] "+
                   "  WHERE  [group] = 'RMA - Chile' "+
                        "    AND [stage  list] = 'Requires PO'"+
                        "    AND Month([created at]) = Month(Sysdatetime()) - 2 "+
                    " UNION"+
                    " SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                     "       'Evaluation'                            AS des "+
                    " FROM   [ZenDeskImport].[dbo].[tickets]"+
                    " WHERE  [group] = 'RMA - Chile'"+
                     "       AND [stage  list] = 'Evaluation' "+
                     "       AND Month([created at]) = Month(Sysdatetime()) - 2) AS "+
                   " t1 "+
            " UNION "+
            " (SELECT Month(Sysdatetime()) - 3 AS mes, "+
                   "  Sum(CASE "+
                        "   WHEN t1.des = 'Technical Completion' THEN t1.cantidad "+
                        "   ELSE 0 "+
                       "  END)                 Technical, "+
                    " Sum(CASE "+
                        "   WHEN t1.des = 'Requires PO' THEN t1.cantidad "+
                        "   ELSE 0 "+
                       "  END)                 Requires, "+
                    " Sum(CASE "+
                        "   WHEN t1.des = 'Evaluation' THEN t1.cantidad  "+
                        "   ELSE 0 "+
                       "  END)                 Evaluation "+
             " FROM   (SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                    "         'Technical Completion'                  AS des  "+
                    "  FROM   [ZenDeskImport].[dbo].[tickets]  "+
                    "  WHERE  [group] = 'RMA - Chile'  "+
                    "         AND [stage  list] = 'Technical Completion'  "+
                    "         AND Month([created at]) = Month(Sysdatetime()) - 3  "+
                    "  UNION "+
                    "  SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                    "         'Requires PO'                           AS des "+
                    "  FROM   [ZenDeskImport].[dbo].[tickets] "+
                    "  WHERE  [group] = 'RMA - Chile' "+
                     "        AND [stage  list] = 'Requires PO' "+
                     "        AND Month([created at]) = Month(Sysdatetime()) - 3 "+
                     " UNION "+
                     " SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                     "        'Evaluation'                            AS des "+
                     " FROM   [ZenDeskImport].[dbo].[tickets] "+
                     " WHERE  [group] = 'RMA - Chile' "+
                     "        AND [stage  list] = 'Evaluation' "+
                     "        AND Month([created at]) = Month(Sysdatetime()) - 3) AS "+
                    " t1 "+
             " UNION "+
            "  (SELECT Month(Sysdatetime()) - 4 AS mes, "+
                   "   Sum(CASE "+
                    "        WHEN t1.des = 'Technical Completion' THEN t1.cantidad "+
                       "     ELSE 0 "+
                       "   END)                 Technical, "+
                     " Sum(CASE "+
                        "    WHEN t1.des = 'Requires PO' THEN t1.cantidad "+
                        "    ELSE 0 "+
                        "  END)                 Requires, "+
                     " Sum(CASE "+
                      "      WHEN t1.des = 'Evaluation' THEN t1.cantidad "+
                      "      ELSE 0 "+
                      "    END)                 Evaluation "+
              " FROM   (SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                      "        'Technical Completion'                  AS des "+
                      " FROM   [ZenDeskImport].[dbo].[tickets] "+
                      " WHERE  [group] = 'RMA - Chile' "+
                      "        AND [stage  list] = 'Technical Completion' "+
                      "        AND Month([created at]) = Month(Sysdatetime()) - 4 "+
                      " UNION"+
                      " SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad, "+
                      "        'Requires PO'                           AS des "+
                      " FROM   [ZenDeskImport].[dbo].[tickets] "+
                      " WHERE  [group] = 'RMA - Chile' "+
                      "        AND [stage  list] = 'Requires PO' "+
                      "        AND Month([created at]) = Month(Sysdatetime()) - 4"+
                      " UNION "+
                      " SELECT Sum(Cast ([quantity returning] AS INT)) AS cantidad,"+
                      "        'Evaluation'                            AS des "+
                      " FROM   [ZenDeskImport].[dbo].[tickets]"+
                      " WHERE  [group] = 'RMA - Chile' "+
                      "        AND [stage  list] = 'Evaluation' "+
                      "        AND Month([created at]) = Month(Sysdatetime()) - 4) "+
                     " AS t1))) "+
          " )) AS FINAL  "; */

          sql = " select  convert(varchar(7), [Created at], 126) as Mes,"+
          " sum(cast([Quantity Returning] as int)) as [Equipos_en_Bodega]"+
          " from ZenDeskImport.dbo.tickets"+
         "  WHERE [Status] not in ('Closed') and [Stage  list ] = 'Requires PO' and [Category  list ] = 'RMA Request' "+
         "  and [GROUP] = 'RMA - Chile' and [Created at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-24, 0)"+
         " and ([Subject] like '%"+ request.query.id_cliente + "%' or [Organization] like '%"+ request.query.cliente +"%')  " +
          " Group by convert(varchar(7), [Created at], 126)"+
          " order by Mes";
          
                //log.trace(sql); 
                //console.log(sql);

            req.query(sql).then(function (recordset) {                 
               
                data['data'].push(recordset);

                
                // console.log('Cantidad de registros' + recordset.length );
                 var acum = 0;
                 var total = 0;
                 for (var a =0; a < recordset.length; a++){
                    acum = eval(recordset[a].Equipos_en_Bodega);
                    total = eval(total + acum);
                    data2['data2'].push(total);
                 }
                 data['data'].push(data2);
                 json = JSON.stringify(data);
                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })

     // return json;


    

   
}