exports.get_minegraphics_data = function (request, response) {


//function get_data_report_I(request, response) {
   /* var fs = require('fs');
    var contents = fs.readFileSync('C:/ftp_modular/dmbl_mobile_data.txt').toString();
    response.writeHead(200, { "Content-Type": "application/json" });

    var metadata = {} // empty Object    
    var key = 'markers';
    metadata[key] = []; // empty Array, which you can push() values into   
    var datos = [];
    var linea = [];
    datos = contents.split('\n');
    for (a = 0; a < datos.length; a++) {
        linea = datos[a].split('|');
        //console.log(linea);
        var location ={
            latitude:linea[3],
            longitude:linea[2]
        };

        var scaledSize ={
            width:42,
            height:42
        };

        var icon ={
            url:'',
            scaledSize:scaledSize
        };

        if ((linea[1]==='Truck') && (linea[5]==='Operativo') && (linea[6]==='Vacio')){
            scaledSize = {
                width: 42,
                height: 42
            };

            icon = {
                scaledSize: scaledSize,
                url: 'img/green_Truck_color-512.png',
                
            };

            
        }
        if((linea[1]==='Truck') && (linea[5]==='Reserva')){
                scaledSize = {
                width: 42,
                height: 42
            };

            icon = {
                scaledSize: scaledSize, 
                url: 'img/yellow_Truck_color-512.png',
                
            };
        }

        if((linea[1]==='Truck') && (linea[5]==='Demora')){
                scaledSize = {
                width: 42,
                height: 42
            };

            icon = {
                scaledSize: scaledSize,
                url: 'img/red_Truck_color-512.png'
                
            };

        }

        if((linea[1]==='Truck') && (linea[5]==='Operativo') && (!linea[6]==='Vacio')){
                scaledSize = {
                width: 42,
                height: 42
            };

            icon = {
                scaledSize: scaledSize,
                url: 'img/Dumping_truck_with_load_color-512_lleno.png'
                
            };

        }

        if((linea[1]==='Truck') && (linea[5]==='F.Servicio')){
                scaledSize = {
                width: 42,
                height: 42
            };

            icon = {
                scaledSize: scaledSize,
                url: 'img/red_Truck_color-512.png'
                
            };

        }

        if((linea[1]==='Truck') && (linea[5]==='Cambio Turno')){
                scaledSize = {
                width: 42,
                height: 42
            };

            icon = {
                scaledSize: scaledSize,
                url: 'img/blue_Truck_color-512.png'
                
            };

        }

        if((linea[1]==='Shovel') ){
                scaledSize = {
                width: '52',
                height: '52'
            };

            icon = {
                scaledSize: scaledSize,
                url: 'img/mining-2-01-128.png'
                
            };

        }

        if((linea[1]==='Botadero') ){
                scaledSize = {
                width: 36,
                height: 36
            };

            icon = {
                scaledSize: scaledSize,
                url: 'img/macadam.png'
                
            };

        }

        if((linea[1]==='Chancador') ){
                scaledSize = {
                width: 128,
                height: 128
            };

            icon = {
                scaledSize: scaledSize,
                url: 'img/mining-17-512.png'
                
            };

        }

        if((linea[1]==='Stockpile') ){
                scaledSize = {
                width: 36,
                height: 36
            };

            icon = {
                scaledSize: scaledSize,
                url: 'img/mining-2-05-128.png'
                
            };

        }

        if((linea[1]==='Frente') ){
                scaledSize = {
                width: 40,
                height: 40
            };

            icon = {
                scaledSize: scaledSize,
                url: 'img/12-512.png'
                
            };

        }


        var marker = {
            icon: icon,
            id:linea[0],
            location: location,
            name:linea[0],
            tipo: linea[1],
            url:'',
        };
        
        metadata[key].push(marker);

    }
    var json = JSON.stringify(metadata);
    response.end(json);*/

    var meta_final = {};
    meta_final = []; 

    var metadata = {} // empty Object    
    var key = 'markers';
    metadata[key] = []; // empty Array, which you can push() values into   
    var datos = [];
    var linea = [];

    var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://modular:12345678@localhost:27017/modular";

    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var query = {};
        db.collection("mlp_mobile_data").find(query).toArray(function (err, result) {
            if (err) throw err;
           
            for (a = 0; a < result.length; a++) {
                
                //console.log(linea);
                var location ={
                    latitude:result[a].latitude,
                    longitude:result[a].longitude
                };
        
                var scaledSize ={
                    width:42,
                    height:42
                };
        
                var icon ={
                    url:'',
                    scaledSize:scaledSize
                };
               // console.log(a);
               // console.log(result[a].type  + " - " + result[a].status + " - " + result[a].load);
               
                
                if((result[a].type==='Truck') && (result[a].status==='Reserva')){
                        scaledSize = {
                        width: 42,
                        height: 42
                    };
        
                    icon = {
                        scaledSize: scaledSize, 
                        url: 'img/yellow_Truck_color-512.png',
                        
                    };
                }
        
                if((result[a].type==='Truck') && (result[a].status==='Demora')){
                        scaledSize = {
                        width: 42,
                        height: 42
                    };
        
                    icon = {
                        scaledSize: scaledSize,
                        url: 'img/red_Truck_color-512.png'
                        
                    };
        
                }
        
                if((result[a].type==='Truck') && (result[a].status==='Efectivo') ){
                        scaledSize = {
                        width: 42,
                        height: 42
                    };
        
                    icon = {
                        scaledSize: scaledSize,
                        url: 'img/Dumping_truck_with_load_color-512_lleno.png'
                        
                    };
        
                }

                if ((result[a].type==='Truck') && (result[a].status==='Efectivo') && (result[a].load==='Vacio')){
                    scaledSize = {
                        width: 42,
                        height: 42
                    };
        
                    icon = {
                        scaledSize: scaledSize,
                        url: 'img/green_Truck_color-512.png'
                        
                    };
        
                    
                }
        
                if((result[a].type==='Truck') && (result[a].status==='Mantencion')){
                        scaledSize = {
                        width: 42,
                        height: 42
                    };
        
                    icon = {
                        scaledSize: scaledSize,
                        url: 'img/red_Truck_color-512.png'
                        
                    };
        
                }
        
                if((result[a].type==='Truck') && (result[a].status==='Cambio Turno')){
                        scaledSize = {
                        width: 42,
                        height: 42
                    };
        
                    icon = {
                        scaledSize: scaledSize,
                        url: 'img/blue_Truck_color-512.png'
                        
                    };
        
                }
        
                if((result[a].type==='Shovel') ){
                        scaledSize = {
                        width: 82,
                        height: 52
                    };
        
                    icon = {
                        scaledSize: scaledSize,
                        url: 'img/mining-2-01-128.png',
                        
                    };
        
                }
        
                if(result[a].type==='Botadero'){
                        scaledSize = {
                        width: 36,
                        height: 36
                    };
        
                    icon = {
                        scaledSize: scaledSize,
                        url: 'img/macadam.png'
                        
                    };
        
                }
        
                if((result[a].type==='Chancador') ){
                        scaledSize = {
                        width: 128,
                        height: 128
                    };
        
                    icon = {
                        scaledSize: scaledSize,
                        url: 'img/mining-17-512.png'
                        
                    };
        
                }
        
                if((result[a].type==='Stockpile') ){
                        scaledSize = {
                        width: 36,
                        height: 36
                    };
        
                    icon = {
                        scaledSize: scaledSize,
                        url: 'img/mining-2-05-128.png'
                        
                    };
        
                }
        
                if((result[a].type==='Frente') ){
                        scaledSize = {
                        width: 40,
                        height: 40
                    };
        
                    icon = {
                        scaledSize: scaledSize,
                        url: 'img/12-512.png'
                        
                    };
        
                }
        
        
                var marker = {
                    icon: icon,
                    id:result[a].name,
                    location: location,
                    name:result[a].name,
                    tipo: result[a].type,
                    url:'',
                };
                
                metadata[key].push(marker);
        
            }




            db.close();
            
            var json = JSON.stringify(metadata);
            response.end(json);
        });
    });
}