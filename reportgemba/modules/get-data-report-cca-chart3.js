exports.get_data_report_cca_chart3 = function (request, response) {
    var sql = require("mssql");

    var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties/app.properties');

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var log = require('debug-logger')('AppReport');

    var json = {};

    
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var data2 = {};
            data2['data2'] = [];

            var labels = {};
            labels['labels'] = [];

            


            var req = new sql.Request(conn);

            //Ranking Integridad sistema
            /*sql = " SELECT K.Organization, "+
               " CASE K.MES "+
               " WHEN 12 THEN 'Diciembre'  "+
               " WHEN 11 THEN 'Noviembre'  "+
               " WHEN 10 THEN 'Octubre' "+
               " WHEN 9 THEN 'Septiembre' "+
               " WHEN 8 THEN 'Agosto'  "+
              "  WHEN 7 THEN 'Julio' "+
              "  WHEN 6 THEN 'Junio' "+
              "  WHEN 5 THEN 'Mayo' "+
              "  WHEN 4 THEN 'Abril' "+
              "  WHEN 3 THEN 'Marzo' "+
              "  WHEN 2 THEN 'Febrero' "+
              "  WHEN 1 THEN 'Enero' "+
              "  ELSE '' "+
             " END as Mes ,"+
            
            " K.ticketLlamados"+ 
       " FROM ((SELECT    COUNT(*) AS ticketLlamados , "+
       
      " [Organization], MONTH(SYSDATETIME()) AS MES "+
     " FROM      ZendeskImport.dbo.tickets "+
     " WHERE  "+  
     "      YEAR([Created at]) = YEAR(SYSDATETIME())"+
     " AND       MONTH([Created at]) = MONTH(SYSDATETIME())"+
     " AND       [Subject] like '%[MOL]%'"+
     " AND       [Organization] LIKE '%MANTOS%'"+
     " GROUP BY  YEAR([Created at]), [Organization])"+
     " UNION"+
     " (SELECT    COUNT(*) AS ticketLlamados ,"+
       
     "  [Organization], MONTH(SYSDATETIME())-1 AS MES"+
     " FROM      ZendeskImport.dbo.tickets "+
     " WHERE   "+  
     "      YEAR([Created at]) = YEAR(SYSDATETIME())"+
     " AND       MONTH([Created at]) = MONTH(SYSDATETIME()) -1"+
     " AND       [Subject] like '%[MOL]%'"+
     " AND       [Organization] LIKE '%MANTOS%'"+
     " GROUP BY  YEAR([Created at]), [Organization])"+
     " UNION"+
     " (SELECT    COUNT(*) AS ticketLlamados ,"+
       
     "  [Organization], MONTH(SYSDATETIME())-2 AS MES"+
     " FROM      ZendeskImport.dbo.tickets "+
     " WHERE    "+ 
     "      YEAR([Created at]) = YEAR(SYSDATETIME())"+
     " AND       MONTH([Created at]) = MONTH(SYSDATETIME()) -2"+
     " AND       [Subject] like '%[MOL]%'"+
     " AND       [Organization] LIKE '%MANTOS%'"+
     " GROUP BY  YEAR([Created at]), [Organization])"+
      
     " UNION"+
     " (SELECT    COUNT(*) AS ticketLlamados ,"+
       
      " [Organization], MONTH(SYSDATETIME())-3 AS MES"+
     " FROM      ZendeskImport.dbo.tickets "+
     " WHERE     "+
      "     YEAR([Created at]) = YEAR(SYSDATETIME())"+
     " AND       MONTH([Created at]) = MONTH(SYSDATETIME()) -3"+
     " AND       [Subject] like '%[MOL]%'"+
     " AND       [Organization] LIKE '%MANTOS%'"+
     " GROUP BY  YEAR([Created at]), [Organization])"+
      
     " UNION"+
     " (SELECT    COUNT(*) AS ticketLlamados ,"+
       
      " [Organization], MONTH(SYSDATETIME())-4 AS MES"+
     " FROM      ZendeskImport.dbo.tickets "+
     " WHERE     "+
      "     YEAR([Created at]) = YEAR(SYSDATETIME())"+
     " AND       MONTH([Created at]) = MONTH(SYSDATETIME()) -4"+
     " AND       [Subject] like '%[MOL]%'"+
     " AND       [Organization] LIKE '%MANTOS%'"+
     " GROUP BY  YEAR([Created at]), [Organization])"+
      
     " UNION"+
     " (SELECT    COUNT(*) AS ticketLlamados ,"+
       
     "  [Organization], MONTH(SYSDATETIME())-5 AS MES"+
     " FROM      ZendeskImport.dbo.tickets "+
     " WHERE    "+ 
      "     YEAR([Created at]) = YEAR(SYSDATETIME())"+
     " AND       MONTH([Created at]) = MONTH(SYSDATETIME()) -5"+
     " AND       [Subject] like '%[MOL]%'"+
     " AND       [Organization] LIKE '%MANTOS%'"+
     " GROUP BY  YEAR([Created at]), [Organization])) AS K"+
      
     " ORDER BY K.MES desc";*/

  sql =   "SELECT  CAST([Solved at] AS varchar(7)) as Mes, count(ID) as [ticketLlamados],Organization as Cliente FROM ZendeskImport.dbo.tickets "+
"     where SUBSTRING( Subject, 1, 5) = '[MOL]' and [Status] <> 'Deleted' AND ([group]='First Tier - Latin America' "+
  "   or [group]='First Tier - Chile') and [Status] <> 'Open'"+
   "  and [Solved at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-6, 0)  "+
     
   "  and Organization like '%" + request.query.cliente +"%'   "+
    " group by CAST([Solved at] AS varchar(7)),Organization "+
    " order by CAST([Solved at] AS varchar(7))";

               // log.trace(sql);
               // console.log(sql);

            req.query(sql).then(function (recordset) {                 
               
                data['data'] = recordset;
                 json = JSON.stringify(data);

                 
                // console.log(json);
                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })

     // return json;


    

   
}