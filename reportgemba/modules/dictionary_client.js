exports.get_dictionary_client = function (request, response) {
   
   

    var meta_final = {};
    meta_final = []; 

    var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://modular:12345678@localhost:27017/modular";

    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var query = {};
        query = {equipment: { $nin: [ 'equipo', 'Total Cargado' ]}} 
        db.collection("dictionary_client").find(query).toArray(function (err, result) {
            if (err) throw err;
            var data = {
             data : {
                name_client: result[0].name_client,
                initlatitude : result[0].initlatitude,
                initlongitude :  result[0].initlongitude,
                filterMapEquipment1 :  result[0].filterMapEquipment1,
                filterMapEquipment2 :  result[0].filterMapEquipment2,
                filterMapEquipment3 :  result[0].filterMapEquipment3,
                equipment1 :  result[0].equipment1,
                equipment2 :  result[0].equipment2,
                equipment3 :  result[0].equipment3,
                equipment4 :  result[0].equipment4,
                stateEquipment1 : result[0].stateEquipment1,
                stateEquipment2 : result[0].stateEquipment2,
                stateEquipment3 :  result[0].stateEquipment3,
                stateEquipment4 :  result[0].stateEquipment4

             }
              
            };
            db.close();
            
            var json = JSON.stringify(data);
            response.end(json);
        });
    });

}