exports.get_data_report_ticket_rma3 = function (request, response) {
    var sql = require("mssql");

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var log = require('debug-logger')('AppReport');

    var json = {};

    
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var data2 = {};
            data2['data2'] = [];

            var labels = {};
            labels['labels'] = [];

            


            var req = new sql.Request(conn);

            //Ranking Integridad sistema
            sql = " SELECT CASE T1.Mes "+
             " WHEN 12 THEN 'Diciembre' "+
           " WHEN 11 THEN 'Noviembre'  "+
          "  WHEN 10 THEN 'Octubre'   "+
          "  WHEN 9 THEN 'Septiembre'  "+
          "  WHEN 8 THEN 'Agosto'  "+
          "  WHEN 7 THEN 'Julio'  "+
          "  WHEN 6 THEN 'Junio'  "+
          "  WHEN 5 THEN 'Mayo'  "+
          "  WHEN 4 THEN 'Abril'  "+
          "  WHEN 3 THEN 'Marzo'   "+
          "  WHEN 2 THEN 'Febrero'  "+
          "  WHEN 1 THEN 'Enero'  "+
          "  ELSE ''  "+
       "  END AS Mes, T1.Tickets FROM ( SELECT   "+
    " count(ID) as Tickets,  "+
    "  MONTH([Created at]) Mes    "+
       
  " FROM [ZenDeskImport].[dbo].[tickets]  "+
   "  where  "+
    "   [Group] = 'RMA - Chile'  "+
    "  and [Stage  list ] = 'Requires PO'     "+  
    " GROUP BY MONTH([Created at])) as T1 ";

  /* sql = " select  convert(varchar(7), getdate(), 126) as Mes," +
    " sum(cast([Quantity Returning] as int)) as [Equipos_en_Bodega]"+
    " from ZenDeskImport.dbo.tickets"+
   " where "+
  "  [Stage  list ] = 'Requires PO' and "+
   " [Category  list ] = 'RMA Request' and [GROUP] = 'RMA - Chile' "+
   " and [Created at] > = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-24, 0)";*/
    
               // log.trace(sql); 
               // console.log(sql);

            req.query(sql).then(function (recordset) {                 
               
                data['data'] = recordset;
                 json = JSON.stringify(data);

                 
                // console.log(json);
                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })

     // return json;


    

   
}