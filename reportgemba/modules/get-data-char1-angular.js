exports.get_data_char1_angular = function (request, response) {
    var sql = require("mssql");

    var PropertiesReader = require('properties-reader');
    var properties = PropertiesReader('./properties/app.properties');

    //var dato = properties.get('main.some.thing');

    //console.log(dato);

    var dbConfig = {
        server: properties.get('main.server.appdata.ip'),
        database: properties.get('main.server.appdata.database'),
        user: properties.get('main.server.appdata.user'),
        password: properties.get('main.server.appdata.password'),
        port: properties.get('main.server.appdata.port')
    };

    var log = require('debug-logger')('AppReport');

    var json = {};

    function getEmp() {
        var conn = new sql.Connection(dbConfig);

        conn.connect().then(function () {
            var data_all = {};
            data_all['data'] = [];
            var data = {};
            data['data'] = [];

            var data2 = {};
            data2['data2'] = [];

            var labels = {};
            labels['labels'] = [];

            


            var req = new sql.Request(conn);

            //Ranking Integridad sistema
            sql = "create table #TempC (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float) " +
                "  create table #TempD (Periodo varchar(20),G1 float,G2 float, G3 float, G4 float) " +
                " insert into #TempC   " +
                 query_cliente(request.query.cliente) +
                //" @cliente = '" + request.query.cliente + "', @periodo = 'semana', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  " +
                " insert into #TempD " +
                " select 'Proyeccion' Periodo, " +
                " avg(G1) 'G1', " +
                " avg(G2) 'G2', " +
                " avg(G3) 'G3', " +
                " avg(G4) 'G4'  " +
                " from	 " +
                " (	  " +
                " select  " + turno(request.query.cliente) +
                //" round(sum(G1/2),0) 'G1',											     " +
                //" round(sum(G2/2),0) 'G2',											     " +
                //" round(sum(G3/2),0) 'G3',											     " +
                //" round(sum(G4/2),0) 'G4'											     " +
                " from #TempC " +
                " )TEMP1 " +
                
                " insert into #TempC  " +
                " select * from #TempD " +               
                " select Periodo, " +
                " G1 'Grupo1',  " +
                " G2 'Grupo2',  " +
                " G3 'Grupo3',   " +
                " G4 'Grupo4' " +
                "  from #Tempc C   " +                
                " drop table #TempC,#TempD   ";

                log.trace(sql);
                console.log();

            req.query(sql).then(function (recordset) {
               
                var datos = {};
                datos = recordset[0]["Periodo"];

                  for (var i =0; i <  recordset.length; i++){
                 labels['labels'].push(recordset[i]["Periodo"]);
                 
                }

                 var items = [ recordset[0]["Grupo1"], recordset[1]["Grupo1"],recordset[2]["Grupo1"],recordset[3]["Grupo1"],recordset[4]["Grupo1"]];
                 // console.log(items);
                  data2['data2'].push(items);

                  var items = [ recordset[0]["Grupo2"], recordset[1]["Grupo2"],recordset[2]["Grupo2"],recordset[3]["Grupo2"],recordset[4]["Grupo2"]];
                 // console.log(items);
                  data2['data2'].push(items);

                  var items = [ recordset[0]["Grupo3"], recordset[1]["Grupo3"],recordset[2]["Grupo3"],recordset[3]["Grupo3"],recordset[4]["Grupo3"]];
                 // console.log(items);
                  data2['data2'].push(items);

                  var items = [ recordset[0]["Grupo4"], recordset[1]["Grupo4"],recordset[2]["Grupo4"],recordset[3]["Grupo4"],recordset[4]["Grupo4"]];
                 // console.log(items);
                  data2['data2'].push(items);

                 

               

                 
                 data_all['data'].push(labels);
                 data_all['data'].push(data2);
                 json = JSON.stringify(data_all);

                response.end(json);
                conn.close();


            })
                .catch(function (err) {
                    console.log(err);
                    conn.close();
                });



        })

     // return json;
    }

    function turno(cliente) {
        var turno = "";
        var cuatro = " round(sum(G1/4),0) 'G1',	" +
            " round(sum(G2/4),0) 'G2', " +
            " round(sum(G3/4),0) 'G3', " +
            " round(sum(G4/4),0) 'G4' ";

        var dos = " round(sum(G1/2),0) 'G1', " +
            " round(sum(G2/2),0) 'G2', " +
            " round(sum(G3/2),0) 'G3', " +
            " round(sum(G4/2),0) 'G4'  ";

        switch (cliente) {
             case "dlb":
                turno = cuatro;
                break;
            case "mlb":
                turno = cuatro;
                break;
            case "cmh":
                turno = cuatro;
                break;
            case "dmbl":
                turno = cuatro;
                break;
            
            //case "msc":
            //    turno = cuatro;
            //    break;
            //case "mel":
            //    turno = cuatro;
            //    break;
            case "dand":
                turno = cuatro;
                break;
            case "mlc":
                turno = cuatro;
                break;       
            default:
                turno = dos;
        }
        return turno;
    }

    function query_cliente(cliente) {
        var string_query = "";

        if (cliente == 'cms')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana] @cliente = 'cms', @periodo = 'semana', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1  ";
        if (cliente == 'cmz')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemanaCMCC]  @cliente = 'cmz', @periodo = 'semana', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'msc')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'msc', @periodo = 'semana', @datefirts = 4, @numdate1 = 35, @numdate2 = 3, @numdate3 = 7, @numdate4 =2 ";
        if (cliente == 'dmbl')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'dmbl', @periodo = 'semana', @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'mlp')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'mlp', @periodo = 'semana', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'dlb')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'dlb', @periodo = 'semana', @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmh')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'cmh', @periodo = 'semana', @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'cmdic')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'cmdic', @periodo = 'semana', @datefirts = 2, @numdate1 = 28, @numdate2 = 1, @numdate3 = 0, @numdate4 = 0 ";
        if (cliente == 'cmcc')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemanaCMCC]  @cliente = 'cmcc', @periodo = 'semana', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 =1 ";
        if (cliente == 'mlc')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'mlc', @periodo = 'semana', @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
        if (cliente == 'dand')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana]  @cliente = 'dand', @periodo = 'semana', @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
         if (cliente == 'mlb')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemanaCMCC] @cliente = 'mlb', @periodo = 'semana', @datefirts = 1, @numdate1 = 28, @numdate2 = 0, @numdate3 = 7, @numdate4 = 6 ";
         if (cliente == 'mel')
            string_query = " exec [MLBOperational].[dbo].[RankingClientesSemana] @cliente = 'mel', @periodo = 'semana', @datefirts = 3, @numdate1 = 35, @numdate2 = 2, @numdate3 = 7, @numdate4 = 1 ";
        return string_query;

    }

    getEmp();
}