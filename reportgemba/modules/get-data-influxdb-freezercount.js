exports.get_data_influxdb_freezecount = function(request, response){
    var PropertiesReader = require('properties-reader');
    var properties = PropertiesReader('./properties/app.properties');

    var server =  properties.get('influxDB.server.ip'),
        database= properties.get('influxDB.server.database'),
        user= properties.get('influxDB.server.user'),
        password= properties.get('influxDB.server.password'),
        port= properties.get('influxDB.server.port')
   // console.log('http://'+user+':'+password +'@'+server+':'+port+'/'+database+'?auth=basic');    
    const Influx = require('influx');
    const client = new Influx.InfluxDB('http://'+user+':'+password +'@'+server+':'+port+'/'+database+'?auth=basic');
    client.query(`
SELECT ProcessMonitorAntiFreezeCount as AntiFreezeCount, AppRestartCount, "DialogsClosedCount", "DeviceMonitorRebootCount", "IgnitionOffCount", "IgnitionOnCount", "WatchdogResetCount", "WindowsShutdownCount", "SystemStartsCount", "TerminalErrorCount" FROM device_monitor_V1 where time >= '`+ request.query.fechaini+`T00:00:00.000000000Z' AND time <= '`+ request.query.fechater+`T00:12:00Z'   GROUP BY "Hostname", "IpSource" ORDER BY time DESC LIMIT 1

  `)
  .then( function (recordset) {
       var data_all = {};
            data_all['data'] = [];
      var data = {};
      data['data'] = [];
       var labels = {};
            labels['labels'] = [];

             var Target = {};
            Target['Target'] = [];

            var data2 = {};
            data2['data2'] = [];

             var datos = {};
                
                 
                var arr1 = new Array();
                var arr2 = new Array();
                

                  for (var i =0; i <  recordset.length; i++){
                 labels['labels'].push(recordset[i]["IpSource"]);

                
                 arr1.push(recordset[i]["AntiFreezeCount"]);
                 arr2.push(recordset[i]["AppRestartCount"]);
                
                 
                 
                }

                data_all['data'].push(labels);                 

                 data2['data2'].push(arr1);
                 data2['data2'].push(arr2);
                 
                 
                  data_all['data'].push(data2);
                var json = JSON.stringify(data_all);

                response.end(json);
  })
  .catch( error => response.status(500).json({ error }) );
}

