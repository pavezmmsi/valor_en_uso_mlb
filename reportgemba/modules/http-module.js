
function handle_GetMetaData_request(request, response) {
   
    var get_data_report = require('./get-meta-data');
    get_data_report.get_meta_data(request, response);

}
function handle_GetDataReport_I_request(request, response) {
    var get_data_report = require('./get-data-report-i');
    get_data_report.get_data_report_i(request, response);        

}

function handle_GetDataReport_II_request(request, response) {
    var get_data_report = require('./get-data-report-ii');
    get_data_report.get_data_report_ii(request, response);          

}
function handle_GetDataReport_III_request(request, response) {
    var get_data_report = require('./get-data-report-iii');
    get_data_report.get_data_report_iii(request, response);          

}

function handle_GetAllDataReport_IV_request(request, response) {
    var get_data_report = require('./get-all-data-report-iv');
    get_data_report.get_all_data_report_iv(request, response);
}

function handle_GetAllDataReport_V_request(request, response) {
    var get_data_report = require('./get-all-data-report-v');
    get_data_report.get_all_data_report_v(request, response);
}

function handle_GetAllDataReport_VI_request(request, response) {
    var get_data_report = require('./get-all-data-report-vi');
    get_data_report.get_all_data_report_vi(request, response);
}

function handle_GetDataReportAllEquipoment_IV_request(request, response) {
    var get_data_report = require('./get-all-equipment-report-iv');
    get_data_report.get_all_equipment_report_iv(request, response);

}

function handle_GetDataReportAllEquipoment_V_request(request, response) {
    var get_data_report = require('./get-all-equipment-report-v');
    get_data_report.get_all_equipment_report_v(request, response);

}

function handle_GetDataReportAllEquipoment_VI_request(request, response) {
    var get_data_report = require('./get-all-equipment-report-vi');
    get_data_report.get_all_equipment_report_vi(request, response);

}

function handle_GetGridStatusEquipment_request (request, response) {
    var get_data_report = require('./get-grid-status-equipment');
    get_data_report.get_grid_status_equipment(request, response);
}
function handle_getDataChar1_request (request, response) {
    var get_data_report = require('./get-data-char1');
    get_data_report.get_data_char1(request, response);
}

function handle_getDataChar1Angular_request (request, response) {
    var get_data_report = require('./get-data-char1-angular');
    get_data_report.get_data_char1_angular(request, response);
}

function handle_getDataChar2Angular_request (request, response) {
    var get_data_report = require('./get-data-char2-angular');
    get_data_report.get_data_char2_angular(request, response);
}

function handle_getInsertData_request (request, response) {
    var get_data_report = require('./get-insert-data');
    get_data_report.get_insert_data(request, response);
}

function handle_GetUpdate_request(request, response) {
    var get_data_report = require('./get-update-data');
    get_data_report.update(request, response);
}


function handle_POST_request(request, response) {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('POST action was requested');
}
function handle_PUT_request(request, response) {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('PUT action was requested');
}
function handle_HEAD_request(request, response) {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('HEAD action was requested');
}
function handle_DELETE_request(request, response) {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('DELETE action was requested');
}
function handle_bad_request(request, response) {
    response.writeHead(400, { 'Content-Type': 'text/plain' });
    response.end('bad action was requested');
}
    exports.handle_request = function (request, response) {
        var express = require("express"),
            app = express(),
            bodyParser = require("body-parser"),
            methodOverride = require("method-override");
        mongoose = require('mongoose');
        fs = require('fs');

        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());
        app.use(methodOverride());

        var router = express.Router();

        router.get('/restservicemantos/services/MetaDataRestService.svc/GetMetaData/', function (request, response) {
            handle_GetMetaData_request(request, response);
       });
    }


/*exports.handle_request = function (request, response) {
    
    switch (request.url) {
        case '/restservicemantos/services/MetaDataRestService.svc/GetMetaData/':
            handle_GetMetaData_request(request, response);
            break;

        case '/restservicemantos/services/ReportRestService.svc/GetDataReport_I/':
            handle_GetDataReport_I_request(request, response);
            break;
        case '/restservicemantos/services/ReportRestService.svc/GetDataReport_II/':
            handle_GetDataReport_II_request(request, response);
            break;
        case '/restservicemantos/services/ReportRestService.svc/GetDataReport_III/':
             handle_GetDataReport_III_request(request, response);
            break;
        case '/restservicemantos/services/ReportRestService.svc/GetAllDataReport_IV/':
             handle_GetAllDataReport_IV_request(request, response);
            break;
        case '/restservicemantos/services/ReportRestService.svc/GetAllDataReport_V/':
             handle_GetAllDataReport_V_request(request, response);
            break;
        case '/restservicemantos/services/ReportRestService.svc/GetAllDataReport_VI/':
             handle_GetAllDataReport_VI_request(request, response);
            break;
       case '/restservicemantos/services/ReportRestService.svc/GetDataReportAllEquipoment_IV/':
             handle_GetDataReportAllEquipoment_IV_request(request, response);
            break; 

        case '/restservicemantos/services/ReportRestService.svc/GetDataReportAllEquipoment_V/':
             handle_GetDataReportAllEquipoment_V_request(request, response);
            break; 

        case '/restservicemantos/services/ReportRestService.svc/GetDataReportAllEquipoment_VI/':
             handle_GetDataReportAllEquipoment_VI_request(request, response);
            break;       

        case '/restservicemantos/services/EquipmentRestService.svc/GetGridStatusEquipment/':
             handle_GetGridStatusEquipment_request(request, response);
            break; 
        case '/restservicemantos/services/MetaDataRestService.svc/GetUpdateData/':
             handle_GetUpdate_request(request, response);
            break; 
        


        case 'HEAD':
            handle_HEAD_request(request, response);
            break;
        default:
            handle_bad_request(request, response);
            break;

    }
    console.log('Request processing by http-module ended');

}*/

