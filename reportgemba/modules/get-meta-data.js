exports.get_meta_data = function (request, response) {


//function get_data_report_I(request, response) {
    /*var fs = require('fs');
    var contents = fs.readFileSync('C:/ftp_modular/dmbl_mobile_data.txt').toString();
    response.writeHead(200, { "Content-Type": "application/json" });

    var metadata = {} // empty Object    
    var key = 'metadata';
    metadata[key] = []; // empty Array, which you can push() values into   
    var datos = [];
    var linea = [];
    datos = contents.split('\n');
    for (a = 0; a < datos.length; a++) {
        linea = datos[a].split('|');
        //console.log(linea);

        var data = {
            EQUIPMENT: linea[0],
            TYPE: linea[1],
            X: linea[2],
            Y: linea[3],
            Z: linea[4],
            STATUS: linea[5],
            LOAD: linea[6],
            TONS: linea[7],
            NAME: linea[8],
            GRADE: linea[9],
            RATE: linea[10],
            LOADING: linea[11],
            LOC: linea[12],
            REGION: linea[13],
            FUEL: linea[14]
        };
        metadata[key].push(data);

    }
    var json = JSON.stringify(metadata);
    response.end(json);*/

    var metadata = {} // empty Object    
    var key = 'metadata';
    metadata[key] = []; // empty Array, which you can push() values into   
  
    var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://modular:12345678@localhost:27017/modular";
    
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var query = {};
      db.collection("mlp_mobile_data").find(query).toArray(function(err, result) {
        if (err) throw err;

        for (a = 0; a < result.length; a++) {

            var data = {
                EQUIPMENT: result[a].name,
                TYPE: result[a].type,
                X: result[a].longitude,
                Y: result[a].latitude,
                Z: result[a].elev,
                STATUS: result[a].status,
                LOAD: result[a].load,
                TONS: result[a].tons,
                NAME:result[a].operator,
                GRADE: result[a].grade,
                RATE: result[a].log,
                LOADING: result[a].region,
                LOC: result[a].fuel,
                REGION: result[a].c14,
                FUEL: result[a].c15
            };
            metadata[key].push(data);

        }
        
        db.close();
        var json = JSON.stringify(metadata);
        response.end(json);
      });
    });

}